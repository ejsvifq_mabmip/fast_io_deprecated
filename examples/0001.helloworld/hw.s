	.file	"hw.cc"
	.text
	.section	.text$_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy,"x"
	.linkonce discard
	.align 2
	.globl	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy
	.def	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy
_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy:
.LFB16256:
	.seh_endprologue
	movq	8(%rcx), %rcx
	movq	4104(%rcx), %rax
	addq	%rax, %rdx
	cmpq	%rdx, 4112(%rcx)
	movl	$0, %edx
	cmovbe	%rdx, %rax
	ret
	.seh_endproc
	.section	.text$_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc,"x"
	.linkonce discard
	.align 2
	.globl	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc
	.def	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc
_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc:
.LFB16258:
	.seh_endprologue
	movq	8(%rcx), %rax
	movq	%rdx, 4104(%rax)
	ret
	.seh_endproc
	.section	.text$_ZN7fast_io11posix_errorD0Ev,"x"
	.linkonce discard
	.align 2
	.globl	_ZN7fast_io11posix_errorD0Ev
	.def	_ZN7fast_io11posix_errorD0Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io11posix_errorD0Ev
_ZN7fast_io11posix_errorD0Ev:
.LFB16216:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	%rcx, %r12
	leaq	16+_ZTVN7fast_io13fast_io_errorE(%rip), %rax
	movq	%rax, (%rcx)
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L6
	call	_ZdaPv
.L6:
	movq	%r12, %rcx
	call	_ZNSt9exceptionD2Ev
	movl	$24, %edx
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZdlPvy
	.seh_endproc
	.section	.text$_ZN7fast_io11posix_errorD1Ev,"x"
	.linkonce discard
	.align 2
	.globl	_ZN7fast_io11posix_errorD1Ev
	.def	_ZN7fast_io11posix_errorD1Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io11posix_errorD1Ev
_ZN7fast_io11posix_errorD1Ev:
.LFB16215:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	%rcx, %r12
	leaq	16+_ZTVN7fast_io13fast_io_errorE(%rip), %rax
	movq	%rax, (%rcx)
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L11
	call	_ZdaPv
.L11:
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZNSt9exceptionD2Ev
	.seh_endproc
	.section	.text$_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_,"x"
	.linkonce discard
	.align 2
	.globl	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_
	.def	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_
_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_:
.LFB16255:
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	%rdx, %r13
	movq	%r8, %r12
	movq	8(%rcx), %rsi
	movq	%r8, %rbx
	subq	%rdx, %rbx
	movq	4112(%rsi), %rax
	movq	4104(%rsi), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %rbx
	ja	.L24
	testq	%rbx, %rbx
	jne	.L25
.L20:
	addq	%rcx, %rbx
.L19:
	movq	%rbx, 4104(%rsi)
	movq	%r12, %rax
	addq	$32, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.L25:
	movq	%rbx, %r8
	movq	%r13, %rdx
	call	memcpy
	movq	%rax, %rcx
	jmp	.L20
.L24:
	subq	4096(%rsi), %rax
	leaq	(%rax,%rax), %rdi
	cmpq	%rbx, %rdi
	cmovb	%rbx, %rdi
	testq	%rdi, %rdi
	js	.L26
	movq	%rdi, %rcx
	call	_Znwy
	movq	%rax, %rbp
	movq	4096(%rsi), %rdx
	movq	4104(%rsi), %r8
	subq	%rdx, %r8
	movq	%rax, %rcx
	call	memcpy
	movq	4096(%rsi), %rcx
	movq	4104(%rsi), %r14
	subq	%rcx, %r14
	cmpq	%rsi, %rcx
	je	.L18
	movq	4112(%rsi), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
.L18:
	movq	%rbp, 4096(%rsi)
	leaq	0(%rbp,%r14), %rcx
	movq	%rcx, 4104(%rsi)
	addq	%rdi, %rbp
	movq	%rbp, 4112(%rsi)
	movq	%rbx, %r8
	movq	%r13, %rdx
	call	memcpy
	addq	4104(%rsi), %rbx
	jmp	.L19
.L26:
	call	_ZSt17__throw_bad_allocv
	nop
	.seh_endproc
	.section	.text$_ZNK7fast_io11posix_error6reportERNS_14error_reporterE,"x"
	.linkonce discard
	.align 2
	.globl	_ZNK7fast_io11posix_error6reportERNS_14error_reporterE
	.def	_ZNK7fast_io11posix_error6reportERNS_14error_reporterE;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK7fast_io11posix_error6reportERNS_14error_reporterE
_ZNK7fast_io11posix_error6reportERNS_14error_reporterE:
.LFB11481:
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	movq	%rdx, %r12
	movl	16(%rcx), %ecx
	call	strerror
	movq	%rax, %r13
	movq	%rax, %rcx
	call	strlen
	movq	%rax, %rbx
	movq	(%r12), %rax
	movq	(%rax), %rax
	leaq	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L28
	movq	8(%r12), %r12
	movq	4112(%r12), %rax
	movq	4104(%r12), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %rbx
	ja	.L37
	testq	%rbx, %rbx
	jne	.L38
.L33:
	addq	%rbx, %rcx
	movq	%rcx, 4104(%r12)
.L27:
	addq	$40, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
.L38:
	movq	%rbx, %r8
	movq	%r13, %rdx
	call	memcpy
	movq	%rax, %rcx
	jmp	.L33
.L28:
	leaq	0(%r13,%rbx), %r8
	movq	%r13, %rdx
	movq	%r12, %rcx
	addq	$40, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	rex.W jmp	*%rax
.L37:
	subq	4096(%r12), %rax
	leaq	(%rax,%rax), %rsi
	cmpq	%rbx, %rsi
	cmovb	%rbx, %rsi
	testq	%rsi, %rsi
	js	.L39
	movq	%rsi, %rcx
	call	_Znwy
	movq	%rax, %rdi
	movq	4096(%r12), %rdx
	movq	4104(%r12), %r8
	subq	%rdx, %r8
	movq	%rax, %rcx
	call	memcpy
	movq	4096(%r12), %rcx
	movq	4104(%r12), %rbp
	subq	%rcx, %rbp
	cmpq	%r12, %rcx
	je	.L31
	movq	4112(%r12), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
.L31:
	movq	%rdi, 4096(%r12)
	leaq	(%rdi,%rbp), %rcx
	movq	%rcx, 4104(%r12)
	addq	%rsi, %rdi
	movq	%rdi, 4112(%r12)
	movq	%rbx, %r8
	movq	%r13, %rdx
	call	memcpy
	addq	%rbx, 4104(%r12)
	jmp	.L27
.L39:
	call	_ZSt17__throw_bad_allocv
	nop
	.seh_endproc
	.section .rdata,"dr"
.LC0:
	.ascii "unknown fast_io_error\0"
	.section	.text$_ZNK7fast_io13fast_io_error4whatEv,"x"
	.linkonce discard
	.align 2
	.globl	_ZNK7fast_io13fast_io_error4whatEv
	.def	_ZNK7fast_io13fast_io_error4whatEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK7fast_io13fast_io_error4whatEv
_ZNK7fast_io13fast_io_error4whatEv:
.LFB5324:
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	movl	$4184, %eax
	call	___chkstk_ms
	subq	%rax, %rsp
	.seh_stackalloc	4184
	.seh_endprologue
	movq	%rcx, %rsi
	leaq	48(%rsp), %rdi
	movq	%rdi, 4144(%rsp)
	movq	%rdi, 4152(%rsp)
	leaq	4144(%rsp), %rax
	movq	%rax, 4160(%rsp)
	leaq	16+_ZTVN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE(%rip), %rax
	movq	%rax, 32(%rsp)
	movq	%rdi, 40(%rsp)
	leaq	32(%rsp), %rdx
	movq	(%rcx), %rax
.LEHB0:
	call	*24(%rax)
	movq	4152(%rsp), %rcx
	cmpq	4160(%rsp), %rcx
	je	.L62
	movb	$0, (%rcx)
	addq	$1, %rcx
	movq	%rcx, 4152(%rsp)
	movq	4144(%rsp), %r12
	cmpq	%rdi, %r12
	je	.L63
.L44:
	movq	8(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L47
	call	_ZdaPv
.L47:
	movq	%r12, 8(%rsi)
.L40:
	movq	%r12, %rax
	addq	$4184, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.L63:
	subq	%rdi, %rcx
	call	_Znay
	movq	%rax, %r12
	movq	8(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L45
	call	_ZdaPv
.L45:
	movq	%r12, 8(%rsi)
	movq	4144(%rsp), %r13
	movq	4152(%rsp), %r8
	subq	%r13, %r8
	jne	.L64
.L46:
	cmpq	%rdi, %r13
	je	.L40
	movq	4160(%rsp), %rdx
	subq	%r13, %rdx
	movq	%r13, %rcx
	call	_ZdlPvy
	jmp	.L40
.L62:
	subq	4144(%rsp), %rcx
	movq	%rcx, %rbx
	addq	%rbx, %rbx
	js	.L65
	movq	%rbx, %rcx
	call	_Znwy
	movq	%rax, %r12
	movq	4144(%rsp), %r14
	movq	4152(%rsp), %r13
	subq	%r14, %r13
	movq	%r13, %r8
	movq	%r14, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%rdi, %r14
	je	.L43
	movq	4160(%rsp), %rdx
	subq	%r14, %rdx
	movq	%r14, %rcx
	call	_ZdlPvy
.L43:
	movq	%r12, 4144(%rsp)
	addq	%r12, %r13
	addq	%r12, %rbx
	movq	%rbx, 4160(%rsp)
	movb	$0, 0(%r13)
	addq	$1, %r13
	movq	%r13, 4152(%rsp)
	jmp	.L44
.L64:
	movq	%r13, %rdx
	movq	%r12, %rcx
	call	memmove
	jmp	.L46
.L65:
	call	_ZSt17__throw_bad_allocv
.LEHE0:
.L52:
	movq	%rax, %r12
	movq	4144(%rsp), %rcx
	cmpq	%rdi, %rcx
	je	.L50
	movq	4160(%rsp), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
.L50:
	movq	%r12, %rcx
	call	__cxa_begin_catch
	call	__cxa_end_catch
	leaq	.LC0(%rip), %r12
	jmp	.L40
	.def	__gxx_personality_seh0;	.scl	2;	.type	32;	.endef
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
	.align 4
.LLSDA5324:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT5324-.LLSDATTD5324
.LLSDATTD5324:
	.byte	0x1
	.uleb128 .LLSDACSE5324-.LLSDACSB5324
.LLSDACSB5324:
	.uleb128 .LEHB0-.LFB5324
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L52-.LFB5324
	.uleb128 0x3
.LLSDACSE5324:
	.byte	0x1
	.byte	0
	.byte	0
	.byte	0x7d
	.align 4
	.long	0

.LLSDATT5324:
	.section	.text$_ZNK7fast_io13fast_io_error4whatEv,"x"
	.linkonce discard
	.seh_endproc
	.def	__main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
.LC1:
	.ascii "Hello World\12\0"
	.section	.text.startup,"x"
	.globl	main
	.def	main;	.scl	2;	.type	32;	.endef
	.seh_proc	main
main:
.LFB14416:
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	call	__main
	movl	$1, %ecx
.LEHB1:
	call	*__imp___acrt_iob_func(%rip)
.LEHE1:
	movq	%rax, %rbx
	movq	__imp___iob_func(%rip), %rsi
	call	*%rsi
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L67
	sarq	$4, %rax
	imull	$-1431655765, %eax, %eax
	leal	16(%rax), %ecx
	call	_lock
	orl	$32768, 24(%rbx)
.L68:
	movq	(%rbx), %rcx
	leaq	12(%rcx), %rdx
	movslq	36(%rbx), %rax
	addq	16(%rbx), %rax
	cmpq	%rax, %rdx
	jnb	.L69
	movabsq	$8022916924116329800, %rax
	movq	%rax, (%rcx)
	movl	$174353522, 8(%rcx)
	orl	$65536, 24(%rbx)
	movq	%rdx, %rax
	subq	(%rbx), %rax
	subl	%eax, 8(%rbx)
	movq	%rdx, (%rbx)
.L70:
	call	*%rsi
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L71
	andl	$-32769, 24(%rbx)
	sarq	$4, %rax
	imull	$-1431655765, %eax, %eax
	leal	16(%rax), %ecx
	call	_unlock
.L80:
	xorl	%eax, %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	ret
.L71:
	leaq	48(%rbx), %rcx
	call	LeaveCriticalSection
	jmp	.L80
.L67:
	leaq	48(%rbx), %rcx
	call	EnterCriticalSection
	.p2align 4,,4
	jmp	.L68
.L69:
	movq	%rbx, %rcx
	call	clearerr
	movq	%rbx, %r9
	movl	$12, %r8d
	movl	$1, %edx
	leaq	.LC1(%rip), %rcx
.LEHB2:
	call	fwrite
	movq	%rbx, %rcx
	call	ferror
	movl	%eax, %edi
	call	*__imp__errno(%rip)
	movl	(%rax), %ebp
	movq	%rbx, %rcx
	call	clearerr
	testl	%edi, %edi
	je	.L70
	movl	$24, %ecx
	call	__cxa_allocate_exception
	movq	%rax, %rcx
	movq	$0, 8(%rax)
	leaq	16+_ZTVN7fast_io11posix_errorE(%rip), %rax
	movq	%rax, (%rcx)
	movl	%ebp, 16(%rcx)
	leaq	_ZN7fast_io11posix_errorD1Ev(%rip), %r8
	leaq	_ZTIN7fast_io11posix_errorE(%rip), %rdx
	call	__cxa_throw
.LEHE2:
.L76:
	movq	%rax, %r12
	call	*%rsi
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	movq	%rax, %rcx
	sarq	$4, %rcx
	movabsq	$-6148914691236517205, %rdx
	imulq	%rdx, %rcx
	cmpq	$912, %rax
	jbe	.L81
	leaq	48(%rbx), %rcx
	call	LeaveCriticalSection
.L75:
	movq	%r12, %rcx
.LEHB3:
	call	_Unwind_Resume
.LEHE3:
.L81:
	andl	$-32769, 24(%rbx)
	addl	$16, %ecx
	call	_unlock
	jmp	.L75
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA14416:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE14416-.LLSDACSB14416
.LLSDACSB14416:
	.uleb128 .LEHB1-.LFB14416
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB2-.LFB14416
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L76-.LFB14416
	.uleb128 0
	.uleb128 .LEHB3-.LFB14416
	.uleb128 .LEHE3-.LEHB3
	.uleb128 0
	.uleb128 0
.LLSDACSE14416:
	.section	.text.startup,"x"
	.seh_endproc
	.globl	_ZTSSt9exception
	.section	.rdata$_ZTSSt9exception,"dr"
	.linkonce same_size
	.align 8
_ZTSSt9exception:
	.ascii "St9exception\0"
	.globl	_ZTISt9exception
	.section	.rdata$_ZTISt9exception,"dr"
	.linkonce same_size
	.align 8
_ZTISt9exception:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSSt9exception
	.globl	_ZTSN7fast_io14error_reporterE
	.section	.rdata$_ZTSN7fast_io14error_reporterE,"dr"
	.linkonce same_size
	.align 16
_ZTSN7fast_io14error_reporterE:
	.ascii "N7fast_io14error_reporterE\0"
	.globl	_ZTIN7fast_io14error_reporterE
	.section	.rdata$_ZTIN7fast_io14error_reporterE,"dr"
	.linkonce same_size
	.align 8
_ZTIN7fast_io14error_reporterE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN7fast_io14error_reporterE
	.globl	_ZTSN7fast_io13fast_io_errorE
	.section	.rdata$_ZTSN7fast_io13fast_io_errorE,"dr"
	.linkonce same_size
	.align 16
_ZTSN7fast_io13fast_io_errorE:
	.ascii "N7fast_io13fast_io_errorE\0"
	.globl	_ZTIN7fast_io13fast_io_errorE
	.section	.rdata$_ZTIN7fast_io13fast_io_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTIN7fast_io13fast_io_errorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN7fast_io13fast_io_errorE
	.quad	_ZTISt9exception
	.globl	_ZTSN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE
	.section	.rdata$_ZTSN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE,"dr"
	.linkonce same_size
	.align 32
_ZTSN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE:
	.ascii "N7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE\0"
	.globl	_ZTIN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE
	.section	.rdata$_ZTIN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE,"dr"
	.linkonce same_size
	.align 8
_ZTIN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE
	.quad	_ZTIN7fast_io14error_reporterE
	.globl	_ZTSN7fast_io11posix_errorE
	.section	.rdata$_ZTSN7fast_io11posix_errorE,"dr"
	.linkonce same_size
	.align 16
_ZTSN7fast_io11posix_errorE:
	.ascii "N7fast_io11posix_errorE\0"
	.globl	_ZTIN7fast_io11posix_errorE
	.section	.rdata$_ZTIN7fast_io11posix_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTIN7fast_io11posix_errorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN7fast_io11posix_errorE
	.quad	_ZTIN7fast_io13fast_io_errorE
	.globl	_ZTVN7fast_io13fast_io_errorE
	.section	.rdata$_ZTVN7fast_io13fast_io_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTVN7fast_io13fast_io_errorE:
	.quad	0
	.quad	_ZTIN7fast_io13fast_io_errorE
	.quad	0
	.quad	0
	.quad	_ZNK7fast_io13fast_io_error4whatEv
	.quad	__cxa_pure_virtual
	.globl	_ZTVN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE
	.section	.rdata$_ZTVN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE,"dr"
	.linkonce same_size
	.align 8
_ZTVN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE:
	.quad	0
	.quad	_ZTIN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE
	.quad	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_
	.quad	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy
	.quad	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc
	.globl	_ZTVN7fast_io11posix_errorE
	.section	.rdata$_ZTVN7fast_io11posix_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTVN7fast_io11posix_errorE:
	.quad	0
	.quad	_ZTIN7fast_io11posix_errorE
	.quad	_ZN7fast_io11posix_errorD1Ev
	.quad	_ZN7fast_io11posix_errorD0Ev
	.quad	_ZNK7fast_io13fast_io_error4whatEv
	.quad	_ZNK7fast_io11posix_error6reportERNS_14error_reporterE
	.weak	__cxa_pure_virtual
	.ident	"GCC: (GCC with MCF thread model, built by LH_Mouse.) 11.0.0 20200712 (experimental)"
	.def	_ZdaPv;	.scl	2;	.type	32;	.endef
	.def	_ZNSt9exceptionD2Ev;	.scl	2;	.type	32;	.endef
	.def	_ZdlPvy;	.scl	2;	.type	32;	.endef
	.def	memcpy;	.scl	2;	.type	32;	.endef
	.def	_Znwy;	.scl	2;	.type	32;	.endef
	.def	_ZSt17__throw_bad_allocv;	.scl	2;	.type	32;	.endef
	.def	strerror;	.scl	2;	.type	32;	.endef
	.def	strlen;	.scl	2;	.type	32;	.endef
	.def	_Znay;	.scl	2;	.type	32;	.endef
	.def	memmove;	.scl	2;	.type	32;	.endef
	.def	__cxa_begin_catch;	.scl	2;	.type	32;	.endef
	.def	__cxa_end_catch;	.scl	2;	.type	32;	.endef
	.def	_lock;	.scl	2;	.type	32;	.endef
	.def	_unlock;	.scl	2;	.type	32;	.endef
	.def	LeaveCriticalSection;	.scl	2;	.type	32;	.endef
	.def	EnterCriticalSection;	.scl	2;	.type	32;	.endef
	.def	clearerr;	.scl	2;	.type	32;	.endef
	.def	fwrite;	.scl	2;	.type	32;	.endef
	.def	ferror;	.scl	2;	.type	32;	.endef
	.def	__cxa_allocate_exception;	.scl	2;	.type	32;	.endef
	.def	__cxa_throw;	.scl	2;	.type	32;	.endef
	.def	_Unwind_Resume;	.scl	2;	.type	32;	.endef
	.def	__cxa_pure_virtual;	.scl	2;	.type	32;	.endef
