	.file	"stream_fd.cc"
	.text
	.def	__tcf_0;	.scl	3;	.type	32;	.endef
	.seh_proc	__tcf_0
__tcf_0:
	.seh_endprologue
	leaq	_ZStL8__ioinit(%rip), %rcx
	jmp	_ZNSt8ios_base4InitD1Ev
	.seh_endproc
	.align 2
	.def	_ZNK7fast_io35basic_general_streambuf_io_observerISt15basic_streambufIcSt11char_traitsIcEEEcvNS_23basic_posix_io_observerIcEEEv.isra.0;	.scl	3;	.type	32;	.endef
	.seh_proc	_ZNK7fast_io35basic_general_streambuf_io_observerISt15basic_streambufIcSt11char_traitsIcEEEcvNS_23basic_posix_io_observerIcEEEv.isra.0
_ZNK7fast_io35basic_general_streambuf_io_observerISt15basic_streambufIcSt11char_traitsIcEEEcvNS_23basic_posix_io_observerIcEEEv.isra.0:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	%rcx, %r12
	testq	%rcx, %rcx
	je	.L4
	xorl	%r9d, %r9d
	movq	.refptr._ZTISt13basic_filebufIcSt11char_traitsIcEE(%rip), %r8
	movq	.refptr._ZTISt15basic_streambufIcSt11char_traitsIcEE(%rip), %rdx
	call	__dynamic_cast
	testq	%rax, %rax
	je	.L5
	movq	72(%rax), %rcx
	testq	%rcx, %rcx
	je	.L16
.L8:
	call	*__imp__fileno(%rip)
	addq	$32, %rsp
	popq	%r12
	ret
.L5:
	xorl	%r9d, %r9d
	movq	.refptr._ZTIN9__gnu_cxx18stdio_sync_filebufIcSt11char_traitsIcEEE(%rip), %r8
	movq	.refptr._ZTISt15basic_streambufIcSt11char_traitsIcEE(%rip), %rdx
	movq	%r12, %rcx
	call	__dynamic_cast
	testq	%rax, %rax
	je	.L4
	movq	64(%rax), %rcx
	testq	%rcx, %rcx
	jne	.L8
.L16:
	movq	__imp__errno(%rip), %r12
.L7:
	call	*%r12
	movl	$9, (%rax)
	movl	$-1, %eax
	addq	$32, %rsp
	popq	%r12
	ret
.L4:
	movq	__imp__errno(%rip), %r12
	call	*%r12
	movl	$9, (%rax)
	jmp	.L7
	.seh_endproc
	.section	.text$_ZN7fast_io7details5decay14print_fallbackILb1ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEENS5_ILb1EPvEES8_SA_S8_NS5_ILb1EiEES8_SA_S8_SA_EEEvT0_DpT1_,"x"
	.linkonce discard
	.globl	_ZN7fast_io7details5decay14print_fallbackILb1ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEENS5_ILb1EPvEES8_SA_S8_NS5_ILb1EiEES8_SA_S8_SA_EEEvT0_DpT1_
	.def	_ZN7fast_io7details5decay14print_fallbackILb1ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEENS5_ILb1EPvEES8_SA_S8_NS5_ILb1EiEES8_SA_S8_SA_EEEvT0_DpT1_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details5decay14print_fallbackILb1ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEENS5_ILb1EPvEES8_SA_S8_NS5_ILb1EiEES8_SA_S8_SA_EEEvT0_DpT1_
_ZN7fast_io7details5decay14print_fallbackILb1ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEENS5_ILb1EPvEES8_SA_S8_NS5_ILb1EiEES8_SA_S8_SA_EEEvT0_DpT1_:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	movl	$4392, %eax
	call	___chkstk_ms
	subq	%rax, %rsp
	.seh_stackalloc	4392
	.seh_endprologue
	movq	%rcx, %r12
	movq	(%rdx), %r11
	movq	8(%rdx), %r10
	movq	%r8, %rbx
	movq	(%r9), %rax
	movq	%rax, 40(%rsp)
	movq	8(%r9), %r15
	movq	4504(%rsp), %rax
	movq	(%rax), %rdi
	movq	%rdi, 48(%rsp)
	movq	8(%rax), %r14
	movq	4520(%rsp), %rax
	movq	(%rax), %rdi
	movq	%rdi, 56(%rsp)
	movq	8(%rax), %rbp
	movq	4536(%rsp), %rax
	movq	(%rax), %rdi
	movq	%rdi, 64(%rsp)
	movq	8(%rax), %r13
	leaq	256(%rsp), %rsi
	movq	%rsi, 4352(%rsp)
	movq	%rsi, 4360(%rsp)
	leaq	4352(%rsp), %rdi
	movq	%rdi, 4368(%rsp)
	cmpq	$4096, %r10
	ja	.L174
	testq	%r10, %r10
	jne	.L175
.L22:
	leaq	(%rsi,%r10), %rax
.L21:
	movq	%rax, 4360(%rsp)
	leaq	20(%rax), %rcx
	cmpq	%rdi, %rcx
	jnb	.L23
	movw	$30768, (%rax)
	leaq	19(%rax), %rdx
	addq	$1, %rax
	jmp	.L27
.L176:
	addl	$48, %ebx
	movb	%bl, (%rdx)
	subq	$1, %rdx
	cmpq	%rdx, %rax
	je	.L170
.L26:
	movq	%r8, %rbx
.L27:
	movq	%rbx, %r8
	shrq	$4, %r8
	andl	$15, %ebx
	cmpb	$9, %bl
	jle	.L176
	addl	$87, %ebx
	movb	%bl, (%rdx)
	subq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L26
.L170:
	movq	%rcx, 4360(%rsp)
	movq	4368(%rsp), %rbx
.L28:
	movq	%rbx, %rax
	subq	%rcx, %rax
	cmpq	%rax, %r15
	ja	.L177
	testq	%r15, %r15
	jne	.L178
.L40:
	addq	%r15, %rcx
.L39:
	movq	%rcx, 4360(%rsp)
	leaq	20(%rcx), %r9
	movq	4496(%rsp), %rax
	cmpq	%rbx, %r9
	jnb	.L41
	movw	$30768, (%rcx)
	leaq	19(%rcx), %rdx
	addq	$1, %rcx
	jmp	.L45
.L179:
	addl	$48, %eax
	movb	%al, (%rdx)
	subq	$1, %rdx
	cmpq	%rdx, %rcx
	je	.L171
.L44:
	movq	%r8, %rax
.L45:
	movq	%rax, %r8
	shrq	$4, %r8
	andl	$15, %eax
	cmpb	$9, %al
	jle	.L179
	addl	$87, %eax
	movb	%al, (%rdx)
	subq	$1, %rdx
	cmpq	%rdx, %rcx
	jne	.L44
.L171:
	movq	%r9, 4360(%rsp)
	movq	4368(%rsp), %rdi
.L46:
	movq	%rdi, %rax
	subq	%r9, %rax
	cmpq	%rax, %r14
	ja	.L180
	testq	%r14, %r14
	jne	.L181
.L57:
	leaq	(%r9,%r14), %rcx
.L56:
	movq	%rcx, 4360(%rsp)
	movl	4512(%rsp), %eax
	leaq	11(%rcx), %rdx
	cmpq	%rdx, %rdi
	jbe	.L58
	testl	%eax, %eax
	jns	.L59
	negl	%eax
	movb	$45, (%rcx)
	addq	$1, %rcx
.L59:
	cmpl	$999999999, %eax
	ja	.L60
	cmpl	$99999999, %eax
	ja	.L129
	cmpl	$9999999, %eax
	ja	.L130
	cmpl	$999999, %eax
	ja	.L131
	cmpl	$99999, %eax
	ja	.L132
	cmpl	$9999, %eax
	ja	.L133
	cmpl	$999, %eax
	ja	.L134
	cmpl	$99, %eax
	ja	.L62
	cmpl	$10, %eax
	sbbq	%rdx, %rdx
	leaq	2(%rcx,%rdx), %r10
	movq	%r10, %r9
.L65:
	cmpl	$9, %eax
	jbe	.L67
.L190:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rdx
	movzwl	(%rdx,%rax,2), %eax
	movw	%ax, -2(%r10)
.L68:
	movq	%r9, 4360(%rsp)
	movq	4368(%rsp), %rdi
.L69:
	movq	%rdi, %rax
	subq	%r9, %rax
	cmpq	%rax, %rbp
	ja	.L182
	testq	%rbp, %rbp
	jne	.L183
.L87:
	addq	%rbp, %r9
.L86:
	movq	%r9, 4360(%rsp)
	leaq	20(%r9), %rcx
	movq	4528(%rsp), %rax
	cmpq	%rdi, %rcx
	jnb	.L88
	movw	$30768, (%r9)
	leaq	19(%r9), %rdx
	addq	$1, %r9
	jmp	.L92
.L184:
	addl	$48, %eax
	movb	%al, (%rdx)
	subq	$1, %rdx
	cmpq	%rdx, %r9
	je	.L172
.L91:
	movq	%r8, %rax
.L92:
	movq	%rax, %r8
	shrq	$4, %r8
	andl	$15, %eax
	cmpb	$9, %al
	jle	.L184
	addl	$87, %eax
	movb	%al, (%rdx)
	subq	$1, %rdx
	cmpq	%r9, %rdx
	jne	.L91
.L172:
	movq	%rcx, 4360(%rsp)
	movq	4368(%rsp), %rbx
.L93:
	movq	%rbx, %rax
	subq	%rcx, %rax
	cmpq	%rax, %r13
	ja	.L185
	testq	%r13, %r13
	jne	.L186
.L104:
	addq	%r13, %rcx
.L103:
	movq	%rcx, 4360(%rsp)
	leaq	20(%rcx), %r9
	movq	4544(%rsp), %rax
	cmpq	%rbx, %r9
	jnb	.L105
	movw	$30768, (%rcx)
	leaq	19(%rcx), %rdx
	addq	$1, %rcx
	jmp	.L109
.L187:
	addl	$48, %eax
	movb	%al, (%rdx)
	subq	$1, %rdx
	cmpq	%rdx, %rcx
	je	.L173
.L108:
	movq	%r8, %rax
.L109:
	movq	%rax, %r8
	shrq	$4, %r8
	andl	$15, %eax
	cmpb	$9, %al
	jle	.L187
	addl	$87, %eax
	movb	%al, (%rdx)
	subq	$1, %rdx
	cmpq	%rdx, %rcx
	jne	.L108
.L173:
	movq	%r9, 4360(%rsp)
	movq	4368(%rsp), %rax
.L110:
	cmpq	%rax, %r9
	je	.L188
	movb	$10, (%r9)
	leaq	1(%r9), %r8
	movq	%r8, 4360(%rsp)
	movq	4352(%rsp), %r13
.L120:
	subq	%r13, %r8
	movq	(%r12), %rcx
	leaq	(%rcx,%r8), %rbx
	movslq	36(%r12), %rax
	addq	16(%r12), %rax
	cmpq	%rax, %rbx
	jnb	.L121
	testq	%r8, %r8
	jne	.L189
	xorl	%eax, %eax
.L122:
	orl	$65536, 24(%r12)
	subl	%eax, 8(%r12)
	movq	%rbx, (%r12)
.L123:
	cmpq	%rsi, %r13
	je	.L17
	movq	4368(%rsp), %rdx
	subq	%r13, %rdx
	movq	%r13, %rcx
	call	_ZdlPvy
	nop
.L17:
	addq	$4392, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L181:
	movq	%r14, %r8
	movq	48(%rsp), %rdx
	movq	%r9, %rcx
	call	memcpy
	movq	%rax, %r9
	movq	4368(%rsp), %rdi
	jmp	.L57
.L178:
	movq	%r15, %r8
	movq	40(%rsp), %rdx
	call	memcpy
	movq	%rax, %rcx
	movq	4368(%rsp), %rbx
	jmp	.L40
.L175:
	movq	%r10, %r8
	movq	%r10, 72(%rsp)
	movq	%r11, %rdx
	movq	%rsi, %rcx
	call	memcpy
	movq	4368(%rsp), %rdi
	movq	72(%rsp), %r10
	jmp	.L22
.L130:
	movl	$8, %r9d
.L61:
	addq	%rcx, %r9
	cmpl	$99, %eax
	jbe	.L135
.L126:
	movq	%r9, %r10
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r8
.L66:
	movl	%eax, %ecx
	leaq	(%rcx,%rcx,4), %rdx
	leaq	(%rcx,%rdx,8), %r11
	movq	%r11, %rdx
	salq	$10, %rdx
	subq	%r11, %rdx
	salq	$5, %rdx
	addq	%rcx, %rdx
	leaq	(%rcx,%rdx,4), %rdx
	leaq	(%rcx,%rdx,8), %rdx
	salq	$5, %rdx
	subq	%rcx, %rdx
	shrq	$37, %rdx
	movl	%edx, %ecx
	leal	(%rdx,%rdx,4), %edx
	leal	(%rdx,%rdx,4), %r11d
	sall	$2, %r11d
	movl	%eax, %edx
	subl	%r11d, %edx
	movl	%eax, %r11d
	movl	%ecx, %eax
	subq	$2, %r10
	movzwl	(%r8,%rdx,2), %edx
	movw	%dx, (%r10)
	cmpl	$9999, %r11d
	ja	.L66
	cmpl	$9, %eax
	ja	.L190
.L67:
	addl	$48, %eax
	movb	%al, -1(%r10)
	jmp	.L68
.L189:
	movq	%r13, %rdx
	call	memcpy
	movq	%rbx, %rax
	subq	(%r12), %rax
	movq	4352(%rsp), %r13
	jmp	.L122
.L186:
	movq	%r13, %r8
	movq	64(%rsp), %rdx
	call	memcpy
	movq	%rax, %rcx
	movq	4368(%rsp), %rbx
	jmp	.L104
.L183:
	movq	%rbp, %r8
	movq	56(%rsp), %rdx
	movq	%r9, %rcx
	call	memcpy
	movq	%rax, %r9
	movq	4368(%rsp), %rdi
	jmp	.L87
.L60:
	leaq	10(%rcx), %r9
	jmp	.L126
.L129:
	movl	$9, %r9d
	jmp	.L61
.L58:
	testl	%eax, %eax
	js	.L191
	leaq	117(%rsp), %r14
	movq	%r14, %rdx
.L70:
	cmpl	$999999999, %eax
	ja	.L71
	cmpl	$99999999, %eax
	ja	.L137
	cmpl	$9999999, %eax
	ja	.L138
	cmpl	$999999, %eax
	ja	.L139
	cmpl	$99999, %eax
	ja	.L140
	cmpl	$9999, %eax
	ja	.L141
	cmpl	$999, %eax
	ja	.L142
	cmpl	$99, %eax
	ja	.L73
	cmpl	$10, %eax
	sbbq	%r8, %r8
	leaq	2(%rdx,%r8), %r10
	movq	%r10, %r11
.L76:
	cmpl	$9, %eax
	jbe	.L78
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rdx
	movzwl	(%rdx,%rax,2), %eax
	movw	%ax, -2(%r10)
.L79:
	movq	%r11, %r15
	subq	%r14, %r15
	movq	%rdi, %rax
	subq	%rcx, %rax
	cmpq	%rax, %r15
	ja	.L192
	testq	%r15, %r15
	jne	.L193
.L83:
	leaq	(%rcx,%r15), %r9
.L82:
	movq	%r9, 4360(%rsp)
	jmp	.L69
.L88:
	movw	$30768, 160(%rsp)
	leaq	179(%rsp), %rdx
	leaq	161(%rsp), %r10
	jmp	.L97
.L194:
	addl	$48, %eax
	movb	%al, (%rdx)
	subq	$1, %rdx
	cmpq	%r10, %rdx
	je	.L95
.L96:
	movq	%r8, %rax
.L97:
	movq	%rax, %r8
	shrq	$4, %r8
	andl	$15, %eax
	cmpb	$9, %al
	jle	.L194
	addl	$87, %eax
	movb	%al, (%rdx)
	subq	$1, %rdx
	cmpq	%r10, %rdx
	jne	.L96
.L95:
	movq	%rdi, %rax
	subq	%r9, %rax
	cmpq	$19, %rax
	jbe	.L195
	movq	160(%rsp), %rax
	movq	%rax, (%r9)
	movq	168(%rsp), %rax
	movq	%rax, 8(%r9)
	movl	176(%rsp), %eax
	movl	%eax, 16(%r9)
	jmp	.L172
.L182:
	subq	4352(%rsp), %rdi
	addq	%rdi, %rdi
	cmpq	%rbp, %rdi
	cmovb	%rbp, %rdi
	testq	%rdi, %rdi
	js	.L34
	movq	%rdi, %rcx
	call	_Znwy
	movq	%rax, %r15
	movq	4352(%rsp), %r14
	movq	4360(%rsp), %rbx
	subq	%r14, %rbx
	movq	%rbx, %r8
	movq	%r14, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%rsi, %r14
	je	.L85
	movq	4368(%rsp), %rdx
	subq	%r14, %rdx
	movq	%r14, %rcx
	call	_ZdlPvy
.L85:
	movq	%r15, 4352(%rsp)
	leaq	(%r15,%rbx), %r9
	addq	%r15, %rdi
	movq	%rdi, 4368(%rsp)
	movq	%rbp, %r8
	movq	56(%rsp), %rdx
	movq	%r9, %rcx
	call	memcpy
	movq	%rax, %r9
	addq	%rbp, %r9
	jmp	.L86
.L188:
	movq	%r9, %rdi
	subq	4352(%rsp), %rdi
	addq	%rdi, %rdi
	js	.L34
	movq	%rdi, %rcx
	call	_Znwy
	movq	%rax, %r13
	movq	4352(%rsp), %r14
	movq	4360(%rsp), %rbx
	subq	%r14, %rbx
	movq	%rbx, %r8
	movq	%r14, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%rsi, %r14
	je	.L119
	movq	4368(%rsp), %rdx
	subq	%r14, %rdx
	movq	%r14, %rcx
	call	_ZdlPvy
.L119:
	movq	%r13, 4352(%rsp)
	leaq	0(%r13,%rbx), %r8
	addq	%r13, %rdi
	movq	%rdi, 4368(%rsp)
	movb	$10, (%r8)
	addq	$1, %r8
	movq	%r8, 4360(%rsp)
	jmp	.L120
.L185:
	subq	4352(%rsp), %rbx
	addq	%rbx, %rbx
	cmpq	%r13, %rbx
	cmovb	%r13, %rbx
	testq	%rbx, %rbx
	js	.L34
	movq	%rbx, %rcx
	call	_Znwy
	movq	%rax, %rbp
	movq	4352(%rsp), %r14
	movq	4360(%rsp), %rdi
	subq	%r14, %rdi
	movq	%rdi, %r8
	movq	%r14, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%rsi, %r14
	je	.L102
	movq	4368(%rsp), %rdx
	subq	%r14, %rdx
	movq	%r14, %rcx
	call	_ZdlPvy
.L102:
	movq	%rbp, 4352(%rsp)
	leaq	0(%rbp,%rdi), %rcx
	addq	%rbp, %rbx
	movq	%rbx, 4368(%rsp)
	movq	%r13, %r8
	movq	64(%rsp), %rdx
	call	memcpy
	movq	%rax, %rcx
	addq	%r13, %rcx
	jmp	.L103
.L121:
	movq	%r8, 40(%rsp)
	movq	%r12, %rcx
	call	clearerr
	movq	%r12, %r9
	movq	40(%rsp), %r8
	movl	$1, %edx
	movq	%r13, %rcx
	call	fwrite
	movq	%r12, %rcx
	call	ferror
	movl	%eax, %ebx
	call	*__imp__errno(%rip)
	movq	%r12, %rcx
	call	clearerr
	testl	%ebx, %ebx
	jne	.L196
	movq	4352(%rsp), %r13
	jmp	.L123
.L105:
	movw	$30768, 128(%rsp)
	leaq	147(%rsp), %rdx
	leaq	129(%rsp), %r10
	jmp	.L114
.L197:
	addl	$48, %eax
	movb	%al, (%rdx)
	subq	$1, %rdx
	cmpq	%r10, %rdx
	je	.L112
.L113:
	movq	%r8, %rax
.L114:
	movq	%rax, %r8
	shrq	$4, %r8
	andl	$15, %eax
	cmpb	$9, %al
	jle	.L197
	addl	$87, %eax
	movb	%al, (%rdx)
	subq	$1, %rdx
	cmpq	%r10, %rdx
	jne	.L113
.L112:
	movq	%rbx, %rax
	subq	%rcx, %rax
	cmpq	$19, %rax
	jbe	.L198
	movq	128(%rsp), %rax
	movq	%rax, (%rcx)
	movq	136(%rsp), %rax
	movq	%rax, 8(%rcx)
	movl	144(%rsp), %eax
	movl	%eax, 16(%rcx)
	jmp	.L173
.L177:
	subq	4352(%rsp), %rbx
	leaq	(%rbx,%rbx), %rax
	cmpq	%r15, %rax
	cmovb	%r15, %rax
	testq	%rax, %rax
	js	.L34
	movq	%rax, %rcx
	movq	%rax, 80(%rsp)
	call	_Znwy
	movq	%rax, %rbx
	movq	4352(%rsp), %rdi
	movq	4360(%rsp), %r8
	subq	%rdi, %r8
	movq	%r8, 72(%rsp)
	movq	%rdi, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%rsi, %rdi
	movq	72(%rsp), %r8
	movq	80(%rsp), %r9
	je	.L38
	movq	%r8, 80(%rsp)
	movq	%r9, 72(%rsp)
	movq	4368(%rsp), %rdx
	subq	%rdi, %rdx
	movq	%rdi, %rcx
	call	_ZdlPvy
	movq	80(%rsp), %r8
	movq	72(%rsp), %r9
.L38:
	movq	%rbx, 4352(%rsp)
	leaq	(%rbx,%r8), %rcx
	addq	%r9, %rbx
	movq	%rbx, 4368(%rsp)
	movq	%r15, %r8
	movq	40(%rsp), %rdx
	call	memcpy
	movq	%rax, %rcx
	addq	%r15, %rcx
	jmp	.L39
.L174:
	cmpq	$8192, %r10
	movl	$8192, %eax
	cmovnb	%r10, %rax
	movq	%rax, 72(%rsp)
	testq	%rax, %rax
	js	.L34
	movq	%rax, %rcx
	movq	%r10, 96(%rsp)
	movq	%r11, 88(%rsp)
	call	_Znwy
	movq	4352(%rsp), %rdi
	movq	4360(%rsp), %r8
	subq	%rdi, %r8
	movq	%r8, 80(%rsp)
	movq	%rdi, %rdx
	movq	%rax, %rcx
	call	memcpy
	movq	%rax, %r9
	cmpq	%rsi, %rdi
	movq	80(%rsp), %r8
	movq	88(%rsp), %r11
	movq	96(%rsp), %r10
	je	.L20
	movq	%r10, 104(%rsp)
	movq	%r11, 96(%rsp)
	movq	%rax, 88(%rsp)
	movq	4368(%rsp), %rdx
	subq	%rdi, %rdx
	movq	%rdi, %rcx
	call	_ZdlPvy
	movq	104(%rsp), %r10
	movq	96(%rsp), %r11
	movq	88(%rsp), %r9
	movq	80(%rsp), %r8
.L20:
	movq	%r9, 4352(%rsp)
	leaq	(%r9,%r8), %rcx
	movq	72(%rsp), %rdi
	addq	%r9, %rdi
	movq	%rdi, 4368(%rsp)
	movq	%r10, %r8
	movq	%r10, 72(%rsp)
	movq	%r11, %rdx
	call	memcpy
	movq	72(%rsp), %r10
	addq	%r10, %rax
	jmp	.L21
.L41:
	movw	$30768, 192(%rsp)
	leaq	211(%rsp), %rdx
	leaq	193(%rsp), %r10
	jmp	.L50
.L199:
	addl	$48, %eax
	movb	%al, (%rdx)
	subq	$1, %rdx
	cmpq	%r10, %rdx
	je	.L48
.L49:
	movq	%r8, %rax
.L50:
	movq	%rax, %r8
	shrq	$4, %r8
	andl	$15, %eax
	cmpb	$9, %al
	jle	.L199
	addl	$87, %eax
	movb	%al, (%rdx)
	subq	$1, %rdx
	cmpq	%r10, %rdx
	jne	.L49
.L48:
	movq	%rbx, %rax
	subq	%rcx, %rax
	cmpq	$19, %rax
	jbe	.L200
	movq	192(%rsp), %rax
	movq	%rax, (%rcx)
	movq	200(%rsp), %rax
	movq	%rax, 8(%rcx)
	movl	208(%rsp), %eax
	movl	%eax, 16(%rcx)
	jmp	.L171
.L180:
	subq	4352(%rsp), %rdi
	addq	%rdi, %rdi
	cmpq	%r14, %rdi
	cmovb	%r14, %rdi
	testq	%rdi, %rdi
	js	.L34
	movq	%rdi, %rcx
	call	_Znwy
	movq	%rax, %rbx
	movq	4352(%rsp), %r15
	movq	4360(%rsp), %r8
	subq	%r15, %r8
	movq	%r8, 40(%rsp)
	movq	%r15, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%rsi, %r15
	movq	40(%rsp), %r8
	je	.L55
	movq	4368(%rsp), %rdx
	subq	%r15, %rdx
	movq	%r15, %rcx
	call	_ZdlPvy
	movq	40(%rsp), %r8
.L55:
	movq	%rbx, 4352(%rsp)
	leaq	(%rbx,%r8), %rcx
	addq	%rbx, %rdi
	movq	%rdi, 4368(%rsp)
	movq	%r14, %r8
	movq	48(%rsp), %rdx
	call	memcpy
	movq	%rax, %rcx
	addq	%r14, %rcx
	jmp	.L56
.L23:
	movw	$30768, 224(%rsp)
	leaq	243(%rsp), %rdx
	leaq	225(%rsp), %r9
	jmp	.L32
.L201:
	addl	$48, %ebx
	movb	%bl, (%rdx)
	subq	$1, %rdx
	cmpq	%r9, %rdx
	je	.L30
.L31:
	movq	%r8, %rbx
.L32:
	movq	%rbx, %r8
	shrq	$4, %r8
	andl	$15, %ebx
	cmpb	$9, %bl
	jle	.L201
	addl	$87, %ebx
	movb	%bl, (%rdx)
	subq	$1, %rdx
	cmpq	%r9, %rdx
	jne	.L31
.L30:
	movq	%rdi, %rdx
	subq	%rax, %rdx
	cmpq	$19, %rdx
	jbe	.L202
	movq	224(%rsp), %rdx
	movq	%rdx, (%rax)
	movq	232(%rsp), %rdx
	movq	%rdx, 8(%rax)
	movl	240(%rsp), %edx
	movl	%edx, 16(%rax)
	jmp	.L170
.L131:
	movl	$7, %r9d
	jmp	.L61
.L132:
	movl	$6, %r9d
	jmp	.L61
.L133:
	movl	$5, %r9d
	jmp	.L61
.L134:
	movl	$4, %r9d
	jmp	.L61
.L138:
	movl	$8, %r11d
.L72:
	addq	%rdx, %r11
	cmpl	$99, %eax
	jbe	.L143
.L127:
	movq	%r11, %r10
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r8
.L77:
	movl	%eax, %r9d
	leaq	(%r9,%r9,4), %rdx
	leaq	(%r9,%rdx,8), %rbx
	movq	%rbx, %rdx
	salq	$10, %rdx
	subq	%rbx, %rdx
	salq	$5, %rdx
	addq	%r9, %rdx
	leaq	(%r9,%rdx,4), %rdx
	leaq	(%r9,%rdx,8), %rdx
	salq	$5, %rdx
	subq	%r9, %rdx
	shrq	$37, %rdx
	movl	%edx, %r9d
	leal	(%rdx,%rdx,4), %edx
	leal	(%rdx,%rdx,4), %ebx
	sall	$2, %ebx
	movl	%eax, %edx
	subl	%ebx, %edx
	movl	%eax, %ebx
	movl	%r9d, %eax
	subq	$2, %r10
	movzwl	(%r8,%rdx,2), %edx
	movw	%dx, (%r10)
	cmpl	$9999, %ebx
	ja	.L77
	jmp	.L76
.L73:
	leaq	3(%rdx), %r11
	movl	%eax, %r9d
	leaq	(%r9,%r9,4), %r8
	leaq	(%r9,%r8,8), %r10
	movq	%r10, %r8
	salq	$10, %r8
	subq	%r10, %r8
	salq	$5, %r8
	addq	%r9, %r8
	leaq	(%r9,%r8,4), %r8
	leaq	(%r9,%r8,8), %r8
	salq	$5, %r8
	subq	%r9, %r8
	shrq	$37, %r8
	movl	%r8d, %r9d
	leal	(%r8,%r8,4), %r8d
	leal	(%r8,%r8,4), %r10d
	sall	$2, %r10d
	subl	%r10d, %eax
	movl	%eax, %r8d
	movl	%r9d, %eax
	leaq	1(%rdx), %r10
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r9
	movzwl	(%r9,%r8,2), %r8d
	movw	%r8w, 1(%rdx)
.L78:
	addl	$48, %eax
	movb	%al, -1(%r10)
	jmp	.L79
.L191:
	negl	%eax
	movb	$45, 117(%rsp)
	leaq	118(%rsp), %rdx
	leaq	117(%rsp), %r14
	jmp	.L70
.L62:
	leaq	3(%rcx), %r9
	movl	%eax, %r8d
	leaq	(%r8,%r8,4), %rdx
	leaq	(%r8,%rdx,8), %r10
	movq	%r10, %rdx
	salq	$10, %rdx
	subq	%r10, %rdx
	salq	$5, %rdx
	addq	%r8, %rdx
	leaq	(%r8,%rdx,4), %rdx
	leaq	(%r8,%rdx,8), %rdx
	salq	$5, %rdx
	subq	%r8, %rdx
	shrq	$37, %rdx
	movl	%edx, %r8d
	leal	(%rdx,%rdx,4), %edx
	leal	(%rdx,%rdx,4), %r10d
	sall	$2, %r10d
	subl	%r10d, %eax
	movl	%eax, %edx
	movl	%r8d, %eax
	leaq	1(%rcx), %r10
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r8
	movzwl	(%r8,%rdx,2), %edx
	movw	%dx, 1(%rcx)
	jmp	.L67
.L193:
	movq	%r15, %r8
	movq	%r14, %rdx
	call	memcpy
	movq	%rax, %rcx
	movq	4368(%rsp), %rdi
	jmp	.L83
.L71:
	leaq	10(%rdx), %r11
	jmp	.L127
.L137:
	movl	$9, %r11d
	jmp	.L72
.L202:
	subq	4352(%rsp), %rdi
	addq	%rdi, %rdi
	cmpq	$20, %rdi
	movl	$20, %eax
	cmovb	%rax, %rdi
	testq	%rdi, %rdi
	js	.L34
	movq	%rdi, %rcx
	call	_Znwy
	movq	%rax, %rbx
	movq	4352(%rsp), %r9
	movq	4360(%rsp), %rax
	subq	%r9, %rax
	movq	%rax, 72(%rsp)
	movq	%rax, %r8
	movq	%r9, %rdx
	movq	%r9, 80(%rsp)
	movq	%rbx, %rcx
	call	memcpy
	movq	80(%rsp), %r9
	cmpq	%rsi, %r9
	je	.L35
	movq	4368(%rsp), %rdx
	subq	%r9, %rdx
	movq	%r9, %rcx
	call	_ZdlPvy
.L35:
	movq	%rbx, 4352(%rsp)
	movq	72(%rsp), %rcx
	addq	%rbx, %rcx
	addq	%rdi, %rbx
	movq	%rbx, 4368(%rsp)
	movq	224(%rsp), %rax
	movq	%rax, (%rcx)
	movq	232(%rsp), %rax
	movq	%rax, 8(%rcx)
	movl	240(%rsp), %eax
	movl	%eax, 16(%rcx)
	addq	$20, %rcx
	movq	%rcx, 4360(%rsp)
	jmp	.L28
.L200:
	subq	4352(%rsp), %rbx
	leaq	(%rbx,%rbx), %r15
	cmpq	$20, %r15
	movl	$20, %eax
	cmovb	%rax, %r15
	testq	%r15, %r15
	js	.L34
	movq	%r15, %rcx
	call	_Znwy
	movq	%rax, %rdi
	movq	4352(%rsp), %r9
	movq	4360(%rsp), %rbx
	subq	%r9, %rbx
	movq	%rbx, %r8
	movq	%r9, %rdx
	movq	%r9, 40(%rsp)
	movq	%rax, %rcx
	call	memcpy
	movq	40(%rsp), %r9
	cmpq	%rsi, %r9
	je	.L52
	movq	4368(%rsp), %rdx
	subq	%r9, %rdx
	movq	%r9, %rcx
	call	_ZdlPvy
.L52:
	movq	%rdi, 4352(%rsp)
	leaq	(%rdi,%rbx), %rcx
	addq	%r15, %rdi
	movq	%rdi, 4368(%rsp)
	movq	192(%rsp), %rax
	movq	%rax, (%rcx)
	movq	200(%rsp), %rax
	movq	%rax, 8(%rcx)
	movl	208(%rsp), %eax
	movl	%eax, 16(%rcx)
	leaq	20(%rcx), %r9
	movq	%r9, 4360(%rsp)
	jmp	.L46
.L198:
	subq	4352(%rsp), %rbx
	addq	%rbx, %rbx
	cmpq	$20, %rbx
	movl	$20, %eax
	cmovb	%rax, %rbx
	testq	%rbx, %rbx
	js	.L34
	movq	%rbx, %rcx
	call	_Znwy
	movq	%rax, %rdi
	movq	4352(%rsp), %r13
	movq	4360(%rsp), %rbp
	subq	%r13, %rbp
	movq	%rbp, %r8
	movq	%r13, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%rsi, %r13
	je	.L116
	movq	4368(%rsp), %rdx
	subq	%r13, %rdx
	movq	%r13, %rcx
	call	_ZdlPvy
.L116:
	movq	%rdi, 4352(%rsp)
	leaq	(%rdi,%rbp), %r9
	leaq	(%rdi,%rbx), %rax
	movq	%rax, 4368(%rsp)
	movq	128(%rsp), %rdx
	movq	%rdx, (%r9)
	movq	136(%rsp), %rdx
	movq	%rdx, 8(%r9)
	movl	144(%rsp), %edx
	movl	%edx, 16(%r9)
	addq	$20, %r9
	movq	%r9, 4360(%rsp)
	jmp	.L110
.L195:
	subq	4352(%rsp), %rdi
	addq	%rdi, %rdi
	cmpq	$20, %rdi
	movl	$20, %eax
	cmovb	%rax, %rdi
	testq	%rdi, %rdi
	js	.L34
	movq	%rdi, %rcx
	call	_Znwy
	movq	%rax, %rbx
	movq	4352(%rsp), %r14
	movq	4360(%rsp), %rbp
	subq	%r14, %rbp
	movq	%rbp, %r8
	movq	%r14, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%rsi, %r14
	je	.L99
	movq	4368(%rsp), %rdx
	subq	%r14, %rdx
	movq	%r14, %rcx
	call	_ZdlPvy
.L99:
	movq	%rbx, 4352(%rsp)
	leaq	(%rbx,%rbp), %rcx
	addq	%rdi, %rbx
	movq	%rbx, 4368(%rsp)
	movq	160(%rsp), %rax
	movq	%rax, (%rcx)
	movq	168(%rsp), %rax
	movq	%rax, 8(%rcx)
	movl	176(%rsp), %eax
	movl	%eax, 16(%rcx)
	addq	$20, %rcx
	movq	%rcx, 4360(%rsp)
	jmp	.L93
.L192:
	subq	4352(%rsp), %rdi
	addq	%rdi, %rdi
	cmpq	%r15, %rdi
	cmovb	%r15, %rdi
	testq	%rdi, %rdi
	js	.L34
	movq	%rdi, %rcx
	call	_Znwy
	movq	4352(%rsp), %r10
	movq	4360(%rsp), %rbx
	subq	%r10, %rbx
	movq	%rbx, %r8
	movq	%r10, %rdx
	movq	%r10, 40(%rsp)
	movq	%rax, %rcx
	call	memcpy
	movq	%rax, %r9
	movq	40(%rsp), %r10
	cmpq	%rsi, %r10
	je	.L81
	movq	%rax, 40(%rsp)
	movq	4368(%rsp), %rdx
	subq	%r10, %rdx
	movq	%r10, %rcx
	call	_ZdlPvy
	movq	40(%rsp), %r9
.L81:
	movq	%r9, 4352(%rsp)
	addq	%r9, %rbx
	addq	%r9, %rdi
	movq	%rdi, 4368(%rsp)
	movq	%r15, %r8
	movq	%r14, %rdx
	movq	%rbx, %rcx
	call	memcpy
	leaq	(%rbx,%r15), %r9
	jmp	.L82
.L139:
	movl	$7, %r11d
	jmp	.L72
.L140:
	movl	$6, %r11d
	jmp	.L72
.L141:
	movl	$5, %r11d
	jmp	.L72
.L142:
	movl	$4, %r11d
	jmp	.L72
.L135:
	movq	%r9, %r10
	jmp	.L65
.L143:
	movq	%r11, %r10
	jmp	.L76
.L34:
	.p2align 4,,5
	call	_ZSt17__throw_bad_allocv
.L196:
	ud2
	.seh_endproc
	.def	__main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
.LC0:
	.ascii "std::cout\12rdbuf():\0"
.LC1:
	.ascii "\12FILE*:\0"
.LC2:
	.ascii "\12fd:\0"
.LC3:
	.ascii "\12win32 HANDLE:\0"
.LC4:
	.ascii "\12nt HANDLE:\0"
	.section	.text.startup,"x"
	.globl	main
	.def	main;	.scl	2;	.type	32;	.endef
	.seh_proc	main
main:
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$176, %rsp
	.seh_stackalloc	176
	.seh_endprologue
	call	__main
	movq	.refptr._ZSt4cout(%rip), %rax
	movq	240(%rax), %rsi
	movq	%rsi, %rcx
	call	_ZNK7fast_io35basic_general_streambuf_io_observerISt15basic_streambufIcSt11char_traitsIcEEEcvNS_23basic_posix_io_observerIcEEEv.isra.0
	movl	%eax, %ecx
	movq	__imp__get_osfhandle(%rip), %rbx
	call	*%rbx
	movq	%rax, %r12
	movq	%rsi, %rcx
	call	_ZNK7fast_io35basic_general_streambuf_io_observerISt15basic_streambufIcSt11char_traitsIcEEEcvNS_23basic_posix_io_observerIcEEEv.isra.0
	movl	%eax, %ecx
	call	*%rbx
	movq	%rax, %rbp
	movq	%rsi, %rcx
	call	_ZNK7fast_io35basic_general_streambuf_io_observerISt15basic_streambufIcSt11char_traitsIcEEEcvNS_23basic_posix_io_observerIcEEEv.isra.0
	movl	%eax, %edi
	testq	%rsi, %rsi
	je	.L204
	xorl	%r9d, %r9d
	movq	.refptr._ZTISt13basic_filebufIcSt11char_traitsIcEE(%rip), %r8
	movq	.refptr._ZTISt15basic_streambufIcSt11char_traitsIcEE(%rip), %rdx
	movq	%rsi, %rcx
	call	__dynamic_cast
	testq	%rax, %rax
	je	.L205
	movq	72(%rax), %r14
.L206:
	movl	$1, %ecx
	call	*__imp___acrt_iob_func(%rip)
	movq	%rax, %rbx
	movq	__imp___iob_func(%rip), %r13
	call	*%r13
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L207
	sarq	$4, %rax
	imull	$-1431655765, %eax, %eax
	leal	16(%rax), %ecx
	call	_lock
	orl	$32768, 24(%rbx)
.L208:
	leaq	.LC0(%rip), %rax
	movq	%rax, 160(%rsp)
	movq	$18, 168(%rsp)
	leaq	.LC1(%rip), %rax
	movq	%rax, 144(%rsp)
	movq	$7, 152(%rsp)
	leaq	.LC2(%rip), %rax
	movq	%rax, 128(%rsp)
	movq	$4, 136(%rsp)
	leaq	.LC3(%rip), %rax
	movq	%rax, 112(%rsp)
	movq	$14, 120(%rsp)
	leaq	.LC4(%rip), %rax
	movq	%rax, 96(%rsp)
	movq	$11, 104(%rsp)
	leaq	160(%rsp), %rdx
	movq	%r12, 80(%rsp)
	leaq	96(%rsp), %rax
	movq	%rax, 72(%rsp)
	movq	%rbp, 64(%rsp)
	leaq	112(%rsp), %rax
	movq	%rax, 56(%rsp)
	movl	%edi, 48(%rsp)
	leaq	128(%rsp), %rax
	movq	%rax, 40(%rsp)
	movq	%r14, 32(%rsp)
	leaq	144(%rsp), %r9
	movq	%rsi, %r8
	movq	%rbx, %rcx
	call	_ZN7fast_io7details5decay14print_fallbackILb1ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEENS5_ILb1EPvEES8_SA_S8_NS5_ILb1EiEES8_SA_S8_SA_EEEvT0_DpT1_
	call	*%r13
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L209
	andl	$-32769, 24(%rbx)
	sarq	$4, %rax
	imull	$-1431655765, %eax, %eax
	leal	16(%rax), %ecx
	call	_unlock
.L210:
	xorl	%eax, %eax
	addq	$176, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.L205:
	xorl	%r9d, %r9d
	movq	.refptr._ZTIN9__gnu_cxx18stdio_sync_filebufIcSt11char_traitsIcEEE(%rip), %r8
	movq	.refptr._ZTISt15basic_streambufIcSt11char_traitsIcEE(%rip), %rdx
	movq	%rsi, %rcx
	call	__dynamic_cast
	testq	%rax, %rax
	je	.L204
	movq	64(%rax), %r14
	jmp	.L206
.L209:
	leaq	48(%rbx), %rcx
	call	LeaveCriticalSection
	jmp	.L210
.L207:
	leaq	48(%rbx), %rcx
	call	EnterCriticalSection
	.p2align 4,,4
	jmp	.L208
.L204:
	call	*__imp__errno(%rip)
	movl	$9, (%rax)
	xorl	%r14d, %r14d
	jmp	.L206
	.seh_endproc
	.def	_GLOBAL__sub_I_main;	.scl	3;	.type	32;	.endef
	.seh_proc	_GLOBAL__sub_I_main
_GLOBAL__sub_I_main:
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	leaq	_ZStL8__ioinit(%rip), %rcx
	call	_ZNSt8ios_base4InitC1Ev
	leaq	__tcf_0(%rip), %rcx
	addq	$40, %rsp
	jmp	atexit
	.seh_endproc
	.section	.ctors,"w"
	.align 8
	.quad	_GLOBAL__sub_I_main
	.globl	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE
	.section	.rdata$_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE,"dr"
	.linkonce same_size
	.align 32
_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE:
	.byte	48
	.byte	48
	.byte	48
	.byte	49
	.byte	48
	.byte	50
	.byte	48
	.byte	51
	.byte	48
	.byte	52
	.byte	48
	.byte	53
	.byte	48
	.byte	54
	.byte	48
	.byte	55
	.byte	48
	.byte	56
	.byte	48
	.byte	57
	.byte	49
	.byte	48
	.byte	49
	.byte	49
	.byte	49
	.byte	50
	.byte	49
	.byte	51
	.byte	49
	.byte	52
	.byte	49
	.byte	53
	.byte	49
	.byte	54
	.byte	49
	.byte	55
	.byte	49
	.byte	56
	.byte	49
	.byte	57
	.byte	50
	.byte	48
	.byte	50
	.byte	49
	.byte	50
	.byte	50
	.byte	50
	.byte	51
	.byte	50
	.byte	52
	.byte	50
	.byte	53
	.byte	50
	.byte	54
	.byte	50
	.byte	55
	.byte	50
	.byte	56
	.byte	50
	.byte	57
	.byte	51
	.byte	48
	.byte	51
	.byte	49
	.byte	51
	.byte	50
	.byte	51
	.byte	51
	.byte	51
	.byte	52
	.byte	51
	.byte	53
	.byte	51
	.byte	54
	.byte	51
	.byte	55
	.byte	51
	.byte	56
	.byte	51
	.byte	57
	.byte	52
	.byte	48
	.byte	52
	.byte	49
	.byte	52
	.byte	50
	.byte	52
	.byte	51
	.byte	52
	.byte	52
	.byte	52
	.byte	53
	.byte	52
	.byte	54
	.byte	52
	.byte	55
	.byte	52
	.byte	56
	.byte	52
	.byte	57
	.byte	53
	.byte	48
	.byte	53
	.byte	49
	.byte	53
	.byte	50
	.byte	53
	.byte	51
	.byte	53
	.byte	52
	.byte	53
	.byte	53
	.byte	53
	.byte	54
	.byte	53
	.byte	55
	.byte	53
	.byte	56
	.byte	53
	.byte	57
	.byte	54
	.byte	48
	.byte	54
	.byte	49
	.byte	54
	.byte	50
	.byte	54
	.byte	51
	.byte	54
	.byte	52
	.byte	54
	.byte	53
	.byte	54
	.byte	54
	.byte	54
	.byte	55
	.byte	54
	.byte	56
	.byte	54
	.byte	57
	.byte	55
	.byte	48
	.byte	55
	.byte	49
	.byte	55
	.byte	50
	.byte	55
	.byte	51
	.byte	55
	.byte	52
	.byte	55
	.byte	53
	.byte	55
	.byte	54
	.byte	55
	.byte	55
	.byte	55
	.byte	56
	.byte	55
	.byte	57
	.byte	56
	.byte	48
	.byte	56
	.byte	49
	.byte	56
	.byte	50
	.byte	56
	.byte	51
	.byte	56
	.byte	52
	.byte	56
	.byte	53
	.byte	56
	.byte	54
	.byte	56
	.byte	55
	.byte	56
	.byte	56
	.byte	56
	.byte	57
	.byte	57
	.byte	48
	.byte	57
	.byte	49
	.byte	57
	.byte	50
	.byte	57
	.byte	51
	.byte	57
	.byte	52
	.byte	57
	.byte	53
	.byte	57
	.byte	54
	.byte	57
	.byte	55
	.byte	57
	.byte	56
	.byte	57
	.byte	57
.lcomm _ZStL8__ioinit,1,1
	.ident	"GCC: (GCC with MCF thread model, built by LH_Mouse.) 11.0.0 20200712 (experimental)"
	.def	_ZNSt8ios_base4InitD1Ev;	.scl	2;	.type	32;	.endef
	.def	__dynamic_cast;	.scl	2;	.type	32;	.endef
	.def	_ZdlPvy;	.scl	2;	.type	32;	.endef
	.def	memcpy;	.scl	2;	.type	32;	.endef
	.def	_Znwy;	.scl	2;	.type	32;	.endef
	.def	clearerr;	.scl	2;	.type	32;	.endef
	.def	fwrite;	.scl	2;	.type	32;	.endef
	.def	ferror;	.scl	2;	.type	32;	.endef
	.def	_ZSt17__throw_bad_allocv;	.scl	2;	.type	32;	.endef
	.def	_lock;	.scl	2;	.type	32;	.endef
	.def	_unlock;	.scl	2;	.type	32;	.endef
	.def	LeaveCriticalSection;	.scl	2;	.type	32;	.endef
	.def	EnterCriticalSection;	.scl	2;	.type	32;	.endef
	.def	_ZNSt8ios_base4InitC1Ev;	.scl	2;	.type	32;	.endef
	.def	atexit;	.scl	2;	.type	32;	.endef
	.section	.rdata$.refptr._ZSt4cout, "dr"
	.globl	.refptr._ZSt4cout
	.linkonce	discard
.refptr._ZSt4cout:
	.quad	_ZSt4cout
	.section	.rdata$.refptr._ZTIN9__gnu_cxx18stdio_sync_filebufIcSt11char_traitsIcEEE, "dr"
	.globl	.refptr._ZTIN9__gnu_cxx18stdio_sync_filebufIcSt11char_traitsIcEEE
	.linkonce	discard
.refptr._ZTIN9__gnu_cxx18stdio_sync_filebufIcSt11char_traitsIcEEE:
	.quad	_ZTIN9__gnu_cxx18stdio_sync_filebufIcSt11char_traitsIcEEE
	.section	.rdata$.refptr._ZTISt15basic_streambufIcSt11char_traitsIcEE, "dr"
	.globl	.refptr._ZTISt15basic_streambufIcSt11char_traitsIcEE
	.linkonce	discard
.refptr._ZTISt15basic_streambufIcSt11char_traitsIcEE:
	.quad	_ZTISt15basic_streambufIcSt11char_traitsIcEE
	.section	.rdata$.refptr._ZTISt13basic_filebufIcSt11char_traitsIcEE, "dr"
	.globl	.refptr._ZTISt13basic_filebufIcSt11char_traitsIcEE
	.linkonce	discard
.refptr._ZTISt13basic_filebufIcSt11char_traitsIcEE:
	.quad	_ZTISt13basic_filebufIcSt11char_traitsIcEE
