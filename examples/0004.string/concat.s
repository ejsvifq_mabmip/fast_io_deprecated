	.file	"concat.cc"
	.text
	.section .rdata,"dr"
.LC0:
	.ascii "basic_string::append\0"
.LC1:
	.ascii "basic_string::_M_create\0"
	.section	.text$_ZN7fast_io5writeIcSt11char_traitsIcESaIcEPKcEEvNS_11ostring_refIT_T0_T1_EET2_SB_,"x"
	.linkonce discard
	.globl	_ZN7fast_io5writeIcSt11char_traitsIcESaIcEPKcEEvNS_11ostring_refIT_T0_T1_EET2_SB_
	.def	_ZN7fast_io5writeIcSt11char_traitsIcESaIcEPKcEEvNS_11ostring_refIT_T0_T1_EET2_SB_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io5writeIcSt11char_traitsIcESaIcEPKcEEvNS_11ostring_refIT_T0_T1_EET2_SB_
_ZN7fast_io5writeIcSt11char_traitsIcESaIcEPKcEEvNS_11ostring_refIT_T0_T1_EET2_SB_:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$56, %rsp
	.seh_stackalloc	56
	.seh_endprologue
	movq	%rcx, %rbx
	movq	%rdx, %r12
	movq	%r8, %rsi
	movq	(%rcx), %rbp
	movq	8(%rcx), %r13
	leaq	0(%rbp,%r13), %rcx
	leaq	16(%rbx), %r14
	cmpq	%rbp, %r14
	je	.L19
	movq	16(%rbx), %rax
.L2:
	subq	%r12, %rsi
	leaq	(%rcx,%rsi), %rdi
	leaq	0(%rbp,%rax), %rdx
	cmpq	%rdx, %rdi
	ja	.L3
	movq	%rsi, %r8
	movq	%r12, %rdx
	call	memcpy
	movb	$0, (%rdi)
	subq	(%rbx), %rdi
	movq	%rdi, 8(%rbx)
.L1:
	addq	$56, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L19:
	movl	$15, %eax
	jmp	.L2
.L3:
	movabsq	$9223372036854775807, %rdx
	subq	%r13, %rdx
	cmpq	%rdx, %rsi
	ja	.L31
	leaq	(%rsi,%r13), %r15
	cmpq	%r15, %rax
	jb	.L6
	testq	%rsi, %rsi
	je	.L7
	cmpq	$1, %rsi
	je	.L32
	movq	%rsi, %r8
	movq	%r12, %rdx
	call	memcpy
	movq	(%rbx), %rbp
.L7:
	movq	%r15, 8(%rbx)
	movb	$0, 0(%rbp,%r15)
	jmp	.L1
.L6:
	testq	%r15, %r15
	js	.L33
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %r15
	jnb	.L20
	testq	%rdi, %rdi
	js	.L11
.L10:
	movq	%rdi, %rcx
	addq	$1, %rcx
	js	.L11
	call	_Znwy
	movq	%rax, %rbp
	movq	(%rbx), %r9
	testq	%r13, %r13
	je	.L14
	cmpq	$1, %r13
	je	.L34
	movq	%r13, %r8
	movq	%r9, %rdx
	movq	%r9, 40(%rsp)
	movq	%rax, %rcx
	call	memcpy
	movq	40(%rsp), %r9
.L14:
	testq	%r12, %r12
	je	.L16
	testq	%rsi, %rsi
	je	.L16
	leaq	0(%rbp,%r13), %rcx
	cmpq	$1, %rsi
	je	.L35
	movq	%r9, 40(%rsp)
	movq	%rsi, %r8
	movq	%r12, %rdx
	call	memcpy
	movq	40(%rsp), %r9
.L16:
	cmpq	%r9, %r14
	je	.L18
	movq	16(%rbx), %rax
	leaq	1(%rax), %rdx
	movq	%r9, %rcx
	call	_ZdlPvy
.L18:
	movq	%rbp, (%rbx)
	movq	%rdi, 16(%rbx)
	jmp	.L7
.L32:
	movzbl	(%r12), %eax
	movb	%al, (%rcx)
	movq	(%rbx), %rbp
	jmp	.L7
.L20:
	movq	%r15, %rdi
	jmp	.L10
.L34:
	movzbl	(%r9), %eax
	movb	%al, 0(%rbp)
	jmp	.L14
.L35:
	movzbl	(%r12), %eax
	movb	%al, (%rcx)
	jmp	.L16
.L31:
	leaq	.LC0(%rip), %rcx
	call	_ZSt20__throw_length_errorPKc
.L11:
	call	_ZSt17__throw_bad_allocv
.L33:
	leaq	.LC1(%rip), %rcx
	call	_ZSt20__throw_length_errorPKc
	nop
	.seh_endproc
	.section	.text$_ZN7fast_io7details5decay14print_fallbackILb1ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_S8_S8_NS5_ILb1ENS_5manip4chvwIcEEEES8_S8_S8_EEEvT0_DpT1_,"x"
	.linkonce discard
	.globl	_ZN7fast_io7details5decay14print_fallbackILb1ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_S8_S8_NS5_ILb1ENS_5manip4chvwIcEEEES8_S8_S8_EEEvT0_DpT1_
	.def	_ZN7fast_io7details5decay14print_fallbackILb1ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_S8_S8_NS5_ILb1ENS_5manip4chvwIcEEEES8_S8_S8_EEEvT0_DpT1_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details5decay14print_fallbackILb1ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_S8_S8_NS5_ILb1ENS_5manip4chvwIcEEEES8_S8_S8_EEEvT0_DpT1_
_ZN7fast_io7details5decay14print_fallbackILb1ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_S8_S8_NS5_ILb1ENS_5manip4chvwIcEEEES8_S8_S8_EEEvT0_DpT1_:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	movl	$4264, %eax
	call	___chkstk_ms
	subq	%rax, %rsp
	.seh_stackalloc	4264
	.seh_endprologue
	movq	%rcx, %r12
	movq	(%rdx), %r11
	movq	8(%rdx), %r10
	movq	(%r8), %rax
	movq	%rax, 32(%rsp)
	movq	8(%r8), %rbp
	movq	(%r9), %rax
	movq	%rax, 40(%rsp)
	movq	8(%r9), %r9
	movq	4368(%rsp), %rax
	movq	(%rax), %rdx
	movq	%rdx, 48(%rsp)
	movq	8(%rax), %rsi
	movq	4384(%rsp), %rax
	movq	(%rax), %rdi
	movq	%rdi, 56(%rsp)
	movq	8(%rax), %r14
	movq	4392(%rsp), %rax
	movq	(%rax), %rdx
	movq	%rdx, 64(%rsp)
	movq	8(%rax), %r13
	movq	4400(%rsp), %rax
	movq	(%rax), %rbx
	movq	%rbx, 72(%rsp)
	movq	8(%rax), %rbx
	leaq	128(%rsp), %r15
	movq	%r15, 4224(%rsp)
	movq	%r15, 4232(%rsp)
	leaq	4224(%rsp), %rdi
	movq	%rdi, 4240(%rsp)
	cmpq	$4096, %r10
	ja	.L98
	testq	%r10, %r10
	jne	.L99
.L41:
	leaq	(%r15,%r10), %rcx
.L40:
	movq	%rcx, 4232(%rsp)
	movq	%rdi, %rax
	subq	%rcx, %rax
	cmpq	%rax, %rbp
	ja	.L100
	testq	%rbp, %rbp
	jne	.L101
.L46:
	addq	%rcx, %rbp
.L45:
	movq	%rbp, 4232(%rsp)
	movq	%rdi, %rax
	subq	%rbp, %rax
	cmpq	%rax, %r9
	ja	.L102
	testq	%r9, %r9
	jne	.L103
.L50:
	addq	%rbp, %r9
.L49:
	movq	%r9, 4232(%rsp)
	movq	%rdi, %rax
	subq	%r9, %rax
	cmpq	%rax, %rsi
	ja	.L104
	testq	%rsi, %rsi
	jne	.L105
.L54:
	addq	%r9, %rsi
.L53:
	movq	%rsi, 4232(%rsp)
	cmpq	%rdi, %rsi
	je	.L106
	movzbl	4376(%rsp), %eax
	movb	%al, (%rsi)
	addq	$1, %rsi
	movq	%rsi, 4232(%rsp)
	movq	4240(%rsp), %rbp
.L57:
	movq	%rbp, %rax
	subq	%rsi, %rax
	cmpq	%rax, %r14
	ja	.L107
	testq	%r14, %r14
	jne	.L108
.L61:
	addq	%rsi, %r14
.L60:
	movq	%r14, 4232(%rsp)
	movq	%rbp, %rax
	subq	%r14, %rax
	cmpq	%rax, %r13
	ja	.L109
	testq	%r13, %r13
	jne	.L110
.L65:
	addq	%r14, %r13
.L64:
	movq	%r13, 4232(%rsp)
	movq	%rbp, %rax
	subq	%r13, %rax
	cmpq	%rax, %rbx
	ja	.L111
	testq	%rbx, %rbx
	jne	.L112
.L69:
	addq	%r13, %rbx
.L68:
	movq	%rbx, 4232(%rsp)
	cmpq	%rbx, %rbp
	je	.L113
	movb	$10, (%rbx)
	addq	$1, %rbx
	movq	%rbx, 4232(%rsp)
	movq	4224(%rsp), %r13
.L72:
	subq	%r13, %rbx
	movq	(%r12), %rcx
	leaq	(%rcx,%rbx), %rsi
	movslq	36(%r12), %rax
	addq	16(%r12), %rax
	cmpq	%rax, %rsi
	jnb	.L73
	testq	%rbx, %rbx
	jne	.L114
	xorl	%eax, %eax
.L74:
	orl	$65536, 24(%r12)
	subl	%eax, 8(%r12)
	movq	%rsi, (%r12)
.L75:
	cmpq	%r15, %r13
	je	.L36
	movq	4240(%rsp), %rdx
	subq	%r13, %rdx
	movq	%r13, %rcx
	call	_ZdlPvy
	nop
.L36:
	addq	$4264, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L99:
	movq	%r9, 88(%rsp)
	movq	%r10, %r8
	movq	%r10, 80(%rsp)
	movq	%r11, %rdx
	movq	%r15, %rcx
	call	memcpy
	movq	4240(%rsp), %rdi
	movq	80(%rsp), %r10
	movq	88(%rsp), %r9
	jmp	.L41
.L105:
	movq	%rsi, %r8
	movq	48(%rsp), %rdx
	movq	%r9, %rcx
	call	memcpy
	movq	%rax, %r9
	movq	4240(%rsp), %rdi
	jmp	.L54
.L103:
	movq	%r9, %r8
	movq	%r9, 32(%rsp)
	movq	40(%rsp), %rdx
	movq	%rbp, %rcx
	call	memcpy
	movq	4240(%rsp), %rdi
	movq	32(%rsp), %r9
	jmp	.L50
.L101:
	movq	%r9, 80(%rsp)
	movq	%rbp, %r8
	movq	32(%rsp), %rdx
	call	memcpy
	movq	%rax, %rcx
	movq	4240(%rsp), %rdi
	movq	80(%rsp), %r9
	jmp	.L46
.L114:
	movq	%rbx, %r8
	movq	%r13, %rdx
	call	memcpy
	movq	%rsi, %rax
	subq	(%r12), %rax
	movq	4224(%rsp), %r13
	jmp	.L74
.L112:
	movq	%rbx, %r8
	movq	72(%rsp), %rdx
	movq	%r13, %rcx
	call	memcpy
	movq	4240(%rsp), %rbp
	jmp	.L69
.L110:
	movq	%r13, %r8
	movq	64(%rsp), %rdx
	movq	%r14, %rcx
	call	memcpy
	movq	4240(%rsp), %rbp
	jmp	.L65
.L108:
	movq	%r14, %r8
	movq	56(%rsp), %rdx
	movq	%rsi, %rcx
	call	memcpy
	movq	4240(%rsp), %rbp
	jmp	.L61
.L111:
	subq	4224(%rsp), %rbp
	addq	%rbp, %rbp
	cmpq	%rbx, %rbp
	cmovb	%rbx, %rbp
	testq	%rbp, %rbp
	js	.L43
	movq	%rbp, %rcx
	call	_Znwy
	movq	%rax, %rdi
	movq	4224(%rsp), %r13
	movq	4232(%rsp), %rsi
	subq	%r13, %rsi
	movq	%rsi, %r8
	movq	%r13, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%r15, %r13
	je	.L67
	movq	4240(%rsp), %rdx
	subq	%r13, %rdx
	movq	%r13, %rcx
	call	_ZdlPvy
.L67:
	movq	%rdi, 4224(%rsp)
	addq	%rdi, %rsi
	addq	%rdi, %rbp
	movq	%rbp, 4240(%rsp)
	movq	%rbx, %r8
	movq	72(%rsp), %rdx
	movq	%rsi, %rcx
	call	memcpy
	addq	%rsi, %rbx
	jmp	.L68
.L100:
	movq	%r9, 96(%rsp)
	subq	4224(%rsp), %rdi
	addq	%rdi, %rdi
	cmpq	%rbp, %rdi
	cmovb	%rbp, %rdi
	testq	%rdi, %rdi
	js	.L43
	movq	%rdi, %rcx
	call	_Znwy
	movq	4224(%rsp), %r11
	movq	4232(%rsp), %r8
	subq	%r11, %r8
	movq	%r8, 88(%rsp)
	movq	%r11, %rdx
	movq	%r11, 80(%rsp)
	movq	%rax, %rcx
	call	memcpy
	movq	%rax, %r10
	movq	80(%rsp), %r11
	cmpq	%r15, %r11
	movq	88(%rsp), %r8
	movq	96(%rsp), %r9
	je	.L44
	movq	%rax, 88(%rsp)
	movq	%r8, 80(%rsp)
	movq	4240(%rsp), %rdx
	subq	%r11, %rdx
	movq	%r11, %rcx
	call	_ZdlPvy
	movq	96(%rsp), %r9
	movq	88(%rsp), %r10
	movq	80(%rsp), %r8
.L44:
	movq	%r9, 80(%rsp)
	movq	%r10, 4224(%rsp)
	leaq	(%r10,%r8), %rcx
	addq	%r10, %rdi
	movq	%rdi, 4240(%rsp)
	movq	%rbp, %r8
	movq	32(%rsp), %rdx
	call	memcpy
	addq	%rax, %rbp
	movq	80(%rsp), %r9
	jmp	.L45
.L102:
	subq	4224(%rsp), %rdi
	addq	%rdi, %rdi
	cmpq	%r9, %rdi
	cmovb	%r9, %rdi
	movq	%r9, 88(%rsp)
	testq	%rdi, %rdi
	js	.L43
	movq	%rdi, %rcx
	call	_Znwy
	movq	%rax, %rbp
	movq	4224(%rsp), %r10
	movq	4232(%rsp), %r8
	subq	%r10, %r8
	movq	%r8, 80(%rsp)
	movq	%r10, %rdx
	movq	%r10, 32(%rsp)
	movq	%rax, %rcx
	call	memcpy
	movq	32(%rsp), %r10
	cmpq	%r15, %r10
	movq	80(%rsp), %r8
	movq	88(%rsp), %r9
	je	.L48
	movq	%r9, 80(%rsp)
	movq	%r8, 32(%rsp)
	movq	4240(%rsp), %rdx
	subq	%r10, %rdx
	movq	%r10, %rcx
	call	_ZdlPvy
	movq	80(%rsp), %r9
	movq	32(%rsp), %r8
.L48:
	movq	%rbp, 4224(%rsp)
	leaq	0(%rbp,%r8), %rcx
	addq	%rbp, %rdi
	movq	%rdi, 4240(%rsp)
	movq	%r9, %r8
	movq	%r9, 32(%rsp)
	movq	40(%rsp), %rdx
	call	memcpy
	movq	32(%rsp), %r9
	addq	%rax, %r9
	jmp	.L49
.L113:
	subq	4224(%rsp), %rbp
	addq	%rbp, %rbp
	js	.L43
	movq	%rbp, %rcx
	call	_Znwy
	movq	%rax, %r13
	movq	4224(%rsp), %r14
	movq	4232(%rsp), %rbx
	subq	%r14, %rbx
	movq	%rbx, %r8
	movq	%r14, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%r15, %r14
	je	.L71
	movq	4240(%rsp), %rdx
	subq	%r14, %rdx
	movq	%r14, %rcx
	call	_ZdlPvy
.L71:
	movq	%r13, 4224(%rsp)
	addq	%r13, %rbx
	addq	%r13, %rbp
	movq	%rbp, 4240(%rsp)
	movb	$10, (%rbx)
	addq	$1, %rbx
	movq	%rbx, 4232(%rsp)
	jmp	.L72
.L73:
	movq	%r12, %rcx
	call	clearerr
	movq	%r12, %r9
	movq	%rbx, %r8
	movl	$1, %edx
	movq	%r13, %rcx
	call	fwrite
	movq	%r12, %rcx
	call	ferror
	movl	%eax, %ebx
	call	*__imp__errno(%rip)
	movq	%r12, %rcx
	call	clearerr
	testl	%ebx, %ebx
	jne	.L115
	movq	4224(%rsp), %r13
	jmp	.L75
.L106:
	subq	4224(%rsp), %rsi
	addq	%rsi, %rsi
	js	.L43
	movq	%rsi, %rcx
	call	_Znwy
	movq	%rax, %rbp
	movq	4224(%rsp), %r9
	movq	4232(%rsp), %rdi
	subq	%r9, %rdi
	movq	%rdi, %r8
	movq	%r9, %rdx
	movq	%r9, 32(%rsp)
	movq	%rax, %rcx
	call	memcpy
	movq	32(%rsp), %r9
	cmpq	%r15, %r9
	je	.L56
	movq	4240(%rsp), %rdx
	subq	%r9, %rdx
	movq	%r9, %rcx
	call	_ZdlPvy
.L56:
	movq	%rbp, 4224(%rsp)
	addq	%rbp, %rdi
	addq	%rsi, %rbp
	movq	%rbp, 4240(%rsp)
	movzbl	4376(%rsp), %eax
	movb	%al, (%rdi)
	leaq	1(%rdi), %rsi
	movq	%rsi, 4232(%rsp)
	jmp	.L57
.L107:
	subq	4224(%rsp), %rbp
	addq	%rbp, %rbp
	cmpq	%r14, %rbp
	cmovb	%r14, %rbp
	testq	%rbp, %rbp
	js	.L43
	movq	%rbp, %rcx
	call	_Znwy
	movq	%rax, %rsi
	movq	4224(%rsp), %rdi
	movq	4232(%rsp), %r8
	subq	%rdi, %r8
	movq	%r8, 32(%rsp)
	movq	%rdi, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%r15, %rdi
	movq	32(%rsp), %r8
	je	.L59
	movq	4240(%rsp), %rdx
	subq	%rdi, %rdx
	movq	%rdi, %rcx
	call	_ZdlPvy
	movq	32(%rsp), %r8
.L59:
	movq	%rsi, 4224(%rsp)
	leaq	(%rsi,%r8), %rcx
	addq	%rsi, %rbp
	movq	%rbp, 4240(%rsp)
	movq	%r14, %r8
	movq	56(%rsp), %rdx
	call	memcpy
	addq	%rax, %r14
	jmp	.L60
.L104:
	subq	4224(%rsp), %rdi
	addq	%rdi, %rdi
	cmpq	%rsi, %rdi
	cmovb	%rsi, %rdi
	testq	%rdi, %rdi
	js	.L43
	movq	%rdi, %rcx
	call	_Znwy
	movq	%rax, %rbp
	movq	4224(%rsp), %r9
	movq	4232(%rsp), %r8
	subq	%r9, %r8
	movq	%r8, 40(%rsp)
	movq	%r9, %rdx
	movq	%r9, 32(%rsp)
	movq	%rax, %rcx
	call	memcpy
	movq	32(%rsp), %r9
	cmpq	%r15, %r9
	movq	40(%rsp), %r8
	je	.L52
	movq	%r8, 32(%rsp)
	movq	4240(%rsp), %rdx
	subq	%r9, %rdx
	movq	%r9, %rcx
	call	_ZdlPvy
	movq	32(%rsp), %r8
.L52:
	movq	%rbp, 4224(%rsp)
	leaq	0(%rbp,%r8), %rcx
	addq	%rbp, %rdi
	movq	%rdi, 4240(%rsp)
	movq	%rsi, %r8
	movq	48(%rsp), %rdx
	call	memcpy
	addq	%rax, %rsi
	jmp	.L53
.L98:
	cmpq	$8192, %r10
	movl	$8192, %eax
	cmovnb	%r10, %rax
	movq	%rax, 80(%rsp)
	testq	%rax, %rax
	js	.L43
	movq	%rax, %rcx
	movq	%r9, 120(%rsp)
	movq	%r10, 112(%rsp)
	movq	%r11, 104(%rsp)
	call	_Znwy
	movq	%rax, %rdi
	movq	4224(%rsp), %rax
	movq	4232(%rsp), %r8
	subq	%rax, %r8
	movq	%r8, 96(%rsp)
	movq	%rax, 88(%rsp)
	movq	%rax, %rdx
	movq	%rdi, %rcx
	call	memcpy
	movq	88(%rsp), %rax
	cmpq	%r15, %rax
	movq	96(%rsp), %r8
	movq	104(%rsp), %r11
	movq	112(%rsp), %r10
	movq	120(%rsp), %r9
	je	.L39
	movq	%r9, 112(%rsp)
	movq	%r10, 104(%rsp)
	movq	%r11, 96(%rsp)
	movq	%r8, 88(%rsp)
	movq	4240(%rsp), %rdx
	subq	%rax, %rdx
	movq	%rax, %rcx
	call	_ZdlPvy
	movq	112(%rsp), %r9
	movq	104(%rsp), %r10
	movq	96(%rsp), %r11
	movq	88(%rsp), %r8
.L39:
	movq	%r9, 88(%rsp)
	movq	%rdi, 4224(%rsp)
	leaq	(%rdi,%r8), %rcx
	addq	80(%rsp), %rdi
	movq	%rdi, 4240(%rsp)
	movq	%r10, %r8
	movq	%r10, 80(%rsp)
	movq	%r11, %rdx
	call	memcpy
	movq	%rax, %rcx
	movq	80(%rsp), %r10
	addq	%r10, %rcx
	movq	88(%rsp), %r9
	jmp	.L40
.L109:
	subq	4224(%rsp), %rbp
	addq	%rbp, %rbp
	cmpq	%r13, %rbp
	cmovb	%r13, %rbp
	testq	%rbp, %rbp
	js	.L43
	movq	%rbp, %rcx
	call	_Znwy
	movq	%rax, %rdi
	movq	4224(%rsp), %r14
	movq	4232(%rsp), %rsi
	subq	%r14, %rsi
	movq	%rsi, %r8
	movq	%r14, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%r15, %r14
	je	.L63
	movq	4240(%rsp), %rdx
	subq	%r14, %rdx
	movq	%r14, %rcx
	call	_ZdlPvy
.L63:
	movq	%rdi, 4224(%rsp)
	leaq	(%rdi,%rsi), %rcx
	addq	%rdi, %rbp
	movq	%rbp, 4240(%rsp)
	movq	%r13, %r8
	movq	64(%rsp), %rdx
	call	memcpy
	addq	%rax, %r13
	jmp	.L64
.L43:
	call	_ZSt17__throw_bad_allocv
.L115:
	ud2
	.seh_endproc
	.def	__main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
.LC2:
	.ascii "okokokok\0"
.LC3:
	.ascii "concat:\0"
.LC4:
	.ascii "\12concatln:\0"
.LC5:
	.ascii "qwr\12\0"
	.section	.text.unlikely,"x"
.LCOLDB6:
	.section	.text.startup,"x"
.LHOTB6:
	.globl	main
	.def	main;	.scl	2;	.type	32;	.endef
	.seh_proc	main
main:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$376, %rsp
	.seh_stackalloc	376
	.seh_endprologue
	call	__main
	leaq	272(%rsp), %rcx
	leaq	288(%rsp), %rbx
	movq	%rbx, 272(%rsp)
	movw	$27503, 288(%rsp)
	movq	$2, 280(%rsp)
	movb	$0, 290(%rsp)
	leaq	8+.LC2(%rip), %r8
	leaq	-8(%r8), %rdx
	call	_ZN7fast_io5writeIcSt11char_traitsIcESaIcEPKcEEvNS_11ostring_refIT_T0_T1_EET2_SB_
	leaq	320(%rsp), %rax
	movq	%rax, 80(%rsp)
	movq	%rax, 304(%rsp)
	movq	272(%rsp), %r12
	cmpq	%rbx, %r12
	je	.L159
	movq	%r12, 304(%rsp)
	movq	288(%rsp), %rax
	movq	%rax, 320(%rsp)
.L118:
	movq	280(%rsp), %rsi
	movq	%rsi, 312(%rsp)
	movq	%rbx, 272(%rsp)
	movq	$0, 280(%rsp)
	movb	$0, 288(%rsp)
	leaq	352(%rsp), %r13
	movw	$27503, 352(%rsp)
	movb	$0, 354(%rsp)
	leaq	256(%rsp), %rbp
	movq	%rbp, 240(%rsp)
	movq	352(%rsp), %rax
	movq	360(%rsp), %rdx
	movq	%rax, 256(%rsp)
	movq	%rdx, 264(%rsp)
	movq	$2, 248(%rsp)
	movq	%r13, 336(%rsp)
	movq	$1, 344(%rsp)
	movw	$10, 352(%rsp)
	leaq	224(%rsp), %r14
	movq	%r14, 208(%rsp)
	movq	$0, 216(%rsp)
	movb	$0, 224(%rsp)
	movl	$1, %ecx
	call	*__imp___acrt_iob_func(%rip)
	movq	%rax, %rdi
	movq	__imp___iob_func(%rip), %r15
	call	*%r15
	movq	%rax, %r8
	movq	%rdi, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	jbe	.L160
	leaq	48(%rdi), %rcx
	call	EnterCriticalSection
.L120:
	leaq	.LC3(%rip), %r11
	movq	16(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L161
	movq	(%rdi), %rcx
	leaq	7(%rcx), %rax
	movslq	36(%rdi), %r8
	addq	%r8, %rdx
	cmpq	%rdx, %rax
	jnb	.L123
	movl	$1668181859, (%rcx)
	movw	$29793, 4(%rcx)
	movb	$58, 6(%rcx)
	orl	$65536, 24(%rdi)
	movq	%rax, %rdx
	subq	(%rdi), %rdx
	subl	%edx, 8(%rdi)
	movq	%rax, (%rdi)
.L124:
	movslq	36(%rdi), %rcx
	addq	16(%rdi), %rcx
	cmpq	%rcx, %rax
	jnb	.L126
	orl	$65536, 24(%rdi)
.L127:
	leaq	10(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L129
	movabsq	$7814978333077103370, %rcx
	movq	%rcx, (%rax)
	movw	$14958, 8(%rax)
	orl	$65536, 24(%rdi)
	movq	%rdx, %rax
	subq	(%rdi), %rax
	subl	%eax, 8(%rdi)
	movq	%rdx, (%rdi)
.L130:
	leaq	1(%rdx), %rax
	movslq	36(%rdi), %rcx
	addq	16(%rdi), %rcx
	cmpq	%rcx, %rax
	jnb	.L131
	movzbl	352(%rsp), %ecx
	movb	%cl, (%rdx)
	orl	$65536, 24(%rdi)
	movq	%rax, %rdx
	subq	(%rdi), %rdx
	subl	%edx, 8(%rdi)
	movq	%rax, (%rdi)
.L132:
	movslq	36(%rdi), %rdx
	addq	16(%rdi), %rdx
	cmpq	%rdx, %rax
	je	.L162
	movb	$10, (%rax)
	addq	$1, %rax
	orl	$65536, 24(%rdi)
	movq	%rax, %rdx
	subq	(%rdi), %rdx
	subl	%edx, 8(%rdi)
	movq	%rax, (%rdi)
.L134:
	leaq	2(%rax), %rdx
	movslq	36(%rdi), %rcx
	addq	16(%rdi), %rcx
	cmpq	%rcx, %rdx
	jnb	.L135
	movzwl	256(%rsp), %ecx
	movw	%cx, (%rax)
	orl	$65536, 24(%rdi)
	movq	%rdx, %rax
	subq	(%rdi), %rax
	subl	%eax, 8(%rdi)
	movq	%rdx, (%rdi)
.L136:
	leaq	4(%rdx), %rcx
	movslq	36(%rdi), %rax
	addq	16(%rdi), %rax
	cmpq	%rax, %rcx
	jnb	.L137
	movl	$175273841, (%rdx)
	orl	$65536, 24(%rdi)
	movq	%rcx, %rax
	subq	(%rdi), %rax
	subl	%eax, 8(%rdi)
	movq	%rcx, (%rdi)
.L138:
	leaq	(%rcx,%rsi), %r9
	movslq	36(%rdi), %r8
	movq	%r8, %rdx
	movq	16(%rdi), %rax
	addq	%rax, %r8
	cmpq	%r8, %r9
	jnb	.L139
	testq	%rsi, %rsi
	jne	.L163
.L140:
	orl	$65536, 24(%rdi)
	movq	%r9, %rcx
	subq	(%rdi), %rcx
	subl	%ecx, 8(%rdi)
	movq	%r9, (%rdi)
.L141:
	addq	%rdx, %rax
	cmpq	%rax, %r9
	je	.L164
	movb	$10, (%r9)
	addq	$1, %r9
	orl	$65536, 24(%rdi)
	movq	%r9, %rax
	subq	(%rdi), %rax
	subl	%eax, 8(%rdi)
	movq	%r9, (%rdi)
.L122:
	call	*%r15
	movq	%rax, %r8
	movq	%rdi, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L143
	andl	$-32769, 24(%rdi)
	sarq	$4, %rax
	imull	$-1431655765, %eax, %eax
	leal	16(%rax), %ecx
	call	_unlock
.L144:
	movq	208(%rsp), %rcx
	cmpq	%r14, %rcx
	je	.L145
	movq	224(%rsp), %rax
	leaq	1(%rax), %rdx
	call	_ZdlPvy
.L145:
	movq	336(%rsp), %rcx
	cmpq	%r13, %rcx
	je	.L146
	movq	352(%rsp), %rax
	leaq	1(%rax), %rdx
	call	_ZdlPvy
.L146:
	movq	240(%rsp), %rcx
	cmpq	%rbp, %rcx
	je	.L147
	movq	256(%rsp), %rax
	leaq	1(%rax), %rdx
	call	_ZdlPvy
.L147:
	movq	304(%rsp), %rcx
	cmpq	80(%rsp), %rcx
	je	.L148
	movq	320(%rsp), %rax
	leaq	1(%rax), %rdx
	call	_ZdlPvy
.L148:
	movq	272(%rsp), %rcx
	cmpq	%rbx, %rcx
	je	.L149
	movq	288(%rsp), %rax
	leaq	1(%rax), %rdx
	call	_ZdlPvy
.L149:
	xorl	%eax, %eax
	addq	$376, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L160:
	sarq	$4, %rax
	imull	$-1431655765, %eax, %eax
	leal	16(%rax), %ecx
	call	_lock
	orl	$32768, 24(%rdi)
	jmp	.L120
.L143:
	leaq	48(%rdi), %rcx
	call	LeaveCriticalSection
	jmp	.L144
.L159:
	movq	288(%rsp), %rax
	movq	296(%rsp), %rdx
	movq	%rax, 320(%rsp)
	movq	%rdx, 328(%rsp)
	movq	80(%rsp), %r12
	jmp	.L118
.L163:
	movq	%r9, 88(%rsp)
	movq	%rsi, %r8
	movq	%r12, %rdx
	call	memcpy
	movq	16(%rdi), %rax
	movslq	36(%rdi), %rdx
	movq	88(%rsp), %r9
	jmp	.L140
.L161:
	movq	%r11, 192(%rsp)
	movq	$7, 200(%rsp)
	movq	%r14, 176(%rsp)
	movq	$0, 184(%rsp)
	leaq	.LC4(%rip), %rax
	movq	%rax, 160(%rsp)
	movq	$10, 168(%rsp)
	movq	%r13, 144(%rsp)
	movq	$1, 152(%rsp)
	movq	%rbp, 128(%rsp)
	movq	$2, 136(%rsp)
	leaq	.LC5(%rip), %rax
	movq	%rax, 112(%rsp)
	movq	$4, 120(%rsp)
	movq	%r12, 96(%rsp)
	movq	%rsi, 104(%rsp)
	leaq	192(%rsp), %rdx
	leaq	96(%rsp), %rax
	movq	%rax, 64(%rsp)
	leaq	112(%rsp), %rax
	movq	%rax, 56(%rsp)
	leaq	128(%rsp), %rax
	movq	%rax, 48(%rsp)
	movb	$10, 40(%rsp)
	leaq	144(%rsp), %rax
	movq	%rax, 32(%rsp)
	leaq	160(%rsp), %r9
	leaq	176(%rsp), %r8
	movq	%rdi, %rcx
	call	_ZN7fast_io7details5decay14print_fallbackILb1ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_S8_S8_NS5_ILb1ENS_5manip4chvwIcEEEES8_S8_S8_EEEvT0_DpT1_
	jmp	.L122
.L164:
	orl	$65536, 24(%rdi)
	movq	%rdi, %rdx
	movl	$10, %ecx
	call	*__imp__flsbuf(%rip)
	addl	$1, %eax
	jne	.L122
	jmp	.L128
.L139:
	movq	%rdi, %rcx
	.p2align 4,,5
	call	clearerr
	movq	%rdi, %r9
	movq	%rsi, %r8
	movl	$1, %edx
	movq	%r12, %rcx
	call	fwrite
	movq	%rdi, %rcx
	call	ferror
	movl	%eax, %esi
	call	*__imp__errno(%rip)
	movq	%rdi, %rcx
	call	clearerr
	testl	%esi, %esi
	jne	.L128
	movq	(%rdi), %r9
	movq	16(%rdi), %rax
	movslq	36(%rdi), %rdx
	jmp	.L141
.L137:
	movq	%rdi, %rcx
	call	clearerr
	movq	%rdi, %r9
	movl	$4, %r8d
	movl	$1, %edx
	leaq	.LC5(%rip), %rcx
	call	fwrite
	movq	%rdi, %rcx
	call	ferror
	movl	%eax, 88(%rsp)
	call	*__imp__errno(%rip)
	movq	%rdi, %rcx
	call	clearerr
	movl	88(%rsp), %edx
	testl	%edx, %edx
	jne	.L128
	movq	(%rdi), %rcx
	jmp	.L138
.L135:
	movq	%rdi, %rcx
	call	clearerr
	movq	%rdi, %r9
	movl	$2, %r8d
	movl	$1, %edx
	movq	%rbp, %rcx
	call	fwrite
	movq	%rdi, %rcx
	call	ferror
	movl	%eax, 88(%rsp)
	call	*__imp__errno(%rip)
	movq	%rdi, %rcx
	call	clearerr
	movl	88(%rsp), %edx
	testl	%edx, %edx
	jne	.L128
	movq	(%rdi), %rdx
	jmp	.L136
.L162:
	orl	$65536, 24(%rdi)
	movq	%rdi, %rdx
	movl	$10, %ecx
	call	*__imp__flsbuf(%rip)
	addl	$1, %eax
	je	.L128
	movq	(%rdi), %rax
	jmp	.L134
.L131:
	movq	%rdi, %rcx
	call	clearerr
	movq	%rdi, %r9
	movl	$1, %r8d
	movl	$1, %edx
	movq	%r13, %rcx
	call	fwrite
	movq	%rdi, %rcx
	call	ferror
	movl	%eax, 88(%rsp)
	call	*__imp__errno(%rip)
	movq	%rdi, %rcx
	call	clearerr
	movl	88(%rsp), %edx
	testl	%edx, %edx
	jne	.L128
	movq	(%rdi), %rax
	jmp	.L132
.L129:
	movq	%rdi, %rcx
	call	clearerr
	movq	%rdi, %r9
	movl	$10, %r8d
	movl	$1, %edx
	leaq	.LC4(%rip), %rcx
	call	fwrite
	movq	%rdi, %rcx
	call	ferror
	movl	%eax, 88(%rsp)
	call	*__imp__errno(%rip)
	movq	%rdi, %rcx
	call	clearerr
	movl	88(%rsp), %edx
	testl	%edx, %edx
	jne	.L128
	movq	(%rdi), %rdx
	jmp	.L130
.L126:
	movq	%rdi, %rcx
	call	clearerr
	movq	%rdi, %r9
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r14, %rcx
	call	fwrite
	movq	%rdi, %rcx
	call	ferror
	movl	%eax, 88(%rsp)
	call	*__imp__errno(%rip)
	movq	%rdi, %rcx
	call	clearerr
	movl	88(%rsp), %edx
	testl	%edx, %edx
	jne	.L128
	movq	(%rdi), %rax
	movslq	36(%rdi), %rcx
	addq	16(%rdi), %rcx
	jmp	.L127
.L123:
	movq	%rdi, %rcx
	call	clearerr
	movq	%rdi, %r9
	movl	$7, %r8d
	movl	$1, %edx
	leaq	.LC3(%rip), %r11
	movq	%r11, %rcx
	call	fwrite
	movq	%rdi, %rcx
	call	ferror
	movl	%eax, 88(%rsp)
	call	*__imp__errno(%rip)
	movq	%rdi, %rcx
	call	clearerr
	movl	88(%rsp), %edx
	testl	%edx, %edx
	jne	.L128
	movq	(%rdi), %rax
	jmp	.L124
	.seh_endproc
	.section	.text.unlikely,"x"
	.def	main.cold;	.scl	3;	.type	32;	.endef
	.seh_proc	main.cold
	.seh_stackalloc	440
	.seh_savereg	%rbx, 376
	.seh_savereg	%rsi, 384
	.seh_savereg	%rdi, 392
	.seh_savereg	%rbp, 400
	.seh_savereg	%r12, 408
	.seh_savereg	%r13, 416
	.seh_savereg	%r14, 424
	.seh_savereg	%r15, 432
	.seh_endprologue
main.cold:
.L128:
	ud2
	.section	.text.startup,"x"
	.section	.text.unlikely,"x"
	.seh_endproc
.LCOLDE6:
	.section	.text.startup,"x"
.LHOTE6:
	.ident	"GCC: (GCC with MCF thread model, built by LH_Mouse.) 11.0.0 20200712 (experimental)"
	.def	memcpy;	.scl	2;	.type	32;	.endef
	.def	_Znwy;	.scl	2;	.type	32;	.endef
	.def	_ZdlPvy;	.scl	2;	.type	32;	.endef
	.def	_ZSt20__throw_length_errorPKc;	.scl	2;	.type	32;	.endef
	.def	_ZSt17__throw_bad_allocv;	.scl	2;	.type	32;	.endef
	.def	clearerr;	.scl	2;	.type	32;	.endef
	.def	fwrite;	.scl	2;	.type	32;	.endef
	.def	ferror;	.scl	2;	.type	32;	.endef
	.def	EnterCriticalSection;	.scl	2;	.type	32;	.endef
	.def	_unlock;	.scl	2;	.type	32;	.endef
	.def	_lock;	.scl	2;	.type	32;	.endef
	.def	LeaveCriticalSection;	.scl	2;	.type	32;	.endef
