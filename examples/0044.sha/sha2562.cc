#include"../../include/fast_io.h"
#include"../../include/fast_io_device.h"
#include"../../include/fast_io_crypto.h"

int main(int argc,char** argv)
{
	fast_io::sha256 sha;
	fast_io::hash_processor processor(sha);
	static_assert(fast_io::scatter_output_stream<decltype(processor)>);
	print(processor,"a","b");
}