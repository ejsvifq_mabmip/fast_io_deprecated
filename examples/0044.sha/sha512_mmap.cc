#include"../../include/fast_io.h"
#include"../../include/fast_io_device.h"
#include"../../include/fast_io_crypto.h"

int main(int argc,char** argv)
try
{
	if(argc!=2)
	{
		perr("Usage: ",fast_io::chvw(*argv)," <file>\n");
		return 1;
	}
	auto t0{std::chrono::high_resolution_clock::now()};
	fast_io::inative_file nfl(argv[1]);
	std::size_t file_size{size(nfl)};
	fast_io::native_memory_map_file mpf(nfl,fast_io::file_map_attribute::read_only,file_size,0);
	fast_io::sha512 sha;
	fast_io::hash_processor processor(sha);
	write(processor,reinterpret_cast<std::byte*>(mpf.address_begin),reinterpret_cast<std::byte*>(mpf.address_end));
//	auto transmitted{transmit(processor,ibf)};
	processor.do_final();
	println(sha," *",fast_io::chvw(argv[1]),"\nTransmitted:",file_size,u8" bytes\tElapsed Time:",std::chrono::high_resolution_clock::now()-t0);
}
catch(std::exception const& e)
{
	perrln(e);
	return 2;
}