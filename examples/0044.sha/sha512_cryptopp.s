	.file	"sha512_cryptopp.cc"
	.text
	.section	.text$_ZNK8CryptoPP9Exception4whatEv,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNK8CryptoPP9Exception4whatEv
	.def	_ZNK8CryptoPP9Exception4whatEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK8CryptoPP9Exception4whatEv
_ZNK8CryptoPP9Exception4whatEv:
.LFB15090:
	.seh_endprologue
	movq	16(%rcx), %rax
	ret
	.seh_endproc
	.section	.text$_ZN8CryptoPP18HashTransformation24CalculateTruncatedDigestEPhyPKhy,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN8CryptoPP18HashTransformation24CalculateTruncatedDigestEPhyPKhy
	.def	_ZN8CryptoPP18HashTransformation24CalculateTruncatedDigestEPhyPKhy;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN8CryptoPP18HashTransformation24CalculateTruncatedDigestEPhyPKhy
_ZN8CryptoPP18HashTransformation24CalculateTruncatedDigestEPhyPKhy:
.LFB15221:
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	(%rcx), %rax
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%r8, %rbx
	movq	%r9, %rdx
	movq	96(%rsp), %r8
	call	*40(%rax)
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	(%r12), %rax
	movq	%r12, %rcx
	movq	128(%rax), %rax
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	rex.W jmp	*%rax
	.seh_endproc
	.section	.text$_ZN8CryptoPP18HashTransformation21VerifyTruncatedDigestEPKhyS2_y,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN8CryptoPP18HashTransformation21VerifyTruncatedDigestEPKhyS2_y
	.def	_ZN8CryptoPP18HashTransformation21VerifyTruncatedDigestEPKhyS2_y;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN8CryptoPP18HashTransformation21VerifyTruncatedDigestEPKhyS2_y
_ZN8CryptoPP18HashTransformation21VerifyTruncatedDigestEPKhyS2_y:
.LFB15222:
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	(%rcx), %rax
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%r8, %rbx
	movq	%r9, %rdx
	movq	96(%rsp), %r8
	call	*40(%rax)
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	(%r12), %rax
	movq	%r12, %rcx
	movq	144(%rax), %rax
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	rex.W jmp	*%rax
	.seh_endproc
	.section	.text$_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EED1Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EED1Ev
	.def	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EED1Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EED1Ev
_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EED1Ev:
.LFB16734:
	pushq	%rdi
	.seh_pushreg	%rdi
	.seh_endprologue
	movq	352(%rcx), %rdi
	leaq	16+_ZTVN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EEE(%rip), %rax
	movq	%rax, (%rcx)
	leaq	192(%rcx), %rax
	movq	%rcx, %rdx
	cmpq	%rax, %rdi
	je	.L9
.L6:
	movq	176(%rdx), %rdi
	leaq	16+_ZTVN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEE(%rip), %rax
	movq	%rax, (%rdx)
	leaq	24(%rdx), %rax
	cmpq	%rax, %rdi
	je	.L10
	popq	%rdi
	ret
	.p2align 4
	.p2align 3
.L10:
	movq	168(%rdx), %rax
	movq	160(%rdx), %rcx
	movb	$0, 153(%rdx)
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	xorl	%eax, %eax
/APP
 # 1425 "D:/msys64/mingw64/include/CryptoPP/misc.h" 1
	rep stosq
 # 0 "" 2
/NO_APP
	popq	%rdi
	ret
	.p2align 4
	.p2align 3
.L9:
	movq	336(%rcx), %rcx
	movq	344(%rdx), %rax
	movb	$0, 321(%rdx)
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	xorl	%eax, %eax
/APP
 # 1425 "D:/msys64/mingw64/include/CryptoPP/misc.h" 1
	rep stosq
 # 0 "" 2
/NO_APP
	jmp	.L6
	.seh_endproc
	.section	.text$_ZNK8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE10DigestSizeEv,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNK8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE10DigestSizeEv
	.def	_ZNK8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE10DigestSizeEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE10DigestSizeEv
_ZNK8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE10DigestSizeEv:
.LFB17960:
	.seh_endprologue
	movl	$64, %eax
	ret
	.seh_endproc
	.section	.text$_ZNK8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEE9BlockSizeEv,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNK8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEE9BlockSizeEv
	.def	_ZNK8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEE9BlockSizeEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEE9BlockSizeEv
_ZNK8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEE9BlockSizeEv:
.LFB17961:
	.seh_endprologue
	movl	$128, %eax
	ret
	.seh_endproc
	.section	.text$_ZNK8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE16OptimalBlockSizeEv,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNK8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE16OptimalBlockSizeEv
	.def	_ZNK8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE16OptimalBlockSizeEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE16OptimalBlockSizeEv
_ZNK8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE16OptimalBlockSizeEv:
.LFB17962:
	.seh_endprologue
	movq	(%rcx), %rax
	rex.W jmp	*80(%rax)
	.seh_endproc
	.section	.text$_ZNK8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE20OptimalDataAlignmentEv,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNK8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE20OptimalDataAlignmentEv
	.def	_ZNK8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE20OptimalDataAlignmentEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE20OptimalDataAlignmentEv
_ZNK8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE20OptimalDataAlignmentEv:
.LFB17963:
	.seh_endprologue
	movl	$8, %eax
	ret
	.seh_endproc
	.section	.text$_ZNK8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEE12GetByteOrderEv,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNK8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEE12GetByteOrderEv
	.def	_ZNK8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEE12GetByteOrderEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEE12GetByteOrderEv
_ZNK8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEE12GetByteOrderEv:
.LFB17964:
	.seh_endprologue
	movl	$1, %eax
	ret
	.seh_endproc
	.section	.text$_ZN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEE7DataBufEv,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEE7DataBufEv
	.def	_ZN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEE7DataBufEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEE7DataBufEv
_ZN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEE7DataBufEv:
.LFB17966:
	.seh_endprologue
	movq	176(%rcx), %rax
	ret
	.seh_endproc
	.section	.text$_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE8StateBufEv,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE8StateBufEv
	.def	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE8StateBufEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE8StateBufEv
_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE8StateBufEv:
.LFB17967:
	.seh_endprologue
	movq	352(%rcx), %rax
	ret
	.seh_endproc
	.section	.text$_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy
	.def	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy
_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy:
.LFB17980:
	.seh_endprologue
	movq	8(%rcx), %rcx
	movq	4104(%rcx), %rax
	addq	%rax, %rdx
	cmpq	%rdx, 4112(%rcx)
	movl	$0, %edx
	cmovbe	%rdx, %rax
	ret
	.seh_endproc
	.section	.text$_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc
	.def	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc
_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc:
.LFB17982:
	.seh_endprologue
	movq	8(%rcx), %rax
	movq	%rdx, 4104(%rax)
	ret
	.seh_endproc
	.section	.text$_ZN7fast_io18fast_io_text_errorD0Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN7fast_io18fast_io_text_errorD0Ev
	.def	_ZN7fast_io18fast_io_text_errorD0Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io18fast_io_text_errorD0Ev
_ZN7fast_io18fast_io_text_errorD0Ev:
.LFB17886:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	leaq	16+_ZTVN7fast_io13fast_io_errorE(%rip), %rax
	movq	%rcx, %r12
	movq	%rax, (%rcx)
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L22
	call	_ZdaPv
.L22:
	movq	%r12, %rcx
	call	_ZNSt9exceptionD2Ev
	movl	$32, %edx
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZdlPvy
	.seh_endproc
	.section	.text$_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE24HashEndianCorrectedBlockEPKy,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE24HashEndianCorrectedBlockEPKy
	.def	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE24HashEndianCorrectedBlockEPKy;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE24HashEndianCorrectedBlockEPKy
_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE24HashEndianCorrectedBlockEPKy:
.LFB17965:
	.seh_endprologue
	movq	352(%rcx), %rcx
	jmp	_ZN8CryptoPP6SHA5129TransformEPyPKy
	.seh_endproc
	.section	.text$_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE4InitEv,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE4InitEv
	.def	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE4InitEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE4InitEv
_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE4InitEv:
.LFB17113:
	.seh_endprologue
	movq	352(%rcx), %rcx
	jmp	_ZN8CryptoPP6SHA5129InitStateEPy
	.seh_endproc
	.section .rdata,"dr"
.LC0:
	.ascii "illegal utf8\0"
	.section	.text.unlikely,"x"
	.def	_ZN7fast_io7details3utf24utf_code_convert_detailsILb0EPKcPwEET1_RT0_S7_S6_.part.0;	.scl	3;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details3utf24utf_code_convert_detailsILb0EPKcPwEET1_RT0_S7_S6_.part.0
_ZN7fast_io7details3utf24utf_code_convert_detailsILb0EPKcPwEET1_RT0_S7_S6_.part.0:
.LFB17999:
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	movl	$32, %ecx
	call	__cxa_allocate_exception
	leaq	_ZN7fast_io18fast_io_text_errorD1Ev(%rip), %r8
	leaq	_ZTIN7fast_io18fast_io_text_errorE(%rip), %rdx
	movq	%rax, %rcx
	movq	$0, 8(%rax)
	leaq	16+_ZTVN7fast_io18fast_io_text_errorE(%rip), %rax
	movq	%rax, (%rcx)
	leaq	.LC0(%rip), %rax
	movq	$12, 16(%rcx)
	movq	%rax, 24(%rcx)
	call	__cxa_throw
	nop
	.seh_endproc
	.section	.text$_ZN8CryptoPP18HashTransformation5FinalEPh,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN8CryptoPP18HashTransformation5FinalEPh
	.def	_ZN8CryptoPP18HashTransformation5FinalEPh;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN8CryptoPP18HashTransformation5FinalEPh
_ZN8CryptoPP18HashTransformation5FinalEPh:
.LFB15213:
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$48, %rsp
	.seh_stackalloc	48
	.seh_endprologue
	movq	(%rcx), %rax
	leaq	_ZNK8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE10DigestSizeEv(%rip), %r9
	movl	$64, %r8d
	movq	128(%rax), %rbx
	movq	72(%rax), %rax
	cmpq	%r9, %rax
	jne	.L32
	movq	%rbx, %rax
	addq	$48, %rsp
	popq	%rbx
	rex.W jmp	*%rax
	.p2align 4
	.p2align 3
.L32:
	movq	%rdx, 40(%rsp)
	movq	%rcx, 32(%rsp)
	call	*%rax
	movq	40(%rsp), %rdx
	movl	%eax, %r8d
	movq	%rbx, %rax
	movq	32(%rsp), %rcx
	addq	$48, %rsp
	popq	%rbx
	rex.W jmp	*%rax
	.seh_endproc
	.section	.text$_ZN8CryptoPP18HashTransformation6VerifyEPKh,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN8CryptoPP18HashTransformation6VerifyEPKh
	.def	_ZN8CryptoPP18HashTransformation6VerifyEPKh;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN8CryptoPP18HashTransformation6VerifyEPKh
_ZN8CryptoPP18HashTransformation6VerifyEPKh:
.LFB15219:
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$48, %rsp
	.seh_stackalloc	48
	.seh_endprologue
	movq	(%rcx), %rax
	leaq	_ZNK8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE10DigestSizeEv(%rip), %r9
	movl	$64, %r8d
	movq	144(%rax), %rbx
	movq	72(%rax), %rax
	cmpq	%r9, %rax
	jne	.L36
	movq	%rbx, %rax
	addq	$48, %rsp
	popq	%rbx
	rex.W jmp	*%rax
	.p2align 4
	.p2align 3
.L36:
	movq	%rdx, 40(%rsp)
	movq	%rcx, 32(%rsp)
	call	*%rax
	movq	40(%rsp), %rdx
	movl	%eax, %r8d
	movq	%rbx, %rax
	movq	32(%rsp), %rcx
	addq	$48, %rsp
	popq	%rbx
	rex.W jmp	*%rax
	.seh_endproc
	.section	.text$_ZN8CryptoPP15InvalidArgumentD0Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN8CryptoPP15InvalidArgumentD0Ev
	.def	_ZN8CryptoPP15InvalidArgumentD0Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN8CryptoPP15InvalidArgumentD0Ev
_ZN8CryptoPP15InvalidArgumentD0Ev:
.LFB15139:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	leaq	16+_ZTVN8CryptoPP9ExceptionE(%rip), %rax
	movq	%rcx, %r12
	movq	%rax, (%rcx)
	movq	16(%rcx), %rcx
	leaq	32(%r12), %rax
	cmpq	%rax, %rcx
	je	.L38
	movq	32(%r12), %rax
	leaq	1(%rax), %rdx
	call	_ZdlPvy
.L38:
	movq	%r12, %rcx
	call	_ZNSt9exceptionD2Ev
	movl	$48, %edx
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZdlPvy
	.seh_endproc
	.section	.text$_ZN8CryptoPP9ExceptionD1Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN8CryptoPP9ExceptionD1Ev
	.def	_ZN8CryptoPP9ExceptionD1Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN8CryptoPP9ExceptionD1Ev
_ZN8CryptoPP9ExceptionD1Ev:
.LFB15085:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	leaq	16+_ZTVN8CryptoPP9ExceptionE(%rip), %rax
	movq	%rcx, %r12
	movq	%rax, (%rcx)
	movq	16(%rcx), %rcx
	leaq	32(%r12), %rax
	cmpq	%rax, %rcx
	je	.L40
	movq	32(%r12), %rax
	leaq	1(%rax), %rdx
	call	_ZdlPvy
.L40:
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZNSt9exceptionD2Ev
	.seh_endproc
	.section	.text$_ZN8CryptoPP9ExceptionD0Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN8CryptoPP9ExceptionD0Ev
	.def	_ZN8CryptoPP9ExceptionD0Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN8CryptoPP9ExceptionD0Ev
_ZN8CryptoPP9ExceptionD0Ev:
.LFB15086:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	leaq	16+_ZTVN8CryptoPP9ExceptionE(%rip), %rax
	movq	%rcx, %r12
	movq	%rax, (%rcx)
	movq	16(%rcx), %rcx
	leaq	32(%r12), %rax
	cmpq	%rax, %rcx
	je	.L42
	movq	32(%r12), %rax
	leaq	1(%rax), %rdx
	call	_ZdlPvy
.L42:
	movq	%r12, %rcx
	call	_ZNSt9exceptionD2Ev
	movl	$48, %edx
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZdlPvy
	.seh_endproc
	.section	.text$_ZN7fast_io11win32_errorD1Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN7fast_io11win32_errorD1Ev
	.def	_ZN7fast_io11win32_errorD1Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io11win32_errorD1Ev
_ZN7fast_io11win32_errorD1Ev:
.LFB17873:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	leaq	16+_ZTVN7fast_io13fast_io_errorE(%rip), %rax
	movq	%rcx, %r12
	movq	%rax, (%rcx)
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L44
	call	_ZdaPv
.L44:
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZNSt9exceptionD2Ev
	.seh_endproc
	.section	.text$_ZN7fast_io11posix_errorD1Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN7fast_io11posix_errorD1Ev
	.def	_ZN7fast_io11posix_errorD1Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io11posix_errorD1Ev
_ZN7fast_io11posix_errorD1Ev:
.LFB17877:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	leaq	16+_ZTVN7fast_io13fast_io_errorE(%rip), %rax
	movq	%rcx, %r12
	movq	%rax, (%rcx)
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L49
	call	_ZdaPv
.L49:
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZNSt9exceptionD2Ev
	.seh_endproc
	.section	.text$_ZN7fast_io18fast_io_text_errorD1Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN7fast_io18fast_io_text_errorD1Ev
	.def	_ZN7fast_io18fast_io_text_errorD1Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io18fast_io_text_errorD1Ev
_ZN7fast_io18fast_io_text_errorD1Ev:
.LFB17885:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	leaq	16+_ZTVN7fast_io13fast_io_errorE(%rip), %rax
	movq	%rcx, %r12
	movq	%rax, (%rcx)
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L54
	call	_ZdaPv
.L54:
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZNSt9exceptionD2Ev
	.seh_endproc
	.section	.text$_ZN7fast_io11win32_errorD0Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN7fast_io11win32_errorD0Ev
	.def	_ZN7fast_io11win32_errorD0Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io11win32_errorD0Ev
_ZN7fast_io11win32_errorD0Ev:
.LFB17874:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	leaq	16+_ZTVN7fast_io13fast_io_errorE(%rip), %rax
	movq	%rcx, %r12
	movq	%rax, (%rcx)
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L59
	call	_ZdaPv
.L59:
	movq	%r12, %rcx
	call	_ZNSt9exceptionD2Ev
	movl	$24, %edx
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZdlPvy
	.seh_endproc
	.section	.text$_ZN7fast_io11posix_errorD0Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN7fast_io11posix_errorD0Ev
	.def	_ZN7fast_io11posix_errorD0Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io11posix_errorD0Ev
_ZN7fast_io11posix_errorD0Ev:
.LFB17878:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	leaq	16+_ZTVN7fast_io13fast_io_errorE(%rip), %rax
	movq	%rcx, %r12
	movq	%rax, (%rcx)
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L64
	call	_ZdaPv
.L64:
	movq	%r12, %rcx
	call	_ZNSt9exceptionD2Ev
	movl	$24, %edx
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZdlPvy
	.seh_endproc
	.section	.text$_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EED0Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EED0Ev
	.def	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EED0Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EED0Ev
_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EED0Ev:
.LFB16735:
	pushq	%rdi
	.seh_pushreg	%rdi
	.seh_endprologue
	movq	352(%rcx), %rdi
	leaq	16+_ZTVN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EEE(%rip), %rax
	movq	%rax, (%rcx)
	leaq	192(%rcx), %rax
	movq	%rcx, %r8
	cmpq	%rax, %rdi
	je	.L71
.L69:
	movq	176(%r8), %rdi
	leaq	16+_ZTVN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEE(%rip), %rax
	movq	%rax, (%r8)
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L72
	movl	$368, %edx
	movq	%r8, %rcx
	popq	%rdi
	jmp	_ZdlPvy
	.p2align 4
	.p2align 3
.L71:
	movq	336(%rcx), %rcx
	movq	344(%r8), %rax
	movb	$0, 321(%r8)
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	xorl	%eax, %eax
/APP
 # 1425 "D:/msys64/mingw64/include/CryptoPP/misc.h" 1
	rep stosq
 # 0 "" 2
/NO_APP
	jmp	.L69
	.p2align 4
	.p2align 3
.L72:
	movq	168(%r8), %rax
	movq	160(%r8), %rcx
	movb	$0, 153(%r8)
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	xorl	%eax, %eax
/APP
 # 1425 "D:/msys64/mingw64/include/CryptoPP/misc.h" 1
	rep stosq
 # 0 "" 2
/NO_APP
	movl	$368, %edx
	movq	%r8, %rcx
	popq	%rdi
	jmp	_ZdlPvy
	.seh_endproc
	.section	.text$_ZN8CryptoPP14NotImplementedD1Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN8CryptoPP14NotImplementedD1Ev
	.def	_ZN8CryptoPP14NotImplementedD1Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN8CryptoPP14NotImplementedD1Ev
_ZN8CryptoPP14NotImplementedD1Ev:
.LFB15282:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	leaq	16+_ZTVN8CryptoPP9ExceptionE(%rip), %rax
	movq	%rcx, %r12
	movq	%rax, (%rcx)
	movq	16(%rcx), %rcx
	leaq	32(%r12), %rax
	cmpq	%rax, %rcx
	je	.L74
	movq	32(%r12), %rax
	leaq	1(%rax), %rdx
	call	_ZdlPvy
.L74:
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZNSt9exceptionD2Ev
	.seh_endproc
	.section	.text$_ZN8CryptoPP15InvalidArgumentD1Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN8CryptoPP15InvalidArgumentD1Ev
	.def	_ZN8CryptoPP15InvalidArgumentD1Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN8CryptoPP15InvalidArgumentD1Ev
_ZN8CryptoPP15InvalidArgumentD1Ev:
.LFB15138:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	leaq	16+_ZTVN8CryptoPP9ExceptionE(%rip), %rax
	movq	%rcx, %r12
	movq	%rax, (%rcx)
	movq	16(%rcx), %rcx
	leaq	32(%r12), %rax
	cmpq	%rax, %rcx
	je	.L76
	movq	32(%r12), %rax
	leaq	1(%rax), %rdx
	call	_ZdlPvy
.L76:
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZNSt9exceptionD2Ev
	.seh_endproc
	.section	.text$_ZN8CryptoPP14NotImplementedD0Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN8CryptoPP14NotImplementedD0Ev
	.def	_ZN8CryptoPP14NotImplementedD0Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN8CryptoPP14NotImplementedD0Ev
_ZN8CryptoPP14NotImplementedD0Ev:
.LFB15283:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	leaq	16+_ZTVN8CryptoPP9ExceptionE(%rip), %rax
	movq	%rcx, %r12
	movq	%rax, (%rcx)
	movq	16(%rcx), %rcx
	leaq	32(%r12), %rax
	cmpq	%rax, %rcx
	je	.L78
	movq	32(%r12), %rax
	leaq	1(%rax), %rdx
	call	_ZdlPvy
.L78:
	movq	%r12, %rcx
	call	_ZNSt9exceptionD2Ev
	movl	$48, %edx
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZdlPvy
	.seh_endproc
	.section	.text$_ZN8CryptoPP18HashTransformation12VerifyDigestEPKhS2_y,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN8CryptoPP18HashTransformation12VerifyDigestEPKhS2_y
	.def	_ZN8CryptoPP18HashTransformation12VerifyDigestEPKhS2_y;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN8CryptoPP18HashTransformation12VerifyDigestEPKhS2_y
_ZN8CryptoPP18HashTransformation12VerifyDigestEPKhS2_y:
.LFB15220:
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	(%rcx), %rax
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%r9, %r8
	call	*40(%rax)
	movq	(%r12), %rax
	leaq	_ZN8CryptoPP18HashTransformation6VerifyEPKh(%rip), %rdx
	movq	112(%rax), %r8
	cmpq	%rdx, %r8
	jne	.L80
	movq	144(%rax), %rbx
	movq	72(%rax), %rax
	leaq	_ZNK8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE10DigestSizeEv(%rip), %rdx
	movl	$64, %r8d
	cmpq	%rdx, %rax
	jne	.L83
.L81:
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	%rbx, %rax
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	rex.W jmp	*%rax
	.p2align 4
	.p2align 3
.L80:
	movq	%r13, %rdx
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	rex.W jmp	*%r8
	.p2align 4
	.p2align 3
.L83:
	movq	%r12, %rcx
	call	*%rax
	movl	%eax, %r8d
	jmp	.L81
	.seh_endproc
	.section	.text$_ZN8CryptoPP18HashTransformation15CalculateDigestEPhPKhy,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN8CryptoPP18HashTransformation15CalculateDigestEPhPKhy
	.def	_ZN8CryptoPP18HashTransformation15CalculateDigestEPhPKhy;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN8CryptoPP18HashTransformation15CalculateDigestEPhPKhy
_ZN8CryptoPP18HashTransformation15CalculateDigestEPhPKhy:
.LFB15218:
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	(%rcx), %rax
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%r9, %r8
	call	*40(%rax)
	movq	(%r12), %rax
	leaq	_ZN8CryptoPP18HashTransformation5FinalEPh(%rip), %rdx
	movq	56(%rax), %r8
	cmpq	%rdx, %r8
	jne	.L85
	movq	128(%rax), %rbx
	movq	72(%rax), %rax
	leaq	_ZNK8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE10DigestSizeEv(%rip), %rdx
	movl	$64, %r8d
	cmpq	%rdx, %rax
	jne	.L88
.L86:
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	%rbx, %rax
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	rex.W jmp	*%rax
	.p2align 4
	.p2align 3
.L85:
	movq	%r13, %rdx
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	rex.W jmp	*%r8
	.p2align 4
	.p2align 3
.L88:
	movq	%r12, %rcx
	call	*%rax
	movl	%eax, %r8d
	jmp	.L86
	.seh_endproc
	.section	.text$_ZNK8CryptoPP9Algorithm13AlgorithmNameB5cxx11Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNK8CryptoPP9Algorithm13AlgorithmNameB5cxx11Ev
	.def	_ZNK8CryptoPP9Algorithm13AlgorithmNameB5cxx11Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK8CryptoPP9Algorithm13AlgorithmNameB5cxx11Ev
_ZNK8CryptoPP9Algorithm13AlgorithmNameB5cxx11Ev:
.LFB15166:
	.seh_endprologue
	movq	%rcx, %rax
	leaq	16(%rcx), %rdx
	movl	$1852534389, 16(%rcx)
	movw	$30575, 20(%rcx)
	movq	%rdx, (%rcx)
	movb	$110, 22(%rcx)
	movq	$7, 8(%rcx)
	movb	$0, 23(%rcx)
	ret
	.seh_endproc
	.section	.text$_ZNK8CryptoPP13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEENS_6SHA512EE13AlgorithmNameB5cxx11Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNK8CryptoPP13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEENS_6SHA512EE13AlgorithmNameB5cxx11Ev
	.def	_ZNK8CryptoPP13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEENS_6SHA512EE13AlgorithmNameB5cxx11Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK8CryptoPP13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEENS_6SHA512EE13AlgorithmNameB5cxx11Ev
_ZNK8CryptoPP13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEENS_6SHA512EE13AlgorithmNameB5cxx11Ev:
.LFB17958:
	.seh_endprologue
	movq	%rcx, %rax
	leaq	16(%rcx), %rdx
	movl	$759253075, 16(%rcx)
	movw	$12597, 20(%rcx)
	movq	%rdx, (%rcx)
	movb	$50, 22(%rcx)
	movq	$7, 8(%rcx)
	movb	$0, 23(%rcx)
	ret
	.seh_endproc
	.section	.text$_ZNK8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE17AlgorithmProviderB5cxx11Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNK8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE17AlgorithmProviderB5cxx11Ev
	.def	_ZNK8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE17AlgorithmProviderB5cxx11Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE17AlgorithmProviderB5cxx11Ev
_ZNK8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE17AlgorithmProviderB5cxx11Ev:
.LFB17959:
	.seh_endprologue
	movq	%rcx, %rax
	leaq	16(%rcx), %rdx
	movw	$11075, 16(%rcx)
	movb	$43, 18(%rcx)
	movq	%rdx, (%rcx)
	movq	$3, 8(%rcx)
	movb	$0, 19(%rcx)
	ret
	.seh_endproc
	.section	.text$_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_
	.def	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_
_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_:
.LFB17979:
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	8(%rcx), %rsi
	movq	4112(%rsi), %rax
	movq	4104(%rsi), %rcx
	movq	%r8, %rbx
	movq	%rdx, %r13
	subq	%rdx, %rbx
	movq	%r8, %r12
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %rbx
	ja	.L97
	movq	%rbx, %r8
	movq	%r13, %rdx
	call	memcpy
	addq	4104(%rsi), %rbx
.L96:
	movq	%r12, %rax
	movq	%rbx, 4104(%rsi)
	addq	$32, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4
	.p2align 3
.L97:
	subq	4096(%rsi), %rax
	leaq	(%rax,%rax), %rdi
	cmpq	%rbx, %rdi
	cmovb	%rbx, %rdi
	testq	%rdi, %rdi
	js	.L98
	movq	%rdi, %rcx
	call	_Znwy
	movq	4096(%rsi), %rdx
	movq	4104(%rsi), %r8
	movq	%rax, %rcx
	movq	%rax, %rbp
	subq	%rdx, %r8
	call	memcpy
	movq	4096(%rsi), %rcx
	movq	4104(%rsi), %r14
	subq	%rcx, %r14
	cmpq	%rsi, %rcx
	je	.L95
	movq	4112(%rsi), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
.L95:
	leaq	0(%rbp,%r14), %rcx
	movq	%rbp, 4096(%rsi)
	addq	%rdi, %rbp
	movq	%rbx, %r8
	movq	%rcx, 4104(%rsi)
	movq	%rbp, 4112(%rsi)
	movq	%r13, %rdx
	call	memcpy
	addq	4104(%rsi), %rbx
	jmp	.L96
.L98:
	call	_ZSt17__throw_bad_allocv
	nop
	.seh_endproc
	.section	.text$_ZNK7fast_io18fast_io_text_error6reportERNS_14error_reporterE,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNK7fast_io18fast_io_text_error6reportERNS_14error_reporterE
	.def	_ZNK7fast_io18fast_io_text_error6reportERNS_14error_reporterE;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK7fast_io18fast_io_text_error6reportERNS_14error_reporterE
_ZNK7fast_io18fast_io_text_error6reportERNS_14error_reporterE:
.LFB5628:
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	movq	%rcx, %rax
	movq	%rdx, %rcx
	movq	24(%rax), %r12
	movq	16(%rax), %rsi
	movq	(%rdx), %rax
	leaq	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_(%rip), %rdx
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L100
	movq	8(%rcx), %rbx
	movq	4112(%rbx), %rax
	movq	4104(%rbx), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %rsi
	ja	.L106
	movq	%rsi, %r8
	movq	%r12, %rdx
	call	memcpy
	addq	%rsi, 4104(%rbx)
.L105:
	addq	$40, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4
	.p2align 3
.L100:
	leaq	(%r12,%rsi), %r8
	movq	%r12, %rdx
	addq	$40, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	rex.W jmp	*%rax
	.p2align 4
	.p2align 3
.L106:
	subq	4096(%rbx), %rax
	leaq	(%rax,%rax), %rdi
	cmpq	%rsi, %rdi
	cmovb	%rsi, %rdi
	testq	%rdi, %rdi
	js	.L107
	movq	%rdi, %rcx
	call	_Znwy
	movq	4096(%rbx), %rdx
	movq	4104(%rbx), %r8
	movq	%rax, %rcx
	movq	%rax, %rbp
	subq	%rdx, %r8
	call	memcpy
	movq	4096(%rbx), %rcx
	movq	4104(%rbx), %r13
	subq	%rcx, %r13
	cmpq	%rbx, %rcx
	je	.L103
	movq	4112(%rbx), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
.L103:
	leaq	0(%rbp,%r13), %rcx
	movq	%rbp, 4096(%rbx)
	addq	%rdi, %rbp
	movq	%rcx, 4104(%rbx)
	movq	%rbp, 4112(%rbx)
	movq	%rsi, %r8
	movq	%r12, %rdx
	call	memcpy
	addq	%rsi, 4104(%rbx)
	jmp	.L105
.L107:
	call	_ZSt17__throw_bad_allocv
	nop
	.seh_endproc
	.section	.text$_ZNK7fast_io11posix_error6reportERNS_14error_reporterE,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNK7fast_io11posix_error6reportERNS_14error_reporterE
	.def	_ZNK7fast_io11posix_error6reportERNS_14error_reporterE;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK7fast_io11posix_error6reportERNS_14error_reporterE
_ZNK7fast_io11posix_error6reportERNS_14error_reporterE:
.LFB11709:
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	movl	16(%rcx), %ecx
	movq	%rdx, %r13
	call	strerror
	movq	%rax, %rcx
	movq	%rax, %r12
	call	strlen
	leaq	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_(%rip), %rdx
	movq	%rax, %rbx
	movq	0(%r13), %rax
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L109
	movq	8(%r13), %r13
	movq	4112(%r13), %rax
	movq	4104(%r13), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %rbx
	ja	.L115
	movq	%rbx, %r8
	movq	%r12, %rdx
	call	memcpy
	addq	%rbx, 4104(%r13)
.L114:
	addq	$40, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4
	.p2align 3
.L109:
	leaq	(%r12,%rbx), %r8
	movq	%r12, %rdx
	movq	%r13, %rcx
	addq	$40, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	rex.W jmp	*%rax
	.p2align 4
	.p2align 3
.L115:
	subq	4096(%r13), %rax
	leaq	(%rax,%rax), %rsi
	cmpq	%rbx, %rsi
	cmovb	%rbx, %rsi
	testq	%rsi, %rsi
	js	.L116
	movq	%rsi, %rcx
	call	_Znwy
	movq	4096(%r13), %rdx
	movq	4104(%r13), %r8
	movq	%rax, %rcx
	movq	%rax, %rdi
	subq	%rdx, %r8
	call	memcpy
	movq	4096(%r13), %rcx
	movq	4104(%r13), %rbp
	subq	%rcx, %rbp
	cmpq	%r13, %rcx
	je	.L112
	movq	4112(%r13), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
.L112:
	leaq	(%rdi,%rbp), %rcx
	movq	%rdi, 4096(%r13)
	addq	%rsi, %rdi
	movq	%rcx, 4104(%r13)
	movq	%rdi, 4112(%r13)
	movq	%rbx, %r8
	movq	%r12, %rdx
	call	memcpy
	addq	%rbx, 4104(%r13)
	jmp	.L114
.L116:
	call	_ZSt17__throw_bad_allocv
	nop
	.seh_endproc
	.section .rdata,"dr"
.LC1:
	.ascii "unknown fast_io_error\0"
	.section	.text$_ZNK7fast_io13fast_io_error4whatEv,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNK7fast_io13fast_io_error4whatEv
	.def	_ZNK7fast_io13fast_io_error4whatEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK7fast_io13fast_io_error4whatEv
_ZNK7fast_io13fast_io_error4whatEv:
.LFB5590:
	pushq	%r14
	.seh_pushreg	%r14
	movl	$4184, %eax
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	call	___chkstk_ms
	subq	%rax, %rsp
	.seh_stackalloc	4184
	.seh_endprologue
	leaq	48(%rsp), %rdi
	leaq	4144(%rsp), %rax
	movq	%rcx, %rsi
	leaq	32(%rsp), %rdx
	vmovq	%rdi, %xmm1
	movq	%rax, 4160(%rsp)
	leaq	16+_ZTVN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE(%rip), %rax
	vpunpcklqdq	%xmm1, %xmm1, %xmm0
	vmovdqa	%xmm0, 4144(%rsp)
	vmovq	%rax, %xmm0
	movq	(%rcx), %rax
	vpinsrq	$1, %rdi, %xmm0, %xmm0
	vmovdqa	%xmm0, 32(%rsp)
.LEHB0:
	call	*24(%rax)
	movq	4152(%rsp), %rcx
	cmpq	4160(%rsp), %rcx
	je	.L140
	movb	$0, (%rcx)
	movq	4144(%rsp), %r12
	incq	%rcx
	movq	%rcx, 4152(%rsp)
	cmpq	%rdi, %r12
	je	.L141
.L121:
	movq	8(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L124
	call	_ZdaPv
.L124:
	movq	%r12, 8(%rsi)
.L117:
	movq	%r12, %rax
	addq	$4184, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4
	.p2align 3
.L141:
	subq	%rdi, %rcx
	call	_Znay
	movq	8(%rsi), %rcx
	movq	%rax, %r12
	testq	%rcx, %rcx
	je	.L122
	call	_ZdaPv
.L122:
	movq	4144(%rsp), %r13
	movq	4152(%rsp), %r8
	movq	%r12, 8(%rsi)
	subq	%r13, %r8
	jne	.L142
.L123:
	cmpq	%rdi, %r13
	je	.L117
	movq	4160(%rsp), %rdx
	movq	%r13, %rcx
	subq	%r13, %rdx
	call	_ZdlPvy
	jmp	.L117
	.p2align 4
	.p2align 3
.L140:
	subq	4144(%rsp), %rcx
	movq	%rcx, %rbx
	addq	%rbx, %rbx
	js	.L143
	movq	%rbx, %rcx
	call	_Znwy
	movq	4144(%rsp), %r14
	movq	4152(%rsp), %r13
	movq	%rax, %rcx
	movq	%rax, %r12
	subq	%r14, %r13
	movq	%r14, %rdx
	movq	%r13, %r8
	call	memcpy
	cmpq	%rdi, %r14
	je	.L120
	movq	4160(%rsp), %rdx
	movq	%r14, %rcx
	subq	%r14, %rdx
	call	_ZdlPvy
.L120:
	addq	%r12, %r13
	addq	%r12, %rbx
	movq	%r12, 4144(%rsp)
	movb	$0, 0(%r13)
	incq	%r13
	movq	%rbx, 4160(%rsp)
	movq	%r13, 4152(%rsp)
	jmp	.L121
	.p2align 4
	.p2align 3
.L142:
	movq	%r13, %rdx
	movq	%r12, %rcx
	call	memmove
	jmp	.L123
.L143:
	call	_ZSt17__throw_bad_allocv
.LEHE0:
.L129:
	movq	4144(%rsp), %rcx
	movq	%rax, %r12
	cmpq	%rdi, %rcx
	je	.L139
	movq	4160(%rsp), %rdx
	subq	%rcx, %rdx
	vzeroupper
	call	_ZdlPvy
.L127:
	movq	%r12, %rcx
	leaq	.LC1(%rip), %r12
	call	__cxa_begin_catch
	call	__cxa_end_catch
	jmp	.L117
.L139:
	vzeroupper
	jmp	.L127
	.def	__gxx_personality_seh0;	.scl	2;	.type	32;	.endef
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
	.align 4
.LLSDA5590:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT5590-.LLSDATTD5590
.LLSDATTD5590:
	.byte	0x1
	.uleb128 .LLSDACSE5590-.LLSDACSB5590
.LLSDACSB5590:
	.uleb128 .LEHB0-.LFB5590
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L129-.LFB5590
	.uleb128 0x3
.LLSDACSE5590:
	.byte	0x1
	.byte	0
	.byte	0
	.byte	0x7d
	.align 4
	.long	0

.LLSDATT5590:
	.section	.text$_ZNK7fast_io13fast_io_error4whatEv,"x"
	.linkonce discard
	.seh_endproc
	.text
	.p2align 4
	.def	_ZN7fast_io7details3ryu15output_shortestILb0ELy0ELb1ELb0EDuLDu46EPcdEET5_NS0_27compile_time_floating_valueIXT2_ET3_XT4_EEES4_T6_.isra.0;	.scl	3;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details3ryu15output_shortestILb0ELy0ELb1ELb0EDuLDu46EPcdEET5_NS0_27compile_time_floating_valueIXT2_ET3_XT4_EEES4_T6_.isra.0
_ZN7fast_io7details3ryu15output_shortestILb0ELy0ELb1ELb0EDuLDu46EPcdEET5_NS0_27compile_time_floating_valueIXT2_ET3_XT4_EEES4_T6_.isra.0:
.LFB18016:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$72, %rsp
	.seh_stackalloc	72
	.seh_endprologue
	movabsq	$4503599627370495, %rsi
	vmovq	%xmm1, %rdx
	vmovq	%xmm1, %r8
	movq	%rcx, %r9
	shrq	$52, %rdx
	andq	%r8, %rsi
	movl	%edx, %r11d
	andl	$2047, %r11d
	cmpl	$2047, %r11d
	je	.L464
	testl	%r11d, %r11d
	je	.L465
	movq	%rsi, %r15
	leal	-1023(%r11), %eax
	btsq	$52, %r15
	cmpl	$52, %eax
	ja	.L152
	movl	$1075, %ecx
	subl	%r11d, %ecx
	bzhi	%rcx, %r15, %rax
	testq	%rax, %rax
	jne	.L152
	shrx	%rcx, %r15, %rcx
	testq	%r8, %r8
	jns	.L153
	movb	$45, (%r9)
	incq	%r9
.L153:
	movabsq	$-3276141747490816367, %rax
	movabsq	$1844674407370955, %rdx
	imulq	%rcx, %rax
	rorx	$4, %rax, %rax
	cmpq	%rdx, %rax
	jbe	.L154
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rcx
	ja	.L155
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rcx
	ja	.L284
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rcx
	ja	.L285
	movabsq	$999999999999, %rax
	cmpq	%rax, %rcx
	ja	.L286
	movabsq	$99999999999, %rax
	cmpq	%rax, %rcx
	ja	.L287
	movabsq	$9999999999, %rax
	cmpq	%rax, %rcx
	ja	.L288
	cmpq	$999999999, %rcx
	ja	.L289
	cmpq	$99999999, %rcx
	ja	.L290
	cmpq	$9999999, %rcx
	ja	.L291
	cmpq	$999999, %rcx
	ja	.L292
	cmpq	$99999, %rcx
	ja	.L293
	cmpq	$9999, %rcx
	ja	.L294
	cmpq	$999, %rcx
	ja	.L295
	cmpq	$99, %rcx
	ja	.L157
	xorl	%eax, %eax
	cmpq	$9, %rcx
	seta	%al
	leaq	1(%r9,%rax), %r8
	movq	%r8, %r11
	.p2align 4
	.p2align 3
.L179:
	cmpq	$9, %rcx
	jbe	.L181
	.p2align 4
	.p2align 3
.L472:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, -2(%r8)
	jmp	.L144
	.p2align 4
	.p2align 3
.L465:
	testq	%rsi, %rsi
	je	.L466
	vmovq	%xmm1, %rax
	movl	$5200, %ecx
	movl	$-325, 36(%rsp)
	movl	$751, %ebp
	andl	$1, %eax
	movl	$1, %r13d
	movq	%rax, 40(%rsp)
	leaq	0(,%rsi,4), %rax
	leaq	-2(%rax), %rdi
	leaq	2(%rax), %r10
	movq	%rdi, 48(%rsp)
	movl	$57, %edi
.L276:
	leaq	_ZN7fast_io7details3ryu4pow5IdLb1EE5splitE(%rip), %r11
	addq	%r11, %rcx
	movq	(%rcx), %r12
	movq	8(%rcx), %r14
	movq	%r12, %rdx
	mulx	%r10, %rcx, %rbx
	movq	%r14, %rdx
	movq	%rbx, %rcx
	mulx	%r10, %r10, %r11
	movq	48(%rsp), %rdx
	xorl	%ebx, %ebx
	addq	%rcx, %r10
	movl	%edi, %ecx
	adcq	%rbx, %r11
	shrdq	%cl, %r11, %r10
	shrx	%rcx, %r11, %r11
	testb	$64, %dil
	cmovne	%r11, %r10
	mulx	%r12, %rcx, %rbx
	movq	%rbx, %rcx
	movq	%r10, 56(%rsp)
	mulx	%r14, %r10, %r11
	xorl	%ebx, %ebx
	addq	%rcx, %r10
	movq	%r12, %rdx
	movl	%edi, %ecx
	adcq	%rbx, %r11
	shrdq	%cl, %r11, %r10
	shrx	%rcx, %r11, %r11
	mulx	%rax, %rcx, %rbx
	movq	%r14, %rdx
	testb	$64, %dil
	movq	%rbx, %rcx
	mulx	%rax, %r14, %r15
	cmovne	%r11, %r10
	xorl	%ebx, %ebx
	addq	%rcx, %r14
	movl	%edi, %ecx
	adcq	%rbx, %r15
	shrdq	%cl, %r15, %r14
	shrx	%rcx, %r15, %r15
	testb	$64, %dil
	cmovne	%r15, %r14
	movq	%r14, %rcx
	cmpl	$1, %ebp
	jbe	.L467
	cmpl	$62, %ebp
	ja	.L184
	bzhi	%rbp, %rax, %rax
	movq	%rsi, %r15
	testq	%rax, %rax
	sete	%dil
	xorl	%r13d, %r13d
	movl	%edi, %eax
.L188:
	testb	$1, %al
	je	.L184
	movq	%r10, %rax
	movabsq	$-3689348814741910323, %rdx
	movq	%r10, %r14
	mulq	%rdx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	movq	%rdx, %r11
	addq	%rax, %rax
	subq	%rax, %r14
	movb	%r14b, 48(%rsp)
.L195:
	movq	56(%rsp), %rax
	movabsq	$-3689348814741910323, %rdx
	mulq	%rdx
	movq	%rdx, %rbx
	shrq	$3, %rbx
	cmpq	%rbx, %r11
	jnb	.L335
.L196:
	movzbl	48(%rsp), %r14d
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movabsq	$-3689348814741910323, %r12
	.p2align 4
	.p2align 3
.L200:
	movq	%rcx, %rax
	movq	%rcx, %r10
	mulq	%r12
	movq	%rdx, %rcx
	movl	%esi, %edx
	movl	%r10d, %esi
	movq	%r11, %r10
	shrq	$3, %rcx
	leaq	(%rcx,%rcx,4), %rax
	addq	%rax, %rax
	subl	%eax, %esi
	testb	%r14b, %r14b
	sete	%al
	andl	%eax, %r13d
	testb	%dl, %dl
	sete	%al
	incl	%ebp
	andl	%eax, %edi
	movq	%rbx, %rax
	mulq	%r12
	movq	%r11, %rax
	movq	%rdx, %rbx
	mulq	%r12
	shrq	$3, %rbx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	movq	%rdx, %r11
	movq	%r10, %rdx
	addq	%rax, %rax
	subq	%rax, %rdx
	movl	%edx, %r14d
	cmpq	%r11, %rbx
	ja	.L200
	movq	%rdx, %r14
.L199:
	testb	$1, %r13b
	je	.L461
	movabsq	$-3689348814741910323, %rbx
	movabsq	$1844674407370955161, %r12
	testq	%r14, %r14
	jne	.L461
	.p2align 4
	.p2align 3
.L203:
	movq	%rcx, %rax
	movq	%rcx, %r10
	mulq	%rbx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	testb	%sil, %sil
	movl	%r10d, %esi
	movq	%r11, %r10
	sete	%al
	incl	%ebp
	andl	%eax, %edi
	leaq	(%rcx,%rcx,4), %rax
	addq	%rax, %rax
	subl	%eax, %esi
	movq	%r11, %rax
	mulq	%rbx
	movq	%r10, %rax
	imulq	%rbx, %rax
	shrq	$3, %rdx
	rorx	$1, %rax, %rax
	movq	%rdx, %r11
	cmpq	%r12, %rax
	jbe	.L203
.L461:
	cmpb	$5, %sil
	sete	%al
	addl	%ebp, 36(%rsp)
	andl	%eax, %edi
	andl	$1, %edi
	je	.L204
	movq	%rcx, %rsi
	andl	$1, %esi
	addl	$4, %esi
.L204:
	cmpq	%r10, %rcx
	je	.L468
.L198:
	xorl	%eax, %eax
	cmpb	$4, %sil
	seta	%al
	movq	%rax, 40(%rsp)
.L197:
	movq	40(%rsp), %rbx
	addq	%rcx, %rbx
.L205:
	testq	%r8, %r8
	jns	.L209
	movb	$45, (%r9)
	incq	%r9
.L209:
	movabsq	$9999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L338
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L339
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L340
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L341
	movabsq	$999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L342
	movabsq	$99999999999, %rax
	cmpq	%rax, %rbx
	ja	.L343
	movabsq	$9999999999, %rax
	cmpq	%rax, %rbx
	ja	.L344
	cmpq	$999999999, %rbx
	ja	.L345
	cmpq	$99999999, %rbx
	ja	.L346
	cmpq	$9999999, %rbx
	ja	.L347
	cmpq	$999999, %rbx
	ja	.L348
	cmpq	$99999, %rbx
	ja	.L349
	cmpq	$9999, %rbx
	ja	.L350
	cmpq	$999, %rbx
	ja	.L351
	cmpq	$99, %rbx
	ja	.L352
	cmpq	$9, %rbx
	ja	.L469
	movl	36(%rsp), %eax
	leal	1(%rax), %ebp
	testl	%eax, %eax
	jle	.L470
	movl	%eax, %esi
	cmpl	$4, %ebp
	ja	.L216
	movl	$1, %edi
	movl	$1, %eax
.L236:
	leaq	(%r9,%rax), %rcx
.L237:
	cmpq	$9, %rbx
	jbe	.L239
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rbx,2), %eax
	movw	%ax, -2(%rcx)
.L240:
	movslq	36(%rsp), %rbx
	leaq	(%r9,%rdi), %r11
	movl	$48, %edx
	movq	%r11, %rcx
	movq	%rbx, %r8
	call	memset
	movq	%rax, %r11
	addq	%rbx, %r11
	jmp	.L144
	.p2align 4
	.p2align 3
.L464:
	testq	%rsi, %rsi
	jne	.L471
	testq	%r8, %r8
	jns	.L148
	movl	$1718511917, (%rcx)
	leaq	4(%rcx), %r11
.L144:
	movq	%r11, %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4
	.p2align 3
.L471:
	movw	$24942, (%rcx)
	movb	$110, 2(%rcx)
	leaq	3(%rcx), %r11
	jmp	.L144
	.p2align 4
	.p2align 3
.L466:
	leaq	1(%rcx), %r11
	testq	%r8, %r8
	jns	.L151
	leaq	2(%rcx), %rax
	movq	%r11, %r9
	movb	$45, (%rcx)
	movq	%rax, %r11
.L151:
	movb	$48, (%r9)
	jmp	.L144
	.p2align 4
	.p2align 3
.L155:
	leaq	16(%r9), %r11
	.p2align 4
	.p2align 3
.L282:
	movq	%r11, %r8
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %rbx
	.p2align 4
	.p2align 3
.L161:
	movq	%rcx, %rdx
	subq	$2, %r8
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%rbx
	movq	%rdx, %rax
	shrq	$2, %rax
	imulq	$100, %rax, %r9
	movq	%rcx, %rax
	subq	%r9, %rax
	movq	%rcx, %r9
	movq	%rdx, %rcx
	movzwl	(%r10,%rax,2), %eax
	shrq	$2, %rcx
	movw	%ax, (%r8)
	cmpq	$9999, %r9
	ja	.L161
	cmpq	$9, %rcx
	ja	.L472
.L181:
	addl	$48, %ecx
	movb	%cl, -1(%r8)
	jmp	.L144
	.p2align 4
	.p2align 3
.L148:
	movw	$28265, (%rcx)
	movb	$102, 2(%rcx)
	leaq	3(%rcx), %r11
	jmp	.L144
	.p2align 4
	.p2align 3
.L152:
	movq	%r8, %rax
	leal	-1077(%r11), %ecx
	andl	$1, %eax
	movq	%rax, 40(%rsp)
	leaq	0(,%r15,4), %rax
	testq	%rsi, %rsi
	jne	.L327
	testl	$2046, %edx
	movabsq	$4503599627370496, %r15
	sete	%dl
	sete	%r13b
	movzbl	%dl, %edx
.L182:
	leaq	-1(%rax), %rbx
	leaq	2(%rax), %rbp
	subq	%rdx, %rbx
	movq	%rbp, %r10
	movq	%rbx, %r14
	movq	%rbx, 48(%rsp)
	testl	%ecx, %ecx
	js	.L183
	movslq	%ecx, %rdx
	movabsq	$169464822037455, %r10
	imulq	%r10, %rdx
	xorl	%r10d, %r10d
	shrq	$49, %rdx
	cmpl	$3, %ecx
	setg	%r10b
	movl	%edx, %r12d
	subl	%r10d, %r12d
	leaq	_ZN7fast_io7details3ryu4pow5IdLb1EE9inv_splitE(%rip), %r10
	movl	%r12d, %edx
	movl	%r12d, %r11d
	movl	%r12d, 36(%rsp)
	salq	$4, %rdx
	subl	%ecx, %r11d
	addq	%r10, %rdx
	imull	$1217359, %r12d, %r10d
	movq	(%rdx), %rdi
	movq	8(%rdx), %rsi
	shrl	$19, %r10d
	leal	61(%r10,%r11), %r13d
	movq	%rdi, %rdx
	mulx	%rbp, %rcx, %rbx
	movq	%rbp, %rdx
	mulx	%rsi, %r10, %r11
	movq	%rbx, %rcx
	xorl	%ebx, %ebx
	movq	%rdi, %rdx
	addq	%rcx, %r10
	movl	%r13d, %ecx
	adcq	%rbx, %r11
	shrdq	%cl, %r11, %r10
	shrx	%rcx, %r11, %r11
	testb	$64, %r13b
	cmovne	%r11, %r10
	movq	%r10, 56(%rsp)
	mulx	%r14, %r10, %r11
	movq	%rsi, %rdx
	mulx	%r14, %rcx, %rbx
	movq	%r11, %r10
	xorl	%r11d, %r11d
	movq	%rdi, %rdx
	addq	%rcx, %r10
	movl	%r13d, %ecx
	adcq	%rbx, %r11
	shrdq	%cl, %r11, %r10
	shrx	%rcx, %r11, %r11
	mulx	%rax, %rcx, %rbx
	movq	%rsi, %rdx
	testb	$64, %r13b
	movq	%rbx, %rcx
	mulx	%rax, %rsi, %rdi
	cmovne	%r11, %r10
	xorl	%ebx, %ebx
	addq	%rcx, %rsi
	movl	%r13d, %ecx
	adcq	%rbx, %rdi
	shrdq	%cl, %rdi, %rsi
	shrx	%rcx, %rdi, %rdi
	testb	$64, %r13b
	cmovne	%rdi, %rsi
	movq	%rsi, %rcx
	cmpl	$21, %r12d
	ja	.L184
	movabsq	$-3689348814741910323, %r11
	movq	%rax, %rdx
	movabsq	$3689348814741910323, %rbx
	imulq	%r11, %rdx
	cmpq	%rbx, %rdx
	ja	.L185
	mulq	%r11
	movl	$1, %edi
	movq	%rdx, %rsi
	shrq	$2, %rsi
	jmp	.L187
	.p2align 4
	.p2align 3
.L328:
	movq	%rdx, %rsi
.L187:
	movq	%rsi, %rax
	imulq	%r11, %rax
	cmpq	%rbx, %rax
	ja	.L186
	movq	%rsi, %rax
	incl	%edi
	mulq	%r11
	shrq	$2, %rdx
	cmpq	$4, %rsi
	ja	.L328
	xorl	%edi, %edi
.L186:
	cmpl	%edi, %r12d
	setbe	%dil
	xorl	%r13d, %r13d
	movl	%edi, %eax
	jmp	.L188
	.p2align 4
	.p2align 3
.L284:
	movl	$15, %r11d
.L156:
	addq	%r9, %r11
	cmpq	$99, %rcx
	ja	.L282
	movq	%r11, %r8
	jmp	.L179
	.p2align 4
	.p2align 3
.L154:
	movabsq	$3777893186295716171, %rdx
	movq	%rcx, %rax
	movabsq	$-3689348814741910323, %r11
	mulq	%rdx
	shrq	$11, %rdx
	movq	%rdx, %r8
	.p2align 4
	.p2align 3
.L163:
	movq	%r8, %rax
	movq	%r8, %rbx
	mulq	%r11
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %r10
	movq	%rdx, %rax
	movq	%r8, %rdx
	addq	%r10, %r10
	movq	%rax, %r8
	subq	%r10, %rdx
	je	.L163
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rcx
	ja	.L297
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rcx
	ja	.L298
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rcx
	ja	.L299
	movabsq	$999999999999, %rax
	cmpq	%rax, %rcx
	ja	.L300
	movabsq	$99999999999, %rax
	cmpq	%rax, %rcx
	ja	.L301
	movabsq	$9999999999, %rax
	cmpq	%rax, %rcx
	ja	.L302
	cmpq	$999999999, %rcx
	ja	.L303
	cmpq	$99999999, %rcx
	ja	.L304
	cmpq	$9999999, %rcx
	ja	.L305
	cmpq	$999999, %rcx
	ja	.L306
	cmpq	$99999, %rcx
	ja	.L307
	cmpq	$9999, %rcx
	ja	.L308
	cmpq	$999, %rcx
	ja	.L309
	cmpq	$99, %rcx
	ja	.L310
	cmpq	$10, %rcx
	sbbq	%r11, %r11
	addq	$2, %r11
	cmpq	$10, %rcx
	sbbl	%eax, %eax
	addl	$2, %eax
	.p2align 4
	.p2align 3
.L164:
	movabsq	$99999999999, %rdx
	cmpq	%rdx, %rbx
	ja	.L165
	movabsq	$9999999999, %rdx
	cmpq	%rdx, %rbx
	ja	.L165
	cmpq	$999999999, %rbx
	ja	.L312
	cmpq	$99999999, %rbx
	ja	.L313
	cmpq	$9999999, %rbx
	ja	.L314
	cmpq	$999999, %rbx
	ja	.L315
	cmpq	$99999, %rbx
	ja	.L316
	cmpq	$9999, %rbx
	ja	.L317
	cmpq	$999, %rbx
	ja	.L318
	cmpq	$99, %rbx
	ja	.L319
	movl	$5, %r10d
	cmpq	$9, %rbx
	ja	.L473
.L168:
	cmpl	%eax, %r10d
	jb	.L474
.L165:
	addq	%r9, %r11
	movq	%r11, %r8
	cmpq	$99, %rcx
	jbe	.L179
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %rbx
	.p2align 4
	.p2align 3
.L180:
	movq	%rcx, %rdx
	subq	$2, %r8
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%rbx
	movq	%rdx, %rax
	shrq	$2, %rax
	imulq	$100, %rax, %r9
	movq	%rcx, %rax
	subq	%r9, %rax
	movq	%rcx, %r9
	movq	%rdx, %rcx
	movzwl	(%r10,%rax,2), %eax
	shrq	$2, %rcx
	movw	%ax, (%r8)
	cmpq	$9999, %r9
	ja	.L180
	jmp	.L179
	.p2align 4
	.p2align 3
.L285:
	movl	$14, %r11d
	jmp	.L156
.L327:
	movl	$1, %edx
	movl	$1, %r13d
	jmp	.L182
.L467:
	movq	%r10, %rax
	movabsq	$-3689348814741910323, %rbx
	mulq	%rbx
	movq	%rdx, %r14
	shrq	$3, %r14
	leaq	(%r14,%r14,4), %rax
	movq	%r14, %rbp
	movq	%r10, %r14
	addq	%rax, %rax
	movq	%rbp, %r11
	subq	%rax, %r14
	cmpq	$0, 40(%rsp)
	movb	%r14b, 48(%rsp)
	je	.L333
	movq	56(%rsp), %rdx
	movq	%rsi, %r15
	movl	$1, %edi
	xorl	%r13d, %r13d
	decq	%rdx
	movq	%rdx, %rax
	mulq	%rbx
	movq	%rdx, %rbx
	shrq	$3, %rbx
	cmpq	%rbx, %rbp
	jb	.L196
	xorl	%esi, %esi
	cmpq	%rcx, %r10
	jne	.L198
	jmp	.L197
	.p2align 4
	.p2align 3
.L290:
	movl	$9, %r11d
	jmp	.L156
.L329:
	xorl	%esi, %esi
	jmp	.L189
	.p2align 4
	.p2align 3
.L332:
	movq	%rdx, %rbp
.L189:
	movq	%rbp, %rax
	imulq	%r11, %rax
	cmpq	%rbx, %rax
	ja	.L193
	movq	%rbp, %rax
	incl	%esi
	mulq	%r11
	shrq	$2, %rdx
	cmpq	$4, %rbp
	ja	.L332
	xorl	%esi, %esi
.L193:
	xorl	%eax, %eax
	cmpl	%esi, %r12d
	setbe	%al
	subq	%rax, 56(%rsp)
	.p2align 4
	.p2align 3
.L184:
	movq	56(%rsp), %rdx
	movabsq	$2951479051793528259, %rbx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%rbx
	movq	%r10, %rax
	shrq	$2, %rax
	shrq	$2, %rdx
	movq	%rdx, %r11
	mulq	%rbx
	movq	%rdx, %rsi
	shrq	$2, %rsi
	cmpq	%rsi, %r11
	jbe	.L337
	movq	%rcx, %rdx
	movq	%rsi, %r10
	movq	%r11, 56(%rsp)
	movl	$2, %edi
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%rbx
	shrq	$2, %rdx
	imulq	$100, %rdx, %rax
	subq	%rax, %rcx
	cmpq	$49, %rcx
	movq	%rdx, %rcx
	seta	%bpl
.L206:
	movq	56(%rsp), %rax
	movabsq	$-3689348814741910323, %rsi
	mulq	%rsi
	movq	%r10, %rax
	movq	%rdx, %rbx
	mulq	%rsi
	shrq	$3, %rbx
	movq	%rdx, %r11
	shrq	$3, %r11
	cmpq	%r11, %rbx
	jbe	.L207
	.p2align 4
	.p2align 3
.L208:
	movq	%rcx, %rax
	movq	%rcx, %rbp
	movq	%r11, %r10
	incl	%edi
	mulq	%rsi
	movq	%rbx, %rax
	movq	%rdx, %rcx
	mulq	%rsi
	movq	%r11, %rax
	shrq	$3, %rcx
	movq	%rdx, %rbx
	mulq	%rsi
	shrq	$3, %rbx
	movq	%rdx, %r11
	shrq	$3, %r11
	cmpq	%r11, %rbx
	ja	.L208
	leaq	(%rcx,%rcx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rbp
	cmpq	$4, %rbp
	seta	%bpl
.L207:
	cmpq	%r10, %rcx
	sete	%bl
	addl	%edi, 36(%rsp)
	orl	%ebp, %ebx
	movzbl	%bl, %ebx
	addq	%rcx, %rbx
	jmp	.L205
.L286:
	movl	$13, %r11d
	jmp	.L156
.L337:
	xorl	%ebp, %ebp
	xorl	%edi, %edi
	jmp	.L206
.L287:
	movl	$12, %r11d
	jmp	.L156
.L338:
	movl	$22, %edx
	movl	$18, %ecx
	movl	$18, %r12d
	movl	$17, %edi
	movl	$19, %r8d
	movl	$17, %eax
.L210:
	movl	36(%rsp), %esi
	leal	(%rsi,%rax), %ebp
	leal	-1(%rsi,%rax), %esi
	cmpl	%eax, %esi
	jl	.L212
	cmpl	%edx, %ebp
	jbe	.L475
.L213:
	cmpq	$9, %rbx
	jbe	.L216
	movabsq	$9999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L356
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L357
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L358
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L359
	movabsq	$999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L360
	movabsq	$99999999999, %rax
	cmpq	%rax, %rbx
	ja	.L361
	movabsq	$9999999999, %rax
	cmpq	%rax, %rbx
	ja	.L362
	cmpq	$999999999, %rbx
	ja	.L363
	cmpq	$99999999, %rbx
	ja	.L364
	cmpq	$9999999, %rbx
	ja	.L365
	cmpq	$999999, %rbx
	ja	.L366
	cmpq	$99999, %rbx
	ja	.L367
	cmpq	$9999, %rbx
	ja	.L368
	cmpq	$999, %rbx
	ja	.L369
	leaq	3(%r9), %r11
	cmpq	$99, %rbx
	ja	.L476
.L225:
	movq	%r11, %rcx
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
.L272:
	movzwl	(%r10,%rbx,2), %eax
	movw	%ax, -2(%rcx)
.L228:
	movzbl	1(%r9), %eax
	movb	$46, 1(%r9)
	movb	%al, (%r9)
.L229:
	movb	$101, (%r11)
	movl	$43, %eax
	testl	%esi, %esi
	jns	.L230
	movl	$1, %esi
	movl	$45, %eax
	subl	%ebp, %esi
.L230:
	movb	%al, 1(%r11)
	cmpl	$99, %esi
	jg	.L477
	movslq	%esi, %rsi
	addq	$4, %r11
	movzwl	(%r10,%rsi,2), %eax
	movw	%ax, -2(%r11)
	jmp	.L144
	.p2align 4
	.p2align 3
.L212:
	testl	%esi, %esi
	js	.L218
	cmpl	%eax, %ebp
	je	.L478
	cmpl	%edx, %r8d
	ja	.L213
	movabsq	$9999999999999999, %rax
	leaq	1(%r9), %r11
	cmpq	%rax, %rbx
	ja	.L250
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L397
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L398
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L399
	movabsq	$999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L400
	movabsq	$99999999999, %rax
	cmpq	%rax, %rbx
	ja	.L401
	movabsq	$9999999999, %rax
	cmpq	%rax, %rbx
	ja	.L402
	cmpq	$999999999, %rbx
	ja	.L403
	cmpq	$99999999, %rbx
	ja	.L404
	cmpq	$9999999, %rbx
	ja	.L405
	cmpq	$999999, %rbx
	ja	.L406
	cmpq	$99999, %rbx
	ja	.L407
	cmpq	$9999, %rbx
	ja	.L408
	cmpq	$999, %rbx
	ja	.L409
	cmpq	$99, %rbx
	ja	.L252
	cmpq	$10, %rbx
	sbbl	%eax, %eax
	leal	2(%rax), %ecx
	addq	%r11, %rcx
.L255:
	cmpq	$9, %rbx
	jbe	.L257
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rbx,2), %eax
	movw	%ax, -2(%rcx)
.L258:
	movslq	%ebp, %rbp
	movq	%r11, %rdx
	movq	%r9, %rcx
	movq	%rbp, %r8
	call	memmove
	movb	$46, (%rax,%rbp)
	leaq	(%rax,%r12), %r11
	jmp	.L144
.L218:
	subl	%esi, %ecx
	cmpl	%edx, %ecx
	ja	.L213
	movw	$11824, (%r9)
	negl	%ebp
	addq	$2, %r9
	movl	$48, %edx
	movq	%rbp, %r8
	movq	%r9, %rcx
	call	memset
	leaq	(%rax,%rbp), %r11
	movabsq	$9999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L259
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L410
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L411
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L412
	movabsq	$999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L413
	movabsq	$99999999999, %rax
	cmpq	%rax, %rbx
	ja	.L414
	movabsq	$9999999999, %rax
	cmpq	%rax, %rbx
	ja	.L415
	cmpq	$999999999, %rbx
	ja	.L416
	cmpq	$99999999, %rbx
	ja	.L417
	cmpq	$9999999, %rbx
	ja	.L418
	cmpq	$999999, %rbx
	ja	.L419
	cmpq	$99999, %rbx
	ja	.L420
	cmpq	$9999, %rbx
	ja	.L421
	cmpq	$999, %rbx
	ja	.L422
	cmpq	$99, %rbx
	ja	.L261
	cmpq	$10, %rbx
	sbbl	%eax, %eax
	addl	$2, %eax
.L263:
	leaq	(%r11,%rax), %rcx
.L264:
	cmpq	$9, %rbx
	jbe	.L266
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rbx,2), %eax
	movw	%ax, -2(%rcx)
.L267:
	addq	%rdi, %r11
	jmp	.L144
.L288:
	movl	$11, %r11d
	jmp	.L156
.L339:
	movl	$21, %edx
	movl	$17, %ecx
	movl	$17, %r12d
	movl	$16, %edi
	movl	$18, %r8d
	movl	$16, %eax
	jmp	.L210
.L468:
	xorl	$1, %r13d
	testb	%r13b, %r13b
	jne	.L336
	andl	$1, %r15d
	je	.L198
.L336:
	movq	$1, 40(%rsp)
	jmp	.L197
.L475:
	movabsq	$9999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L232
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L371
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L372
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L373
	movabsq	$999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L374
	movabsq	$99999999999, %rax
	cmpq	%rax, %rbx
	ja	.L375
	movabsq	$9999999999, %rax
	cmpq	%rax, %rbx
	ja	.L376
	cmpq	$999999999, %rbx
	ja	.L377
	cmpq	$99999999, %rbx
	ja	.L378
	cmpq	$9999999, %rbx
	ja	.L379
	cmpq	$999999, %rbx
	ja	.L380
	cmpq	$99999, %rbx
	ja	.L381
	cmpq	$9999, %rbx
	ja	.L382
	cmpq	$999, %rbx
	ja	.L383
	cmpq	$99, %rbx
	jbe	.L479
	movl	$100, %ecx
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rcx
	leaq	1(%r9), %rcx
	movq	%rax, %rbx
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rdx,2), %eax
	movw	%ax, 1(%r9)
.L239:
	addl	$48, %ebx
	movb	%bl, -1(%rcx)
	jmp	.L240
.L185:
	cmpq	$0, 40(%rsp)
	jne	.L329
	movq	%r14, %rax
	imulq	%r11, %rax
	cmpq	%rbx, %rax
	ja	.L330
	movq	%r14, %rax
	movl	$1, %esi
	mulq	%r11
	movq	%rdx, %r14
	shrq	$2, %r14
	jmp	.L192
	.p2align 4
	.p2align 3
.L331:
	movq	%rdx, %r14
.L192:
	movq	%r14, %rax
	imulq	%r11, %rax
	cmpq	%rbx, %rax
	ja	.L190
	movq	%r14, %rax
	incl	%esi
	mulq	%r11
	shrq	$2, %rdx
	cmpq	$4, %r14
	ja	.L331
.L330:
	xorl	%esi, %esi
.L190:
	cmpl	%esi, %r12d
	setbe	%r13b
	xorl	%edi, %edi
	movl	%r13d, %eax
	jmp	.L188
.L289:
	movl	$10, %r11d
	jmp	.L156
.L340:
	movl	$20, %edx
	movl	$16, %ecx
	movl	$16, %r12d
	movl	$15, %edi
	movl	$17, %r8d
	movl	$15, %eax
	jmp	.L210
.L341:
	movl	$19, %edx
	movl	$15, %ecx
	movl	$15, %r12d
	movl	$14, %edi
	movl	$16, %r8d
	movl	$14, %eax
	jmp	.L210
.L356:
	movl	$18, %r11d
.L222:
	addq	%r9, %r11
	cmpq	$99, %rbx
	jbe	.L225
	movq	%r11, %rcx
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %rdi
	.p2align 4
	.p2align 3
.L226:
	movq	%rbx, %rdx
	subq	$2, %rcx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%rdi
	movq	%rdx, %rax
	shrq	$2, %rax
	imulq	$100, %rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	movq	%rbx, %r8
	movq	%rdx, %rbx
	movzwl	(%r10,%rax,2), %eax
	shrq	$2, %rbx
	movw	%ax, (%rcx)
	cmpq	$9999, %r8
	ja	.L226
	cmpq	$999, %r8
	ja	.L272
.L227:
	addl	$48, %ebx
	movb	%bl, -1(%rcx)
	jmp	.L228
.L261:
	movl	$100, %ecx
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rcx
	leaq	1(%r11), %rcx
	movq	%rax, %rbx
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rdx,2), %eax
	movw	%ax, 1(%r11)
.L266:
	addl	$48, %ebx
	movb	%bl, -1(%rcx)
	jmp	.L267
.L333:
	movq	%rsi, %r15
	movl	$1, %edi
	jmp	.L195
.L297:
	movl	$16, %r11d
	movl	$16, %eax
	jmp	.L164
.L342:
	movl	$18, %edx
	movl	$14, %ecx
	movl	$14, %r12d
	movl	$13, %edi
	movl	$15, %r8d
	movl	$13, %eax
	jmp	.L210
.L291:
	movl	$8, %r11d
	jmp	.L156
.L478:
	cmpl	%edx, %ecx
	ja	.L213
	movabsq	$9999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L480
.L215:
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L384
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L385
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L386
	movabsq	$999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L387
	movabsq	$99999999999, %rax
	cmpq	%rax, %rbx
	ja	.L388
	movabsq	$9999999999, %rax
	cmpq	%rax, %rbx
	ja	.L389
	cmpq	$999999999, %rbx
	ja	.L390
	cmpq	$99999999, %rbx
	ja	.L391
	cmpq	$9999999, %rbx
	ja	.L392
	cmpq	$999999, %rbx
	ja	.L393
	cmpq	$99999, %rbx
	ja	.L394
	cmpq	$9999, %rbx
	ja	.L395
	cmpq	$999, %rbx
	ja	.L396
	cmpq	$99, %rbx
	ja	.L243
	cmpq	$10, %rbx
	sbbl	%eax, %eax
	leal	2(%rax), %ecx
	addq	%r9, %rcx
.L246:
	cmpq	$9, %rbx
	jbe	.L248
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rbx,2), %eax
	movw	%ax, -2(%rcx)
.L249:
	leaq	(%r9,%rdi), %r11
	jmp	.L144
.L357:
	movl	$17, %r11d
	jmp	.L222
.L292:
	movl	$7, %r11d
	jmp	.L156
.L343:
	movl	$17, %edx
	movl	$13, %ecx
	movl	$13, %r12d
	movl	$12, %edi
	movl	$14, %r8d
	movl	$12, %eax
	jmp	.L210
.L358:
	movl	$16, %r11d
	jmp	.L222
.L252:
	movl	$100, %ecx
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rcx
	leaq	2(%r9), %rcx
	movq	%rax, %rbx
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rdx,2), %eax
	movw	%ax, 2(%r9)
.L257:
	addl	$48, %ebx
	movb	%bl, -1(%rcx)
	jmp	.L258
.L477:
	movl	%esi, %eax
	addq	$5, %r11
	imulq	$1374389535, %rax, %rax
	shrq	$37, %rax
	leal	48(%rax), %edx
	imull	$100, %eax, %eax
	movb	%dl, -3(%r11)
	subl	%eax, %esi
	movzwl	(%r10,%rsi,2), %eax
	movw	%ax, -2(%r11)
	jmp	.L144
.L470:
	je	.L353
	movl	$2, %eax
	subl	36(%rsp), %eax
	cmpl	$4, %eax
	jbe	.L481
	movl	36(%rsp), %esi
.L216:
	addl	$48, %ebx
	leaq	1(%r9), %r11
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movb	%bl, (%r9)
	jmp	.L229
.L344:
	movl	$16, %edx
	movl	$12, %ecx
	movl	$12, %r12d
	movl	$11, %edi
	movl	$13, %r8d
	movl	$11, %eax
	jmp	.L210
.L293:
	movl	$6, %r11d
	jmp	.L156
.L298:
	movl	$15, %r11d
	movl	$15, %eax
	jmp	.L164
.L359:
	movl	$15, %r11d
	jmp	.L222
.L335:
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	jmp	.L199
.L299:
	movl	$14, %r11d
	movl	$14, %eax
	jmp	.L164
.L294:
	movl	$5, %r11d
	jmp	.L156
.L345:
	movl	$15, %edx
	movl	$11, %ecx
	movl	$11, %r12d
	movl	$10, %edi
	movl	$12, %r8d
	movl	$10, %eax
	jmp	.L210
.L232:
	leaq	17(%r9), %rcx
.L278:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %r11
	.p2align 4
	.p2align 3
.L238:
	movq	%rbx, %rdx
	subq	$2, %rcx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%r11
	movq	%rdx, %rax
	shrq	$2, %rax
	imulq	$100, %rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	movq	%rbx, %r8
	movq	%rdx, %rbx
	movzwl	(%r10,%rax,2), %eax
	shrq	$2, %rbx
	movw	%ax, (%rcx)
	cmpq	$9999, %r8
	ja	.L238
	jmp	.L237
.L360:
	movl	$14, %r11d
	jmp	.L222
.L259:
	leaq	17(%r11), %rcx
.L281:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %r9
	.p2align 4
	.p2align 3
.L265:
	movq	%rbx, %rdx
	subq	$2, %rcx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%r9
	movq	%rdx, %rax
	shrq	$2, %rax
	imulq	$100, %rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	movq	%rbx, %r8
	movq	%rdx, %rbx
	movzwl	(%r10,%rax,2), %eax
	shrq	$2, %rbx
	movw	%ax, (%rcx)
	cmpq	$9999, %r8
	ja	.L265
	jmp	.L264
.L295:
	movl	$4, %r11d
	jmp	.L156
.L346:
	movl	$14, %edx
	movl	$10, %ecx
	movl	$10, %r12d
	movl	$9, %edi
	movl	$11, %r8d
	movl	$9, %eax
	jmp	.L210
.L300:
	movl	$13, %r11d
	movl	$13, %eax
	jmp	.L164
.L183:
	movl	$1077, %ebx
	movabsq	$196742565691928, %rsi
	subl	%r11d, %ebx
	movslq	%ebx, %r11
	imulq	%rsi, %r11
	xorl	%esi, %esi
	shrq	$48, %r11
	cmpl	$1, %ebx
	seta	%sil
	movl	%r11d, %ebp
	subl	%esi, %ebp
	movq	%r15, %rsi
	subl	%ebp, %ebx
	leal	(%rcx,%rbp), %edi
	movslq	%ebx, %rcx
	movl	%ebp, %ebx
	movl	%edi, 36(%rsp)
	imull	$1217359, %ecx, %r11d
	salq	$4, %rcx
	shrl	$19, %r11d
	subl	%r11d, %ebx
	leal	60(%rbx), %edi
	jmp	.L276
.L371:
	movl	$16, %ecx
.L233:
	addq	%r9, %rcx
	cmpq	$99, %rbx
	ja	.L278
	jmp	.L237
.L243:
	movl	$100, %ecx
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rcx
	leaq	1(%r9), %rcx
	movq	%rax, %rbx
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rdx,2), %eax
	movw	%ax, 1(%r9)
.L248:
	addl	$48, %ebx
	movb	%bl, -1(%rcx)
	jmp	.L249
.L361:
	movl	$13, %r11d
	jmp	.L222
.L410:
	movl	$16, %ecx
.L260:
	addq	%r11, %rcx
	cmpq	$99, %rbx
	ja	.L281
	jmp	.L264
.L474:
	addl	$48, %ebx
	leaq	1(%r9), %rax
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movb	%bl, (%r9)
.L170:
	movw	$11109, (%rax)
	movzwl	(%r10,%r11,2), %edx
	leaq	4(%rax), %r11
	movw	%dx, 2(%rax)
	jmp	.L144
.L250:
	leaq	18(%r9), %rcx
.L280:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %rsi
	.p2align 4
	.p2align 3
.L256:
	movq	%rbx, %rdx
	subq	$2, %rcx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%rsi
	movq	%rdx, %rax
	shrq	$2, %rax
	imulq	$100, %rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	movq	%rbx, %r8
	movq	%rdx, %rbx
	movzwl	(%r10,%rax,2), %eax
	shrq	$2, %rbx
	movw	%ax, (%rcx)
	cmpq	$9999, %r8
	ja	.L256
	jmp	.L255
.L157:
	movq	%rcx, %rdx
	movabsq	$2951479051793528259, %r8
	leaq	3(%r9), %r11
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%r8
	movq	%rdx, %rax
	shrq	$2, %rax
	imulq	$100, %rax, %r8
	movq	%rcx, %rax
	movq	%rdx, %rcx
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rdx
	shrq	$2, %rcx
	subq	%r8, %rax
	leaq	1(%r9), %r8
	movzwl	(%rdx,%rax,2), %eax
	movw	%ax, 1(%r9)
	jmp	.L181
.L347:
	movl	$13, %edx
	movl	$9, %ecx
	movl	$9, %r12d
	movl	$8, %edi
	movl	$10, %r8d
	movl	$8, %eax
	jmp	.L210
.L301:
	movl	$12, %r11d
	movl	$12, %eax
	jmp	.L164
.L372:
	movl	$15, %ecx
	jmp	.L233
.L362:
	movl	$12, %r11d
	jmp	.L222
.L411:
	movl	$15, %ecx
	jmp	.L260
.L412:
	movl	$14, %ecx
	jmp	.L260
.L363:
	movl	$11, %r11d
	jmp	.L222
.L302:
	movl	$11, %r11d
	movl	$11, %eax
	jmp	.L164
.L373:
	movl	$14, %ecx
	jmp	.L233
.L397:
	movl	$16, %ecx
.L251:
	addq	%r11, %rcx
	cmpq	$99, %rbx
	ja	.L280
	jmp	.L255
.L348:
	movl	$12, %edx
	movl	$8, %ecx
	movl	$8, %r12d
	movl	$7, %edi
	movl	$9, %r8d
	movl	$7, %eax
	jmp	.L210
.L312:
	movl	$14, %r10d
	movl	$15, %edx
	movl	$10, %r8d
.L166:
	cmpq	$9, %rbx
	jbe	.L168
	cmpl	%eax, %edx
	jnb	.L165
	cmpq	$999999999, %rbx
	ja	.L171
	cmpq	$99999999, %rbx
	ja	.L320
	cmpq	$9999999, %rbx
	ja	.L321
	cmpq	$999999, %rbx
	ja	.L322
	cmpq	$99999, %rbx
	ja	.L323
	cmpq	$9999, %rbx
	ja	.L324
	movl	$4, %eax
	cmpq	$999, %rbx
	jbe	.L482
.L172:
	leal	1(%rax), %ecx
	addq	%r9, %rcx
	cmpq	$99, %rbx
	jbe	.L460
.L283:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %rdi
.L176:
	movq	%rbx, %rdx
	subq	$2, %rcx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%rdi
	movq	%rdx, %rax
	shrq	$2, %rax
	imulq	$100, %rax, %rsi
	movq	%rbx, %rax
	subq	%rsi, %rax
	movq	%rbx, %rsi
	movq	%rdx, %rbx
	movzwl	(%r10,%rax,2), %eax
	shrq	$2, %rbx
	movw	%ax, (%rcx)
	cmpq	$9999, %rsi
	ja	.L176
	cmpq	$999, %rsi
	ja	.L274
.L177:
	addl	$48, %ebx
	movb	%bl, -1(%rcx)
.L178:
	movzbl	1(%r9), %eax
	movb	$46, 1(%r9)
	movb	%al, (%r9)
	leal	1(%r8), %eax
	addq	%r9, %rax
	jmp	.L170
.L398:
	movl	$15, %ecx
	jmp	.L251
.L303:
	movl	$10, %r11d
	movl	$10, %eax
	jmp	.L164
.L349:
	movl	$11, %edx
	movl	$7, %ecx
	movl	$7, %r12d
	movl	$6, %edi
	movl	$8, %r8d
	movl	$6, %eax
	jmp	.L210
.L313:
	movl	$13, %r10d
	movl	$14, %edx
	movl	$9, %r8d
	jmp	.L166
.L413:
	movl	$13, %ecx
	jmp	.L260
.L374:
	movl	$13, %ecx
	jmp	.L233
.L480:
	leaq	17(%r9), %rcx
.L279:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %r11
	.p2align 4
	.p2align 3
.L247:
	movq	%rbx, %rdx
	subq	$2, %rcx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%r11
	movq	%rdx, %rax
	shrq	$2, %rax
	imulq	$100, %rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	movq	%rbx, %r8
	movq	%rdx, %rbx
	movzwl	(%r10,%rax,2), %eax
	shrq	$2, %rbx
	movw	%ax, (%rcx)
	cmpq	$9999, %r8
	ja	.L247
	jmp	.L246
.L304:
	movl	$9, %r11d
	movl	$9, %eax
	jmp	.L164
.L399:
	movl	$14, %ecx
	jmp	.L251
.L350:
	movl	$10, %edx
	movl	$6, %ecx
	movl	$6, %r12d
	movl	$5, %edi
	movl	$7, %r8d
	movl	$5, %eax
	jmp	.L210
.L364:
	movl	$10, %r11d
	jmp	.L222
.L400:
	movl	$13, %ecx
	jmp	.L251
.L351:
	movl	$9, %edx
	movl	$5, %ecx
	movl	$5, %r12d
	movl	$4, %edi
	movl	$6, %r8d
	movl	$4, %eax
	jmp	.L210
.L384:
	movl	$16, %ecx
.L242:
	addq	%r9, %rcx
	cmpq	$99, %rbx
	ja	.L279
	jmp	.L246
.L305:
	movl	$8, %r11d
	movl	$8, %eax
	jmp	.L164
.L366:
	movl	$8, %r11d
	jmp	.L222
.L365:
	movl	$9, %r11d
	jmp	.L222
.L314:
	movl	$12, %r10d
	movl	$13, %edx
	movl	$8, %r8d
	jmp	.L166
.L415:
	movl	$11, %ecx
	jmp	.L260
.L414:
	movl	$12, %ecx
	jmp	.L260
.L375:
	movl	$12, %ecx
	jmp	.L233
.L376:
	movl	$11, %ecx
	jmp	.L233
.L377:
	movl	$10, %ecx
	jmp	.L233
.L353:
	movl	$1, %edi
	jmp	.L215
.L368:
	movl	$6, %r11d
	jmp	.L222
.L367:
	movl	$7, %r11d
	jmp	.L222
.L378:
	movl	$9, %ecx
	jmp	.L233
.L416:
	movl	$10, %ecx
	jmp	.L260
.L402:
	movl	$11, %ecx
	jmp	.L251
.L401:
	movl	$12, %ecx
	jmp	.L251
.L386:
	movl	$14, %ecx
	jmp	.L242
.L385:
	movl	$15, %ecx
	jmp	.L242
.L306:
	movl	$7, %r11d
	movl	$7, %eax
	jmp	.L164
.L315:
	movl	$11, %r10d
	movl	$12, %edx
	movl	$7, %r8d
	jmp	.L166
.L473:
	cmpl	$7, %eax
	jbe	.L165
	movl	$2, %r8d
.L174:
	leaq	3(%r9), %rcx
.L460:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
.L274:
	movzwl	(%r10,%rbx,2), %eax
	movw	%ax, -2(%rcx)
	jmp	.L178
.L316:
	movl	$10, %r10d
	movl	$11, %edx
	movl	$6, %r8d
	jmp	.L166
.L307:
	movl	$6, %r11d
	movl	$6, %eax
	jmp	.L164
.L469:
	movl	$7, %edx
	movl	$3, %ecx
	movl	$3, %r12d
	movl	$2, %edi
	movl	$4, %r8d
	movl	$2, %eax
	jmp	.L210
.L352:
	movl	$8, %edx
	movl	$4, %ecx
	movl	$4, %r12d
	movl	$3, %edi
	movl	$5, %r8d
	movl	$3, %eax
	jmp	.L210
.L417:
	movl	$9, %ecx
	jmp	.L260
.L418:
	movl	$8, %ecx
	jmp	.L260
.L481:
	movl	36(%rsp), %esi
	movw	$11824, (%r9)
	addq	$2, %r9
	movl	$48, %edx
	movq	%r9, %rcx
	movl	$1, %edi
	notl	%esi
	movq	%rsi, %r8
	call	memset
	leaq	(%rax,%rsi), %r11
	movl	$1, %eax
	jmp	.L263
.L379:
	movl	$8, %ecx
	jmp	.L233
.L381:
	movl	$6, %ecx
	jmp	.L233
.L380:
	movl	$7, %ecx
	jmp	.L233
.L390:
	movl	$10, %ecx
	jmp	.L242
.L389:
	movl	$11, %ecx
	jmp	.L242
.L388:
	movl	$12, %ecx
	jmp	.L242
.L387:
	movl	$13, %ecx
	jmp	.L242
.L476:
	movl	$100, %ecx
	movq	%rbx, %rax
	xorl	%edx, %edx
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	divq	%rcx
	leaq	4(%r9), %r11
	leaq	2(%r9), %rcx
	movq	%rax, %rbx
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, 2(%r9)
	jmp	.L227
.L369:
	movl	$5, %r11d
	jmp	.L222
.L391:
	movl	$9, %ecx
	jmp	.L242
.L420:
	movl	$6, %ecx
	jmp	.L260
.L419:
	movl	$7, %ecx
	jmp	.L260
.L171:
	leaq	11(%r9), %rcx
	jmp	.L283
.L319:
	movl	$7, %r10d
	movl	$8, %edx
	movl	$3, %r8d
	jmp	.L166
.L318:
	movl	$8, %r10d
	movl	$9, %edx
	movl	$4, %r8d
	jmp	.L166
.L407:
	movl	$6, %ecx
	jmp	.L251
.L406:
	movl	$7, %ecx
	jmp	.L251
.L405:
	movl	$8, %ecx
	jmp	.L251
.L404:
	movl	$9, %ecx
	jmp	.L251
.L403:
	movl	$10, %ecx
	jmp	.L251
.L317:
	movl	$9, %r10d
	movl	$10, %edx
	movl	$5, %r8d
	jmp	.L166
.L308:
	movl	$5, %r11d
	movl	$5, %eax
	jmp	.L164
.L310:
	movl	$3, %r11d
	movl	$3, %eax
	jmp	.L164
.L309:
	movl	$4, %r11d
	movl	$4, %eax
	jmp	.L164
.L382:
	movl	$5, %ecx
	jmp	.L233
.L421:
	movl	$5, %ecx
	jmp	.L260
.L409:
	movl	$4, %ecx
	jmp	.L251
.L408:
	movl	$5, %ecx
	jmp	.L251
.L479:
	cmpq	$10, %rbx
	sbbl	%eax, %eax
	addl	$2, %eax
	jmp	.L236
.L383:
	movl	$4, %ecx
	jmp	.L233
.L422:
	movl	$4, %ecx
	jmp	.L260
.L482:
	cmpq	$99, %rbx
	jbe	.L174
	movl	$100, %ecx
	movq	%rbx, %rax
	xorl	%edx, %edx
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	divq	%rcx
	leaq	2(%r9), %rcx
	movq	%rax, %rbx
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, 2(%r9)
	jmp	.L177
.L324:
	movl	$5, %eax
	jmp	.L172
.L392:
	movl	$8, %ecx
	jmp	.L242
.L323:
	movl	$6, %eax
	jmp	.L172
.L322:
	movl	$7, %eax
	jmp	.L172
.L321:
	movl	$8, %eax
	jmp	.L172
.L320:
	movl	$9, %eax
	jmp	.L172
.L396:
	movl	$4, %ecx
	jmp	.L242
.L395:
	movl	$5, %ecx
	jmp	.L242
.L394:
	movl	$6, %ecx
	jmp	.L242
.L393:
	movl	$7, %ecx
	jmp	.L242
	.seh_endproc
	.section	.text$_ZN7fast_io17throw_win32_errorEv,"x"
	.linkonce discard
	.globl	_ZN7fast_io17throw_win32_errorEv
	.def	_ZN7fast_io17throw_win32_errorEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io17throw_win32_errorEv
_ZN7fast_io17throw_win32_errorEv:
.LFB12984:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movl	$24, %ecx
	call	__cxa_allocate_exception
	movq	%rax, %r12
	call	GetLastError
	leaq	16+_ZTVN7fast_io11win32_errorE(%rip), %rdx
	leaq	_ZN7fast_io11win32_errorD1Ev(%rip), %r8
	movq	%rdx, (%r12)
	movq	%r12, %rcx
	leaq	_ZTIN7fast_io11win32_errorE(%rip), %rdx
	movq	$0, 8(%r12)
	movl	%eax, 16(%r12)
	call	__cxa_throw
	nop
	.seh_endproc
	.section .rdata,"dr"
	.align 8
.LC2:
	.ascii "basic_string::_M_construct null not valid\0"
.LC3:
	.ascii "basic_string::_M_create\0"
	.section	.text$_ZN8CryptoPP9ExceptionC2ENS0_9ErrorTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN8CryptoPP9ExceptionC2ENS0_9ErrorTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.def	_ZN8CryptoPP9ExceptionC2ENS0_9ErrorTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN8CryptoPP9ExceptionC2ENS0_9ErrorTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
_ZN8CryptoPP9ExceptionC2ENS0_9ErrorTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB15088:
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	leaq	16+_ZTVN8CryptoPP9ExceptionE(%rip), %rax
	movq	8(%r8), %rbx
	movq	%rax, (%rcx)
	movq	%rcx, %r12
	movl	%edx, 8(%rcx)
	leaq	32(%rcx), %rcx
	movq	%rcx, 16(%r12)
	movq	(%r8), %r13
	movq	%r13, %rax
	addq	%rbx, %rax
	je	.L485
	testq	%r13, %r13
	je	.L505
.L485:
	cmpq	$15, %rbx
	ja	.L506
	cmpq	$1, %rbx
	jne	.L490
	movzbl	0(%r13), %eax
	movb	%al, 32(%r12)
.L491:
	movq	%rbx, 24(%r12)
	movb	$0, (%rcx,%rbx)
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	ret
	.p2align 4
	.p2align 3
.L490:
	testq	%rbx, %rbx
	je	.L491
	jmp	.L489
	.p2align 4
	.p2align 3
.L506:
	testq	%rbx, %rbx
	js	.L507
	movq	%rbx, %rcx
	incq	%rcx
	js	.L508
.LEHB1:
	call	_Znwy
	movq	%rax, %rcx
	movq	%rax, 16(%r12)
	movq	%rbx, 32(%r12)
.L489:
	movq	%rbx, %r8
	movq	%r13, %rdx
	call	memcpy
	movq	16(%r12), %rcx
	jmp	.L491
.L505:
	leaq	.LC2(%rip), %rcx
	call	_ZSt19__throw_logic_errorPKc
.L507:
	leaq	.LC3(%rip), %rcx
	call	_ZSt20__throw_length_errorPKc
.L508:
	call	_ZSt17__throw_bad_allocv
.LEHE1:
.L493:
	movq	%rax, %r13
	movq	%r12, %rcx
	vzeroupper
	call	_ZNSt9exceptionD2Ev
	movq	%r13, %rcx
.LEHB2:
	call	_Unwind_Resume
	nop
.LEHE2:
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA15088:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE15088-.LLSDACSB15088
.LLSDACSB15088:
	.uleb128 .LEHB1-.LFB15088
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L493-.LFB15088
	.uleb128 0
	.uleb128 .LEHB2-.LFB15088
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
.LLSDACSE15088:
	.section	.text$_ZN8CryptoPP9ExceptionC2ENS0_9ErrorTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"x"
	.linkonce discard
	.seh_endproc
	.section	.text$_ZN7fast_io10basic_ibufINS_18basic_file_wrapperINS_16basic_win32_fileIcEELNS_9open_modeE4104EEENS_17basic_buf_handlerIcLb0ELy1048576ENS_20io_aligned_allocatorIcLy4096EEEEEED1Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN7fast_io10basic_ibufINS_18basic_file_wrapperINS_16basic_win32_fileIcEELNS_9open_modeE4104EEENS_17basic_buf_handlerIcLb0ELy1048576ENS_20io_aligned_allocatorIcLy4096EEEEEED1Ev
	.def	_ZN7fast_io10basic_ibufINS_18basic_file_wrapperINS_16basic_win32_fileIcEELNS_9open_modeE4104EEENS_17basic_buf_handlerIcLb0ELy1048576ENS_20io_aligned_allocatorIcLy4096EEEEEED1Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io10basic_ibufINS_18basic_file_wrapperINS_16basic_win32_fileIcEELNS_9open_modeE4104EEENS_17basic_buf_handlerIcLb0ELy1048576ENS_20io_aligned_allocatorIcLy4096EEEEEED1Ev
_ZN7fast_io10basic_ibufINS_18basic_file_wrapperINS_16basic_win32_fileIcEELNS_9open_modeE4104EEENS_17basic_buf_handlerIcLb0ELy1048576ENS_20io_aligned_allocatorIcLy4096EEEEEED1Ev:
.LFB15996:
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	%rcx, %rbx
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L510
	call	CloseHandle
.L510:
	movq	(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L515
	movl	$4096, %r8d
	movl	$1048576, %edx
	addq	$32, %rsp
	popq	%rbx
	jmp	_ZdlPvySt11align_val_t
	.p2align 4
	.p2align 3
.L515:
	addq	$32, %rsp
	popq	%rbx
	ret
	.seh_endproc
	.section	.text$_ZN7fast_io7details23bufferred_transmit_implINS_8cryptopp17iterated_hash_refIN8CryptoPP6SHA512EEENS_10basic_ibufINS_18basic_file_wrapperINS_16basic_win32_fileIcEELNS_9open_modeE4104EEENS_17basic_buf_handlerIcLb0ELy1048576ENS_20io_aligned_allocatorIcLy4096EEEEEEEEEyRT_RT0_,"x"
	.linkonce discard
	.p2align 4
	.globl	_ZN7fast_io7details23bufferred_transmit_implINS_8cryptopp17iterated_hash_refIN8CryptoPP6SHA512EEENS_10basic_ibufINS_18basic_file_wrapperINS_16basic_win32_fileIcEELNS_9open_modeE4104EEENS_17basic_buf_handlerIcLb0ELy1048576ENS_20io_aligned_allocatorIcLy4096EEEEEEEEEyRT_RT0_
	.def	_ZN7fast_io7details23bufferred_transmit_implINS_8cryptopp17iterated_hash_refIN8CryptoPP6SHA512EEENS_10basic_ibufINS_18basic_file_wrapperINS_16basic_win32_fileIcEELNS_9open_modeE4104EEENS_17basic_buf_handlerIcLb0ELy1048576ENS_20io_aligned_allocatorIcLy4096EEEEEEEEEyRT_RT0_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details23bufferred_transmit_implINS_8cryptopp17iterated_hash_refIN8CryptoPP6SHA512EEENS_10basic_ibufINS_18basic_file_wrapperINS_16basic_win32_fileIcEELNS_9open_modeE4104EEENS_17basic_buf_handlerIcLb0ELy1048576ENS_20io_aligned_allocatorIcLy4096EEEEEEEEEyRT_RT0_
_ZN7fast_io7details23bufferred_transmit_implINS_8cryptopp17iterated_hash_refIN8CryptoPP6SHA512EEENS_10basic_ibufINS_18basic_file_wrapperINS_16basic_win32_fileIcEELNS_9open_modeE4104EEENS_17basic_buf_handlerIcLb0ELy1048576ENS_20io_aligned_allocatorIcLy4096EEEEEEEEEyRT_RT0_:
.LFB16006:
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$64, %rsp
	.seh_stackalloc	64
	.seh_endprologue
	xorl	%r12d, %r12d
	movq	%rdx, %rsi
	movq	8(%rdx), %rdx
	movq	%rcx, %rdi
	movq	16(%rsi), %rbx
	cmpq	%rbx, %rdx
	jne	.L523
	jmp	.L517
	.p2align 4
	.p2align 3
.L520:
	movl	60(%rsp), %eax
	addq	%rax, %rbx
.L521:
	movq	(%rsi), %rdx
	movq	%rbx, 16(%rsi)
	movq	%rdx, 8(%rsi)
	cmpq	%rbx, %rdx
	je	.L528
.L523:
	movq	(%rdi), %rcx
	subq	%rdx, %rbx
	movq	%rbx, %r8
	addq	%rbx, %r12
	movq	(%rcx), %rax
	call	*40(%rax)
	movq	16(%rsi), %rbx
.L517:
	testq	%rbx, %rbx
	je	.L518
	movq	(%rsi), %rbx
.L519:
	movq	24(%rsi), %rcx
	movq	$0, 32(%rsp)
	leaq	60(%rsp), %r9
	movl	$1048576, %r8d
	movq	%rbx, %rdx
	movl	$0, 60(%rsp)
	call	ReadFile
	testl	%eax, %eax
	jne	.L520
	call	GetLastError
	movl	%eax, %ebp
	cmpl	$109, %eax
	je	.L521
	movl	$24, %ecx
	call	__cxa_allocate_exception
	leaq	_ZN7fast_io11win32_errorD1Ev(%rip), %r8
	leaq	_ZTIN7fast_io11win32_errorE(%rip), %rdx
	movq	%rax, %rcx
	movq	$0, 8(%rax)
	leaq	16+_ZTVN7fast_io11win32_errorE(%rip), %rax
	movq	%rax, (%rcx)
	movl	%ebp, 16(%rcx)
	call	__cxa_throw
	.p2align 4
	.p2align 3
.L518:
	movl	$4096, %edx
	movl	$1048576, %ecx
	call	_ZnwySt11align_val_t
	vmovq	%rax, %xmm1
	movq	%rax, %rbx
	movq	%rax, 16(%rsi)
	vpunpcklqdq	%xmm1, %xmm1, %xmm0
	vmovdqu	%xmm0, (%rsi)
	jmp	.L519
	.p2align 4
	.p2align 3
.L528:
	movq	%r12, %rax
	addq	$64, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	ret
	.seh_endproc
	.section	.text$_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1IS3_EEPKcRKS3_,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1IS3_EEPKcRKS3_
	.def	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1IS3_EEPKcRKS3_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1IS3_EEPKcRKS3_
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1IS3_EEPKcRKS3_:
.LFB16679:
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	leaq	16(%rcx), %r13
	movq	%rcx, %rsi
	movq	%rdx, %r12
	movq	%r13, (%rcx)
	testq	%rdx, %rdx
	je	.L530
	movq	%rdx, %rcx
	call	strlen
	movq	%rax, %rbx
	cmpq	$15, %rax
	ja	.L540
	cmpq	$1, %rax
	jne	.L534
	movzbl	(%r12), %eax
	movb	%al, 16(%rsi)
.L535:
	movq	%rbx, 8(%rsi)
	movb	$0, 0(%r13,%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%rsi
	popq	%r12
	popq	%r13
	ret
	.p2align 4
	.p2align 3
.L534:
	testq	%rax, %rax
	je	.L535
	jmp	.L533
	.p2align 4
	.p2align 3
.L540:
	leaq	1(%rax), %rcx
	call	_Znwy
	movq	%rbx, 16(%rsi)
	movq	%rax, %r13
	movq	%rax, (%rsi)
.L533:
	movq	%r13, %rcx
	movq	%rbx, %r8
	movq	%r12, %rdx
	call	memcpy
	movq	(%rsi), %r13
	jmp	.L535
.L530:
	leaq	.LC2(%rip), %rcx
	call	_ZSt19__throw_logic_errorPKc
	nop
	.seh_endproc
	.section .rdata,"dr"
	.align 8
.LC4:
	.ascii "Clone() is not implemented yet.\0"
	.section	.text$_ZNK8CryptoPP8Clonable5CloneEv,"x"
	.linkonce discard
	.align 2
	.globl	_ZNK8CryptoPP8Clonable5CloneEv
	.def	_ZNK8CryptoPP8Clonable5CloneEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK8CryptoPP8Clonable5CloneEv
_ZNK8CryptoPP8Clonable5CloneEv:
.LFB15161:
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$80, %rsp
	.seh_stackalloc	80
	.seh_endprologue
	movl	$48, %ecx
	leaq	48(%rsp), %r12
	call	__cxa_allocate_exception
	leaq	47(%rsp), %r8
	leaq	.LC4(%rip), %rdx
	movq	%r12, %rcx
	movq	%rax, %r13
.LEHB3:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1IS3_EEPKcRKS3_
.LEHE3:
	movq	%r12, %r8
	xorl	%edx, %edx
	movq	%r13, %rcx
	leaq	64(%rsp), %rbx
.LEHB4:
	call	_ZN8CryptoPP9ExceptionC2ENS0_9ErrorTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LEHE4:
	movq	48(%rsp), %rcx
	leaq	16+_ZTVN8CryptoPP14NotImplementedE(%rip), %rax
	movq	%rax, 0(%r13)
	cmpq	%rbx, %rcx
	je	.L542
	movq	64(%rsp), %rax
	leaq	1(%rax), %rdx
	call	_ZdlPvy
.L542:
	leaq	_ZN8CryptoPP14NotImplementedD1Ev(%rip), %r8
	leaq	_ZTIN8CryptoPP14NotImplementedE(%rip), %rdx
	movq	%r13, %rcx
.LEHB5:
	call	__cxa_throw
.L547:
	movq	48(%rsp), %rcx
	movq	%rax, %r12
	cmpq	%rbx, %rcx
	je	.L548
	movq	64(%rsp), %rax
	leaq	1(%rax), %rdx
	vzeroupper
	call	_ZdlPvy
	jmp	.L545
.L546:
	movq	%rax, %r12
	vzeroupper
	jmp	.L545
.L548:
	vzeroupper
.L545:
	movq	%r13, %rcx
	call	__cxa_free_exception
	movq	%r12, %rcx
	call	_Unwind_Resume
	nop
.LEHE5:
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA15161:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE15161-.LLSDACSB15161
.LLSDACSB15161:
	.uleb128 .LEHB3-.LFB15161
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L546-.LFB15161
	.uleb128 0
	.uleb128 .LEHB4-.LFB15161
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L547-.LFB15161
	.uleb128 0
	.uleb128 .LEHB5-.LFB15161
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0
	.uleb128 0
.LLSDACSE15161:
	.section	.text$_ZNK8CryptoPP8Clonable5CloneEv,"x"
	.linkonce discard
	.seh_endproc
	.section	.text$_ZNK8CryptoPP12ClonableImplINS_6SHA512ENS_13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEES1_EEE5CloneEv,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNK8CryptoPP12ClonableImplINS_6SHA512ENS_13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEES1_EEE5CloneEv
	.def	_ZNK8CryptoPP12ClonableImplINS_6SHA512ENS_13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEES1_EEE5CloneEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK8CryptoPP12ClonableImplINS_6SHA512ENS_13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEES1_EEE5CloneEv
_ZNK8CryptoPP12ClonableImplINS_6SHA512ENS_13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEES1_EEE5CloneEv:
.LFB17921:
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	movq	%rcx, %rbx
	movl	$368, %ecx
	call	_Znwy
	vmovdqu	8(%rbx), %xmm0
	vmovdqu	160(%rbx), %xmm1
	movq	168(%rbx), %r8
	movq	%rax, %r12
	vmovdqu	%xmm0, 8(%rax)
	leaq	16+_ZTVN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEE(%rip), %rax
	movb	$0, 153(%r12)
	movq	%rax, (%r12)
	vmovdqu	%xmm1, 160(%r12)
	cmpq	$16, %r8
	ja	.L561
	leaq	24(%r12), %rcx
	movb	$1, 153(%r12)
	movq	176(%rbx), %rdx
	movq	%rcx, 176(%r12)
	testq	%rdx, %rdx
	je	.L551
	salq	$3, %r8
	call	memcpy
.L551:
	vmovdqa	336(%rbx), %xmm2
	movq	344(%rbx), %r8
	leaq	16+_ZTVN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EEE(%rip), %rax
	movb	$0, 321(%r12)
	movq	%rax, (%r12)
	vmovdqa	%xmm2, 336(%r12)
	cmpq	$16, %r8
	ja	.L562
	movq	352(%rbx), %rdx
	leaq	192(%r12), %rcx
	movb	$1, 321(%r12)
	movq	%rcx, 352(%r12)
	testq	%rdx, %rdx
	je	.L552
	salq	$3, %r8
	call	memcpy
.L552:
	movq	.refptr._ZTVN8CryptoPP6SHA512E(%rip), %rax
	addq	$16, %rax
	movq	%rax, (%r12)
	movq	%r12, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	ret
	.p2align 4
	.p2align 3
.L561:
	movq	$0, 176(%r12)
	jmp	.L551
	.p2align 4
	.p2align 3
.L562:
	movq	$0, 352(%r12)
	jmp	.L552
	.seh_endproc
	.section	.text$_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EED2Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EED2Ev
	.def	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EED2Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EED2Ev
_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EED2Ev:
.LFB16733:
	pushq	%rdi
	.seh_pushreg	%rdi
	.seh_endprologue
	movq	352(%rcx), %rdi
	leaq	16+_ZTVN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EEE(%rip), %rax
	movq	%rax, (%rcx)
	leaq	192(%rcx), %rax
	movq	%rcx, %rdx
	cmpq	%rax, %rdi
	je	.L567
.L564:
	movq	176(%rdx), %rdi
	leaq	16+_ZTVN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEE(%rip), %rax
	movq	%rax, (%rdx)
	leaq	24(%rdx), %rax
	cmpq	%rax, %rdi
	je	.L568
	popq	%rdi
	ret
	.p2align 4
	.p2align 3
.L568:
	movq	168(%rdx), %rax
	movq	160(%rdx), %rcx
	movb	$0, 153(%rdx)
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	xorl	%eax, %eax
/APP
 # 1425 "D:/msys64/mingw64/include/CryptoPP/misc.h" 1
	rep stosq
 # 0 "" 2
/NO_APP
	popq	%rdi
	ret
	.p2align 4
	.p2align 3
.L567:
	movq	336(%rcx), %rcx
	movq	344(%rdx), %rax
	movb	$0, 321(%rdx)
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	xorl	%eax, %eax
/APP
 # 1425 "D:/msys64/mingw64/include/CryptoPP/misc.h" 1
	rep stosq
 # 0 "" 2
/NO_APP
	jmp	.L564
	.seh_endproc
	.section	.text$_ZN7fast_io7details33reserve_write_extremely_cold_pathINS_14error_reporterEZNS0_18report_win32_errorERS2_jEUlT_E_EEvRS4_yRT0_,"x"
	.linkonce discard
	.p2align 4
	.globl	_ZN7fast_io7details33reserve_write_extremely_cold_pathINS_14error_reporterEZNS0_18report_win32_errorERS2_jEUlT_E_EEvRS4_yRT0_
	.def	_ZN7fast_io7details33reserve_write_extremely_cold_pathINS_14error_reporterEZNS0_18report_win32_errorERS2_jEUlT_E_EEvRS4_yRT0_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details33reserve_write_extremely_cold_pathINS_14error_reporterEZNS0_18report_win32_errorERS2_jEUlT_E_EEvRS4_yRT0_
_ZN7fast_io7details33reserve_write_extremely_cold_pathINS_14error_reporterEZNS0_18report_win32_errorERS2_jEUlT_E_EEvRS4_yRT0_:
.LFB17234:
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$1104, %rsp
	.seh_stackalloc	1104
	.seh_endprologue
	movq	%rcx, %rsi
	cmpq	$1024, %rdx
	ja	.L570
	movq	(%r8), %rax
	leaq	80(%rsp), %r12
	movq	$0, 48(%rsp)
	movl	$32768, 40(%rsp)
	movq	%r12, 32(%rsp)
	movl	$1024, %r9d
	xorl	%edx, %edx
	movl	$4608, %ecx
	leaq	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_(%rip), %rdi
	movl	(%rax), %r8d
	call	FormatMessageA
	movl	%eax, %ebx
	addq	%r12, %rbx
	.p2align 4
	.p2align 3
.L576:
	movq	(%rsi), %rax
	movq	(%rax), %rax
	cmpq	%rdi, %rax
	jne	.L571
	movq	8(%rsi), %rsi
	subq	%r12, %rbx
	movq	4112(%rsi), %rax
	movq	4104(%rsi), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %rbx
	ja	.L589
	movq	%rbx, %r8
	movq	%r12, %rdx
	call	memcpy
	addq	%rbx, 4104(%rsi)
.L588:
	addq	$1104, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4
	.p2align 3
.L571:
	movq	%r12, %rdx
	movq	%rbx, %r8
	movq	%rsi, %rcx
.LEHB6:
	call	*%rax
	movq	%rax, %r12
	cmpq	%rbx, %rax
	jne	.L576
	jmp	.L588
	.p2align 4
	.p2align 3
.L589:
	subq	4096(%rsi), %rax
	leaq	(%rax,%rax), %rdi
	cmpq	%rbx, %rdi
	cmovb	%rbx, %rdi
	testq	%rdi, %rdi
	js	.L590
	movq	%rdi, %rcx
	call	_Znwy
	movq	4096(%rsi), %rdx
	movq	4104(%rsi), %r8
	movq	%rax, %rcx
	movq	%rax, %rbp
	subq	%rdx, %r8
	call	memcpy
	movq	4096(%rsi), %rcx
	movq	4104(%rsi), %r13
	subq	%rcx, %r13
	cmpq	%rsi, %rcx
	je	.L574
	movq	4112(%rsi), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
.L574:
	leaq	0(%rbp,%r13), %rcx
	movq	%rbp, 4096(%rsi)
	addq	%rdi, %rbp
	movq	%rcx, 4104(%rsi)
	movq	%rbp, 4112(%rsi)
	movq	%rbx, %r8
	movq	%r12, %rdx
	call	memcpy
	addq	%rbx, 4104(%rsi)
	jmp	.L588
	.p2align 4
	.p2align 3
.L570:
	movq	%rdx, %rcx
	movq	%r8, 72(%rsp)
	leaq	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_(%rip), %rdi
	call	_Znay
.LEHE6:
	movl	$1024, %r9d
	xorl	%edx, %edx
	movl	$4608, %ecx
	movq	72(%rsp), %r8
	movq	%rax, %r13
	movq	%r13, %r12
	movq	(%r8), %rax
	movq	$0, 48(%rsp)
	movl	$32768, 40(%rsp)
	movq	%r13, 32(%rsp)
	movl	(%rax), %r8d
	call	FormatMessageA
	movl	%eax, %ebx
	addq	%r13, %rbx
.L583:
	movq	(%rsi), %rax
	movq	(%rax), %rax
	cmpq	%rdi, %rax
	jne	.L578
	movq	8(%rsi), %rsi
	subq	%r12, %rbx
	movq	4112(%rsi), %rax
	movq	4104(%rsi), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %rbx
	ja	.L591
	movq	%rbx, %r8
	movq	%r12, %rdx
	call	memcpy
	addq	%rbx, 4104(%rsi)
.L582:
	movq	%r13, %rcx
	addq	$1104, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	jmp	_ZdaPv
	.p2align 4
	.p2align 3
.L578:
	movq	%rbx, %r8
	movq	%r12, %rdx
	movq	%rsi, %rcx
.LEHB7:
	call	*%rax
	movq	%rax, %r12
	cmpq	%rbx, %rax
	jne	.L583
	jmp	.L582
	.p2align 4
	.p2align 3
.L591:
	subq	4096(%rsi), %rax
	leaq	(%rax,%rax), %rdi
	cmpq	%rbx, %rdi
	cmovb	%rbx, %rdi
	testq	%rdi, %rdi
	js	.L592
	movq	%rdi, %rcx
	call	_Znwy
	movq	4096(%rsi), %rdx
	movq	4104(%rsi), %r8
	movq	%rax, %rcx
	movq	%rax, %rbp
	subq	%rdx, %r8
	call	memcpy
	movq	4096(%rsi), %rcx
	movq	4104(%rsi), %r14
	subq	%rcx, %r14
	cmpq	%rsi, %rcx
	je	.L581
	movq	4112(%rsi), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
.L581:
	leaq	0(%rbp,%r14), %rcx
	movq	%rbp, 4096(%rsi)
	addq	%rdi, %rbp
	movq	%rcx, 4104(%rsi)
	movq	%rbp, 4112(%rsi)
	movq	%rbx, %r8
	movq	%r12, %rdx
	call	memcpy
	addq	%rbx, 4104(%rsi)
	jmp	.L582
.L592:
	call	_ZSt17__throw_bad_allocv
.LEHE7:
.L590:
.LEHB8:
	call	_ZSt17__throw_bad_allocv
.L585:
	movq	%rax, %r12
	movq	%r13, %rcx
	vzeroupper
	call	_ZdaPv
	movq	%r12, %rcx
	call	_Unwind_Resume
	nop
.LEHE8:
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA17234:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE17234-.LLSDACSB17234
.LLSDACSB17234:
	.uleb128 .LEHB6-.LFB17234
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB7-.LFB17234
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L585-.LFB17234
	.uleb128 0
	.uleb128 .LEHB8-.LFB17234
	.uleb128 .LEHE8-.LEHB8
	.uleb128 0
	.uleb128 0
.LLSDACSE17234:
	.section	.text$_ZN7fast_io7details33reserve_write_extremely_cold_pathINS_14error_reporterEZNS0_18report_win32_errorERS2_jEUlT_E_EEvRS4_yRT0_,"x"
	.linkonce discard
	.seh_endproc
	.section	.text$_ZNK7fast_io11win32_error6reportERNS_14error_reporterE,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNK7fast_io11win32_error6reportERNS_14error_reporterE
	.def	_ZNK7fast_io11win32_error6reportERNS_14error_reporterE;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK7fast_io11win32_error6reportERNS_14error_reporterE
_ZNK7fast_io11win32_error6reportERNS_14error_reporterE:
.LFB12983:
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$104, %rsp
	.seh_stackalloc	104
	.seh_endprologue
	movl	16(%rcx), %eax
	movl	%eax, 68(%rsp)
	leaq	68(%rsp), %rax
	movq	%rdx, %r12
	movq	$32768, 72(%rsp)
	vmovq	%rax, %xmm0
	leaq	72(%rsp), %rax
	vpinsrq	$1, %rax, %xmm0, %xmm0
	movq	(%rdx), %rax
	leaq	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy(%rip), %rdx
	vmovdqa	%xmm0, 80(%rsp)
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L594
	movq	8(%r12), %rax
	movq	4104(%rax), %rbx
	leaq	32768(%rbx), %rdx
	cmpq	%rdx, 4112(%rax)
	jbe	.L597
.L595:
	testq	%rbx, %rbx
	je	.L597
	movq	80(%rsp), %rax
	movq	$0, 48(%rsp)
	movl	$32768, 40(%rsp)
	movq	%rbx, 32(%rsp)
	xorl	%edx, %edx
	movl	$4608, %ecx
	movl	$1024, %r9d
	movl	(%rax), %r8d
	call	FormatMessageA
	leaq	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc(%rip), %rcx
	movl	%eax, %eax
	leaq	(%rbx,%rax), %rdx
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L598
	movq	8(%r12), %rax
	movq	%rdx, 4104(%rax)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	ret
	.p2align 4
	.p2align 3
.L594:
	movl	$32768, %edx
	movq	%r12, %rcx
	call	*%rax
	movq	%rax, %rbx
	jmp	.L595
	.p2align 4
	.p2align 3
.L598:
	movq	%r12, %rcx
	call	*%rax
	nop
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	ret
	.p2align 4
	.p2align 3
.L597:
	leaq	80(%rsp), %r8
	movl	$32768, %edx
	movq	%r12, %rcx
	call	_ZN7fast_io7details33reserve_write_extremely_cold_pathINS_14error_reporterEZNS0_18report_win32_errorERS2_jEUlT_E_EEvRS4_yRT0_
	nop
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	ret
	.seh_endproc
	.section	.text$_ZN7fast_io7details14print_fallbackILb1ENS_23basic_win32_io_observerIcEEJRKSt9exceptionEEEvRT0_DpOT1_,"x"
	.linkonce discard
	.p2align 4
	.globl	_ZN7fast_io7details14print_fallbackILb1ENS_23basic_win32_io_observerIcEEJRKSt9exceptionEEEvRT0_DpOT1_
	.def	_ZN7fast_io7details14print_fallbackILb1ENS_23basic_win32_io_observerIcEEJRKSt9exceptionEEEvRT0_DpOT1_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details14print_fallbackILb1ENS_23basic_win32_io_observerIcEEJRKSt9exceptionEEEvRT0_DpOT1_
_ZN7fast_io7details14print_fallbackILb1ENS_23basic_win32_io_observerIcEEJRKSt9exceptionEEEvRT0_DpOT1_:
.LFB17346:
	pushq	%r15
	.seh_pushreg	%r15
	movl	$4200, %eax
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	call	___chkstk_ms
	subq	%rax, %rsp
	.seh_stackalloc	4200
	.seh_endprologue
	leaq	4160(%rsp), %rax
	leaq	64(%rsp), %rsi
	movq	%rcx, %rdi
	movq	%rdx, %rcx
	movq	%rax, 4176(%rsp)
	movq	(%rdx), %rax
	vmovq	%rsi, %xmm1
	vpunpcklqdq	%xmm1, %xmm1, %xmm0
	vmovdqa	%xmm0, 4160(%rsp)
	call	*16(%rax)
	movq	%rax, %rcx
	movq	%rax, %r12
	call	strlen
	movq	4168(%rsp), %rcx
	movq	%rax, %rbx
	movq	4176(%rsp), %rax
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %rbx
	ja	.L619
	movq	%rbx, %r8
	movq	%r12, %rdx
	call	memcpy
	addq	4168(%rsp), %rbx
	movq	4176(%rsp), %rbp
	movq	%rbx, %r8
	movq	%rbx, 4168(%rsp)
.L607:
	cmpq	%r8, %rbp
	je	.L620
	movb	$10, (%r8)
	movq	4160(%rsp), %r12
	incq	%r8
	movq	%r8, 4168(%rsp)
.L611:
	movq	(%rdi), %rcx
	subq	%r12, %r8
	movl	$4294967295, %eax
	movq	$0, 32(%rsp)
	cmpq	%rax, %r8
	leaq	60(%rsp), %r9
	movq	%r12, %rdx
	movl	$0, 60(%rsp)
	cmova	%rax, %r8
	call	WriteFile
	testl	%eax, %eax
	je	.L621
	movq	4160(%rsp), %rcx
	cmpq	%rsi, %rcx
	je	.L618
	movq	4176(%rsp), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
	nop
.L618:
	addq	$4200, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4
	.p2align 3
.L619:
	subq	4160(%rsp), %rax
	leaq	(%rax,%rax), %r13
	cmpq	%rbx, %r13
	cmovb	%rbx, %r13
	testq	%r13, %r13
	js	.L622
	movq	%r13, %rcx
.LEHB9:
	call	_Znwy
	movq	4160(%rsp), %r15
	movq	4168(%rsp), %r14
	movq	%rax, %rcx
	movq	%rax, %rbp
	subq	%r15, %r14
	movq	%r15, %rdx
	movq	%r14, %r8
	call	memcpy
	cmpq	%rsi, %r15
	je	.L606
	movq	4176(%rsp), %rdx
	movq	%r15, %rcx
	subq	%r15, %rdx
	call	_ZdlPvy
.L606:
	leaq	0(%rbp,%r14), %rcx
	movq	%rbp, 4160(%rsp)
	movq	%rbx, %r8
	addq	%r13, %rbp
	movq	%r12, %rdx
	movq	%rcx, 4168(%rsp)
	movq	%rbp, 4176(%rsp)
	call	memcpy
	leaq	(%rax,%rbx), %r8
	movq	%r8, 4168(%rsp)
	jmp	.L607
	.p2align 4
	.p2align 3
.L620:
	subq	4160(%rsp), %rbp
	addq	%rbp, %rbp
	js	.L623
	movq	%rbp, %rcx
	call	_Znwy
	movq	4160(%rsp), %r13
	movq	4168(%rsp), %rbx
	movq	%rax, %rcx
	movq	%rax, %r12
	subq	%r13, %rbx
	movq	%r13, %rdx
	movq	%rbx, %r8
	call	memcpy
	cmpq	%rsi, %r13
	je	.L610
	movq	4176(%rsp), %rdx
	movq	%r13, %rcx
	subq	%r13, %rdx
	call	_ZdlPvy
.L610:
	leaq	(%r12,%rbx), %r8
	addq	%r12, %rbp
	movq	%r12, 4160(%rsp)
	movb	$10, (%r8)
	incq	%r8
	movq	%rbp, 4176(%rsp)
	movq	%r8, 4168(%rsp)
	jmp	.L611
.L622:
	call	_ZSt17__throw_bad_allocv
.L623:
	call	_ZSt17__throw_bad_allocv
.L621:
	call	_ZN7fast_io17throw_win32_errorEv
.LEHE9:
.L616:
	movq	4160(%rsp), %rcx
	movq	%rax, %r12
	cmpq	%rsi, %rcx
	je	.L617
	movq	4176(%rsp), %rdx
	subq	%rcx, %rdx
	vzeroupper
	call	_ZdlPvy
.L615:
	movq	%r12, %rcx
.LEHB10:
	call	_Unwind_Resume
.LEHE10:
.L617:
	vzeroupper
	jmp	.L615
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA17346:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE17346-.LLSDACSB17346
.LLSDACSB17346:
	.uleb128 .LEHB9-.LFB17346
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L616-.LFB17346
	.uleb128 0
	.uleb128 .LEHB10-.LFB17346
	.uleb128 .LEHE10-.LEHB10
	.uleb128 0
	.uleb128 0
.LLSDACSE17346:
	.section	.text$_ZN7fast_io7details14print_fallbackILb1ENS_23basic_win32_io_observerIcEEJRKSt9exceptionEEEvRT0_DpOT1_,"x"
	.linkonce discard
	.seh_endproc
	.def	__main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
.LC5:
	.ascii "Usage: \0"
.LC11:
	.ascii "\12Transmitted:\0"
	.section	.text.startup,"x"
	.p2align 4
	.globl	main
	.def	main;	.scl	2;	.type	32;	.endef
	.seh_proc	main
main:
.LFB15972:
	pushq	%r15
	.seh_pushreg	%r15
	movl	$4840, %eax
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	call	___chkstk_ms
	subq	%rax, %rsp
	.seh_stackalloc	4840
	vmovaps	%xmm6, 4784(%rsp)
	.seh_savexmm	%xmm6, 4784
	vmovaps	%xmm7, 4800(%rsp)
	.seh_savexmm	%xmm7, 4800
	vmovaps	%xmm8, 4816(%rsp)
	.seh_savexmm	%xmm8, 4816
	.seh_endprologue
	movl	%ecx, %ebx
	movq	%rdx, %rsi
	call	__main
	cmpl	$2, %ebx
	je	.L625
	movl	$-12, %ecx
	movq	(%rsi), %r14
	leaq	656(%rsp), %r12
	leaq	.LC5(%rip), %rsi
	call	GetStdHandle
	movq	%r12, %rdi
	movq	%r12, 4752(%rsp)
	movl	$7, %ecx
	movq	%rax, %r13
	leaq	4752(%rsp), %rax
	leaq	663(%rsp), %r15
	movq	%rax, 4768(%rsp)
	rep movsb
	movq	%r14, %rcx
	movq	%r15, 4760(%rsp)
	call	strlen
	movq	%rax, %rbx
	cmpq	$4089, %rax
	ja	.L859
	movq	%rax, %r8
	movq	%r14, %rdx
	movq	%r15, %rcx
	call	memcpy
	addq	4760(%rsp), %rbx
.L628:
	movq	4768(%rsp), %rax
	movq	%rbx, 4760(%rsp)
	movq	%rax, %rdx
	subq	%rbx, %rdx
	cmpq	$7, %rdx
	jbe	.L860
	movabsq	$738138905234521120, %rax
	movq	%rax, (%rbx)
	movq	4760(%rsp), %rax
	leaq	8(%rax), %r8
.L632:
	movq	4752(%rsp), %rdx
	movl	$4294967295, %eax
	movq	%r8, 4760(%rsp)
	movq	$0, 32(%rsp)
	leaq	288(%rsp), %r9
	movq	%r13, %rcx
	movl	$0, 288(%rsp)
	subq	%rdx, %r8
	cmpq	%rax, %r8
	cmova	%rax, %r8
	call	WriteFile
	testl	%eax, %eax
	je	.L861
	movq	4752(%rsp), %rcx
	cmpq	%r12, %rcx
	je	.L634
	movq	4768(%rsp), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
.L634:
	movl	$1, %eax
.L624:
	vmovaps	4784(%rsp), %xmm6
	vmovaps	4800(%rsp), %xmm7
	vmovaps	4816(%rsp), %xmm8
	addq	$4840, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L625:
	leaq	288(%rsp), %r13
	call	_ZNSt6chrono3_V212system_clock3nowEv
	movl	$1, %edx
	movq	%r13, %rcx
	movq	%rax, %rdi
.LEHB11:
	call	_ZN8CryptoPP9AlgorithmC2Eb
.LEHE11:
	vpxor	%xmm0, %xmm0, %xmm0
	leaq	480(%rsp), %rbp
	leaq	312(%rsp), %rbx
	leaq	16+_ZTVN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EEE(%rip), %rax
	vmovdqu	%xmm0, 296(%rsp)
	vmovdqa	.LC6(%rip), %xmm0
	movq	%rbp, %rcx
	movb	$1, 441(%rsp)
	movq	%rbx, 464(%rsp)
	movq	%rax, 288(%rsp)
	movb	$1, 609(%rsp)
	movq	%rbp, 640(%rsp)
	vmovdqa	%xmm0, 448(%rsp)
	vmovdqa	%xmm0, 624(%rsp)
.LEHB12:
	call	_ZN8CryptoPP6SHA5129InitStateEPy
.LEHE12:
	vpxor	%xmm0, %xmm0, %xmm0
	movq	%r13, 120(%rsp)
	movq	$0, 208(%rsp)
	vmovdqa	%xmm0, 192(%rsp)
	movq	.refptr._ZTVN8CryptoPP6SHA512E(%rip), %rax
	movq	8(%rsi), %rbx
	addq	$16, %rax
	movq	%rbx, %rcx
	movq	%rax, 288(%rsp)
	call	strlen
	movq	%rax, %rbp
	cmpq	$510, %rax
	ja	.L643
	leaq	(%rbx,%rax), %r8
	leaq	656(%rsp), %r12
	leaq	16(%rbx), %rax
	movq	%r12, %r10
	cmpq	%rax, %r8
	jbe	.L644
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE15first_unit_infoE(%rip), %r11
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE11transitionsE(%rip), %r9
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE14octet_categoryE(%rip), %rcx
	vpxor	%xmm1, %xmm1, %xmm1
	.p2align 4
	.p2align 3
.L645:
	movzbl	(%rbx), %eax
	testb	$-128, %al
	je	.L862
	leaq	(%r11,%rax,2), %rdx
	incq	%rbx
	movzbl	(%rdx), %eax
	movzbl	1(%rdx), %ebp
	movzbl	%al, %edx
	cmpl	$12, %ebp
	jle	.L651
	cmpq	%rbx, %r8
	jbe	.L653
	.p2align 4
	.p2align 3
.L652:
	movzbl	(%rbx), %eax
	sall	$6, %edx
	incq	%rbx
	movl	%eax, %r14d
	movzbl	(%rcx,%rax), %eax
	andl	$63, %r14d
	orl	%r14d, %edx
	addl	%ebp, %eax
	cltq
	movzbl	(%r9,%rax), %ebp
	cmpl	$12, %ebp
	jle	.L863
	cmpq	%rbx, %r8
	jne	.L652
.L653:
	movl	$32, %ecx
	call	__cxa_allocate_exception
	leaq	_ZN7fast_io18fast_io_text_errorD1Ev(%rip), %r8
	leaq	_ZTIN7fast_io18fast_io_text_errorE(%rip), %rdx
	movq	%rax, %rcx
	movq	$0, 8(%rax)
	leaq	16+_ZTVN7fast_io18fast_io_text_errorE(%rip), %rax
	movq	%rax, (%rcx)
	leaq	.LC0(%rip), %rax
	movq	$12, 16(%rcx)
	movq	%rax, 24(%rcx)
.LEHB13:
	call	__cxa_throw
	.p2align 4
	.p2align 3
.L863:
	cmpb	$12, %bpl
	je	.L653
	movl	%edx, %eax
	cmpl	$65535, %edx
	ja	.L655
.L756:
	movl	$2, %edx
.L656:
	movw	%ax, (%r10)
	addq	%rdx, %r10
.L650:
	leaq	16(%rbx), %rax
	cmpq	%rax, %r8
	ja	.L645
.L644:
	cmpq	%rbx, %r8
	jbe	.L646
	movsbw	(%rbx), %ax
	movq	%r10, %r14
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE15first_unit_infoE(%rip), %r15
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE11transitionsE(%rip), %rbp
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE14octet_categoryE(%rip), %r11
	incq	%rbx
	testw	%ax, %ax
	jns	.L864
	.p2align 4
	.p2align 3
.L657:
	movzbl	%al, %edx
	leaq	(%r15,%rdx,2), %rax
	movzbl	(%rax), %edx
	movzbl	1(%rax), %eax
	movl	%eax, %r9d
	cmpl	$12, %eax
	jle	.L659
	cmpq	%rbx, %r8
	jbe	.L661
	.p2align 4
	.p2align 3
.L660:
	movzbl	(%rbx), %ecx
	sall	$6, %edx
	incq	%rbx
	movl	%ecx, %eax
	andl	$63, %eax
	orl	%eax, %edx
	movzbl	(%r11,%rcx), %eax
	addl	%r9d, %eax
	cltq
	movzbl	0(%rbp,%rax), %r9d
	cmpl	$12, %r9d
	jle	.L865
	cmpq	%rbx, %r8
	jne	.L660
.L661:
	call	_ZN7fast_io7details3utf24utf_code_convert_detailsILb0EPKcPwEET1_RT0_S7_S6_.part.0
.LEHE13:
	.p2align 4
	.p2align 3
.L865:
	cmpb	$12, %r9b
	je	.L661
	movl	%edx, %eax
	cmpl	$65535, %edx
	ja	.L663
.L755:
	movl	$2, %edx
.L664:
	addq	%rdx, %r10
.L658:
	movw	%ax, (%r14)
	cmpq	%rbx, %r8
	jbe	.L646
	movsbw	(%rbx), %ax
	movq	%r10, %r14
	incq	%rbx
	testw	%ax, %ax
	js	.L657
.L864:
	addq	$2, %r10
	jmp	.L658
	.p2align 4
	.p2align 3
.L655:
	movl	%edx, %ebp
	andw	$1023, %dx
	leal	-9216(%rdx), %eax
	shrl	$10, %ebp
	movl	$4, %edx
	movw	%ax, 2(%r10)
	leal	-10304(%rbp), %eax
	jmp	.L656
	.p2align 4
	.p2align 3
.L663:
	movl	%edx, %ecx
	andw	$1023, %dx
	leal	-9216(%rdx), %eax
	shrl	$10, %ecx
	movl	$4, %edx
	movw	%ax, 2(%r10)
	leal	-10304(%rcx), %eax
	jmp	.L664
.L646:
	movw	$0, (%r10)
	xorl	%r9d, %r9d
	movq	$0, 48(%rsp)
	movl	$268435584, 40(%rsp)
	movl	$3, 32(%rsp)
	movl	$3, %r8d
	movl	$-2147483648, %edx
	movq	%r12, %rcx
	call	CreateFileW
	movq	%rax, %rbx
	cmpq	$-1, %rax
	je	.L866
.L666:
	leaq	192(%rsp), %r14
	leaq	120(%rsp), %rcx
	movq	%rbx, 216(%rsp)
	movq	%r14, %rdx
.LEHB14:
	call	_ZN7fast_io7details23bufferred_transmit_implINS_8cryptopp17iterated_hash_refIN8CryptoPP6SHA512EEENS_10basic_ibufINS_18basic_file_wrapperINS_16basic_win32_fileIcEELNS_9open_modeE4104EEENS_17basic_buf_handlerIcLb0ELy1048576ENS_20io_aligned_allocatorIcLy4096EEEEEEEEEyRT_RT0_
	movq	%rax, %rbp
	call	_ZNSt6chrono3_V212system_clock3nowEv
	movq	120(%rsp), %rcx
	leaq	224(%rsp), %rdx
	subq	%rdi, %rax
	movq	8(%rsi), %r15
	movl	$64, %r8d
	movq	%rax, 80(%rsp)
	movq	(%rcx), %rax
	call	*128(%rax)
	movl	$1, %ecx
	call	*__imp___acrt_iob_func(%rip)
.LEHE14:
	movq	%rax, %rbx
	movq	__imp___iob_func(%rip), %rax
	movq	%rax, 72(%rsp)
	call	*%rax
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L694
	sarq	$4, %rax
	imull	$-1431655765, %eax, %eax
	leal	16(%rax), %ecx
	call	_lock
	orl	$32768, 24(%rbx)
.L695:
	vmovdqu	224(%rsp), %ymm3
	vmovdqa	.LC7(%rip), %ymm6
	leaq	656(%rsp), %r12
	leaq	4752(%rsp), %rax
	vpand	224(%rsp), %ymm6, %ymm2
	leaq	786(%rsp), %rsi
	movq	%r15, %rcx
	movq	%r12, 4752(%rsp)
	vmovdqa	.LC9(%rip), %ymm0
	movq	%rax, 4768(%rsp)
	movw	$10784, 784(%rsp)
	movq	%rsi, 4760(%rsp)
	vmovdqa	.LC10(%rip), %ymm4
	vpsrlw	$4, %ymm3, %ymm1
	vmovdqa	.LC8(%rip), %ymm3
	vpand	%ymm1, %ymm6, %ymm1
	vpaddb	%ymm0, %ymm2, %ymm5
	vpaddb	%ymm4, %ymm2, %ymm7
	vpaddb	%ymm4, %ymm1, %ymm8
	vpcmpgtb	%ymm3, %ymm2, %ymm2
	vpblendvb	%ymm2, %ymm5, %ymm7, %ymm7
	vpaddb	%ymm0, %ymm1, %ymm2
	vpxor	%xmm5, %xmm5, %xmm5
	vpsubusb	%ymm3, %ymm1, %ymm1
	vpcmpeqb	%ymm5, %ymm1, %ymm1
	vpblendvb	%ymm1, %ymm8, %ymm2, %ymm1
	vpunpcklbw	%ymm7, %ymm1, %ymm2
	vpunpckhbw	%ymm7, %ymm1, %ymm1
	vperm2i128	$32, %ymm1, %ymm2, %ymm7
	vperm2i128	$49, %ymm1, %ymm2, %ymm2
	vmovdqu	%ymm7, 656(%rsp)
	vmovdqu	%ymm2, 688(%rsp)
	vmovdqu	256(%rsp), %ymm7
	vpand	256(%rsp), %ymm6, %ymm2
	vpsrlw	$4, %ymm7, %ymm1
	vpaddb	%ymm0, %ymm2, %ymm7
	vpand	%ymm1, %ymm6, %ymm1
	vpaddb	%ymm4, %ymm2, %ymm6
	vpcmpgtb	%ymm3, %ymm2, %ymm2
	vpaddb	%ymm0, %ymm1, %ymm0
	vpblendvb	%ymm2, %ymm7, %ymm6, %ymm6
	vpaddb	%ymm4, %ymm1, %ymm2
	vpsubusb	%ymm3, %ymm1, %ymm1
	vpcmpeqb	%ymm5, %ymm1, %ymm1
	vpblendvb	%ymm1, %ymm2, %ymm0, %ymm0
	vpunpcklbw	%ymm6, %ymm0, %ymm1
	vpunpckhbw	%ymm6, %ymm0, %ymm0
	vperm2i128	$32, %ymm0, %ymm1, %ymm2
	vperm2i128	$49, %ymm0, %ymm1, %ymm1
	vmovdqu	%ymm2, 720(%rsp)
	vmovdqu	%ymm1, 752(%rsp)
	vzeroupper
	call	strlen
	movq	%rax, %rdi
	cmpq	$3966, %rax
	ja	.L867
	movq	%rax, %r8
	movq	%r15, %rdx
	movq	%rsi, %rcx
	call	memcpy
	movq	4760(%rsp), %rax
	addq	%rdi, %rax
.L698:
	movq	4768(%rsp), %rdx
	movq	%rax, 4760(%rsp)
	movq	%rdx, %rcx
	subq	%rax, %rcx
	cmpq	$12, %rcx
	jbe	.L868
	movq	%rax, %rdi
	leaq	.LC11(%rip), %rsi
	movl	$13, %ecx
	rep movsb
	movq	4760(%rsp), %rax
	leaq	13(%rax), %r9
.L702:
	movq	4768(%rsp), %rsi
	leaq	20(%r9), %r8
	movq	%r9, 4760(%rsp)
	movabsq	$-8446744073709551617, %rax
	cmpq	%r8, %rsi
	jbe	.L703
	cmpq	%rax, %rbp
	ja	.L759
	movabsq	$999999999999999999, %rax
	cmpq	%rax, %rbp
	ja	.L763
	movabsq	$99999999999999999, %rax
	cmpq	%rax, %rbp
	ja	.L764
	movabsq	$9999999999999999, %rax
	cmpq	%rax, %rbp
	ja	.L765
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rbp
	ja	.L766
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rbp
	ja	.L767
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rbp
	ja	.L768
	movabsq	$999999999999, %rax
	cmpq	%rax, %rbp
	ja	.L769
	movabsq	$99999999999, %rax
	cmpq	%rax, %rbp
	ja	.L770
	movabsq	$9999999999, %rax
	cmpq	%rax, %rbp
	ja	.L771
	cmpq	$999999999, %rbp
	ja	.L772
	cmpq	$99999999, %rbp
	ja	.L773
	cmpq	$9999999, %rbp
	ja	.L774
	cmpq	$999999, %rbp
	ja	.L775
	cmpq	$99999, %rbp
	ja	.L776
	cmpq	$9999, %rbp
	ja	.L777
	cmpq	$999, %rbp
	ja	.L778
	cmpq	$99, %rbp
	ja	.L706
	cmpq	$10, %rbp
	sbbl	%eax, %eax
	leal	2(%rax), %r8d
	addq	%r9, %r8
	movq	%r8, %r10
	jmp	.L709
.L763:
	movl	$19, %eax
.L705:
	leaq	(%r9,%rax), %r8
	cmpq	$99, %rbp
	jbe	.L779
.L759:
	movq	%r8, %rcx
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %r9
	.p2align 4
	.p2align 3
.L710:
	movq	%rbp, %rdx
	subq	$2, %rcx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%r9
	movq	%rdx, %rax
	shrq	$2, %rdx
	shrq	$2, %rax
	imulq	$100, %rax, %r11
	movq	%rbp, %rax
	subq	%r11, %rax
	movq	%rbp, %r11
	movq	%rdx, %rbp
	movzwl	(%r10,%rax,2), %eax
	movw	%ax, (%rcx)
	cmpq	$9999, %r11
	ja	.L710
	movq	%r8, %r10
	movq	%rcx, %r8
.L709:
	cmpq	$9, %rbp
	jbe	.L711
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rbp,2), %eax
	movw	%ax, -2(%r8)
.L712:
	movq	%r10, 4760(%rsp)
.L713:
	movq	4768(%rsp), %rdx
	movq	4760(%rsp), %rax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	cmpq	$19, %rcx
	jbe	.L869
	movabsq	$4974634142990426656, %rdi
	movabsq	$6061955485331906924, %rbp
	movl	$979725673, 16(%rax)
	movq	%rdi, (%rax)
	movq	%rbp, 8(%rax)
	movq	4760(%rsp), %rax
	leaq	20(%rax), %rcx
.L726:
	vxorps	%xmm1, %xmm1, %xmm1
	movq	%rcx, 4760(%rsp)
	leaq	30(%rcx), %rax
	vcvtsi2sdq	80(%rsp), %xmm1, %xmm1
	vmulsd	.LC12(%rip), %xmm1, %xmm1
	cmpq	%rax, 4768(%rsp)
	jbe	.L727
	call	_ZN7fast_io7details3ryu15output_shortestILb0ELy0ELb1ELb0EDuLDu46EPcdEET5_NS0_27compile_time_floating_valueIXT2_ET3_XT4_EEES4_T6_.isra.0
	movq	%rax, 4760(%rsp)
.L728:
	movq	4760(%rsp), %rax
	cmpq	4768(%rsp), %rax
	je	.L870
	movb	$115, (%rax)
	incq	%rax
.L736:
	movq	%rax, 4760(%rsp)
	cmpq	%rax, 4768(%rsp)
	je	.L871
	movb	$10, (%rax)
	leaq	1(%rax), %r8
.L740:
	movq	4752(%rsp), %r15
	movq	%r8, 4760(%rsp)
	movslq	36(%rbx), %rax
	movq	(%rbx), %rcx
	addq	16(%rbx), %rax
	subq	%r15, %r8
	leaq	(%rcx,%r8), %rsi
	cmpq	%rax, %rsi
	jnb	.L741
	movq	%r15, %rdx
	call	memcpy
	movq	%rsi, %rax
	subq	(%rbx), %rax
	movq	%rsi, (%rbx)
	orl	$65536, 24(%rbx)
	subl	%eax, 8(%rbx)
.L742:
	movq	4752(%rsp), %rcx
	cmpq	%r12, %rcx
	je	.L743
	movq	4768(%rsp), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
.L743:
	movq	72(%rsp), %rax
	call	*%rax
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L872
	sarq	$4, %rax
	andl	$-32769, 24(%rbx)
	imull	$-1431655765, %eax, %eax
	leal	16(%rax), %ecx
	call	_unlock
.L750:
	movq	%r14, %rcx
	call	_ZN7fast_io10basic_ibufINS_18basic_file_wrapperINS_16basic_win32_fileIcEELNS_9open_modeE4104EEENS_17basic_buf_handlerIcLb0ELy1048576ENS_20io_aligned_allocatorIcLy4096EEEEEED1Ev
	movq	.refptr._ZTVN8CryptoPP6SHA512E(%rip), %rax
	movq	%r13, %rcx
	addq	$16, %rax
	movq	%rax, 288(%rsp)
	call	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EED2Ev
	xorl	%eax, %eax
	jmp	.L624
.L862:
	vmovdqu	(%rbx), %xmm0
	vpmovmskb	%xmm0, %edx
	vpunpcklbw	%xmm1, %xmm0, %xmm2
	vpunpckhbw	%xmm1, %xmm0, %xmm0
	vmovdqu	%xmm2, (%r10)
	vmovdqu	%xmm0, 16(%r10)
	movzwl	%dx, %eax
	testw	%dx, %dx
	je	.L761
	tzcntl	%eax, %eax
.L649:
	cltq
	addq	%rax, %rbx
	addq	%rax, %rax
	addq	%rax, %r10
	jmp	.L650
.L872:
	leaq	48(%rbx), %rcx
	call	LeaveCriticalSection
	jmp	.L750
.L694:
	leaq	48(%rbx), %rcx
	call	EnterCriticalSection
	jmp	.L695
.L706:
	movq	%rbp, %rax
	movl	$100, %ecx
	xorl	%edx, %edx
	leaq	3(%r9), %r10
	divq	%rcx
	leaq	1(%r9), %r8
	movq	%rax, %rbp
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rdx,2), %eax
	movw	%ax, 1(%r9)
.L711:
	addl	$48, %ebp
	movb	%bpl, -1(%r8)
	jmp	.L712
.L659:
	cmpb	$12, %al
	je	.L661
	movzbl	%dl, %eax
	jmp	.L755
.L651:
	cmpb	$12, %bpl
	jne	.L756
	jmp	.L653
.L761:
	movl	$16, %eax
	jmp	.L649
.L764:
	movl	$18, %eax
	jmp	.L705
.L859:
	cmpq	$8192, %rax
	movl	$8192, %edi
	cmovnb	%rax, %rdi
	movq	%rdi, %rcx
.LEHB15:
	call	_Znwy
.LEHE15:
	movq	4752(%rsp), %r15
	movq	4760(%rsp), %rbp
	movq	%rax, %rcx
	movq	%rax, %rsi
	subq	%r15, %rbp
	movq	%r15, %rdx
	movq	%rbp, %r8
	call	memcpy
	cmpq	%r12, %r15
	je	.L627
	movq	4768(%rsp), %rdx
	movq	%r15, %rcx
	subq	%r15, %rdx
	call	_ZdlPvy
.L627:
	leaq	(%rsi,%rbp), %rcx
	movq	%rsi, 4752(%rsp)
	movq	%rbx, %r8
	addq	%rdi, %rsi
	movq	%r14, %rdx
	movq	%rcx, 4760(%rsp)
	movq	%rsi, 4768(%rsp)
	call	memcpy
	addq	%rax, %rbx
	jmp	.L628
.L779:
	movq	%r8, %r10
	jmp	.L709
.L765:
	movl	$17, %eax
	jmp	.L705
.L770:
	movl	$12, %eax
	jmp	.L705
.L703:
	cmpq	%rax, %rbp
	ja	.L780
	movabsq	$999999999999999999, %rax
	cmpq	%rax, %rbp
	ja	.L781
	movabsq	$99999999999999999, %rax
	cmpq	%rax, %rbp
	ja	.L782
	movabsq	$9999999999999999, %rax
	cmpq	%rax, %rbp
	ja	.L783
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rbp
	ja	.L784
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rbp
	ja	.L785
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rbp
	ja	.L786
	movabsq	$999999999999, %rax
	cmpq	%rax, %rbp
	ja	.L787
	movabsq	$99999999999, %rax
	cmpq	%rax, %rbp
	ja	.L788
	movabsq	$9999999999, %rax
	cmpq	%rax, %rbp
	ja	.L789
	cmpq	$999999999, %rbp
	ja	.L790
	cmpq	$99999999, %rbp
	ja	.L791
	cmpq	$9999999, %rbp
	ja	.L792
	cmpq	$999999, %rbp
	ja	.L793
	cmpq	$99999, %rbp
	ja	.L794
	cmpq	$9999, %rbp
	ja	.L795
	cmpq	$999, %rbp
	ja	.L796
	cmpq	$99, %rbp
	ja	.L797
	cmpq	$10, %rbp
	sbbl	%eax, %eax
	addl	$2, %eax
.L714:
	movl	%eax, %r15d
	leaq	128(%rsp), %rax
	movl	$100, %r8d
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movq	%rax, 88(%rsp)
	leaq	(%rax,%r15), %rcx
	movq	%rbp, %rax
	jmp	.L716
.L873:
	xorl	%edx, %edx
	subq	$2, %rcx
	divq	%r8
	movzwl	(%r10,%rdx,2), %edx
	movw	%dx, (%rcx)
.L716:
	cmpq	$99, %rax
	ja	.L873
	movq	%rax, %rbp
	cmpq	$9, %rax
	jbe	.L717
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rbp,2), %eax
	movw	%ax, -2(%rcx)
.L718:
	movq	%rsi, %rax
	subq	%r9, %rax
	cmpq	%r15, %rax
	jb	.L874
	movq	%r15, %rcx
	movq	88(%rsp), %rsi
	movq	%r9, %rdi
	rep movsb
	addq	4760(%rsp), %r15
.L722:
	movq	%r15, 4760(%rsp)
	jmp	.L713
.L870:
	subq	4752(%rsp), %rax
	addq	%rax, %rax
	movq	%rax, %rdi
	js	.L875
	movq	%rax, %rcx
.LEHB16:
	call	_Znwy
	movq	4752(%rsp), %r15
	movq	4760(%rsp), %rsi
	movq	%rax, %rcx
	movq	%rax, %rbp
	subq	%r15, %rsi
	movq	%r15, %rdx
	movq	%rsi, %r8
	call	memcpy
	cmpq	%r12, %r15
	je	.L735
	movq	4768(%rsp), %rdx
	movq	%r15, %rcx
	subq	%r15, %rdx
	call	_ZdlPvy
.L735:
	leaq	0(%rbp,%rsi), %rax
	movq	%rbp, 4752(%rsp)
	addq	%rdi, %rbp
	movq	%rbp, 4768(%rsp)
	incq	%rax
	movb	$115, -1(%rax)
	jmp	.L736
.L871:
	subq	4752(%rsp), %rax
	addq	%rax, %rax
	movq	%rax, %rdi
	js	.L876
	movq	%rax, %rcx
	call	_Znwy
	movq	4752(%rsp), %r15
	movq	4760(%rsp), %rsi
	movq	%rax, %rcx
	movq	%rax, %rbp
	subq	%r15, %rsi
	movq	%r15, %rdx
	movq	%rsi, %r8
	call	memcpy
	cmpq	%r12, %r15
	je	.L739
	movq	4768(%rsp), %rdx
	movq	%r15, %rcx
	subq	%r15, %rdx
	call	_ZdlPvy
.L739:
	leaq	0(%rbp,%rsi), %r8
	movq	%rbp, 4752(%rsp)
	addq	%rdi, %rbp
	movb	$10, (%r8)
	movq	%rbp, 4768(%rsp)
	incq	%r8
	jmp	.L740
.L867:
	cmpq	$8192, %rax
	movl	$8192, %eax
	cmovnb	%rdi, %rax
	movq	%rax, %rcx
	movq	%rax, 88(%rsp)
	call	_Znwy
	movq	4752(%rsp), %r10
	movq	%rax, %rsi
	movq	4760(%rsp), %rax
	movq	%rsi, %rcx
	subq	%r10, %rax
	movq	%r10, %rdx
	movq	%r10, 104(%rsp)
	movq	%rax, %r8
	movq	%rax, 96(%rsp)
	call	memcpy
	movq	104(%rsp), %r10
	cmpq	%r12, %r10
	je	.L697
	movq	4768(%rsp), %rdx
	movq	%r10, %rcx
	subq	%r10, %rdx
	call	_ZdlPvy
.L697:
	movq	96(%rsp), %rcx
	movq	88(%rsp), %r9
	movq	%rdi, %r8
	movq	%r15, %rdx
	movq	%rsi, 4752(%rsp)
	addq	%rsi, %rcx
	addq	%rsi, %r9
	movq	%rcx, 4760(%rsp)
	movq	%r9, 4768(%rsp)
	call	memcpy
	addq	%rdi, %rax
	jmp	.L698
.L868:
	subq	4752(%rsp), %rdx
	movl	$13, %eax
	addq	%rdx, %rdx
	cmpq	$13, %rdx
	cmovnb	%rdx, %rax
	movq	%rax, %rsi
	testq	%rax, %rax
	js	.L877
	movq	%rax, %rcx
	call	_Znwy
.LEHE16:
	movq	4752(%rsp), %r9
	movq	4760(%rsp), %rdi
	movq	%rax, %rcx
	movq	%rax, %r15
	subq	%r9, %rdi
	movq	%r9, %rdx
	movq	%r9, 88(%rsp)
	movq	%rdi, %r8
	call	memcpy
	movq	88(%rsp), %r9
	cmpq	%r12, %r9
	je	.L701
	movq	4768(%rsp), %rdx
	movq	%r9, %rcx
	subq	%r9, %rdx
	call	_ZdlPvy
.L701:
	leaq	(%r15,%rdi), %r9
	movq	%r15, 4752(%rsp)
	movl	$13, %ecx
	addq	%rsi, %r15
	movq	%r9, %rdi
	leaq	.LC11(%rip), %rsi
	rep movsb
	movq	%r15, 4768(%rsp)
	movq	%rdi, %r9
	jmp	.L702
.L643:
	leaq	1(%rax), %rcx
	movabsq	$4611686018427387900, %rax
	cmpq	%rax, %rcx
	ja	.L667
	addq	%rcx, %rcx
.LEHB17:
	call	_Znay
.LEHE17:
	movq	%rax, %r12
	leaq	(%rbx,%rbp), %rcx
	movq	%rax, %r8
	vpxor	%xmm1, %xmm1, %xmm1
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE15first_unit_infoE(%rip), %r11
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE11transitionsE(%rip), %r10
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE14octet_categoryE(%rip), %r9
.L668:
	leaq	16(%rbx), %rax
	cmpq	%rax, %rcx
	jbe	.L878
	movzbl	(%rbx), %eax
	testb	$-128, %al
	je	.L879
	addq	%rax, %rax
	incq	%rbx
	addq	%r11, %rax
	movzbl	1(%rax), %ebp
	movzbl	(%rax), %edx
	cmpl	$12, %ebp
	jle	.L673
	cmpq	%rbx, %rcx
	jbe	.L675
	.p2align 4
	.p2align 3
.L674:
	movzbl	(%rbx), %eax
	sall	$6, %edx
	incq	%rbx
	movl	%eax, %r14d
	movzbl	(%r9,%rax), %eax
	andl	$63, %r14d
	orl	%r14d, %edx
	addl	%ebp, %eax
	cltq
	movzbl	(%r10,%rax), %ebp
	cmpl	$12, %ebp
	jle	.L880
	cmpq	%rbx, %rcx
	jne	.L674
.L675:
	movl	$32, %ecx
	call	__cxa_allocate_exception
	leaq	_ZN7fast_io18fast_io_text_errorD1Ev(%rip), %r8
	leaq	_ZTIN7fast_io18fast_io_text_errorE(%rip), %rdx
	movq	%rax, %rcx
	movq	$0, 8(%rax)
	leaq	16+_ZTVN7fast_io18fast_io_text_errorE(%rip), %rax
	movq	%rax, (%rcx)
	leaq	.LC0(%rip), %rax
	movq	$12, 16(%rcx)
	movq	%rax, 24(%rcx)
.LEHB18:
	call	__cxa_throw
.L880:
	cmpb	$12, %bpl
	je	.L675
	cmpl	$65535, %edx
	ja	.L677
.L758:
	movl	%edx, %ebp
	movl	$1, %eax
.L678:
	addq	%rax, %rax
	movw	%bp, (%r8)
	addq	%rax, %r8
	jmp	.L668
.L677:
	movl	%edx, %eax
	movl	%edx, %ebp
	andw	$1023, %ax
	shrl	$10, %ebp
	subw	$9216, %ax
	subw	$10304, %bp
	movw	%ax, 2(%r8)
	movl	$2, %eax
	jmp	.L678
.L878:
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE15first_unit_infoE(%rip), %r11
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE11transitionsE(%rip), %r10
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE14octet_categoryE(%rip), %r9
.L669:
	cmpq	%rbx, %rcx
	jbe	.L679
	movsbw	(%rbx), %dx
	incq	%rbx
	testw	%dx, %dx
	jns	.L881
	movzbl	%dl, %eax
	addq	%rax, %rax
	addq	%r11, %rax
	movzbl	1(%rax), %ebp
	movzbl	(%rax), %edx
	cmpl	$12, %ebp
	jle	.L682
	cmpq	%rbx, %rcx
	jbe	.L684
	.p2align 4
	.p2align 3
.L683:
	movzbl	(%rbx), %eax
	sall	$6, %edx
	incq	%rbx
	movl	%eax, %r14d
	movzbl	(%r9,%rax), %eax
	andl	$63, %r14d
	orl	%r14d, %edx
	addl	%ebp, %eax
	cltq
	movzbl	(%r10,%rax), %ebp
	cmpl	$12, %ebp
	jle	.L882
	cmpq	%rbx, %rcx
	jne	.L683
.L684:
	call	_ZN7fast_io7details3utf24utf_code_convert_detailsILb0EPKcPwEET1_RT0_S7_S6_.part.0
.LEHE18:
.L882:
	cmpb	$12, %bpl
	je	.L684
	cmpl	$65535, %edx
	ja	.L686
.L757:
	movl	%edx, %ebp
	movl	$1, %eax
.L687:
	addq	%rax, %rax
	addq	%r8, %rax
.L681:
	movw	%bp, (%r8)
	movq	%rax, %r8
	jmp	.L669
.L686:
	movl	%edx, %eax
	movl	%edx, %ebp
	andw	$1023, %ax
	shrl	$10, %ebp
	subw	$9216, %ax
	subw	$10304, %bp
	movw	%ax, 2(%r8)
	movl	$2, %eax
	jmp	.L687
.L679:
	movw	$0, (%r8)
	xorl	%r9d, %r9d
	movq	$0, 48(%rsp)
	movl	$268435584, 40(%rsp)
	movl	$3, 32(%rsp)
	movl	$3, %r8d
	movl	$-2147483648, %edx
	movq	%r12, %rcx
	call	CreateFileW
	movq	%rax, %rbx
	cmpq	$-1, %rax
	je	.L883
	movq	%r12, %rcx
	call	_ZdaPv
	jmp	.L666
.L869:
	subq	4752(%rsp), %rdx
	movl	$20, %eax
	addq	%rdx, %rdx
	cmpq	$20, %rdx
	cmovnb	%rdx, %rax
	movq	%rax, %rsi
	testq	%rax, %rax
	js	.L884
	movq	%rax, %rcx
.LEHB19:
	call	_Znwy
.LEHE19:
	movq	4752(%rsp), %r15
	movq	4760(%rsp), %rdi
	movq	%rax, %rcx
	movq	%rax, %rbp
	subq	%r15, %rdi
	movq	%r15, %rdx
	movq	%rdi, %r8
	call	memcpy
	cmpq	%r12, %r15
	je	.L725
	movq	4768(%rsp), %rdx
	movq	%r15, %rcx
	subq	%r15, %rdx
	call	_ZdlPvy
.L725:
	leaq	0(%rbp,%rdi), %rcx
	movq	%rbp, 4752(%rsp)
	movabsq	$4974634142990426656, %rax
	addq	%rsi, %rbp
	movabsq	$6061955485331906924, %rdx
	movq	%rax, (%rcx)
	movl	$979725673, 16(%rcx)
	movq	%rdx, 8(%rcx)
	movq	%rbp, 4768(%rsp)
	addq	$20, %rcx
	jmp	.L726
.L860:
	subq	4752(%rsp), %rax
	movq	%rax, %rsi
	addq	%rax, %rsi
	movl	$8, %eax
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	testq	%rsi, %rsi
	js	.L885
	movq	%rsi, %rcx
.LEHB20:
	call	_Znwy
.LEHE20:
	movq	4752(%rsp), %r14
	movq	4760(%rsp), %rbx
	movq	%rax, %rcx
	movq	%rax, %rdi
	subq	%r14, %rbx
	movq	%r14, %rdx
	movq	%rbx, %r8
	call	memcpy
	cmpq	%r12, %r14
	je	.L631
	movq	4768(%rsp), %rdx
	movq	%r14, %rcx
	subq	%r14, %rdx
	call	_ZdlPvy
.L631:
	leaq	(%rdi,%rbx), %r8
	movq	%rdi, 4752(%rsp)
	movabsq	$738138905234521120, %rax
	addq	%rsi, %rdi
	movq	%rax, (%r8)
	addq	$8, %r8
	movq	%rdi, 4768(%rsp)
	jmp	.L632
.L741:
	movq	%rbx, %rcx
	movq	%r8, 80(%rsp)
	call	clearerr
	movq	%rbx, %r9
	movl	$1, %edx
	movq	%r15, %rcx
	movq	80(%rsp), %r8
.LEHB21:
	call	fwrite
	movq	%rbx, %rcx
	call	ferror
	movl	%eax, %edi
	call	*__imp__errno(%rip)
	movq	%rbx, %rcx
	movl	(%rax), %esi
	call	clearerr
	testl	%edi, %edi
	je	.L742
	movl	$24, %ecx
	call	__cxa_allocate_exception
	leaq	_ZN7fast_io11posix_errorD1Ev(%rip), %r8
	leaq	_ZTIN7fast_io11posix_errorE(%rip), %rdx
	movq	%rax, %rcx
	movq	$0, 8(%rax)
	leaq	16+_ZTVN7fast_io11posix_errorE(%rip), %rax
	movq	%rax, (%rcx)
	movl	%esi, 16(%rcx)
	call	__cxa_throw
.L727:
	leaq	160(%rsp), %r15
	movq	%r15, %rcx
	call	_ZN7fast_io7details3ryu15output_shortestILb0ELy0ELb1ELb0EDuLDu46EPcdEET5_NS0_27compile_time_floating_valueIXT2_ET3_XT4_EEES4_T6_.isra.0
	movq	4760(%rsp), %rcx
	subq	%r15, %rax
	movq	%rax, %rsi
	movq	4768(%rsp), %rax
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %rsi
	ja	.L886
	movq	%rsi, %r8
	movq	%r15, %rdx
	call	memcpy
	addq	4760(%rsp), %rsi
.L732:
	movq	%rsi, 4760(%rsp)
	jmp	.L728
.L879:
	vmovdqu	(%rbx), %xmm0
	vpmovmskb	%xmm0, %edx
	vpunpcklbw	%xmm1, %xmm0, %xmm2
	vpunpckhbw	%xmm1, %xmm0, %xmm0
	vmovdqu	%xmm2, (%r8)
	vmovdqu	%xmm0, 16(%r8)
	movzwl	%dx, %eax
	testw	%dx, %dx
	je	.L762
	tzcntl	%eax, %eax
.L671:
	cltq
	addq	%rax, %rbx
	addq	%rax, %rax
	addq	%rax, %r8
	jmp	.L668
.L881:
	movl	%edx, %ebp
	leaq	2(%r8), %rax
	jmp	.L681
.L766:
	movl	$16, %eax
	jmp	.L705
.L767:
	movl	$15, %eax
	jmp	.L705
.L768:
	movl	$14, %eax
	jmp	.L705
.L717:
	addl	$48, %ebp
	movb	%bpl, -1(%rcx)
	jmp	.L718
.L769:
	movl	$13, %eax
	jmp	.L705
.L682:
	cmpb	$12, %bpl
	jne	.L757
	jmp	.L684
.L673:
	cmpb	$12, %bpl
	jne	.L758
	jmp	.L675
.L886:
	subq	4752(%rsp), %rax
	movq	%rax, %rdi
	addq	%rax, %rdi
	cmpq	%rsi, %rdi
	cmovb	%rsi, %rdi
	testq	%rdi, %rdi
	js	.L887
	movq	%rdi, %rcx
	call	_Znwy
	movq	4752(%rsp), %r10
	movq	4760(%rsp), %rbp
	movq	%rax, %rcx
	subq	%r10, %rbp
	movq	%r10, %rdx
	movq	%r10, 80(%rsp)
	movq	%rbp, %r8
	call	memcpy
	movq	80(%rsp), %r10
	movq	%rax, %r9
	cmpq	%r12, %r10
	je	.L731
	movq	4768(%rsp), %rdx
	movq	%r10, %rcx
	movq	%rax, 80(%rsp)
	subq	%r10, %rdx
	call	_ZdlPvy
	movq	80(%rsp), %r9
.L731:
	addq	%r9, %rbp
	movq	%r9, 4752(%rsp)
	movq	%rsi, %r8
	addq	%rdi, %r9
	movq	%r15, %rdx
	movq	%rbp, %rcx
	addq	%rbp, %rsi
	movq	%r9, 4768(%rsp)
	call	memcpy
	jmp	.L732
.L783:
	movl	$17, %eax
	jmp	.L714
.L782:
	movl	$18, %eax
	jmp	.L714
.L874:
	subq	4752(%rsp), %rsi
	addq	%rsi, %rsi
	cmpq	%r15, %rsi
	cmovb	%r15, %rsi
	testq	%rsi, %rsi
	js	.L888
	movq	%rsi, %rcx
	call	_Znwy
	movq	4752(%rsp), %r9
	movq	4760(%rsp), %rdi
	movq	%rax, %rcx
	movq	%rax, %rbp
	subq	%r9, %rdi
	movq	%r9, %rdx
	movq	%r9, 96(%rsp)
	movq	%rdi, %r8
	call	memcpy
	movq	96(%rsp), %r9
	cmpq	%r12, %r9
	je	.L721
	movq	4768(%rsp), %rdx
	movq	%r9, %rcx
	subq	%r9, %rdx
	call	_ZdlPvy
.L721:
	leaq	0(%rbp,%rdi), %rax
	movq	%r15, %rcx
	movq	%rbp, 4752(%rsp)
	movq	%rax, %rdi
	addq	%rsi, %rbp
	movq	88(%rsp), %rsi
	rep movsb
	movq	%rbp, 4768(%rsp)
	movq	%rdi, %r15
	jmp	.L722
.L762:
	movl	$16, %eax
	jmp	.L671
.L781:
	movl	$19, %eax
	jmp	.L714
.L780:
	movl	$20, %eax
	jmp	.L714
.L774:
	movl	$8, %eax
	jmp	.L705
.L773:
	movl	$9, %eax
	jmp	.L705
.L772:
	movl	$10, %eax
	jmp	.L705
.L771:
	movl	$11, %eax
	jmp	.L705
.L884:
	call	_ZSt17__throw_bad_allocv
.L888:
	call	_ZSt17__throw_bad_allocv
.LEHE21:
.L885:
.LEHB22:
	call	_ZSt17__throw_bad_allocv
.LEHE22:
.L778:
	movl	$4, %eax
	jmp	.L705
.L777:
	movl	$5, %eax
	jmp	.L705
.L776:
	movl	$6, %eax
	jmp	.L705
.L775:
	movl	$7, %eax
	jmp	.L705
.L887:
.LEHB23:
	call	_ZSt17__throw_bad_allocv
.L785:
	movl	$15, %eax
	jmp	.L714
.L784:
	movl	$16, %eax
	jmp	.L714
.L789:
	movl	$11, %eax
	jmp	.L714
.L788:
	movl	$12, %eax
	jmp	.L714
.L787:
	movl	$13, %eax
	jmp	.L714
.L786:
	movl	$14, %eax
	jmp	.L714
.L797:
	movl	$3, %eax
	jmp	.L714
.L796:
	movl	$4, %eax
	jmp	.L714
.L795:
	movl	$5, %eax
	jmp	.L714
.L794:
	movl	$6, %eax
	jmp	.L714
.L793:
	movl	$7, %eax
	jmp	.L714
.L792:
	movl	$8, %eax
	jmp	.L714
.L791:
	movl	$9, %eax
	jmp	.L714
.L790:
	movl	$10, %eax
	jmp	.L714
.L877:
	call	_ZSt17__throw_bad_allocv
.L876:
	call	_ZSt17__throw_bad_allocv
.L875:
	call	_ZSt17__throw_bad_allocv
.LEHE23:
.L806:
	movq	4752(%rsp), %rcx
	movq	%rax, %rdi
	movq	%rdx, %rsi
	cmpq	%r12, %rcx
	je	.L856
	movq	4768(%rsp), %rdx
	subq	%rcx, %rdx
	vzeroupper
	call	_ZdlPvy
.L747:
	movq	72(%rsp), %rax
	call	*%rax
	movabsq	$-6148914691236517205, %rdx
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	movq	%rax, %rcx
	sarq	$4, %rcx
	imulq	%rdx, %rcx
	cmpq	$912, %rax
	jbe	.L889
	leaq	48(%rbx), %rcx
	call	LeaveCriticalSection
.L751:
	movq	%rdi, %rbx
.L752:
	movq	%r14, %rcx
	call	_ZN7fast_io10basic_ibufINS_18basic_file_wrapperINS_16basic_win32_fileIcEELNS_9open_modeE4104EEENS_17basic_buf_handlerIcLb0ELy1048576ENS_20io_aligned_allocatorIcLy4096EEEEEED1Ev
.L693:
	movq	.refptr._ZTVN8CryptoPP6SHA512E(%rip), %rax
	movq	%r13, %rcx
	addq	$16, %rax
	movq	%rax, 288(%rsp)
	call	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EED2Ev
.L857:
	movq	%rbx, %rcx
	movq	%rsi, %rax
.L638:
	decq	%rax
	jne	.L858
	call	__cxa_begin_catch
	movl	$-12, %ecx
	movq	%rax, %r12
	call	GetStdHandle
	leaq	288(%rsp), %rcx
	movq	%r12, %rdx
	movq	%rax, 288(%rsp)
.LEHB24:
	call	_ZN7fast_io7details14print_fallbackILb1ENS_23basic_win32_io_observerIcEEJRKSt9exceptionEEEvRT0_DpOT1_
.LEHE24:
	call	__cxa_end_catch
	movl	$2, %eax
	jmp	.L624
.L883:
.LEHB25:
	call	_ZN7fast_io17throw_win32_errorEv
.LEHE25:
.L805:
	movq	%rax, %rbx
	movq	%rdx, %rsi
	movq	%r12, %rcx
	vzeroupper
	call	_ZdaPv
.L691:
	movq	192(%rsp), %rcx
	testq	%rcx, %rcx
	je	.L693
	movl	$4096, %r8d
	movl	$1048576, %edx
	call	_ZdlPvySt11align_val_t
	jmp	.L693
.L856:
	vzeroupper
	jmp	.L747
.L889:
	andl	$-32769, 24(%rbx)
	addl	$16, %ecx
	call	_unlock
	jmp	.L751
.L801:
	movq	%rax, %r12
	vzeroupper
	call	__cxa_end_catch
	movq	%r12, %rcx
.L858:
.LEHB26:
	call	_Unwind_Resume
.LEHE26:
.L667:
.LEHB27:
	call	__cxa_throw_bad_array_new_length
.L866:
	call	_ZN7fast_io17throw_win32_errorEv
.LEHE27:
.L800:
	movq	%rax, %rbx
	movq	%rdx, %rsi
	vzeroupper
	jmp	.L752
.L803:
	movq	%rax, %r8
	movq	632(%rsp), %rax
	cmpq	%rax, 624(%rsp)
	cmovbe	624(%rsp), %rax
	cmpq	%rbp, 640(%rsp)
	jne	.L641
	movq	%rax, %rcx
	movb	$0, 609(%rsp)
	movq	%rbp, %rdi
	xorl	%eax, %eax
/APP
 # 1425 "D:/msys64/mingw64/include/CryptoPP/misc.h" 1
	rep stosq
 # 0 "" 2
/NO_APP
.L641:
	leaq	16+_ZTVN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEE(%rip), %rax
	movq	%rax, 288(%rsp)
	movq	456(%rsp), %rax
	cmpq	%rax, 448(%rsp)
	cmovbe	448(%rsp), %rax
	cmpq	%rbx, 464(%rsp)
	jne	.L642
	movq	%rax, %rcx
	movb	$0, 441(%rsp)
	movq	%rbx, %rdi
	xorl	%eax, %eax
/APP
 # 1425 "D:/msys64/mingw64/include/CryptoPP/misc.h" 1
	rep stosq
 # 0 "" 2
/NO_APP
.L642:
	movq	%r8, %rcx
	movq	%rdx, %rax
	vzeroupper
	jmp	.L638
.L802:
	movq	4752(%rsp), %rcx
	movq	%rax, %rbx
	movq	%rdx, %rsi
	cmpq	%r12, %rcx
	je	.L855
	movq	4768(%rsp), %rdx
	subq	%rcx, %rdx
	vzeroupper
	call	_ZdlPvy
	jmp	.L857
.L855:
	vzeroupper
	jmp	.L857
.L804:
	movq	%rax, %rbx
	movq	%rdx, %rsi
	vzeroupper
	jmp	.L691
.L861:
.LEHB28:
	call	_ZN7fast_io17throw_win32_errorEv
.LEHE28:
.L799:
	movq	%rax, %rcx
	movq	%rdx, %rax
	vzeroupper
	jmp	.L638
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
	.align 4
.LLSDA15972:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT15972-.LLSDATTD15972
.LLSDATTD15972:
	.byte	0x1
	.uleb128 .LLSDACSE15972-.LLSDACSB15972
.LLSDACSB15972:
	.uleb128 .LEHB11-.LFB15972
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L799-.LFB15972
	.uleb128 0x1
	.uleb128 .LEHB12-.LFB15972
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L803-.LFB15972
	.uleb128 0x3
	.uleb128 .LEHB13-.LFB15972
	.uleb128 .LEHE13-.LEHB13
	.uleb128 .L804-.LFB15972
	.uleb128 0x3
	.uleb128 .LEHB14-.LFB15972
	.uleb128 .LEHE14-.LEHB14
	.uleb128 .L800-.LFB15972
	.uleb128 0x3
	.uleb128 .LEHB15-.LFB15972
	.uleb128 .LEHE15-.LEHB15
	.uleb128 .L802-.LFB15972
	.uleb128 0x3
	.uleb128 .LEHB16-.LFB15972
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L806-.LFB15972
	.uleb128 0x3
	.uleb128 .LEHB17-.LFB15972
	.uleb128 .LEHE17-.LEHB17
	.uleb128 .L804-.LFB15972
	.uleb128 0x3
	.uleb128 .LEHB18-.LFB15972
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L805-.LFB15972
	.uleb128 0x3
	.uleb128 .LEHB19-.LFB15972
	.uleb128 .LEHE19-.LEHB19
	.uleb128 .L806-.LFB15972
	.uleb128 0x3
	.uleb128 .LEHB20-.LFB15972
	.uleb128 .LEHE20-.LEHB20
	.uleb128 .L802-.LFB15972
	.uleb128 0x3
	.uleb128 .LEHB21-.LFB15972
	.uleb128 .LEHE21-.LEHB21
	.uleb128 .L806-.LFB15972
	.uleb128 0x3
	.uleb128 .LEHB22-.LFB15972
	.uleb128 .LEHE22-.LEHB22
	.uleb128 .L802-.LFB15972
	.uleb128 0x3
	.uleb128 .LEHB23-.LFB15972
	.uleb128 .LEHE23-.LEHB23
	.uleb128 .L806-.LFB15972
	.uleb128 0x3
	.uleb128 .LEHB24-.LFB15972
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L801-.LFB15972
	.uleb128 0
	.uleb128 .LEHB25-.LFB15972
	.uleb128 .LEHE25-.LEHB25
	.uleb128 .L805-.LFB15972
	.uleb128 0x3
	.uleb128 .LEHB26-.LFB15972
	.uleb128 .LEHE26-.LEHB26
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB27-.LFB15972
	.uleb128 .LEHE27-.LEHB27
	.uleb128 .L804-.LFB15972
	.uleb128 0x3
	.uleb128 .LEHB28-.LFB15972
	.uleb128 .LEHE28-.LEHB28
	.uleb128 .L802-.LFB15972
	.uleb128 0x3
.LLSDACSE15972:
	.byte	0x1
	.byte	0
	.byte	0
	.byte	0x7d
	.align 4
	.long	.LDFCM0-.
.LLSDATT15972:
	.section	.text.startup,"x"
	.seh_endproc
	.globl	_ZTSSt9exception
	.section	.rdata$_ZTSSt9exception,"dr"
	.linkonce same_size
	.align 8
_ZTSSt9exception:
	.ascii "St9exception\0"
	.globl	_ZTISt9exception
	.section	.rdata$_ZTISt9exception,"dr"
	.linkonce same_size
	.align 8
_ZTISt9exception:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSSt9exception
	.globl	_ZTSN7fast_io14error_reporterE
	.section	.rdata$_ZTSN7fast_io14error_reporterE,"dr"
	.linkonce same_size
	.align 16
_ZTSN7fast_io14error_reporterE:
	.ascii "N7fast_io14error_reporterE\0"
	.globl	_ZTIN7fast_io14error_reporterE
	.section	.rdata$_ZTIN7fast_io14error_reporterE,"dr"
	.linkonce same_size
	.align 8
_ZTIN7fast_io14error_reporterE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN7fast_io14error_reporterE
	.globl	_ZTSN7fast_io13fast_io_errorE
	.section	.rdata$_ZTSN7fast_io13fast_io_errorE,"dr"
	.linkonce same_size
	.align 16
_ZTSN7fast_io13fast_io_errorE:
	.ascii "N7fast_io13fast_io_errorE\0"
	.globl	_ZTIN7fast_io13fast_io_errorE
	.section	.rdata$_ZTIN7fast_io13fast_io_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTIN7fast_io13fast_io_errorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN7fast_io13fast_io_errorE
	.quad	_ZTISt9exception
	.globl	_ZTSN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE
	.section	.rdata$_ZTSN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE,"dr"
	.linkonce same_size
	.align 32
_ZTSN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE:
	.ascii "N7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE\0"
	.globl	_ZTIN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE
	.section	.rdata$_ZTIN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE,"dr"
	.linkonce same_size
	.align 8
_ZTIN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE
	.quad	_ZTIN7fast_io14error_reporterE
	.globl	_ZTSN7fast_io18fast_io_text_errorE
	.section	.rdata$_ZTSN7fast_io18fast_io_text_errorE,"dr"
	.linkonce same_size
	.align 16
_ZTSN7fast_io18fast_io_text_errorE:
	.ascii "N7fast_io18fast_io_text_errorE\0"
	.globl	_ZTIN7fast_io18fast_io_text_errorE
	.section	.rdata$_ZTIN7fast_io18fast_io_text_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTIN7fast_io18fast_io_text_errorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN7fast_io18fast_io_text_errorE
	.quad	_ZTIN7fast_io13fast_io_errorE
	.globl	_ZTSN7fast_io11posix_errorE
	.section	.rdata$_ZTSN7fast_io11posix_errorE,"dr"
	.linkonce same_size
	.align 16
_ZTSN7fast_io11posix_errorE:
	.ascii "N7fast_io11posix_errorE\0"
	.globl	_ZTIN7fast_io11posix_errorE
	.section	.rdata$_ZTIN7fast_io11posix_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTIN7fast_io11posix_errorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN7fast_io11posix_errorE
	.quad	_ZTIN7fast_io13fast_io_errorE
	.globl	_ZTSN7fast_io11win32_errorE
	.section	.rdata$_ZTSN7fast_io11win32_errorE,"dr"
	.linkonce same_size
	.align 16
_ZTSN7fast_io11win32_errorE:
	.ascii "N7fast_io11win32_errorE\0"
	.globl	_ZTIN7fast_io11win32_errorE
	.section	.rdata$_ZTIN7fast_io11win32_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTIN7fast_io11win32_errorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN7fast_io11win32_errorE
	.quad	_ZTIN7fast_io13fast_io_errorE
	.globl	_ZTSN8CryptoPP9ExceptionE
	.section	.rdata$_ZTSN8CryptoPP9ExceptionE,"dr"
	.linkonce same_size
	.align 16
_ZTSN8CryptoPP9ExceptionE:
	.ascii "N8CryptoPP9ExceptionE\0"
	.globl	_ZTIN8CryptoPP9ExceptionE
	.section	.rdata$_ZTIN8CryptoPP9ExceptionE,"dr"
	.linkonce same_size
	.align 8
_ZTIN8CryptoPP9ExceptionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8CryptoPP9ExceptionE
	.quad	_ZTISt9exception
	.globl	_ZTSN8CryptoPP14NotImplementedE
	.section	.rdata$_ZTSN8CryptoPP14NotImplementedE,"dr"
	.linkonce same_size
	.align 16
_ZTSN8CryptoPP14NotImplementedE:
	.ascii "N8CryptoPP14NotImplementedE\0"
	.globl	_ZTIN8CryptoPP14NotImplementedE
	.section	.rdata$_ZTIN8CryptoPP14NotImplementedE,"dr"
	.linkonce same_size
	.align 8
_ZTIN8CryptoPP14NotImplementedE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8CryptoPP14NotImplementedE
	.quad	_ZTIN8CryptoPP9ExceptionE
	.globl	_ZTSN8CryptoPP8ClonableE
	.section	.rdata$_ZTSN8CryptoPP8ClonableE,"dr"
	.linkonce same_size
	.align 16
_ZTSN8CryptoPP8ClonableE:
	.ascii "N8CryptoPP8ClonableE\0"
	.globl	_ZTIN8CryptoPP8ClonableE
	.section	.rdata$_ZTIN8CryptoPP8ClonableE,"dr"
	.linkonce same_size
	.align 8
_ZTIN8CryptoPP8ClonableE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN8CryptoPP8ClonableE
	.globl	_ZTSN8CryptoPP9AlgorithmE
	.section	.rdata$_ZTSN8CryptoPP9AlgorithmE,"dr"
	.linkonce same_size
	.align 16
_ZTSN8CryptoPP9AlgorithmE:
	.ascii "N8CryptoPP9AlgorithmE\0"
	.globl	_ZTIN8CryptoPP9AlgorithmE
	.section	.rdata$_ZTIN8CryptoPP9AlgorithmE,"dr"
	.linkonce same_size
	.align 8
_ZTIN8CryptoPP9AlgorithmE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8CryptoPP9AlgorithmE
	.quad	_ZTIN8CryptoPP8ClonableE
	.globl	_ZTSN8CryptoPP18HashTransformationE
	.section	.rdata$_ZTSN8CryptoPP18HashTransformationE,"dr"
	.linkonce same_size
	.align 32
_ZTSN8CryptoPP18HashTransformationE:
	.ascii "N8CryptoPP18HashTransformationE\0"
	.globl	_ZTIN8CryptoPP18HashTransformationE
	.section	.rdata$_ZTIN8CryptoPP18HashTransformationE,"dr"
	.linkonce same_size
	.align 8
_ZTIN8CryptoPP18HashTransformationE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8CryptoPP18HashTransformationE
	.quad	_ZTIN8CryptoPP9AlgorithmE
	.globl	_ZTSN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEEE
	.section	.rdata$_ZTSN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEEE,"dr"
	.linkonce same_size
	.align 32
_ZTSN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEEE:
	.ascii "N8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEEE\0"
	.globl	_ZTIN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEEE
	.section	.rdata$_ZTIN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEEE,"dr"
	.linkonce same_size
	.align 8
_ZTIN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEEE
	.quad	_ZTIN8CryptoPP18HashTransformationE
	.globl	_ZTSN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEE
	.section	.rdata$_ZTSN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEE,"dr"
	.linkonce same_size
	.align 32
_ZTSN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEE:
	.ascii "N8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEE\0"
	.globl	_ZTIN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEE
	.section	.rdata$_ZTIN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEE,"dr"
	.linkonce same_size
	.align 8
_ZTIN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEE
	.quad	_ZTIN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEEE
	.globl	_ZTSN8CryptoPP13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEENS_6SHA512EEE
	.section	.rdata$_ZTSN8CryptoPP13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEENS_6SHA512EEE,"dr"
	.linkonce same_size
	.align 32
_ZTSN8CryptoPP13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEENS_6SHA512EEE:
	.ascii "N8CryptoPP13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEENS_6SHA512EEE\0"
	.globl	_ZTIN8CryptoPP13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEENS_6SHA512EEE
	.section	.rdata$_ZTIN8CryptoPP13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEENS_6SHA512EEE,"dr"
	.linkonce same_size
	.align 8
_ZTIN8CryptoPP13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEENS_6SHA512EEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8CryptoPP13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEENS_6SHA512EEE
	.quad	_ZTIN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEE
	.globl	_ZTSN8CryptoPP12ClonableImplINS_6SHA512ENS_13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEES1_EEEE
	.section	.rdata$_ZTSN8CryptoPP12ClonableImplINS_6SHA512ENS_13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEES1_EEEE,"dr"
	.linkonce same_size
	.align 32
_ZTSN8CryptoPP12ClonableImplINS_6SHA512ENS_13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEES1_EEEE:
	.ascii "N8CryptoPP12ClonableImplINS_6SHA512ENS_13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEES1_EEEE\0"
	.globl	_ZTIN8CryptoPP12ClonableImplINS_6SHA512ENS_13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEES1_EEEE
	.section	.rdata$_ZTIN8CryptoPP12ClonableImplINS_6SHA512ENS_13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEES1_EEEE,"dr"
	.linkonce same_size
	.align 8
_ZTIN8CryptoPP12ClonableImplINS_6SHA512ENS_13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEES1_EEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8CryptoPP12ClonableImplINS_6SHA512ENS_13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEES1_EEEE
	.quad	_ZTIN8CryptoPP13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEENS_6SHA512EEE
	.globl	_ZTSN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EEE
	.section	.rdata$_ZTSN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EEE,"dr"
	.linkonce same_size
	.align 32
_ZTSN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EEE:
	.ascii "N8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EEE\0"
	.globl	_ZTIN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EEE
	.section	.rdata$_ZTIN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EEE,"dr"
	.linkonce same_size
	.align 8
_ZTIN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EEE
	.quad	_ZTIN8CryptoPP12ClonableImplINS_6SHA512ENS_13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEES1_EEEE
	.globl	_ZTVN7fast_io13fast_io_errorE
	.section	.rdata$_ZTVN7fast_io13fast_io_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTVN7fast_io13fast_io_errorE:
	.quad	0
	.quad	_ZTIN7fast_io13fast_io_errorE
	.quad	0
	.quad	0
	.quad	_ZNK7fast_io13fast_io_error4whatEv
	.quad	__cxa_pure_virtual
	.globl	_ZTVN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE
	.section	.rdata$_ZTVN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE,"dr"
	.linkonce same_size
	.align 8
_ZTVN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE:
	.quad	0
	.quad	_ZTIN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE
	.quad	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_
	.quad	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy
	.quad	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc
	.globl	_ZTVN7fast_io18fast_io_text_errorE
	.section	.rdata$_ZTVN7fast_io18fast_io_text_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTVN7fast_io18fast_io_text_errorE:
	.quad	0
	.quad	_ZTIN7fast_io18fast_io_text_errorE
	.quad	_ZN7fast_io18fast_io_text_errorD1Ev
	.quad	_ZN7fast_io18fast_io_text_errorD0Ev
	.quad	_ZNK7fast_io13fast_io_error4whatEv
	.quad	_ZNK7fast_io18fast_io_text_error6reportERNS_14error_reporterE
	.globl	_ZTVN7fast_io11posix_errorE
	.section	.rdata$_ZTVN7fast_io11posix_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTVN7fast_io11posix_errorE:
	.quad	0
	.quad	_ZTIN7fast_io11posix_errorE
	.quad	_ZN7fast_io11posix_errorD1Ev
	.quad	_ZN7fast_io11posix_errorD0Ev
	.quad	_ZNK7fast_io13fast_io_error4whatEv
	.quad	_ZNK7fast_io11posix_error6reportERNS_14error_reporterE
	.globl	_ZTVN7fast_io11win32_errorE
	.section	.rdata$_ZTVN7fast_io11win32_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTVN7fast_io11win32_errorE:
	.quad	0
	.quad	_ZTIN7fast_io11win32_errorE
	.quad	_ZN7fast_io11win32_errorD1Ev
	.quad	_ZN7fast_io11win32_errorD0Ev
	.quad	_ZNK7fast_io13fast_io_error4whatEv
	.quad	_ZNK7fast_io11win32_error6reportERNS_14error_reporterE
	.globl	_ZTVN8CryptoPP9ExceptionE
	.section	.rdata$_ZTVN8CryptoPP9ExceptionE,"dr"
	.linkonce same_size
	.align 8
_ZTVN8CryptoPP9ExceptionE:
	.quad	0
	.quad	_ZTIN8CryptoPP9ExceptionE
	.quad	_ZN8CryptoPP9ExceptionD1Ev
	.quad	_ZN8CryptoPP9ExceptionD0Ev
	.quad	_ZNK8CryptoPP9Exception4whatEv
	.globl	_ZTVN8CryptoPP14NotImplementedE
	.section	.rdata$_ZTVN8CryptoPP14NotImplementedE,"dr"
	.linkonce same_size
	.align 8
_ZTVN8CryptoPP14NotImplementedE:
	.quad	0
	.quad	_ZTIN8CryptoPP14NotImplementedE
	.quad	_ZN8CryptoPP14NotImplementedD1Ev
	.quad	_ZN8CryptoPP14NotImplementedD0Ev
	.quad	_ZNK8CryptoPP9Exception4whatEv
	.globl	_ZTVN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEE
	.section	.rdata$_ZTVN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEE,"dr"
	.linkonce same_size
	.align 8
_ZTVN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEE:
	.quad	0
	.quad	_ZTIN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEE
	.quad	0
	.quad	0
	.quad	_ZNK8CryptoPP8Clonable5CloneEv
	.quad	_ZNK8CryptoPP9Algorithm13AlgorithmNameB5cxx11Ev
	.quad	_ZNK8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE17AlgorithmProviderB5cxx11Ev
	.quad	_ZN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE6UpdateEPKhy
	.quad	_ZN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE17CreateUpdateSpaceERy
	.quad	_ZN8CryptoPP18HashTransformation5FinalEPh
	.quad	_ZN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE7RestartEv
	.quad	__cxa_pure_virtual
	.quad	_ZNK8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEE9BlockSizeEv
	.quad	_ZNK8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE16OptimalBlockSizeEv
	.quad	_ZNK8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE20OptimalDataAlignmentEv
	.quad	_ZN8CryptoPP18HashTransformation15CalculateDigestEPhPKhy
	.quad	_ZN8CryptoPP18HashTransformation6VerifyEPKh
	.quad	_ZN8CryptoPP18HashTransformation12VerifyDigestEPKhS2_y
	.quad	_ZN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE14TruncatedFinalEPhy
	.quad	_ZN8CryptoPP18HashTransformation24CalculateTruncatedDigestEPhyPKhy
	.quad	_ZN8CryptoPP18HashTransformation15TruncatedVerifyEPKhy
	.quad	_ZN8CryptoPP18HashTransformation21VerifyTruncatedDigestEPKhyS2_y
	.quad	__cxa_pure_virtual
	.quad	_ZNK8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEE12GetByteOrderEv
	.quad	__cxa_pure_virtual
	.quad	_ZN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE18HashMultipleBlocksEPKyy
	.quad	_ZN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEE7DataBufEv
	.quad	__cxa_pure_virtual
	.globl	_ZTVN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EEE
	.section	.rdata$_ZTVN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EEE,"dr"
	.linkonce same_size
	.align 8
_ZTVN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EEE:
	.quad	0
	.quad	_ZTIN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EEE
	.quad	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EED1Ev
	.quad	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EED0Ev
	.quad	_ZNK8CryptoPP12ClonableImplINS_6SHA512ENS_13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEES1_EEE5CloneEv
	.quad	_ZNK8CryptoPP13AlgorithmImplINS_12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEEENS_6SHA512EE13AlgorithmNameB5cxx11Ev
	.quad	_ZNK8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE17AlgorithmProviderB5cxx11Ev
	.quad	_ZN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE6UpdateEPKhy
	.quad	_ZN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE17CreateUpdateSpaceERy
	.quad	_ZN8CryptoPP18HashTransformation5FinalEPh
	.quad	_ZN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE7RestartEv
	.quad	_ZNK8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE10DigestSizeEv
	.quad	_ZNK8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEE9BlockSizeEv
	.quad	_ZNK8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE16OptimalBlockSizeEv
	.quad	_ZNK8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE20OptimalDataAlignmentEv
	.quad	_ZN8CryptoPP18HashTransformation15CalculateDigestEPhPKhy
	.quad	_ZN8CryptoPP18HashTransformation6VerifyEPKh
	.quad	_ZN8CryptoPP18HashTransformation12VerifyDigestEPKhS2_y
	.quad	_ZN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE14TruncatedFinalEPhy
	.quad	_ZN8CryptoPP18HashTransformation24CalculateTruncatedDigestEPhyPKhy
	.quad	_ZN8CryptoPP18HashTransformation15TruncatedVerifyEPKhy
	.quad	_ZN8CryptoPP18HashTransformation21VerifyTruncatedDigestEPKhyS2_y
	.quad	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE4InitEv
	.quad	_ZNK8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEE12GetByteOrderEv
	.quad	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE24HashEndianCorrectedBlockEPKy
	.quad	_ZN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE18HashMultipleBlocksEPKyy
	.quad	_ZN8CryptoPP12IteratedHashIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ENS_18HashTransformationEE7DataBufEv
	.quad	_ZN8CryptoPP31IteratedHashWithStaticTransformIyNS_10EnumToTypeINS_9ByteOrderELi1EEELj128ELj64ENS_6SHA512ELj64ELb1EE8StateBufEv
	.globl	_ZN7fast_io7details3ryu4pow5IdLb1EE5splitE
	.section	.rdata$_ZN7fast_io7details3ryu4pow5IdLb1EE5splitE,"dr"
	.linkonce same_size
	.align 32
_ZN7fast_io7details3ryu4pow5IdLb1EE5splitE:
	.quad	0
	.quad	1152921504606846976
	.quad	0
	.quad	1441151880758558720
	.quad	0
	.quad	1801439850948198400
	.quad	0
	.quad	2251799813685248000
	.quad	0
	.quad	1407374883553280000
	.quad	0
	.quad	1759218604441600000
	.quad	0
	.quad	2199023255552000000
	.quad	0
	.quad	1374389534720000000
	.quad	0
	.quad	1717986918400000000
	.quad	0
	.quad	2147483648000000000
	.quad	0
	.quad	1342177280000000000
	.quad	0
	.quad	1677721600000000000
	.quad	0
	.quad	2097152000000000000
	.quad	0
	.quad	1310720000000000000
	.quad	0
	.quad	1638400000000000000
	.quad	0
	.quad	2048000000000000000
	.quad	0
	.quad	1280000000000000000
	.quad	0
	.quad	1600000000000000000
	.quad	0
	.quad	2000000000000000000
	.quad	0
	.quad	1250000000000000000
	.quad	0
	.quad	1562500000000000000
	.quad	0
	.quad	1953125000000000000
	.quad	0
	.quad	1220703125000000000
	.quad	0
	.quad	1525878906250000000
	.quad	0
	.quad	1907348632812500000
	.quad	0
	.quad	1192092895507812500
	.quad	0
	.quad	1490116119384765625
	.quad	4611686018427387904
	.quad	1862645149230957031
	.quad	-8646911284551352320
	.quad	1164153218269348144
	.quad	-6196953087261802496
	.quad	1455191522836685180
	.quad	-3134505340649865216
	.quad	1818989403545856475
	.quad	-3918131675812331520
	.quad	2273736754432320594
	.quad	-4754675306596401152
	.quad	1421085471520200371
	.quad	-5943344133245501440
	.quad	1776356839400250464
	.quad	-2817494148129488896
	.quad	2220446049250313080
	.quad	-8678462870222012416
	.quad	1387778780781445675
	.quad	7598665485932036096
	.quad	1734723475976807094
	.quad	274959820560269312
	.quad	2168404344971008868
	.quad	-9051522149004607488
	.quad	1355252715606880542
	.quad	2520655369026404352
	.quad	1694065894508600678
	.quad	-6072552825571770368
	.quad	2117582368135750847
	.quad	-3795345515982356480
	.quad	1323488980084844279
	.quad	-4744181894977945600
	.quad	1654361225106055349
	.quad	3293144668132343808
	.quad	2067951531382569187
	.quad	-247627591630979072
	.quad	1292469707114105741
	.quad	8913837547316051968
	.quad	1615587133892632177
	.quad	-2692761121137098752
	.quad	2019483917365790221
	.quad	-6294661719138074624
	.quad	1262177448353618888
	.quad	-3256641130495205376
	.quad	1577721810442023610
	.quad	-8682487431546394624
	.quad	1972152263052529513
	.quad	-814868626289108736
	.quad	1232595164407830945
	.quad	8204786253993389888
	.quad	1540743955509788682
	.quad	1032610780636961552
	.quad	1925929944387235853
	.quad	2951224747111794922
	.quad	1203706215242022408
	.quad	3689030933889743652
	.quad	1504632769052528010
	.quad	-4612083369492596243
	.quad	1880790961315660012
	.quad	-576709096719178700
	.quad	1175494350822287507
	.quad	-720886370898973375
	.quad	1469367938527859384
	.quad	3710578054803671186
	.quad	1836709923159824231
	.quad	26536550077201078
	.quad	2295887403949780289
	.quad	-6900943683842831182
	.quad	1434929627468612680
	.quad	-4014493586376151074
	.quad	1793662034335765850
	.quad	8816941072311974870
	.quad	2242077542919707313
	.quad	-1406940857446097563
	.quad	1401298464324817070
	.quad	-6370362090235009857
	.quad	1751623080406021338
	.quad	5872105442488401391
	.quad	2189528850507526673
	.quad	-3247463126085830987
	.quad	1368455531567204170
	.quad	-8671014926034676638
	.quad	1710569414459005213
	.quad	-1615396620688569989
	.quad	2138211768073756516
	.quad	1296220121283337709
	.quad	1336382355046097823
	.quad	-2991410866823215768
	.quad	1670477943807622278
	.quad	-8350949601956407614
	.quad	2088097429759527848
	.quad	6309871544845715001
	.quad	1305060893599704905
	.quad	-5947718624225019960
	.quad	1631326116999631131
	.quad	-7434648280281274950
	.quad	2039157646249538914
	.quad	-6952498184389490796
	.quad	1274473528905961821
	.quad	532749306367912313
	.quad	1593091911132452277
	.quad	5277622651387278295
	.quad	1991364888915565346
	.quad	7910200175544436838
	.quad	1244603055572228341
	.quad	-3947307835851617664
	.quad	1555753819465285426
	.quad	8900923260467641632
	.quad	1944692274331606783
	.quad	-5966138008276193740
	.quad	1215432671457254239
	.quad	-7457672510345242175
	.quad	1519290839321567799
	.quad	9124653435777998898
	.quad	1899113549151959749
	.quad	8008751406574943263
	.quad	1186945968219974843
	.quad	5399253239791291175
	.quad	1483682460274968554
	.quad	-2474305487115661840
	.quad	1854603075343710692
	.quad	759402079766405302
	.quad	1159126922089819183
	.quad	-3662433418719381276
	.quad	1448908652612273978
	.quad	-9189727791826614499
	.quad	1811135815765342473
	.quad	-2263787702928492316
	.quad	2263919769706678091
	.quad	7808504722524468110
	.quad	1414949856066673807
	.quad	5148944884728197234
	.quad	1768687320083342259
	.quad	1824495087482858639
	.quad	2210859150104177824
	.quad	1140309429676786649
	.quad	1381786968815111140
	.quad	1425386787095983311
	.quad	1727233711018888925
	.quad	6393419502297367043
	.quad	2159042138773611156
	.quad	-5227484847918921406
	.quad	1349401336733506972
	.quad	-1922670041471263854
	.quad	1686751670916883715
	.quad	-2403337551839079817
	.quad	2108439588646104644
	.quad	803757039314269066
	.quad	1317774742903815403
	.quad	-3606989719284551571
	.quad	1647218428629769253
	.quad	4714634887749086344
	.quad	2059023035787211567
	.quad	-8582568241225290795
	.quad	1286889397367007229
	.quad	-1504838264676837686
	.quad	1608611746708759036
	.quad	2730638187581340797
	.quad	2010764683385948796
	.quad	-7516723169616437810
	.quad	1256727927116217997
	.quad	-172531925165771454
	.quad	1570909908895272496
	.quad	4396021111970173586
	.quad	1963637386119090621
	.quad	5053356204195052443
	.quad	1227273366324431638
	.quad	-2906676781610960254
	.quad	1534091707905539547
	.quad	-3633345977013700317
	.quad	1917614634881924434
	.quad	-4576684244847256650
	.quad	1198509146801202771
	.quad	-5720855306059070813
	.quad	1498136433501503464
	.quad	-2539383114146450612
	.quad	1872670541876879330
	.quad	-3892957455555225585
	.quad	1170419088673049581
	.quad	4357175217410743827
	.quad	1463023860841311977
	.quad	-8388589033518733928
	.quad	1828779826051639971
	.quad	7961007781811134206
	.quad	2285974782564549964
	.quad	-4247742173222816929
	.quad	1428734239102843727
	.quad	-5309677716528521161
	.quad	1785917798878554659
	.quad	-6637097145660651452
	.quad	2232397248598193324
	.quad	-1842342706824213205
	.quad	1395248280373870827
	.quad	-2302928383530266507
	.quad	1744060350467338534
	.quad	-7490346497840221037
	.quad	2180075438084173168
	.quad	6847748484918331612
	.quad	1362547148802608230
	.quad	-663686430706861293
	.quad	1703183936003260287
	.quad	-829608038383576617
	.quad	2128979920004075359
	.quad	-518505023989735386
	.quad	1330612450002547099
	.quad	-648131279987169232
	.quad	1663265562503183874
	.quad	-5421850118411349444
	.quad	2079081953128979843
	.quad	5834715712847682405
	.quad	1299426220705612402
	.quad	-1929977395795172801
	.quad	1624282775882015502
	.quad	-7024157763171353905
	.quad	2030353469852519378
	.quad	-6695941611195790143
	.quad	1268970918657824611
	.quad	-8369927013994737679
	.quad	1586213648322280764
	.quad	-5850722749066034194
	.quad	1982767060402850955
	.quad	5566670318688504437
	.quad	1239229412751781847
	.quad	2346651879933242642
	.quad	1549036765939727309
	.quad	7545000868343941206
	.quad	1936295957424659136
	.quad	4715625542714963254
	.quad	1210184973390411960
	.quad	5894531928393704067
	.quad	1512731216738014950
	.quad	-1855207126362645724
	.quad	1890914020922518687
	.quad	-1159504453976653577
	.quad	1181821263076574179
	.quad	-1449380567470816972
	.quad	1477276578845717724
	.quad	2799960309088866689
	.quad	1846595723557147156
	.quad	-7473396843674234127
	.quad	1154122327223216972
	.quad	-4730060036165404755
	.quad	1442652909029021215
	.quad	-5912575045206755944
	.quad	1803316136286276519
	.quad	-7390718806508444929
	.quad	2254145170357845649
	.quad	-7513235640390177
	.quad	1408840731473653530
	.quad	-4621077562977875625
	.quad	1761050914342066913
	.quad	3447025083132431277
	.quad	2201313642927583642
	.quad	6766076695385157452
	.quad	1375821026829739776
	.quad	8457595869231446815
	.quad	1719776283537174720
	.quad	-7874749237170243097
	.quad	2149720354421468400
	.quad	6607496772837067824
	.quad	1343575221513417750
	.quad	-964001070808441028
	.quad	1679469026891772187
	.quad	-1205001338510551285
	.quad	2099336283614715234
	.quad	-3058968845782788505
	.quad	1312085177259197021
	.quad	5399660979626290177
	.quad	1640106471573996277
	.quad	-7085481830749300991
	.quad	2050133089467495346
	.quad	-6734269153432007072
	.quad	1281333180917184591
	.quad	-8417836441790008839
	.quad	1601666476146480739
	.quad	7924448521472040567
	.quad	2002083095183100924
	.quad	-4270591710934750454
	.quad	1251301934489438077
	.quad	3885132398186337741
	.quad	1564127418111797597
	.quad	-8978642557549241536
	.quad	1955159272639746996
	.quad	-3305808589254582008
	.quad	1221974545399841872
	.quad	479425281859160394
	.quad	1527468181749802341
	.quad	5210967620751338397
	.quad	1909335227187252926
	.quad	-1354831255457801406
	.quad	1193334516992033078
	.quad	-6305225087749639662
	.quad	1491668146240041348
	.quad	-3269845341259661673
	.quad	1864585182800051685
	.quad	-6655339356714676450
	.quad	1165365739250032303
	.quad	-8319174195893345562
	.quad	1456707174062540379
	.quad	8047776328842869663
	.quad	1820883967578175474
	.quad	836348374198811271
	.quad	2276104959472719343
	.quad	7440246761515338900
	.quad	1422565599670449589
	.quad	-4534749603387990086
	.quad	1778206999588061986
	.quad	8166621051047176104
	.quad	2222758749485077483
	.quad	2798295147690791113
	.quad	1389224218428173427
	.quad	-1113817083813899013
	.quad	1736530273035216783
	.quad	-1392271354767373766
	.quad	2170662841294020979
	.quad	8353202440125167204
	.quad	1356664275808763112
	.quad	-8005241023553092611
	.quad	1695830344760953890
	.quad	3828506775840797949
	.quad	2119787930951192363
	.quad	86973725686804766
	.quad	1324867456844495227
	.quad	-4502968861318881947
	.quad	1656084321055619033
	.quad	3594660960206173375
	.quad	2070105401319523792
	.quad	2246663100128858359
	.quad	1293815875824702370
	.quad	-6415043161693702859
	.quad	1617269844780877962
	.quad	5816254103165035138
	.quad	2021587305976097453
	.quad	5941001823691840913
	.quad	1263492066235060908
	.quad	7426252279614801142
	.quad	1579365082793826135
	.quad	4671129331091113523
	.quad	1974206353492282669
	.quad	5225298841145639904
	.quad	1233878970932676668
	.quad	6531623551432049880
	.quad	1542348713665845835
	.quad	3552843420862674446
	.quad	1927935892082307294
	.quad	-2391158880388216375
	.quad	1204959932551442058
	.quad	-7600634618912658373
	.quad	1506199915689302573
	.quad	-277421236786047158
	.quad	1882749894611628216
	.quad	-7090917300632361330
	.quad	1176718684132267635
	.quad	-8863646625790451662
	.quad	1470898355165334544
	.quad	-6467872263810676674
	.quad	1838622943956668180
	.quad	-3473154311335957938
	.quad	2298278679945835225
	.quad	2440964573842414192
	.quad	1436424174966147016
	.quad	3051205717303017741
	.quad	1795530218707683770
	.quad	-5409364890226003632
	.quad	2244412773384604712
	.quad	8148361989677217490
	.quad	1402757983365377945
	.quad	-3649605568185641850
	.quad	1753447479206722431
	.quad	-4562006960232052312
	.quad	2191809349008403039
	.quad	-2851254350145032695
	.quad	1369880843130251899
	.quad	-3564067937681290869
	.quad	1712351053912814874
	.quad	-9066770940529001490
	.quad	2140438817391018593
	.quad	-1055045819403238027
	.quad	1337774260869386620
	.quad	3292878744173340370
	.quad	1672217826086733276
	.quad	4116098430216675462
	.quad	2090272282608416595
	.quad	266718509671728212
	.quad	1306420176630260372
	.quad	333398137089660265
	.quad	1633025220787825465
	.quad	5028433689789463235
	.quad	2041281525984781831
	.quad	-8386443989950055238
	.quad	1275800953740488644
	.quad	-5871368969010181144
	.quad	1594751192175610805
	.quad	1884160825592049379
	.quad	1993438990219513507
	.quad	-1128242493218663091
	.quad	1245899368887195941
	.quad	7813068920331446945
	.quad	1557374211108994927
	.quad	5154650131986920777
	.quad	1946717763886243659
	.quad	915813323278131534
	.quad	1216698602428902287
	.quad	-3466919364329723487
	.quad	1520873253036127858
	.quad	-8945335223839542262
	.quad	1901091566295159823
	.quad	-5590834514899713914
	.quad	1188182228934474889
	.quad	2234828893230133415
	.quad	1485227786168093612
	.quad	2793536116537666769
	.quad	1856534732710117015
	.quad	8663489100477123587
	.quad	1160334207943823134
	.quad	1605989338741628675
	.quad	1450417759929778918
	.quad	-7215885363427739964
	.quad	1813022199912223647
	.quad	-9019856704284674954
	.quad	2266277749890279559
	.quad	-5637410440177921847
	.quad	1416423593681424724
	.quad	-2435077031795014404
	.quad	1770529492101780905
	.quad	6179525747111007803
	.quad	2213161865127226132
	.quad	-5361168444910395931
	.quad	1383226165704516332
	.quad	-2089774537710607010
	.quad	1729032707130645415
	.quad	-2612218172138258762
	.quad	2161290883913306769
	.quad	2979049660840976177
	.quad	1350806802445816731
	.quad	-887873942376167682
	.quad	1688508503057270913
	.quad	8113529608884566205
	.quad	2110635628821588642
	.quad	-8764102049729309834
	.quad	1319147268013492901
	.quad	-1731755525306861484
	.quad	1648934085016866126
	.quad	-6776380425060964759
	.quad	2061167606271082658
	.quad	-6541080774876796927
	.quad	1288229753919426661
	.quad	1047021068258779650
	.quad	1610287192399283327
	.quad	-3302909683103913342
	.quad	2012858990499104158
	.quad	4853210475701136017
	.quad	1258036869061940099
	.quad	1454827076199032118
	.quad	1572546086327425124
	.quad	1818533845248790147
	.quad	1965682607909281405
	.quad	3442426662494187794
	.quad	1228551629943300878
	.quad	-4920338708737041066
	.quad	1535689537429126097
	.quad	3072948650933474476
	.quad	1919611921786407622
	.quad	-2691093111593966357
	.quad	1199757451116504763
	.quad	-3363866389492457946
	.quad	1499696813895630954
	.quad	-8816519005292960336
	.quad	1874621017369538693
	.quad	8324733676974063502
	.quad	1171638135855961683
	.quad	5794231077790191473
	.quad	1464547669819952104
	.quad	7242788847237739342
	.quad	1830684587274940130
	.quad	-169885977807601630
	.quad	2288355734093675162
	.quad	-2412021745343444971
	.quad	1430222333808546976
	.quad	1596658836748081690
	.quad	1787777917260683721
	.quad	6607509564362490017
	.quad	2234722396575854651
	.quad	1823850468512862308
	.quad	1396701497859909157
	.quad	6891499104068465790
	.quad	1745876872324886446
	.quad	-608998156769193571
	.quad	2182346090406108057
	.quad	4231062170446641922
	.quad	1363966306503817536
	.quad	5288827713058302403
	.quad	1704957883129771920
	.quad	6611034641322878003
	.quad	2131197353912214900
	.quad	-5091475386027977056
	.quad	1331998346195134312
	.quad	-1752658214107583416
	.quad	1664997932743917890
	.quad	-6802508786061867174
	.quad	2081247415929897363
	.quad	4971804045566108824
	.quad	1300779634956185852
	.quad	6214755056957636030
	.quad	1625974543695232315
	.quad	3156757802769657134
	.quad	2032468179619040394
	.quad	6584659645158423613
	.quad	1270292612261900246
	.quad	-992547480406746292
	.quad	1587865765327375307
	.quad	-1240684350508432865
	.quad	1984832206659219134
	.quad	6142101308573311315
	.quad	1240520129162011959
	.quad	3065940617289251240
	.quad	1550650161452514949
	.quad	8444111790038951954
	.quad	1938312701815643686
	.quad	665883850346957067
	.quad	1211445438634777304
	.quad	832354812933696334
	.quad	1514306798293471630
	.quad	-8182928520687655390
	.quad	1892883497866839537
	.quad	-502644307002396715
	.quad	1183052186166774710
	.quad	-5239991402180383798
	.quad	1478815232708468388
	.quad	-1938303234298091843
	.quad	1848519040885585485
	.quad	-5823125539863695306
	.quad	1155324400553490928
	.quad	-2667220906402231229
	.quad	1444155500691863660
	.quad	1277659885424598868
	.quad	1805194375864829576
	.quad	1597074856780748586
	.quad	2256492969831036970
	.quad	5609857803915355770
	.quad	1410308106144398106
	.quad	-2211049781960581095
	.quad	1762885132680497632
	.quad	1847873790976661535
	.quad	2203606415850622041
	.quad	-5762607908280668397
	.quad	1377254009906638775
	.quad	-7203259885350835496
	.quad	1721567512383298469
	.quad	219297180166231438
	.quad	2151959390479123087
	.quad	7054589765244976505
	.quad	1344974619049451929
	.quad	-5016820848725943081
	.quad	1681218273811814911
	.quad	-6271026060907428851
	.quad	2101522842264768639
	.quad	-3919391288067143032
	.quad	1313451776415480399
	.quad	-4899239110083928790
	.quad	1641814720519350499
	.quad	-6124048887604910988
	.quad	2052268400649188124
	.quad	-1521687545539375415
	.quad	1282667750405742577
	.quad	7321262604930556539
	.quad	1603334688007178222
	.quad	-71793780691580134
	.quad	2004168360008972777
	.quad	4566814905495150320
	.quad	1252605225005607986
	.quad	-3514853404985837908
	.quad	1565756531257009982
	.quad	-9005252774659685289
	.quad	1957195664071262478
	.quad	1289246043478778550
	.quad	1223247290044539049
	.quad	6223243572775861092
	.quad	1529059112555673811
	.quad	3167368447542438461
	.quad	1911323890694592264
	.quad	1979605279714024038
	.quad	1194577431684120165
	.quad	7086192618069917952
	.quad	1493221789605150206
	.quad	-365631264267378368
	.quad	1866527237006437757
	.quad	-4840205558594499384
	.quad	1166579523129023598
	.quad	7784801107039039482
	.quad	1458224403911279498
	.quad	507629346944023544
	.quad	1822780504889099373
	.quad	5246222702107417334
	.quad	2278475631111374216
	.quad	3278889188817135834
	.quad	1424047269444608885
	.quad	8710297504448807696
	.quad	1780059086805761106
	.globl	_ZN7fast_io7details3ryu4pow5IdLb1EE9inv_splitE
	.section	.rdata$_ZN7fast_io7details3ryu4pow5IdLb1EE9inv_splitE,"dr"
	.linkonce same_size
	.align 32
_ZN7fast_io7details3ryu4pow5IdLb1EE9inv_splitE:
	.quad	1
	.quad	2305843009213693952
	.quad	-7378697629483820646
	.quad	1844674407370955161
	.quad	5165088340638674453
	.quad	1475739525896764129
	.quad	7821419487252849886
	.quad	1180591620717411303
	.quad	8824922364862649494
	.quad	1888946593147858085
	.quad	7059937891890119595
	.quad	1511157274518286468
	.quad	-5420096130713635294
	.quad	1208925819614629174
	.quad	-8672153809141816470
	.quad	1934281311383406679
	.quad	-6937723047313453176
	.quad	1547425049106725343
	.quad	-1860829623108852217
	.quad	1237940039285380274
	.quad	-2977327396974163548
	.quad	1980704062856608439
	.quad	-2381861917579330838
	.quad	1584563250285286751
	.quad	9162556910162266299
	.quad	1267650600228229401
	.quad	7281393426775805432
	.quad	2028240960365167042
	.quad	-1553582888063176301
	.quad	1622592768292133633
	.quad	2446482504291369283
	.quad	1298074214633706907
	.quad	7603720821608101175
	.quad	2076918743413931051
	.quad	2393627842544570617
	.quad	1661534994731144841
	.quad	-1774446540706253830
	.quad	1329227995784915872
	.quad	-6528463279871916451
	.quad	2126764793255865396
	.quad	5845275820328197809
	.quad	1701411834604692317
	.quad	-2702476973221262399
	.quad	1361129467683753853
	.quad	3054734472329800808
	.quad	2177807148294006166
	.quad	-1245561236878069677
	.quad	1742245718635204932
	.quad	6382248639981364905
	.quad	1393796574908163946
	.quad	2832900194486363201
	.quad	2230074519853062314
	.quad	5955668970331000884
	.quad	1784059615882449851
	.quad	1075186361522890384
	.quad	1427247692705959881
	.quad	-5658399451047196032
	.quad	2283596308329535809
	.quad	-4526719560837756825
	.quad	1826877046663628647
	.quad	3757321980813615186
	.quad	1461501637330902918
	.quad	-8062188859574838821
	.quad	1169201309864722334
	.quad	5547241898389809503
	.quad	1870722095783555735
	.quad	4437793518711847602
	.quad	1496577676626844588
	.quad	-7517811629256252888
	.quad	1197262141301475670
	.quad	-960452162584273651
	.quad	1915619426082361072
	.quad	6610335899416401726
	.quad	1532495540865888858
	.quad	-5779777724692609589
	.quad	1225996432692711086
	.quad	-5558295544766265019
	.quad	1961594292308337738
	.quad	-757287621071101692
	.quad	1569275433846670190
	.quad	-4295178911598791677
	.quad	1255420347077336152
	.quad	7885109000409574610
	.quad	2008672555323737844
	.quad	-8449308058639981605
	.quad	1606938044258990275
	.quad	7997948812055656009
	.quad	1285550435407192220
	.quad	-5650025974420502002
	.quad	2056880696651507552
	.quad	2858676849947419045
	.quad	1645504557321206042
	.quad	-5091756149525885410
	.quad	1316403645856964833
	.quad	-768112209757596011
	.quad	2106245833371143733
	.quad	3074859046935833515
	.quad	1684996666696914987
	.quad	-4918810391935153834
	.quad	1347997333357531989
	.quad	-7870096627096246135
	.quad	2156795733372051183
	.quad	-2606728486935086585
	.quad	1725436586697640946
	.quad	8982663654677661702
	.quad	1380349269358112757
	.quad	-385133411483382570
	.quad	2208558830972980411
	.quad	-7686804358670526703
	.quad	1766847064778384329
	.quad	-6149443486936421362
	.quad	1413477651822707463
	.quad	-2460411949614453533
	.quad	2261564242916331941
	.quad	9099716884534168143
	.quad	1809251394333065553
	.quad	-3788272936598396455
	.quad	1447401115466452442
	.quad	4348079280205103483
	.quad	1157920892373161954
	.quad	-4111119595897565398
	.quad	1852673427797059126
	.quad	7779150767507678651
	.quad	1482138742237647301
	.quad	2533971799264232598
	.quad	1185710993790117841
	.quad	-3324342750661048490
	.quad	1897137590064188545
	.quad	-6348823015270749115
	.quad	1517710072051350836
	.quad	5988988032009131678
	.quad	1214168057641080669
	.quad	-1485665593011120286
	.quad	1942668892225729070
	.quad	-4877881289150806552
	.quad	1554135113780583256
	.quad	7165741412905085728
	.quad	1243308091024466605
	.quad	-6981557813061414451
	.quad	1989292945639146568
	.quad	-1895897435707221237
	.quad	1591434356511317254
	.quad	-1516717948565776990
	.quad	1273147485209053803
	.quad	4951948911778577463
	.quad	2037035976334486086
	.quad	272210314680951647
	.quad	1629628781067588869
	.quad	3907117066486671641
	.quad	1303703024854071095
	.quad	6251387306378674625
	.quad	2085924839766513752
	.quad	-2377587784380880946
	.quad	1668739871813211001
	.quad	9165976216721026213
	.quad	1334991897450568801
	.quad	7286864317269821294
	.quad	2135987035920910082
	.quad	-1549206175667963611
	.quad	1708789628736728065
	.quad	-4928713755276281212
	.quad	1367031702989382452
	.quad	6871453250525591353
	.quad	2187250724783011924
	.quad	9186511415162383406
	.quad	1749800579826409539
	.quad	-7408186126837734568
	.quad	1399840463861127631
	.quad	-8163748988198464986
	.quad	2239744742177804210
	.quad	8226396068408869304
	.quad	1791795793742243368
	.quad	-4486929589498635526
	.quad	1433436634993794694
	.quad	-7179087343197816842
	.quad	2293498615990071511
	.quad	5324776569667477496
	.quad	1834798892792057209
	.quad	7949170070475892320
	.quad	1467839114233645767
	.quad	-1019361573103106790
	.quad	1174271291386916613
	.quad	5747719112518849781
	.quad	1878834066219066582
	.quad	-2780522339468740821
	.quad	1503067252975253265
	.quad	-5913766686316902980
	.quad	1202453802380202612
	.quad	5295368560860596524
	.quad	1923926083808324180
	.quad	4236294848688477220
	.quad	1539140867046659344
	.quad	7078384693692692099
	.quad	1231312693637327475
	.quad	-7121328563801244258
	.quad	1970100309819723960
	.quad	9060332407926645887
	.quad	1576080247855779168
	.quad	-3819780517884414260
	.quad	1260864198284623334
	.quad	-6111648828615062817
	.quad	2017382717255397335
	.quad	-8578667877633960576
	.quad	1613906173804317868
	.quad	-3173585487365258138
	.quad	1291124939043454294
	.quad	-5077736779784413021
	.quad	2065799902469526871
	.quad	7005857020398200553
	.quad	1652639921975621497
	.quad	-1774012013165260204
	.quad	1322111937580497197
	.quad	-6527768035806326650
	.quad	2115379100128795516
	.quad	5845832015580669650
	.quad	1692303280103036413
	.quad	-6391380831761195250
	.quad	1353842624082429130
	.quad	841837113407818570
	.quad	2166148198531886609
	.quad	4362818505468165179
	.quad	1732918558825509287
	.quad	-3888442825109288503
	.quad	1386334847060407429
	.quad	-6221508520174861605
	.quad	2218135755296651887
	.quad	2401490813343931363
	.quad	1774508604237321510
	.quad	1921192650675145090
	.quad	1419606883389857208
	.quad	-615440573661678179
	.quad	2271371013423771532
	.quad	6886345170554478103
	.quad	1817096810739017226
	.quad	1819727321701672159
	.quad	1453677448591213781
	.quad	-2233566957380572596
	.quad	1162941958872971024
	.quad	-3573707131808916153
	.quad	1860707134196753639
	.quad	-2858965705447132922
	.quad	1488565707357402911
	.quad	8780873879868024632
	.quad	1190852565885922329
	.quad	2981351763563108441
	.quad	1905364105417475727
	.quad	-4993616218633333894
	.quad	1524291284333980581
	.quad	7073153469319063855
	.quad	1219433027467184465
	.quad	-7129698522799049449
	.quad	1951092843947495144
	.quad	-5703758818239239559
	.quad	1560874275157996115
	.quad	-8252355869333301970
	.quad	1248699420126396892
	.quad	1553625868034358140
	.quad	1997919072202235028
	.quad	8621598323911307159
	.quad	1598335257761788022
	.quad	-481418970354774919
	.quad	1278668206209430417
	.quad	-4459619167309550194
	.quad	2045869129935088668
	.quad	121653480894270168
	.quad	1636695303948070935
	.quad	97322784715416134
	.quad	1309356243158456748
	.quad	-3533632359197244509
	.quad	2094969989053530796
	.quad	8241140556867935363
	.quad	1675975991242824637
	.quad	-785785183989472356
	.quad	1340780792994259709
	.quad	-1257256294383155770
	.quad	2145249268790815535
	.quad	-4695153850248434939
	.quad	1716199415032652428
	.quad	-66774265456837628
	.quad	1372959532026121942
	.quad	-3796187639472850528
	.quad	2196735251241795108
	.quad	652398703163629901
	.quad	1757388200993436087
	.quad	-6856778666952916726
	.quad	1405910560794748869
	.quad	7475898206584884855
	.quad	2249456897271598191
	.quad	2291369750525997561
	.quad	1799565517817278553
	.quad	9211793429904618695
	.quad	1439652414253822842
	.quad	-18525771120251381
	.quad	2303443862806116547
	.quad	7363877012587619542
	.quad	1842755090244893238
	.quad	-5176944834155635336
	.quad	1474204072195914590
	.quad	-7830904682066418592
	.quad	1179363257756731672
	.quad	2227947767661371545
	.quad	1886981212410770676
	.quad	-1906990600612813087
	.quad	1509584969928616540
	.quad	-5214941295232160793
	.quad	1207667975942893232
	.quad	6413489186596184024
	.quad	1932268761508629172
	.quad	-2247906280206873427
	.quad	1545815009206903337
	.quad	5580372605318321905
	.quad	1236652007365522670
	.quad	8928596168509315048
	.quad	1978643211784836272
	.quad	-235820694676368608
	.quad	1582914569427869017
	.quad	7190041073742725760
	.quad	1266331655542295214
	.quad	436019273762630246
	.quad	2026130648867672343
	.quad	7727513048493924843
	.quad	1620904519094137874
	.quad	-8575384820172501418
	.quad	1296723615275310299
	.quad	4726128361433549347
	.quad	2074757784440496479
	.quad	7470251503888749801
	.quad	1659806227552397183
	.quad	-5091845241114731129
	.quad	1327844982041917746
	.quad	-4457603571041659483
	.quad	2124551971267068394
	.quad	-3566082856833327587
	.quad	1699641577013654715
	.quad	-6542215100208572392
	.quad	1359713261610923772
	.quad	4289851098633925465
	.quad	2175541218577478036
	.quad	-257467935834769951
	.quad	1740432974861982428
	.quad	3483374466074094362
	.quad	1392346379889585943
	.quad	1884050330976640656
	.quad	2227754207823337509
	.quad	5196589079523222848
	.quad	1782203366258670007
	.quad	-3221426365865242368
	.quad	1425762693006936005
	.quad	5913764258841343181
	.quad	2281220308811097609
	.quad	8420360221814984868
	.quad	1824976247048878087
	.quad	-642409452031832752
	.quad	1459980997639102469
	.quad	-513927561625466201
	.quad	1167984798111281975
	.quad	-8200981728084566569
	.quad	1868775676978051161
	.quad	4507261061758077715
	.quad	1495020541582440929
	.quad	7295157664148372495
	.quad	1196016433265952743
	.quad	7982903447895485668
	.quad	1913626293225524389
	.quad	-8371072500651252758
	.quad	1530901034580419511
	.quad	4371188443704728763
	.quad	1224720827664335609
	.quad	-4074144934298164949
	.quad	1959553324262936974
	.quad	-3259315947438531959
	.quad	1567642659410349579
	.quad	-2607452757950825567
	.quad	1254114127528279663
	.quad	3206773216762499739
	.quad	2006582604045247462
	.quad	-4813279056073820855
	.quad	1605266083236197969
	.quad	-3850623244859056684
	.quad	1284212866588958375
	.quad	4907049252451240275
	.quad	2054740586542333401
	.quad	236290587219081897
	.quad	1643792469233866721
	.quad	-3500316344966644806
	.quad	1315033975387093376
	.quad	-1911157337204721366
	.quad	2104054360619349402
	.quad	5849771759720043554
	.quad	1683243488495479522
	.quad	-2698880221707785803
	.quad	1346594790796383617
	.quad	-8007557169474367609
	.quad	2154551665274213788
	.quad	-2716696920837583764
	.quad	1723641332219371030
	.quad	-5862706351411977334
	.quad	1378913065775496824
	.quad	9066413911450387881
	.quad	2206260905240794919
	.quad	-7504264129807330988
	.quad	1765008724192635935
	.quad	8753983955121776503
	.quad	1412006979354108748
	.quad	-8129718560256619535
	.quad	2259211166966573997
	.quad	874922781278525018
	.quad	1807368933573259198
	.quad	8078635854506640661
	.quad	1445895146858607358
	.quad	-4605137760620418441
	.quad	1156716117486885886
	.quad	-3678871602250759182
	.quad	1850745787979017418
	.quad	746251532941302978
	.quad	1480596630383213935
	.quad	597001226353042382
	.quad	1184477304306571148
	.quad	-2734146852577042512
	.quad	1895163686890513836
	.quad	8880728962164096960
	.quad	1516130949512411069
	.quad	-7652812089236363725
	.quad	1212904759609928855
	.quad	-1176452898552450990
	.quad	1940647615375886168
	.quad	2748186495899949531
	.quad	1552518092300708935
	.quad	2198549196719959625
	.quad	1242014473840567148
	.quad	-171670099989974923
	.quad	1987223158144907436
	.quad	-7516033709475800585
	.quad	1589778526515925949
	.quad	-6012826967580640468
	.quad	1271822821212740759
	.quad	8826220925580526867
	.quad	2034916513940385215
	.quad	7060976740464421494
	.quad	1627933211152308172
	.quad	-1729916237112283451
	.quad	1302346568921846537
	.quad	-6457214794121563846
	.quad	2083754510274954460
	.quad	-8855120650039161400
	.quad	1667003608219963568
	.quad	-3394747705289418796
	.quad	1333602886575970854
	.quad	-5431596328463070074
	.quad	2133764618521553367
	.quad	3033420566713364587
	.quad	1707011694817242694
	.quad	6116085268112601993
	.quad	1365609355853794155
	.quad	-8661007644729388428
	.quad	2184974969366070648
	.quad	-3239457301041600419
	.quad	1747979975492856518
	.quad	1097782973908629988
	.quad	1398383980394285215
	.quad	1756452758253807981
	.quad	2237414368630856344
	.quad	5094511021344956708
	.quad	1789931494904685075
	.quad	4075608817075965366
	.quad	1431945195923748060
	.quad	6520974107321544586
	.quad	2291112313477996896
	.quad	1527430471115325346
	.quad	1832889850782397517
	.quad	-6156753252591560370
	.quad	1466311880625918013
	.quad	-1236053787331337972
	.quad	1173049504500734410
	.quad	9090360384495590213
	.quad	1876879207201175057
	.quad	-106409321887348476
	.quad	1501503365760940045
	.quad	-3774476272251789104
	.quad	1201202692608752036
	.quad	-2349813220860952243
	.quad	1921924308174003258
	.quad	1809498238053148529
	.quad	1537539446539202607
	.quad	-5931099039041301823
	.quad	1230031557231362085
	.quad	1578287981759648052
	.quad	1968050491570179337
	.quad	-6116067244076102204
	.quad	1574440393256143469
	.quad	-4892853795260881763
	.quad	1259552314604914775
	.quad	3239480371808320148
	.quad	2015283703367863641
	.quad	-1097764517295254205
	.quad	1612226962694290912
	.quad	6500486015647617283
	.quad	1289781570155432730
	.quad	-8045966448673363964
	.quad	2063650512248692368
	.quad	-2747424344196780848
	.quad	1650920409798953894
	.quad	-2197939475357424678
	.quad	1320736327839163115
	.quad	7551343283653851484
	.quad	2113178124542660985
	.quad	6041074626923081187
	.quad	1690542499634128788
	.quad	-6235186742687266020
	.quad	1352433999707303030
	.quad	1091747655926105338
	.quad	2163894399531684849
	.quad	4562746939482794594
	.quad	1731115519625347879
	.quad	7339546366328145998
	.quad	1384892415700278303
	.quad	8053925371383123274
	.quad	2215827865120445285
	.quad	6443140297106498619
	.quad	1772662292096356228
	.quad	-5913534206540532074
	.quad	1418129833677084982
	.quad	5295740528502789974
	.quad	2269007733883335972
	.quad	-3142105206681588667
	.quad	1815206187106668777
	.quad	4865013464138549713
	.quad	1452164949685335022
	.quad	-3486686858172980876
	.quad	1161731959748268017
	.quad	9178696285890871890
	.quad	1858771135597228828
	.quad	-3725089415513033457
	.quad	1487016908477783062
	.quad	4398626097073393881
	.quad	1189613526782226450
	.quad	7037801755317430209
	.quad	1903381642851562320
	.quad	5630241404253944167
	.quad	1522705314281249856
	.quad	814844308661245011
	.quad	1218164251424999885
	.quad	1303750893857992017
	.quad	1949062802279999816
	.quad	-2646348099655516710
	.quad	1559250241823999852
	.quad	5261619149759407279
	.quad	1247400193459199882
	.quad	-6338804619352589647
	.quad	1995840309534719811
	.quad	5997002748743659252
	.quad	1596672247627775849
	.quad	8486951013736837725
	.quad	1277337798102220679
	.quad	2511075177753209390
	.quad	2043740476963553087
	.quad	-5369837487281253134
	.quad	1634992381570842469
	.quad	-4295869989825002507
	.quad	1307993905256673975
	.quad	4194654460505726958
	.quad	2092790248410678361
	.quad	-333625246337328757
	.quad	1674232198728542688
	.quad	3422448617672047318
	.quad	1339385758982834151
	.quad	-1902779841208544938
	.quad	2143017214372534641
	.quad	-8900921502450656597
	.quad	1714413771498027713
	.quad	-3431388387218614954
	.quad	1371531017198422170
	.quad	5577825024675947042
	.quad	2194449627517475473
	.quad	-6605786424484973336
	.quad	1755559702013980378
	.quad	-1595280324846068345
	.quad	1404447761611184302
	.quad	-6241797334495619676
	.quad	2247116418577894884
	.quad	-4993437867596495741
	.quad	1797693134862315907
	.quad	3383947335406624054
	.quad	1438154507889852726
	.quad	-1964381892833222160
	.quad	2301047212623764361
	.quad	-8950203143750398374
	.quad	1840837770099011489
	.quad	-7160162515000318699
	.quad	1472670216079209191
	.quad	5339916432225476010
	.quad	1178136172863367353
	.quad	4854517476818851293
	.quad	1885017876581387765
	.quad	3883613981455081034
	.quad	1508014301265110212
	.quad	-4271806444319755819
	.quad	1206411441012088169
	.quad	-6834890310911609310
	.quad	1930258305619341071
	.quad	5600134195496443521
	.quad	1544206644495472857
	.quad	-2898590273086665829
	.quad	1235365315596378285
	.quad	6430302007287065643
	.quad	1976584504954205257
	.quad	-2234456023654168132
	.quad	1581267603963364205
	.quad	-5476913633665244829
	.quad	1265014083170691364
	.quad	-8763061813864391727
	.quad	2024022533073106183
	.quad	-3321100636349603058
	.quad	1619218026458484946
	.quad	8411165935146048523
	.quad	1295374421166787957
	.quad	-1299529762733963656
	.quad	2072599073866860731
	.quad	-8418321439670991571
	.quad	1658079259093488585
	.quad	8022738107230848036
	.quad	1326463407274790868
	.quad	9147032156827446534
	.quad	2122341451639665389
	.quad	-7439769533505684065
	.quad	1697873161311732311
	.quad	5116230817421183718
	.quad	1358298529049385849
	.quad	-2882077136351837022
	.quad	2173277646479017358
	.quad	1383687105660440706
	.quad	1738622117183213887
	.quad	-6271747944955468082
	.quad	1390897693746571109
	.quad	8411947361780802685
	.quad	2225436309994513775
	.quad	6729557889424642148
	.quad	1780349047995611020
	.quad	5383646311539713719
	.quad	1424279238396488816
	.quad	1235136468979721303
	.quad	2278846781434382106
	.quad	-2701239639558133281
	.quad	1823077425147505684
	.quad	-2160991711646506624
	.quad	1458461940118004547
	.quad	5649904260166615347
	.quad	1166769552094403638
	.quad	5350498001524674232
	.quad	1866831283351045821
	.quad	591049586477829062
	.quad	1493465026680836657
	.quad	-6905857960301557397
	.quad	1194772021344669325
	.quad	18673707743239135
	.quad	1911635234151470921
	.quad	-3674409848547319015
	.quad	1529308187321176736
	.quad	8128518565387875758
	.quad	1223446549856941389
	.quad	1937583260394870242
	.quad	1957514479771106223
	.quad	8928764237799716840
	.quad	1566011583816884978
	.quad	-3925035053985957497
	.quad	1252809267053507982
	.quad	8477339172590109297
	.quad	2004494827285612772
	.quad	-596826291411733209
	.quad	1603595861828490217
	.quad	6901236596354434079
	.quad	1282876689462792174
	.quad	-26067890058636443
	.quad	2052602703140467478
	.quad	3668494502695001169
	.quad	1642082162512373983
	.quad	-8133250842069730034
	.quad	1313665730009899186
	.quad	9122891541139893884
	.quad	2101865168015838698
	.quad	-3769733211313815862
	.quad	1681492134412670958
	.quad	673562245690857633
	.quad	1345193707530136767
	.globl	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE
	.section	.rdata$_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE,"dr"
	.linkonce same_size
	.align 32
_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE:
	.byte	48
	.byte	48
	.byte	48
	.byte	49
	.byte	48
	.byte	50
	.byte	48
	.byte	51
	.byte	48
	.byte	52
	.byte	48
	.byte	53
	.byte	48
	.byte	54
	.byte	48
	.byte	55
	.byte	48
	.byte	56
	.byte	48
	.byte	57
	.byte	49
	.byte	48
	.byte	49
	.byte	49
	.byte	49
	.byte	50
	.byte	49
	.byte	51
	.byte	49
	.byte	52
	.byte	49
	.byte	53
	.byte	49
	.byte	54
	.byte	49
	.byte	55
	.byte	49
	.byte	56
	.byte	49
	.byte	57
	.byte	50
	.byte	48
	.byte	50
	.byte	49
	.byte	50
	.byte	50
	.byte	50
	.byte	51
	.byte	50
	.byte	52
	.byte	50
	.byte	53
	.byte	50
	.byte	54
	.byte	50
	.byte	55
	.byte	50
	.byte	56
	.byte	50
	.byte	57
	.byte	51
	.byte	48
	.byte	51
	.byte	49
	.byte	51
	.byte	50
	.byte	51
	.byte	51
	.byte	51
	.byte	52
	.byte	51
	.byte	53
	.byte	51
	.byte	54
	.byte	51
	.byte	55
	.byte	51
	.byte	56
	.byte	51
	.byte	57
	.byte	52
	.byte	48
	.byte	52
	.byte	49
	.byte	52
	.byte	50
	.byte	52
	.byte	51
	.byte	52
	.byte	52
	.byte	52
	.byte	53
	.byte	52
	.byte	54
	.byte	52
	.byte	55
	.byte	52
	.byte	56
	.byte	52
	.byte	57
	.byte	53
	.byte	48
	.byte	53
	.byte	49
	.byte	53
	.byte	50
	.byte	53
	.byte	51
	.byte	53
	.byte	52
	.byte	53
	.byte	53
	.byte	53
	.byte	54
	.byte	53
	.byte	55
	.byte	53
	.byte	56
	.byte	53
	.byte	57
	.byte	54
	.byte	48
	.byte	54
	.byte	49
	.byte	54
	.byte	50
	.byte	54
	.byte	51
	.byte	54
	.byte	52
	.byte	54
	.byte	53
	.byte	54
	.byte	54
	.byte	54
	.byte	55
	.byte	54
	.byte	56
	.byte	54
	.byte	57
	.byte	55
	.byte	48
	.byte	55
	.byte	49
	.byte	55
	.byte	50
	.byte	55
	.byte	51
	.byte	55
	.byte	52
	.byte	55
	.byte	53
	.byte	55
	.byte	54
	.byte	55
	.byte	55
	.byte	55
	.byte	56
	.byte	55
	.byte	57
	.byte	56
	.byte	48
	.byte	56
	.byte	49
	.byte	56
	.byte	50
	.byte	56
	.byte	51
	.byte	56
	.byte	52
	.byte	56
	.byte	53
	.byte	56
	.byte	54
	.byte	56
	.byte	55
	.byte	56
	.byte	56
	.byte	56
	.byte	57
	.byte	57
	.byte	48
	.byte	57
	.byte	49
	.byte	57
	.byte	50
	.byte	57
	.byte	51
	.byte	57
	.byte	52
	.byte	57
	.byte	53
	.byte	57
	.byte	54
	.byte	57
	.byte	55
	.byte	57
	.byte	56
	.byte	57
	.byte	57
	.globl	_ZN7fast_io7details3utf14utf_util_tableILb1EE11transitionsE
	.section	.rdata$_ZN7fast_io7details3utf14utf_util_tableILb1EE11transitionsE,"dr"
	.linkonce same_size
	.align 32
_ZN7fast_io7details3utf14utf_util_tableILb1EE11transitionsE:
	.byte	12
	.byte	0
	.byte	12
	.byte	12
	.byte	12
	.byte	24
	.byte	60
	.byte	36
	.byte	72
	.byte	84
	.byte	48
	.byte	96
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	0
	.byte	0
	.byte	0
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	24
	.byte	24
	.byte	24
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	36
	.byte	36
	.byte	36
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	24
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	24
	.byte	24
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	36
	.byte	36
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	36
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.globl	_ZN7fast_io7details3utf14utf_util_tableILb1EE14octet_categoryE
	.section	.rdata$_ZN7fast_io7details3utf14utf_util_tableILb1EE14octet_categoryE,"dr"
	.linkonce same_size
	.align 32
_ZN7fast_io7details3utf14utf_util_tableILb1EE14octet_categoryE:
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	0
	.byte	0
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	6
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	8
	.byte	7
	.byte	7
	.byte	9
	.byte	10
	.byte	10
	.byte	10
	.byte	11
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.globl	_ZN7fast_io7details3utf14utf_util_tableILb1EE15first_unit_infoE
	.section	.rdata$_ZN7fast_io7details3utf14utf_util_tableILb1EE15first_unit_infoE,"dr"
	.linkonce same_size
	.align 32
_ZN7fast_io7details3utf14utf_util_tableILb1EE15first_unit_infoE:
	.byte	0
	.byte	0
	.byte	1
	.byte	0
	.byte	2
	.byte	0
	.byte	3
	.byte	0
	.byte	4
	.byte	0
	.byte	5
	.byte	0
	.byte	6
	.byte	0
	.byte	7
	.byte	0
	.byte	8
	.byte	0
	.byte	9
	.byte	0
	.byte	10
	.byte	0
	.byte	11
	.byte	0
	.byte	12
	.byte	0
	.byte	13
	.byte	0
	.byte	14
	.byte	0
	.byte	15
	.byte	0
	.byte	16
	.byte	0
	.byte	17
	.byte	0
	.byte	18
	.byte	0
	.byte	19
	.byte	0
	.byte	20
	.byte	0
	.byte	21
	.byte	0
	.byte	22
	.byte	0
	.byte	23
	.byte	0
	.byte	24
	.byte	0
	.byte	25
	.byte	0
	.byte	26
	.byte	0
	.byte	27
	.byte	0
	.byte	28
	.byte	0
	.byte	29
	.byte	0
	.byte	30
	.byte	0
	.byte	31
	.byte	0
	.byte	32
	.byte	0
	.byte	33
	.byte	0
	.byte	34
	.byte	0
	.byte	35
	.byte	0
	.byte	36
	.byte	0
	.byte	37
	.byte	0
	.byte	38
	.byte	0
	.byte	39
	.byte	0
	.byte	40
	.byte	0
	.byte	41
	.byte	0
	.byte	42
	.byte	0
	.byte	43
	.byte	0
	.byte	44
	.byte	0
	.byte	45
	.byte	0
	.byte	46
	.byte	0
	.byte	47
	.byte	0
	.byte	48
	.byte	0
	.byte	49
	.byte	0
	.byte	50
	.byte	0
	.byte	51
	.byte	0
	.byte	52
	.byte	0
	.byte	53
	.byte	0
	.byte	54
	.byte	0
	.byte	55
	.byte	0
	.byte	56
	.byte	0
	.byte	57
	.byte	0
	.byte	58
	.byte	0
	.byte	59
	.byte	0
	.byte	60
	.byte	0
	.byte	61
	.byte	0
	.byte	62
	.byte	0
	.byte	63
	.byte	0
	.byte	64
	.byte	0
	.byte	65
	.byte	0
	.byte	66
	.byte	0
	.byte	67
	.byte	0
	.byte	68
	.byte	0
	.byte	69
	.byte	0
	.byte	70
	.byte	0
	.byte	71
	.byte	0
	.byte	72
	.byte	0
	.byte	73
	.byte	0
	.byte	74
	.byte	0
	.byte	75
	.byte	0
	.byte	76
	.byte	0
	.byte	77
	.byte	0
	.byte	78
	.byte	0
	.byte	79
	.byte	0
	.byte	80
	.byte	0
	.byte	81
	.byte	0
	.byte	82
	.byte	0
	.byte	83
	.byte	0
	.byte	84
	.byte	0
	.byte	85
	.byte	0
	.byte	86
	.byte	0
	.byte	87
	.byte	0
	.byte	88
	.byte	0
	.byte	89
	.byte	0
	.byte	90
	.byte	0
	.byte	91
	.byte	0
	.byte	92
	.byte	0
	.byte	93
	.byte	0
	.byte	94
	.byte	0
	.byte	95
	.byte	0
	.byte	96
	.byte	0
	.byte	97
	.byte	0
	.byte	98
	.byte	0
	.byte	99
	.byte	0
	.byte	100
	.byte	0
	.byte	101
	.byte	0
	.byte	102
	.byte	0
	.byte	103
	.byte	0
	.byte	104
	.byte	0
	.byte	105
	.byte	0
	.byte	106
	.byte	0
	.byte	107
	.byte	0
	.byte	108
	.byte	0
	.byte	109
	.byte	0
	.byte	110
	.byte	0
	.byte	111
	.byte	0
	.byte	112
	.byte	0
	.byte	113
	.byte	0
	.byte	114
	.byte	0
	.byte	115
	.byte	0
	.byte	116
	.byte	0
	.byte	117
	.byte	0
	.byte	118
	.byte	0
	.byte	119
	.byte	0
	.byte	120
	.byte	0
	.byte	121
	.byte	0
	.byte	122
	.byte	0
	.byte	123
	.byte	0
	.byte	124
	.byte	0
	.byte	125
	.byte	0
	.byte	126
	.byte	0
	.byte	127
	.byte	0
	.byte	0
	.byte	12
	.byte	1
	.byte	12
	.byte	2
	.byte	12
	.byte	3
	.byte	12
	.byte	4
	.byte	12
	.byte	5
	.byte	12
	.byte	6
	.byte	12
	.byte	7
	.byte	12
	.byte	8
	.byte	12
	.byte	9
	.byte	12
	.byte	10
	.byte	12
	.byte	11
	.byte	12
	.byte	12
	.byte	12
	.byte	13
	.byte	12
	.byte	14
	.byte	12
	.byte	15
	.byte	12
	.byte	16
	.byte	12
	.byte	17
	.byte	12
	.byte	18
	.byte	12
	.byte	19
	.byte	12
	.byte	20
	.byte	12
	.byte	21
	.byte	12
	.byte	22
	.byte	12
	.byte	23
	.byte	12
	.byte	24
	.byte	12
	.byte	25
	.byte	12
	.byte	26
	.byte	12
	.byte	27
	.byte	12
	.byte	28
	.byte	12
	.byte	29
	.byte	12
	.byte	30
	.byte	12
	.byte	31
	.byte	12
	.byte	32
	.byte	12
	.byte	33
	.byte	12
	.byte	34
	.byte	12
	.byte	35
	.byte	12
	.byte	36
	.byte	12
	.byte	37
	.byte	12
	.byte	38
	.byte	12
	.byte	39
	.byte	12
	.byte	40
	.byte	12
	.byte	41
	.byte	12
	.byte	42
	.byte	12
	.byte	43
	.byte	12
	.byte	44
	.byte	12
	.byte	45
	.byte	12
	.byte	46
	.byte	12
	.byte	47
	.byte	12
	.byte	48
	.byte	12
	.byte	49
	.byte	12
	.byte	50
	.byte	12
	.byte	51
	.byte	12
	.byte	52
	.byte	12
	.byte	53
	.byte	12
	.byte	54
	.byte	12
	.byte	55
	.byte	12
	.byte	56
	.byte	12
	.byte	57
	.byte	12
	.byte	58
	.byte	12
	.byte	59
	.byte	12
	.byte	60
	.byte	12
	.byte	61
	.byte	12
	.byte	62
	.byte	12
	.byte	63
	.byte	12
	.byte	-64
	.byte	12
	.byte	-63
	.byte	12
	.byte	2
	.byte	24
	.byte	3
	.byte	24
	.byte	4
	.byte	24
	.byte	5
	.byte	24
	.byte	6
	.byte	24
	.byte	7
	.byte	24
	.byte	8
	.byte	24
	.byte	9
	.byte	24
	.byte	10
	.byte	24
	.byte	11
	.byte	24
	.byte	12
	.byte	24
	.byte	13
	.byte	24
	.byte	14
	.byte	24
	.byte	15
	.byte	24
	.byte	16
	.byte	24
	.byte	17
	.byte	24
	.byte	18
	.byte	24
	.byte	19
	.byte	24
	.byte	20
	.byte	24
	.byte	21
	.byte	24
	.byte	22
	.byte	24
	.byte	23
	.byte	24
	.byte	24
	.byte	24
	.byte	25
	.byte	24
	.byte	26
	.byte	24
	.byte	27
	.byte	24
	.byte	28
	.byte	24
	.byte	29
	.byte	24
	.byte	30
	.byte	24
	.byte	31
	.byte	24
	.byte	0
	.byte	60
	.byte	1
	.byte	36
	.byte	2
	.byte	36
	.byte	3
	.byte	36
	.byte	4
	.byte	36
	.byte	5
	.byte	36
	.byte	6
	.byte	36
	.byte	7
	.byte	36
	.byte	8
	.byte	36
	.byte	9
	.byte	36
	.byte	10
	.byte	36
	.byte	11
	.byte	36
	.byte	12
	.byte	36
	.byte	13
	.byte	72
	.byte	14
	.byte	36
	.byte	15
	.byte	36
	.byte	0
	.byte	84
	.byte	1
	.byte	48
	.byte	2
	.byte	48
	.byte	3
	.byte	48
	.byte	4
	.byte	96
	.byte	-11
	.byte	12
	.byte	-10
	.byte	12
	.byte	-9
	.byte	12
	.byte	-8
	.byte	12
	.byte	-7
	.byte	12
	.byte	-6
	.byte	12
	.byte	-5
	.byte	12
	.byte	-4
	.byte	12
	.byte	-3
	.byte	12
	.byte	-2
	.byte	12
	.byte	-1
	.byte	12
	.section .rdata,"dr"
	.align 16
.LC6:
	.quad	2305843009213693951
	.quad	16
	.align 32
.LC7:
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.align 32
.LC8:
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.align 32
.LC9:
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.byte	87
	.align 32
.LC10:
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.align 8
.LC12:
	.long	-400107883
	.long	1041313291
	.weak	__cxa_pure_virtual
	.data
	.align 8
.LDFCM0:
	.quad	_ZTISt9exception
	.ident	"GCC: (GCC with MCF thread model, built by LH_Mouse.) 11.0.0 20200712 (experimental)"
	.def	_ZdaPv;	.scl	2;	.type	32;	.endef
	.def	_ZNSt9exceptionD2Ev;	.scl	2;	.type	32;	.endef
	.def	_ZdlPvy;	.scl	2;	.type	32;	.endef
	.def	_ZN8CryptoPP6SHA5129TransformEPyPKy;	.scl	2;	.type	32;	.endef
	.def	_ZN8CryptoPP6SHA5129InitStateEPy;	.scl	2;	.type	32;	.endef
	.def	__cxa_allocate_exception;	.scl	2;	.type	32;	.endef
	.def	__cxa_throw;	.scl	2;	.type	32;	.endef
	.def	memcpy;	.scl	2;	.type	32;	.endef
	.def	_Znwy;	.scl	2;	.type	32;	.endef
	.def	_ZSt17__throw_bad_allocv;	.scl	2;	.type	32;	.endef
	.def	strerror;	.scl	2;	.type	32;	.endef
	.def	strlen;	.scl	2;	.type	32;	.endef
	.def	_Znay;	.scl	2;	.type	32;	.endef
	.def	memmove;	.scl	2;	.type	32;	.endef
	.def	__cxa_begin_catch;	.scl	2;	.type	32;	.endef
	.def	__cxa_end_catch;	.scl	2;	.type	32;	.endef
	.def	memset;	.scl	2;	.type	32;	.endef
	.def	memmove;	.scl	2;	.type	32;	.endef
	.def	GetLastError;	.scl	2;	.type	32;	.endef
	.def	_ZSt19__throw_logic_errorPKc;	.scl	2;	.type	32;	.endef
	.def	_ZSt20__throw_length_errorPKc;	.scl	2;	.type	32;	.endef
	.def	_Unwind_Resume;	.scl	2;	.type	32;	.endef
	.def	CloseHandle;	.scl	2;	.type	32;	.endef
	.def	_ZdlPvySt11align_val_t;	.scl	2;	.type	32;	.endef
	.def	ReadFile;	.scl	2;	.type	32;	.endef
	.def	_ZnwySt11align_val_t;	.scl	2;	.type	32;	.endef
	.def	__cxa_free_exception;	.scl	2;	.type	32;	.endef
	.def	FormatMessageA;	.scl	2;	.type	32;	.endef
	.def	WriteFile;	.scl	2;	.type	32;	.endef
	.def	GetStdHandle;	.scl	2;	.type	32;	.endef
	.def	_ZNSt6chrono3_V212system_clock3nowEv;	.scl	2;	.type	32;	.endef
	.def	_ZN8CryptoPP9AlgorithmC2Eb;	.scl	2;	.type	32;	.endef
	.def	CreateFileW;	.scl	2;	.type	32;	.endef
	.def	_lock;	.scl	2;	.type	32;	.endef
	.def	_unlock;	.scl	2;	.type	32;	.endef
	.def	LeaveCriticalSection;	.scl	2;	.type	32;	.endef
	.def	EnterCriticalSection;	.scl	2;	.type	32;	.endef
	.def	clearerr;	.scl	2;	.type	32;	.endef
	.def	fwrite;	.scl	2;	.type	32;	.endef
	.def	ferror;	.scl	2;	.type	32;	.endef
	.def	__cxa_throw_bad_array_new_length;	.scl	2;	.type	32;	.endef
	.def	__cxa_pure_virtual;	.scl	2;	.type	32;	.endef
	.def	_ZN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE6UpdateEPKhy;	.scl	2;	.type	32;	.endef
	.def	_ZN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE17CreateUpdateSpaceERy;	.scl	2;	.type	32;	.endef
	.def	_ZN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE7RestartEv;	.scl	2;	.type	32;	.endef
	.def	_ZN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE14TruncatedFinalEPhy;	.scl	2;	.type	32;	.endef
	.def	_ZN8CryptoPP18HashTransformation15TruncatedVerifyEPKhy;	.scl	2;	.type	32;	.endef
	.def	_ZN8CryptoPP16IteratedHashBaseIyNS_18HashTransformationEE18HashMultipleBlocksEPKyy;	.scl	2;	.type	32;	.endef
	.section	.rdata$.refptr._ZTVN8CryptoPP6SHA512E, "dr"
	.globl	.refptr._ZTVN8CryptoPP6SHA512E
	.linkonce	discard
.refptr._ZTVN8CryptoPP6SHA512E:
	.quad	_ZTVN8CryptoPP6SHA512E
