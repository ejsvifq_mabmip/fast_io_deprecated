	.file	"hello_before.cc"
	.text
	.section	.text$_Z5printIRA15_KcJEEvOT_DpOT0_,"x"
	.linkonce discard
	.globl	_Z5printIRA15_KcJEEvOT_DpOT0_
	.def	_Z5printIRA15_KcJEEvOT_DpOT0_;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z5printIRA15_KcJEEvOT_DpOT0_
_Z5printIRA15_KcJEEvOT_DpOT0_:
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	%rcx, %r12
	movl	$1, %ecx
	call	*__imp___acrt_iob_func(%rip)
	movq	%rax, %rbx
	movq	__imp___iob_func(%rip), %rsi
	call	*%rsi
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L2
	sarq	$4, %rax
	movq	%rax, %rdx
	leaq	(%rax,%rax,4), %rax
	movq	%rax, %rcx
	salq	$4, %rcx
	addq	%rcx, %rax
	movq	%rax, %rcx
	salq	$8, %rcx
	addq	%rcx, %rax
	movq	%rax, %rcx
	salq	$16, %rcx
	addq	%rcx, %rax
	leal	16(%rdx,%rax,2), %ecx
	call	_lock
	orl	$32768, 24(%rbx)
.L3:
	movq	(%rbx), %rax
	leaq	14(%rax), %rcx
	movslq	36(%rbx), %rdx
	addq	16(%rbx), %rdx
	cmpq	%rdx, %rcx
	jnb	.L4
	movq	(%r12), %rdx
	movq	%rdx, (%rax)
	movl	8(%r12), %edx
	movl	%edx, 8(%rax)
	movzwl	12(%r12), %edx
	movw	%dx, 12(%rax)
	orl	$65536, 24(%rbx)
	movq	%rcx, %rax
	subq	(%rbx), %rax
	subl	%eax, 8(%rbx)
	movq	%rcx, (%rbx)
.L5:
	call	*%rsi
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L6
	andl	$-32769, 24(%rbx)
	sarq	$4, %rax
	movq	%rax, %rdx
	leaq	(%rax,%rax,4), %rax
	movq	%rax, %rcx
	salq	$4, %rcx
	addq	%rcx, %rax
	movq	%rax, %rcx
	salq	$8, %rcx
	addq	%rcx, %rax
	movq	%rax, %rcx
	salq	$16, %rcx
	addq	%rcx, %rax
	leal	16(%rdx,%rax,2), %ecx
	addq	$32, %rsp
	popq	%rbx
	popq	%rsi
	popq	%r12
	jmp	_unlock
.L6:
	leaq	48(%rbx), %rcx
	addq	$32, %rsp
	popq	%rbx
	popq	%rsi
	popq	%r12
	jmp	LeaveCriticalSection
.L2:
	leaq	48(%rbx), %rcx
	call	EnterCriticalSection
	jmp	.L3
.L4:
	movq	%rbx, %rcx
	call	clearerr
	movq	%rbx, %r9
	movl	$14, %r8d
	movl	$1, %edx
	movq	%r12, %rcx
	call	fwrite
	movq	%rbx, %rcx
	call	ferror
	movl	%eax, %r12d
	call	*__imp__errno(%rip)
	movq	%rbx, %rcx
	call	clearerr
	testl	%r12d, %r12d
	je	.L5
	ud2
	.seh_endproc
	.section	.text$_Z5printIRA13_KcJEEvOT_DpOT0_,"x"
	.linkonce discard
	.globl	_Z5printIRA13_KcJEEvOT_DpOT0_
	.def	_Z5printIRA13_KcJEEvOT_DpOT0_;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z5printIRA13_KcJEEvOT_DpOT0_
_Z5printIRA13_KcJEEvOT_DpOT0_:
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	%rcx, %r12
	movl	$1, %ecx
	call	*__imp___acrt_iob_func(%rip)
	movq	%rax, %rbx
	movq	__imp___iob_func(%rip), %rsi
	call	*%rsi
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L12
	sarq	$4, %rax
	movq	%rax, %rdx
	leaq	(%rax,%rax,4), %rax
	movq	%rax, %rcx
	salq	$4, %rcx
	addq	%rcx, %rax
	movq	%rax, %rcx
	salq	$8, %rcx
	addq	%rcx, %rax
	movq	%rax, %rcx
	salq	$16, %rcx
	addq	%rcx, %rax
	leal	16(%rdx,%rax,2), %ecx
	call	_lock
	orl	$32768, 24(%rbx)
.L13:
	movq	(%rbx), %rcx
	leaq	12(%rcx), %rdx
	movslq	36(%rbx), %rax
	addq	16(%rbx), %rax
	cmpq	%rax, %rdx
	jnb	.L14
	movq	(%r12), %rax
	movq	%rax, (%rcx)
	movl	8(%r12), %eax
	movl	%eax, 8(%rcx)
	orl	$65536, 24(%rbx)
	movq	%rdx, %rax
	subq	(%rbx), %rax
	subl	%eax, 8(%rbx)
	movq	%rdx, (%rbx)
.L15:
	call	*%rsi
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L16
	andl	$-32769, 24(%rbx)
	sarq	$4, %rax
	movq	%rax, %rdx
	leaq	(%rax,%rax,4), %rax
	movq	%rax, %rcx
	salq	$4, %rcx
	addq	%rcx, %rax
	movq	%rax, %rcx
	salq	$8, %rcx
	addq	%rcx, %rax
	movq	%rax, %rcx
	salq	$16, %rcx
	addq	%rcx, %rax
	leal	16(%rdx,%rax,2), %ecx
	addq	$32, %rsp
	popq	%rbx
	popq	%rsi
	popq	%r12
	jmp	_unlock
.L16:
	leaq	48(%rbx), %rcx
	addq	$32, %rsp
	popq	%rbx
	popq	%rsi
	popq	%r12
	jmp	LeaveCriticalSection
.L12:
	leaq	48(%rbx), %rcx
	call	EnterCriticalSection
	jmp	.L13
.L14:
	movq	%rbx, %rcx
	call	clearerr
	movq	%rbx, %r9
	movl	$12, %r8d
	movl	$1, %edx
	movq	%r12, %rcx
	call	fwrite
	movq	%rbx, %rcx
	call	ferror
	movl	%eax, %r12d
	call	*__imp__errno(%rip)
	movq	%rbx, %rcx
	call	clearerr
	testl	%r12d, %r12d
	je	.L15
	ud2
	.seh_endproc
	.section	.text$_ZN7fast_io7details14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJRA12_KciRA2_S4_EEEvRT0_DpOT1_,"x"
	.linkonce discard
	.globl	_ZN7fast_io7details14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJRA12_KciRA2_S4_EEEvRT0_DpOT1_
	.def	_ZN7fast_io7details14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJRA12_KciRA2_S4_EEEvRT0_DpOT1_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJRA12_KciRA2_S4_EEEvRT0_DpOT1_
_ZN7fast_io7details14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJRA12_KciRA2_S4_EEEvRT0_DpOT1_:
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	movl	$4176, %eax
	call	___chkstk_ms
	subq	%rax, %rsp
	.seh_stackalloc	4176
	.seh_endprologue
	movq	%rcx, %rbx
	leaq	48(%rsp), %rsi
	movq	%rsi, 4144(%rsp)
	leaq	4144(%rsp), %rax
	movq	%rax, 4160(%rsp)
	movq	(%rdx), %rax
	movq	%rax, 48(%rsp)
	movzwl	8(%rdx), %eax
	movw	%ax, 56(%rsp)
	movzbl	10(%rdx), %eax
	movb	%al, 58(%rsp)
	leaq	59(%rsp), %rax
	movl	(%r8), %ecx
	testl	%ecx, %ecx
	jns	.L21
	negl	%ecx
	movb	$45, 59(%rsp)
	leaq	60(%rsp), %rax
.L21:
	cmpl	$999999999, %ecx
	ja	.L22
	cmpl	$99999999, %ecx
	ja	.L41
	cmpl	$9999999, %ecx
	ja	.L42
	cmpl	$999999, %ecx
	ja	.L43
	cmpl	$99999, %ecx
	ja	.L44
	cmpl	$9999, %ecx
	ja	.L45
	cmpl	$999, %ecx
	ja	.L46
	cmpl	$99, %ecx
	ja	.L24
	cmpl	$10, %ecx
	sbbq	%rdx, %rdx
	leaq	2(%rax,%rdx), %r8
	movq	%r8, %r10
.L27:
	cmpl	$9, %ecx
	jbe	.L29
.L55:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, -2(%r8)
.L30:
	movq	%r10, 4152(%rsp)
	movzbl	(%r9), %r12d
	cmpq	%r10, 4160(%rsp)
	je	.L53
	movb	%r12b, (%r10)
	leaq	1(%r10), %r8
	movq	%r8, 4152(%rsp)
	movq	4144(%rsp), %r13
.L34:
	movq	(%rbx), %r12
	subq	%r13, %r8
	movq	(%r12), %rcx
	leaq	(%rcx,%r8), %rbx
	movslq	36(%r12), %rax
	addq	16(%r12), %rax
	cmpq	%rax, %rbx
	jnb	.L35
	testq	%r8, %r8
	jne	.L54
	xorl	%eax, %eax
.L36:
	orl	$65536, 24(%r12)
	subl	%eax, 8(%r12)
	movq	%rbx, (%r12)
.L37:
	movq	4144(%rsp), %rcx
	cmpq	%rsi, %rcx
	je	.L20
	movq	4160(%rsp), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
	nop
.L20:
	addq	$4176, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.L42:
	movl	$8, %r10d
.L23:
	addq	%rax, %r10
	cmpl	$99, %ecx
	jbe	.L47
.L39:
	movq	%r10, %r8
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r11
.L28:
	movl	%ecx, %edx
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%rdx,%rax,8), %rdi
	movq	%rdi, %rax
	salq	$10, %rax
	subq	%rdi, %rax
	salq	$5, %rax
	addq	%rdx, %rax
	leaq	(%rdx,%rax,4), %rax
	leaq	(%rdx,%rax,8), %rax
	salq	$5, %rax
	subq	%rdx, %rax
	shrq	$37, %rax
	movl	%eax, %edx
	leal	(%rax,%rax,4), %eax
	leal	(%rax,%rax,4), %edi
	sall	$2, %edi
	movl	%ecx, %eax
	subl	%edi, %eax
	movl	%ecx, %edi
	movl	%edx, %ecx
	subq	$2, %r8
	movzwl	(%r11,%rax,2), %eax
	movw	%ax, (%r8)
	cmpl	$9999, %edi
	ja	.L28
	cmpl	$9, %ecx
	ja	.L55
.L29:
	addl	$48, %ecx
	movb	%cl, -1(%r8)
	jmp	.L30
.L54:
	movq	%r13, %rdx
	call	memcpy
	movq	%rbx, %rax
	subq	(%r12), %rax
	jmp	.L36
.L22:
	leaq	10(%rax), %r10
	jmp	.L39
.L41:
	movl	$9, %r10d
	jmp	.L23
.L35:
	movq	%r8, 40(%rsp)
	movq	%r12, %rcx
	call	clearerr
	movq	%r12, %r9
	movq	40(%rsp), %r8
	movl	$1, %edx
	movq	%r13, %rcx
	call	fwrite
	movq	%r12, %rcx
	call	ferror
	movl	%eax, %ebx
	call	*__imp__errno(%rip)
	movq	%r12, %rcx
	call	clearerr
	testl	%ebx, %ebx
	je	.L37
	ud2
.L53:
	subq	4144(%rsp), %r10
	movq	%r10, %rbp
	addq	%rbp, %rbp
	js	.L56
	movq	%rbp, %rcx
	call	_Znwy
	movq	%rax, %r13
	movq	4144(%rsp), %r14
	movq	4152(%rsp), %rdi
	subq	%r14, %rdi
	movq	%rdi, %r8
	movq	%r14, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%rsi, %r14
	je	.L33
	movq	4160(%rsp), %rdx
	subq	%r14, %rdx
	movq	%r14, %rcx
	call	_ZdlPvy
.L33:
	movq	%r13, 4144(%rsp)
	leaq	0(%r13,%rdi), %r8
	addq	%r13, %rbp
	movq	%rbp, 4160(%rsp)
	movb	%r12b, (%r8)
	addq	$1, %r8
	movq	%r8, 4152(%rsp)
	jmp	.L34
.L43:
	movl	$7, %r10d
	jmp	.L23
.L44:
	movl	$6, %r10d
	jmp	.L23
.L45:
	movl	$5, %r10d
	jmp	.L23
.L46:
	movl	$4, %r10d
	jmp	.L23
.L24:
	leaq	3(%rax), %r10
	movl	%ecx, %r8d
	leaq	(%r8,%r8,4), %rdx
	leaq	(%r8,%rdx,8), %r11
	movq	%r11, %rdx
	salq	$10, %rdx
	subq	%r11, %rdx
	salq	$5, %rdx
	addq	%r8, %rdx
	leaq	(%r8,%rdx,4), %rdx
	leaq	(%r8,%rdx,8), %rdx
	salq	$5, %rdx
	subq	%r8, %rdx
	shrq	$37, %rdx
	movl	%edx, %r8d
	leal	(%rdx,%rdx,4), %edx
	leal	(%rdx,%rdx,4), %r11d
	sall	$2, %r11d
	subl	%r11d, %ecx
	movl	%ecx, %edx
	movl	%r8d, %ecx
	leaq	1(%rax), %r8
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r11
	movzwl	(%r11,%rdx,2), %edx
	movw	%dx, 1(%rax)
	jmp	.L29
.L47:
	movq	%r10, %r8
	jmp	.L27
.L56:
	call	_ZSt17__throw_bad_allocv
	nop
	.seh_endproc
	.section	.text$_Z5printIRA12_KcJiRA2_S0_EEvOT_DpOT0_,"x"
	.linkonce discard
	.globl	_Z5printIRA12_KcJiRA2_S0_EEvOT_DpOT0_
	.def	_Z5printIRA12_KcJiRA2_S0_EEvOT_DpOT0_;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z5printIRA12_KcJiRA2_S0_EEvOT_DpOT0_
_Z5printIRA12_KcJiRA2_S0_EEvOT_DpOT0_:
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$88, %rsp
	.seh_stackalloc	88
	.seh_endprologue
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%r8, %rbx
	movl	$1, %ecx
	call	*__imp___acrt_iob_func(%rip)
	movq	%rax, %rdi
	movq	__imp___iob_func(%rip), %rsi
	call	*%rsi
	movq	%rax, %r8
	movq	%rdi, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L58
	sarq	$4, %rax
	movq	%rax, %rdx
	leaq	(%rax,%rax,4), %rax
	movq	%rax, %rcx
	salq	$4, %rcx
	addq	%rcx, %rax
	movq	%rax, %rcx
	salq	$8, %rcx
	addq	%rcx, %rax
	movq	%rax, %rcx
	salq	$16, %rcx
	addq	%rcx, %rax
	leal	16(%rdx,%rax,2), %ecx
	call	_lock
	orl	$32768, 24(%rdi)
	movq	%rdi, 56(%rsp)
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L118
.L60:
	movq	(%rdi), %rdx
	leaq	11(%rdx), %rcx
	movslq	36(%rdi), %r8
	addq	%r8, %rax
	cmpq	%rax, %rcx
	jnb	.L62
	movq	(%r12), %rax
	movq	%rax, (%rdx)
	movzwl	8(%r12), %eax
	movw	%ax, 8(%rdx)
	movzbl	10(%r12), %eax
	movb	%al, 10(%rdx)
	orl	$65536, 24(%rdi)
	movq	%rcx, %rax
	subq	(%rdi), %rax
	subl	%eax, 8(%rdi)
	movq	%rcx, (%rdi)
.L63:
	movq	56(%rsp), %r12
	movq	(%r12), %rax
	movl	0(%r13), %ecx
	leaq	11(%rax), %r8
	movslq	36(%r12), %rdx
	addq	16(%r12), %rdx
	cmpq	%rdx, %r8
	jnb	.L64
	testl	%ecx, %ecx
	js	.L119
	cmpl	$999999999, %ecx
	ja	.L66
.L121:
	cmpl	$99999999, %ecx
	ja	.L94
	cmpl	$9999999, %ecx
	ja	.L95
	cmpl	$999999, %ecx
	ja	.L96
	cmpl	$99999, %ecx
	ja	.L97
	cmpl	$9999, %ecx
	ja	.L98
	cmpl	$999, %ecx
	ja	.L99
	cmpl	$99, %ecx
	ja	.L68
	cmpl	$10, %ecx
	sbbq	%rdx, %rdx
	leaq	2(%rax,%rdx), %r8
	movq	%r8, %r9
.L71:
	cmpl	$9, %ecx
	jbe	.L73
.L122:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, -2(%r8)
.L74:
	movq	56(%rsp), %rdx
	orl	$65536, 24(%rdx)
	movq	%r9, %rax
	subq	(%rdx), %rax
	subl	%eax, 8(%rdx)
	movq	%r9, (%rdx)
	movslq	36(%rdx), %rax
	addq	16(%rdx), %rax
.L75:
	movzbl	(%rbx), %ecx
	cmpq	%rax, %r9
	je	.L120
	movb	%cl, (%r9)
	addq	$1, %r9
	movq	56(%rsp), %rax
	orl	$65536, 24(%rax)
	movq	%r9, %rdx
	subq	(%rax), %rdx
	subl	%edx, 8(%rax)
	movq	%r9, (%rax)
.L61:
	call	*%rsi
	movq	%rax, %r8
	movq	%rdi, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L91
	andl	$-32769, 24(%rdi)
	sarq	$4, %rax
	movq	%rax, %rdx
	leaq	(%rax,%rax,4), %rax
	movq	%rax, %rcx
	salq	$4, %rcx
	addq	%rcx, %rax
	movq	%rax, %rcx
	salq	$8, %rcx
	addq	%rcx, %rax
	movq	%rax, %rcx
	salq	$16, %rcx
	addq	%rcx, %rax
	leal	16(%rdx,%rax,2), %ecx
	addq	$88, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	_unlock
.L58:
	leaq	48(%rdi), %rcx
	call	EnterCriticalSection
	movq	%rdi, 56(%rsp)
	movq	16(%rdi), %rax
	testq	%rax, %rax
	jne	.L60
.L118:
	leaq	56(%rsp), %rcx
	movq	%rbx, %r9
	movq	%r13, %r8
	movq	%r12, %rdx
	call	_ZN7fast_io7details14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJRA12_KciRA2_S4_EEEvRT0_DpOT1_
	jmp	.L61
.L119:
	negl	%ecx
	movb	$45, (%rax)
	addq	$1, %rax
	cmpl	$999999999, %ecx
	jbe	.L121
.L66:
	leaq	10(%rax), %r9
	jmp	.L92
.L94:
	movl	$9, %r9d
.L67:
	addq	%rax, %r9
	cmpl	$99, %ecx
	jbe	.L100
.L92:
	movq	%r9, %r8
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
.L72:
	movl	%ecx, %edx
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%rdx,%rax,8), %r11
	movq	%r11, %rax
	salq	$10, %rax
	subq	%r11, %rax
	salq	$5, %rax
	addq	%rdx, %rax
	leaq	(%rdx,%rax,4), %rax
	leaq	(%rdx,%rax,8), %rax
	salq	$5, %rax
	subq	%rdx, %rax
	shrq	$37, %rax
	movl	%eax, %edx
	leal	(%rax,%rax,4), %eax
	leal	(%rax,%rax,4), %r11d
	sall	$2, %r11d
	movl	%ecx, %eax
	subl	%r11d, %eax
	movl	%ecx, %r11d
	movl	%edx, %ecx
	subq	$2, %r8
	movzwl	(%r10,%rax,2), %eax
	movw	%ax, (%r8)
	cmpl	$9999, %r11d
	ja	.L72
	cmpl	$9, %ecx
	ja	.L122
.L73:
	addl	$48, %ecx
	movb	%cl, -1(%r8)
	jmp	.L74
.L98:
	movl	$5, %r9d
	jmp	.L67
.L91:
	leaq	48(%rdi), %rcx
	addq	$88, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	LeaveCriticalSection
.L95:
	movl	$8, %r9d
	jmp	.L67
.L64:
	testl	%ecx, %ecx
	js	.L123
	leaq	69(%rsp), %r13
	movq	%r13, %rax
.L76:
	cmpl	$999999999, %ecx
	ja	.L77
	cmpl	$99999999, %ecx
	ja	.L102
	cmpl	$9999999, %ecx
	ja	.L103
	cmpl	$999999, %ecx
	ja	.L104
	cmpl	$99999, %ecx
	ja	.L105
	cmpl	$9999, %ecx
	ja	.L106
	cmpl	$999, %ecx
	ja	.L107
	cmpl	$99, %ecx
	ja	.L79
	cmpl	$10, %ecx
	sbbq	%rdx, %rdx
	leaq	2(%rax,%rdx), %r9
	movq	%r9, %r8
.L82:
	cmpl	$9, %ecx
	jbe	.L84
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rcx,2), %eax
	movw	%ax, -2(%r9)
.L85:
	subq	%r13, %r8
	movq	(%r12), %rcx
	movslq	36(%r12), %rax
	addq	16(%r12), %rax
	leaq	(%rcx,%r8), %rbp
	cmpq	%rbp, %rax
	jbe	.L86
	testq	%r8, %r8
	jne	.L124
	movq	%r12, %rdx
	xorl	%ecx, %ecx
.L87:
	orl	$65536, 24(%r12)
	subl	%ecx, 8(%r12)
	movq	%rbp, (%r12)
.L88:
	movq	(%rdx), %r9
	jmp	.L75
.L120:
	orl	$65536, 24(%rdx)
	call	*__imp__flsbuf(%rip)
	cmpl	$-1, %eax
	jne	.L61
.L89:
	ud2
.L62:
	movq	%rdi, %rcx
	call	clearerr
	movq	%rdi, %r9
	movl	$11, %r8d
	movl	$1, %edx
	movq	%r12, %rcx
	call	fwrite
	movq	%rdi, %rcx
	call	ferror
	movl	%eax, %r12d
	call	*__imp__errno(%rip)
	movq	%rdi, %rcx
	call	clearerr
	testl	%r12d, %r12d
	je	.L63
	ud2
.L96:
	movl	$7, %r9d
	jmp	.L67
.L97:
	movl	$6, %r9d
	jmp	.L67
.L103:
	movl	$8, %r8d
.L78:
	addq	%rax, %r8
	cmpl	$99, %ecx
	jbe	.L108
.L93:
	movq	%r8, %r9
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
.L83:
	movl	%ecx, %edx
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%rdx,%rax,8), %r11
	movq	%r11, %rax
	salq	$10, %rax
	subq	%r11, %rax
	salq	$5, %rax
	addq	%rdx, %rax
	leaq	(%rdx,%rax,4), %rax
	leaq	(%rdx,%rax,8), %rax
	salq	$5, %rax
	subq	%rdx, %rax
	shrq	$37, %rax
	movl	%eax, %edx
	leal	(%rax,%rax,4), %eax
	leal	(%rax,%rax,4), %r11d
	sall	$2, %r11d
	movl	%ecx, %eax
	subl	%r11d, %eax
	movl	%ecx, %r11d
	movl	%edx, %ecx
	subq	$2, %r9
	movzwl	(%r10,%rax,2), %eax
	movw	%ax, (%r9)
	cmpl	$9999, %r11d
	ja	.L83
	jmp	.L82
.L79:
	leaq	3(%rax), %r8
	movl	%ecx, %r9d
	leaq	(%r9,%r9,4), %rdx
	leaq	(%r9,%rdx,8), %r10
	movq	%r10, %rdx
	salq	$10, %rdx
	subq	%r10, %rdx
	salq	$5, %rdx
	addq	%r9, %rdx
	leaq	(%r9,%rdx,4), %rdx
	leaq	(%r9,%rdx,8), %rdx
	salq	$5, %rdx
	subq	%r9, %rdx
	shrq	$37, %rdx
	movl	%edx, %r9d
	leal	(%rdx,%rdx,4), %edx
	leal	(%rdx,%rdx,4), %r10d
	sall	$2, %r10d
	subl	%r10d, %ecx
	movl	%ecx, %edx
	movl	%r9d, %ecx
	leaq	1(%rax), %r9
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movzwl	(%r10,%rdx,2), %edx
	movw	%dx, 1(%rax)
.L84:
	addl	$48, %ecx
	movb	%cl, -1(%r9)
	jmp	.L85
.L99:
	movl	$4, %r9d
	jmp	.L67
.L123:
	negl	%ecx
	movb	$45, 69(%rsp)
	leaq	70(%rsp), %rax
	leaq	69(%rsp), %r13
	jmp	.L76
.L68:
	leaq	3(%rax), %r9
	movl	%ecx, %r8d
	leaq	(%r8,%r8,4), %rdx
	leaq	(%r8,%rdx,8), %r10
	movq	%r10, %rdx
	salq	$10, %rdx
	subq	%r10, %rdx
	salq	$5, %rdx
	addq	%r8, %rdx
	leaq	(%r8,%rdx,4), %rdx
	leaq	(%r8,%rdx,8), %rdx
	salq	$5, %rdx
	subq	%r8, %rdx
	shrq	$37, %rdx
	movl	%edx, %r8d
	leal	(%rdx,%rdx,4), %edx
	leal	(%rdx,%rdx,4), %r10d
	sall	$2, %r10d
	subl	%r10d, %ecx
	movl	%ecx, %edx
	movl	%r8d, %ecx
	leaq	1(%rax), %r8
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movzwl	(%r10,%rdx,2), %edx
	movw	%dx, 1(%rax)
	jmp	.L73
.L124:
	movq	%r13, %rdx
	call	memcpy
	movq	%rbp, %rcx
	subq	(%r12), %rcx
	movq	56(%rsp), %rdx
	movslq	36(%rdx), %rax
	addq	16(%rdx), %rax
	jmp	.L87
.L77:
	leaq	10(%rax), %r8
	jmp	.L93
.L102:
	movl	$9, %r8d
	jmp	.L78
.L104:
	movl	$7, %r8d
	jmp	.L78
.L86:
	movq	%r8, 40(%rsp)
	movq	%r12, %rcx
	call	clearerr
	movq	%r12, %r9
	movq	40(%rsp), %r8
	movl	$1, %edx
	movq	%r13, %rcx
	call	fwrite
	movq	%r12, %rcx
	call	ferror
	movl	%eax, %ebp
	call	*__imp__errno(%rip)
	movq	%r12, %rcx
	call	clearerr
	testl	%ebp, %ebp
	jne	.L89
	movq	56(%rsp), %rdx
	movslq	36(%rdx), %rax
	addq	16(%rdx), %rax
	jmp	.L88
.L105:
	movl	$6, %r8d
	jmp	.L78
.L106:
	movl	$5, %r8d
	jmp	.L78
.L107:
	movl	$4, %r8d
	jmp	.L78
.L108:
	movq	%r8, %r9
	jmp	.L82
.L100:
	movq	%r9, %r8
	jmp	.L71
	.seh_endproc
	.def	__main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
.LC0:
	.ascii "\12\0"
.LC1:
	.ascii "Hello World\0"
.LC2:
	.ascii "Hello World 2\12\0"
.LC3:
	.ascii "Hello World\12\0"
	.section	.text.startup,"x"
	.globl	main
	.def	main;	.scl	2;	.type	32;	.endef
	.seh_proc	main
main:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$48, %rsp
	.seh_stackalloc	48
	.seh_endprologue
	call	__main
	movl	$4, 44(%rsp)
	leaq	44(%rsp), %r12
	leaq	.LC0(%rip), %r8
	movq	%r12, %rdx
	leaq	.LC1(%rip), %rcx
	call	_Z5printIRA12_KcJiRA2_S0_EEvOT_DpOT0_
	movl	$4, 44(%rsp)
	leaq	.LC0(%rip), %r8
	movq	%r12, %rdx
	leaq	.LC1(%rip), %rcx
	call	_Z5printIRA12_KcJiRA2_S0_EEvOT_DpOT0_
	movl	$4, 44(%rsp)
	leaq	.LC0(%rip), %r8
	movq	%r12, %rdx
	leaq	.LC1(%rip), %rcx
	call	_Z5printIRA12_KcJiRA2_S0_EEvOT_DpOT0_
	movl	$4, 44(%rsp)
	leaq	.LC0(%rip), %r8
	movq	%r12, %rdx
	leaq	.LC1(%rip), %rcx
	call	_Z5printIRA12_KcJiRA2_S0_EEvOT_DpOT0_
	movl	$4, 44(%rsp)
	leaq	.LC0(%rip), %r8
	movq	%r12, %rdx
	leaq	.LC1(%rip), %rcx
	call	_Z5printIRA12_KcJiRA2_S0_EEvOT_DpOT0_
	movl	$4, 44(%rsp)
	leaq	.LC0(%rip), %r8
	movq	%r12, %rdx
	leaq	.LC1(%rip), %rcx
	call	_Z5printIRA12_KcJiRA2_S0_EEvOT_DpOT0_
	movl	$4, 44(%rsp)
	leaq	.LC0(%rip), %r8
	movq	%r12, %rdx
	leaq	.LC1(%rip), %rcx
	call	_Z5printIRA12_KcJiRA2_S0_EEvOT_DpOT0_
	movl	$4, 44(%rsp)
	leaq	.LC0(%rip), %r8
	movq	%r12, %rdx
	leaq	.LC1(%rip), %rcx
	call	_Z5printIRA12_KcJiRA2_S0_EEvOT_DpOT0_
	movl	$4, 44(%rsp)
	leaq	.LC0(%rip), %r8
	movq	%r12, %rdx
	leaq	.LC1(%rip), %rcx
	call	_Z5printIRA12_KcJiRA2_S0_EEvOT_DpOT0_
	movl	$4, 44(%rsp)
	leaq	.LC0(%rip), %r8
	movq	%r12, %rdx
	leaq	.LC1(%rip), %rcx
	call	_Z5printIRA12_KcJiRA2_S0_EEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	leaq	.LC3(%rip), %rcx
	call	_Z5printIRA13_KcJEEvOT_DpOT0_
	leaq	.LC2(%rip), %rcx
	call	_Z5printIRA15_KcJEEvOT_DpOT0_
	xorl	%eax, %eax
	addq	$48, %rsp
	popq	%r12
	ret
	.seh_endproc
	.globl	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE
	.section	.rdata$_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE,"dr"
	.linkonce same_size
	.align 32
_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE:
	.byte	48
	.byte	48
	.byte	48
	.byte	49
	.byte	48
	.byte	50
	.byte	48
	.byte	51
	.byte	48
	.byte	52
	.byte	48
	.byte	53
	.byte	48
	.byte	54
	.byte	48
	.byte	55
	.byte	48
	.byte	56
	.byte	48
	.byte	57
	.byte	49
	.byte	48
	.byte	49
	.byte	49
	.byte	49
	.byte	50
	.byte	49
	.byte	51
	.byte	49
	.byte	52
	.byte	49
	.byte	53
	.byte	49
	.byte	54
	.byte	49
	.byte	55
	.byte	49
	.byte	56
	.byte	49
	.byte	57
	.byte	50
	.byte	48
	.byte	50
	.byte	49
	.byte	50
	.byte	50
	.byte	50
	.byte	51
	.byte	50
	.byte	52
	.byte	50
	.byte	53
	.byte	50
	.byte	54
	.byte	50
	.byte	55
	.byte	50
	.byte	56
	.byte	50
	.byte	57
	.byte	51
	.byte	48
	.byte	51
	.byte	49
	.byte	51
	.byte	50
	.byte	51
	.byte	51
	.byte	51
	.byte	52
	.byte	51
	.byte	53
	.byte	51
	.byte	54
	.byte	51
	.byte	55
	.byte	51
	.byte	56
	.byte	51
	.byte	57
	.byte	52
	.byte	48
	.byte	52
	.byte	49
	.byte	52
	.byte	50
	.byte	52
	.byte	51
	.byte	52
	.byte	52
	.byte	52
	.byte	53
	.byte	52
	.byte	54
	.byte	52
	.byte	55
	.byte	52
	.byte	56
	.byte	52
	.byte	57
	.byte	53
	.byte	48
	.byte	53
	.byte	49
	.byte	53
	.byte	50
	.byte	53
	.byte	51
	.byte	53
	.byte	52
	.byte	53
	.byte	53
	.byte	53
	.byte	54
	.byte	53
	.byte	55
	.byte	53
	.byte	56
	.byte	53
	.byte	57
	.byte	54
	.byte	48
	.byte	54
	.byte	49
	.byte	54
	.byte	50
	.byte	54
	.byte	51
	.byte	54
	.byte	52
	.byte	54
	.byte	53
	.byte	54
	.byte	54
	.byte	54
	.byte	55
	.byte	54
	.byte	56
	.byte	54
	.byte	57
	.byte	55
	.byte	48
	.byte	55
	.byte	49
	.byte	55
	.byte	50
	.byte	55
	.byte	51
	.byte	55
	.byte	52
	.byte	55
	.byte	53
	.byte	55
	.byte	54
	.byte	55
	.byte	55
	.byte	55
	.byte	56
	.byte	55
	.byte	57
	.byte	56
	.byte	48
	.byte	56
	.byte	49
	.byte	56
	.byte	50
	.byte	56
	.byte	51
	.byte	56
	.byte	52
	.byte	56
	.byte	53
	.byte	56
	.byte	54
	.byte	56
	.byte	55
	.byte	56
	.byte	56
	.byte	56
	.byte	57
	.byte	57
	.byte	48
	.byte	57
	.byte	49
	.byte	57
	.byte	50
	.byte	57
	.byte	51
	.byte	57
	.byte	52
	.byte	57
	.byte	53
	.byte	57
	.byte	54
	.byte	57
	.byte	55
	.byte	57
	.byte	56
	.byte	57
	.byte	57
	.ident	"GCC: (GCC with MCF thread model, built by LH_Mouse.) 11.0.0 20200712 (experimental)"
	.def	_lock;	.scl	2;	.type	32;	.endef
	.def	_unlock;	.scl	2;	.type	32;	.endef
	.def	LeaveCriticalSection;	.scl	2;	.type	32;	.endef
	.def	EnterCriticalSection;	.scl	2;	.type	32;	.endef
	.def	clearerr;	.scl	2;	.type	32;	.endef
	.def	fwrite;	.scl	2;	.type	32;	.endef
	.def	ferror;	.scl	2;	.type	32;	.endef
	.def	_ZdlPvy;	.scl	2;	.type	32;	.endef
	.def	memcpy;	.scl	2;	.type	32;	.endef
	.def	_Znwy;	.scl	2;	.type	32;	.endef
	.def	_ZSt17__throw_bad_allocv;	.scl	2;	.type	32;	.endef
