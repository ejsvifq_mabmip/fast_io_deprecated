	.file	"hello.cc"
	.text
	.section	.text$_ZN7fast_io24print_freestanding_decayINS_19basic_c_io_observerIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEEEEEvT_DpT0_,"x"
	.linkonce discard
	.globl	_ZN7fast_io24print_freestanding_decayINS_19basic_c_io_observerIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEEEEEvT_DpT0_
	.def	_ZN7fast_io24print_freestanding_decayINS_19basic_c_io_observerIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEEEEEvT_DpT0_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io24print_freestanding_decayINS_19basic_c_io_observerIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEEEEEvT_DpT0_
_ZN7fast_io24print_freestanding_decayINS_19basic_c_io_observerIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEEEEEvT_DpT0_:
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	%rcx, %rbx
	movq	(%rdx), %r12
	movq	8(%rdx), %rsi
	movq	__imp___iob_func(%rip), %rdi
	call	*%rdi
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L2
	sarq	$4, %rax
	movq	%rax, %rdx
	leaq	(%rax,%rax,4), %rax
	movq	%rax, %rcx
	salq	$4, %rcx
	addq	%rcx, %rax
	movq	%rax, %rcx
	salq	$8, %rcx
	addq	%rcx, %rax
	movq	%rax, %rcx
	salq	$16, %rcx
	addq	%rcx, %rax
	leal	16(%rdx,%rax,2), %ecx
	call	_lock
	orl	$32768, 24(%rbx)
.L3:
	movq	(%rbx), %rcx
	leaq	(%rcx,%rsi), %rbp
	movslq	36(%rbx), %rax
	addq	16(%rbx), %rax
	cmpq	%rax, %rbp
	jnb	.L4
	testq	%rsi, %rsi
	jne	.L15
.L5:
	orl	$65536, 24(%rbx)
	movq	%rbp, %rax
	subq	%rcx, %rax
	subl	%eax, 8(%rbx)
	movq	%rbp, (%rbx)
.L6:
	call	*%rdi
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L7
	andl	$-32769, 24(%rbx)
	sarq	$4, %rax
	movq	%rax, %rdx
	leaq	(%rax,%rax,4), %rax
	movq	%rax, %rcx
	salq	$4, %rcx
	addq	%rcx, %rax
	movq	%rax, %rcx
	salq	$8, %rcx
	addq	%rcx, %rax
	movq	%rax, %rcx
	salq	$16, %rcx
	addq	%rcx, %rax
	leal	16(%rdx,%rax,2), %ecx
	addq	$32, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	jmp	_unlock
.L7:
	leaq	48(%rbx), %rcx
	addq	$32, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	jmp	LeaveCriticalSection
.L2:
	leaq	48(%rbx), %rcx
	call	EnterCriticalSection
	jmp	.L3
.L15:
	movq	%rsi, %r8
	movq	%r12, %rdx
	call	memcpy
	movq	(%rbx), %rcx
	jmp	.L5
.L4:
	movq	%rbx, %rcx
	call	clearerr
	movq	%rbx, %r9
	movq	%rsi, %r8
	movl	$1, %edx
	movq	%r12, %rcx
	call	fwrite
	movq	%rbx, %rcx
	call	ferror
	movl	%eax, %esi
	call	*__imp__errno(%rip)
	movq	%rbx, %rcx
	call	clearerr
	testl	%esi, %esi
	je	.L6
	ud2
	.seh_endproc
	.section	.text$_ZN7fast_io7details5decay14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_EEEvT0_DpT1_,"x"
	.linkonce discard
	.globl	_ZN7fast_io7details5decay14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_EEEvT0_DpT1_
	.def	_ZN7fast_io7details5decay14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_EEEvT0_DpT1_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details5decay14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_EEEvT0_DpT1_
_ZN7fast_io7details5decay14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_EEEvT0_DpT1_:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	movl	$4184, %eax
	call	___chkstk_ms
	subq	%rax, %rsp
	.seh_stackalloc	4184
	.seh_endprologue
	movq	%rcx, %r12
	movq	(%rdx), %r14
	movq	8(%rdx), %rsi
	movq	(%r8), %r13
	movq	8(%r8), %rbx
	leaq	48(%rsp), %rbp
	movq	%rbp, 4144(%rsp)
	movq	%rbp, 4152(%rsp)
	leaq	4144(%rsp), %rax
	movq	%rax, 4160(%rsp)
	cmpq	$4096, %rsi
	ja	.L37
	testq	%rsi, %rsi
	jne	.L38
	movq	%rax, %rdi
.L21:
	addq	%rbp, %rsi
.L20:
	movq	%rsi, 4152(%rsp)
	movq	%rdi, %rax
	subq	%rsi, %rax
	cmpq	%rax, %rbx
	ja	.L39
	testq	%rbx, %rbx
	jne	.L40
.L26:
	leaq	(%rsi,%rbx), %r8
	movq	%r8, 4152(%rsp)
	movq	4144(%rsp), %r14
.L25:
	subq	%r14, %r8
	movq	(%r12), %rcx
	leaq	(%rcx,%r8), %rbx
	movslq	36(%r12), %rax
	addq	16(%r12), %rax
	cmpq	%rax, %rbx
	jnb	.L27
	testq	%r8, %r8
	jne	.L41
	xorl	%eax, %eax
.L28:
	orl	$65536, 24(%r12)
	subl	%eax, 8(%r12)
	movq	%rbx, (%r12)
.L29:
	cmpq	%rbp, %r14
	je	.L16
	movq	4160(%rsp), %rdx
	subq	%r14, %rdx
	movq	%r14, %rcx
	call	_ZdlPvy
	nop
.L16:
	addq	$4184, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L38:
	movq	%rsi, %r8
	movq	%r14, %rdx
	movq	%rbp, %rcx
	call	memcpy
	movq	4160(%rsp), %rdi
	jmp	.L21
.L41:
	movq	%r14, %rdx
	call	memcpy
	movq	%rbx, %rax
	subq	(%r12), %rax
	movq	4144(%rsp), %r14
	jmp	.L28
.L40:
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	%rsi, %rcx
	call	memcpy
	jmp	.L26
.L39:
	subq	4144(%rsp), %rdi
	addq	%rdi, %rdi
	cmpq	%rbx, %rdi
	cmovb	%rbx, %rdi
	testq	%rdi, %rdi
	js	.L23
	movq	%rdi, %rcx
	call	_Znwy
	movq	%rax, %r14
	movq	4144(%rsp), %r15
	movq	4152(%rsp), %rsi
	subq	%r15, %rsi
	movq	%rsi, %r8
	movq	%r15, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%rbp, %r15
	je	.L24
	movq	4160(%rsp), %rdx
	subq	%r15, %rdx
	movq	%r15, %rcx
	call	_ZdlPvy
.L24:
	movq	%r14, 4144(%rsp)
	addq	%r14, %rsi
	addq	%r14, %rdi
	movq	%rdi, 4160(%rsp)
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	%rsi, %rcx
	call	memcpy
	leaq	(%rsi,%rbx), %r8
	movq	%r8, 4152(%rsp)
	jmp	.L25
.L27:
	movq	%r8, 32(%rsp)
	movq	%r12, %rcx
	call	clearerr
	movq	%r12, %r9
	movq	32(%rsp), %r8
	movl	$1, %edx
	movq	%r14, %rcx
	call	fwrite
	movq	%r12, %rcx
	call	ferror
	movl	%eax, %ebx
	call	*__imp__errno(%rip)
	movq	%r12, %rcx
	call	clearerr
	testl	%ebx, %ebx
	jne	.L42
	movq	4144(%rsp), %r14
	jmp	.L29
.L37:
	cmpq	$8192, %rsi
	movl	$8192, %r9d
	cmovnb	%rsi, %r9
	testq	%r9, %r9
	js	.L23
	movq	%r9, %rcx
	movq	%r9, 40(%rsp)
	call	_Znwy
	movq	%rax, %rdi
	movq	4144(%rsp), %r10
	movq	4152(%rsp), %r15
	subq	%r10, %r15
	movq	%r15, %r8
	movq	%r10, %rdx
	movq	%r10, 32(%rsp)
	movq	%rax, %rcx
	call	memcpy
	movq	32(%rsp), %r10
	cmpq	%rbp, %r10
	movq	40(%rsp), %r9
	je	.L19
	movq	%r9, 32(%rsp)
	movq	4160(%rsp), %rdx
	subq	%r10, %rdx
	movq	%r10, %rcx
	call	_ZdlPvy
	movq	32(%rsp), %r9
.L19:
	movq	%rdi, 4144(%rsp)
	addq	%rdi, %r15
	addq	%r9, %rdi
	movq	%rdi, 4160(%rsp)
	movq	%rsi, %r8
	movq	%r14, %rdx
	movq	%r15, %rcx
	call	memcpy
	addq	%r15, %rsi
	jmp	.L20
.L23:
	call	_ZSt17__throw_bad_allocv
.L42:
	ud2
	.seh_endproc
	.section	.text$_ZN7fast_io7details5decay14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_S8_EEEvT0_DpT1_,"x"
	.linkonce discard
	.globl	_ZN7fast_io7details5decay14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_S8_EEEvT0_DpT1_
	.def	_ZN7fast_io7details5decay14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_S8_EEEvT0_DpT1_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details5decay14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_S8_EEEvT0_DpT1_
_ZN7fast_io7details5decay14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_S8_EEEvT0_DpT1_:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	movl	$4200, %eax
	call	___chkstk_ms
	subq	%rax, %rsp
	.seh_stackalloc	4200
	.seh_endprologue
	movq	%rcx, %r12
	movq	(%rdx), %r10
	movq	8(%rdx), %rdi
	movq	(%r8), %r15
	movq	8(%r8), %rbp
	movq	(%r9), %r14
	movq	8(%r9), %rbx
	leaq	64(%rsp), %r13
	movq	%r13, 4160(%rsp)
	movq	%r13, 4168(%rsp)
	leaq	4160(%rsp), %rsi
	movq	%rsi, 4176(%rsp)
	cmpq	$4096, %rdi
	ja	.L71
	testq	%rdi, %rdi
	jne	.L72
.L48:
	addq	%r13, %rdi
.L47:
	movq	%rdi, 4168(%rsp)
	movq	%rsi, %rax
	subq	%rdi, %rax
	cmpq	%rax, %rbp
	ja	.L73
	testq	%rbp, %rbp
	jne	.L74
.L53:
	addq	%rdi, %rbp
.L52:
	movq	%rbp, 4168(%rsp)
	movq	%rsi, %rax
	subq	%rbp, %rax
	cmpq	%rax, %rbx
	ja	.L75
	testq	%rbx, %rbx
	jne	.L76
.L57:
	leaq	0(%rbp,%rbx), %r8
	movq	%r8, 4168(%rsp)
	movq	4160(%rsp), %r15
.L56:
	subq	%r15, %r8
	movq	(%r12), %rcx
	leaq	(%rcx,%r8), %rbx
	movslq	36(%r12), %rax
	addq	16(%r12), %rax
	cmpq	%rax, %rbx
	jnb	.L58
	testq	%r8, %r8
	jne	.L77
	xorl	%eax, %eax
.L59:
	orl	$65536, 24(%r12)
	subl	%eax, 8(%r12)
	movq	%rbx, (%r12)
.L60:
	cmpq	%r13, %r15
	je	.L43
	movq	4176(%rsp), %rdx
	subq	%r15, %rdx
	movq	%r15, %rcx
	call	_ZdlPvy
	nop
.L43:
	addq	$4200, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L72:
	movq	%rdi, %r8
	movq	%r10, %rdx
	movq	%r13, %rcx
	call	memcpy
	movq	4176(%rsp), %rsi
	jmp	.L48
.L77:
	movq	%r15, %rdx
	call	memcpy
	movq	%rbx, %rax
	subq	(%r12), %rax
	movq	4160(%rsp), %r15
	jmp	.L59
.L76:
	movq	%rbx, %r8
	movq	%r14, %rdx
	movq	%rbp, %rcx
	call	memcpy
	jmp	.L57
.L74:
	movq	%rbp, %r8
	movq	%r15, %rdx
	movq	%rdi, %rcx
	call	memcpy
	movq	4176(%rsp), %rsi
	jmp	.L53
.L75:
	subq	4160(%rsp), %rsi
	addq	%rsi, %rsi
	cmpq	%rbx, %rsi
	cmovb	%rbx, %rsi
	testq	%rsi, %rsi
	js	.L50
	movq	%rsi, %rcx
	call	_Znwy
	movq	%rax, %r15
	movq	4160(%rsp), %rbp
	movq	4168(%rsp), %rdi
	subq	%rbp, %rdi
	movq	%rdi, %r8
	movq	%rbp, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%r13, %rbp
	je	.L55
	movq	4176(%rsp), %rdx
	subq	%rbp, %rdx
	movq	%rbp, %rcx
	call	_ZdlPvy
.L55:
	movq	%r15, 4160(%rsp)
	addq	%r15, %rdi
	addq	%r15, %rsi
	movq	%rsi, 4176(%rsp)
	movq	%rbx, %r8
	movq	%r14, %rdx
	movq	%rdi, %rcx
	call	memcpy
	leaq	(%rdi,%rbx), %r8
	movq	%r8, 4168(%rsp)
	jmp	.L56
.L58:
	movq	%r8, 32(%rsp)
	movq	%r12, %rcx
	call	clearerr
	movq	%r12, %r9
	movq	32(%rsp), %r8
	movl	$1, %edx
	movq	%r15, %rcx
	call	fwrite
	movq	%r12, %rcx
	call	ferror
	movl	%eax, %ebx
	call	*__imp__errno(%rip)
	movq	%r12, %rcx
	call	clearerr
	testl	%ebx, %ebx
	jne	.L78
	movq	4160(%rsp), %r15
	jmp	.L60
.L71:
	cmpq	$8192, %rdi
	movl	$8192, %r9d
	cmovnb	%rdi, %r9
	testq	%r9, %r9
	js	.L50
	movq	%r10, 56(%rsp)
	movq	%r9, %rcx
	movq	%r9, 48(%rsp)
	call	_Znwy
	movq	%rax, %rsi
	movq	4160(%rsp), %r11
	movq	4168(%rsp), %r8
	subq	%r11, %r8
	movq	%r8, 40(%rsp)
	movq	%r11, %rdx
	movq	%r11, 32(%rsp)
	movq	%rax, %rcx
	call	memcpy
	movq	32(%rsp), %r11
	cmpq	%r13, %r11
	movq	40(%rsp), %r8
	movq	48(%rsp), %r9
	movq	56(%rsp), %r10
	je	.L46
	movq	%r10, 48(%rsp)
	movq	%r9, 32(%rsp)
	movq	4176(%rsp), %rdx
	subq	%r11, %rdx
	movq	%r11, %rcx
	call	_ZdlPvy
	movq	48(%rsp), %r10
	movq	40(%rsp), %r8
	movq	32(%rsp), %r9
.L46:
	movq	%rsi, 4160(%rsp)
	leaq	(%rsi,%r8), %rcx
	addq	%r9, %rsi
	movq	%rsi, 4176(%rsp)
	movq	%rdi, %r8
	movq	%r10, %rdx
	call	memcpy
	addq	%rax, %rdi
	jmp	.L47
.L73:
	subq	4160(%rsp), %rsi
	addq	%rsi, %rsi
	cmpq	%rbp, %rsi
	cmovb	%rbp, %rsi
	testq	%rsi, %rsi
	js	.L50
	movq	%rsi, %rcx
	call	_Znwy
	movq	4160(%rsp), %rdi
	movq	4168(%rsp), %r8
	subq	%rdi, %r8
	movq	%r8, 32(%rsp)
	movq	%rdi, %rdx
	movq	%rax, %rcx
	call	memcpy
	movq	%rax, %r9
	cmpq	%r13, %rdi
	movq	32(%rsp), %r8
	je	.L51
	movq	%rax, 40(%rsp)
	movq	4176(%rsp), %rdx
	subq	%rdi, %rdx
	movq	%rdi, %rcx
	call	_ZdlPvy
	movq	40(%rsp), %r9
	movq	32(%rsp), %r8
.L51:
	movq	%r9, 4160(%rsp)
	leaq	(%r9,%r8), %rcx
	addq	%r9, %rsi
	movq	%rsi, 4176(%rsp)
	movq	%rbp, %r8
	movq	%r15, %rdx
	call	memcpy
	addq	%rax, %rbp
	jmp	.L52
.L50:
	call	_ZSt17__throw_bad_allocv
.L78:
	ud2
	.seh_endproc
	.section	.text$_ZN7fast_io7details5decay14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_S8_S8_EEEvT0_DpT1_,"x"
	.linkonce discard
	.globl	_ZN7fast_io7details5decay14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_S8_S8_EEEvT0_DpT1_
	.def	_ZN7fast_io7details5decay14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_S8_S8_EEEvT0_DpT1_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details5decay14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_S8_S8_EEEvT0_DpT1_
_ZN7fast_io7details5decay14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_S8_S8_EEEvT0_DpT1_:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	movl	$4216, %eax
	call	___chkstk_ms
	subq	%rax, %rsp
	.seh_stackalloc	4216
	.seh_endprologue
	movq	%rcx, %r12
	movq	(%rdx), %r11
	movq	8(%rdx), %rsi
	movq	(%r8), %rax
	movq	%rax, 40(%rsp)
	movq	8(%r8), %rdi
	movq	(%r9), %r14
	movq	8(%r9), %rbp
	movq	4320(%rsp), %rax
	movq	(%rax), %r15
	movq	8(%rax), %rbx
	leaq	80(%rsp), %r13
	movq	%r13, 4176(%rsp)
	movq	%r13, 4184(%rsp)
	leaq	4176(%rsp), %r9
	movq	%r9, 4192(%rsp)
	cmpq	$4096, %rsi
	ja	.L114
	testq	%rsi, %rsi
	jne	.L115
.L84:
	addq	%r13, %rsi
.L83:
	movq	%rsi, 4184(%rsp)
	movq	%r9, %rax
	subq	%rsi, %rax
	cmpq	%rax, %rdi
	ja	.L116
	testq	%rdi, %rdi
	jne	.L117
.L89:
	addq	%rsi, %rdi
.L88:
	movq	%rdi, 4184(%rsp)
	movq	%r9, %rax
	subq	%rdi, %rax
	cmpq	%rax, %rbp
	ja	.L118
	testq	%rbp, %rbp
	jne	.L119
.L93:
	addq	%rdi, %rbp
.L92:
	movq	%rbp, 4184(%rsp)
	movq	%r9, %rax
	subq	%rbp, %rax
	cmpq	%rax, %rbx
	ja	.L120
	testq	%rbx, %rbx
	jne	.L121
.L97:
	leaq	0(%rbp,%rbx), %r8
	movq	%r8, 4184(%rsp)
	movq	4176(%rsp), %r14
.L96:
	subq	%r14, %r8
	movq	(%r12), %rcx
	leaq	(%rcx,%r8), %rbx
	movslq	36(%r12), %rax
	addq	16(%r12), %rax
	cmpq	%rax, %rbx
	jnb	.L98
	testq	%r8, %r8
	jne	.L122
	xorl	%eax, %eax
.L99:
	orl	$65536, 24(%r12)
	subl	%eax, 8(%r12)
	movq	%rbx, (%r12)
.L100:
	cmpq	%r13, %r14
	je	.L79
	movq	4192(%rsp), %rdx
	subq	%r14, %rdx
	movq	%r14, %rcx
	call	_ZdlPvy
	nop
.L79:
	addq	$4216, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L115:
	movq	%rsi, %r8
	movq	%r11, %rdx
	movq	%r13, %rcx
	call	memcpy
	movq	4192(%rsp), %r9
	jmp	.L84
.L122:
	movq	%r14, %rdx
	call	memcpy
	movq	%rbx, %rax
	subq	(%r12), %rax
	movq	4176(%rsp), %r14
	jmp	.L99
.L121:
	movq	%rbx, %r8
	movq	%r15, %rdx
	movq	%rbp, %rcx
	call	memcpy
	jmp	.L97
.L119:
	movq	%rbp, %r8
	movq	%r14, %rdx
	movq	%rdi, %rcx
	call	memcpy
	movq	4192(%rsp), %r9
	jmp	.L93
.L117:
	movq	%rdi, %r8
	movq	40(%rsp), %rdx
	movq	%rsi, %rcx
	call	memcpy
	movq	4192(%rsp), %r9
	jmp	.L89
.L116:
	subq	4176(%rsp), %r9
	addq	%r9, %r9
	cmpq	%rdi, %r9
	movq	%r9, %rsi
	cmovb	%rdi, %rsi
	testq	%rsi, %rsi
	js	.L86
	movq	%rsi, %rcx
	call	_Znwy
	movq	4176(%rsp), %r11
	movq	4184(%rsp), %r8
	subq	%r11, %r8
	movq	%r8, 56(%rsp)
	movq	%r11, %rdx
	movq	%r11, 48(%rsp)
	movq	%rax, %rcx
	call	memcpy
	movq	%rax, %r9
	movq	48(%rsp), %r11
	cmpq	%r13, %r11
	movq	56(%rsp), %r8
	je	.L87
	movq	%rax, 56(%rsp)
	movq	%r8, 48(%rsp)
	movq	4192(%rsp), %rdx
	subq	%r11, %rdx
	movq	%r11, %rcx
	call	_ZdlPvy
	movq	56(%rsp), %r9
	movq	48(%rsp), %r8
.L87:
	movq	%r9, 4176(%rsp)
	leaq	(%r9,%r8), %rcx
	addq	%rsi, %r9
	movq	%r9, 4192(%rsp)
	movq	%r9, 48(%rsp)
	movq	%rdi, %r8
	movq	40(%rsp), %rdx
	call	memcpy
	addq	%rax, %rdi
	movq	48(%rsp), %r9
	jmp	.L88
.L118:
	subq	4176(%rsp), %r9
	addq	%r9, %r9
	cmpq	%rbp, %r9
	movq	%r9, %rsi
	cmovb	%rbp, %rsi
	testq	%rsi, %rsi
	js	.L86
	movq	%rsi, %rcx
	call	_Znwy
	movq	4176(%rsp), %rdi
	movq	4184(%rsp), %r8
	subq	%rdi, %r8
	movq	%r8, 40(%rsp)
	movq	%rdi, %rdx
	movq	%rax, %rcx
	call	memcpy
	movq	%rax, %r9
	cmpq	%r13, %rdi
	movq	40(%rsp), %r8
	je	.L91
	movq	%rax, 48(%rsp)
	movq	4192(%rsp), %rdx
	subq	%rdi, %rdx
	movq	%rdi, %rcx
	call	_ZdlPvy
	movq	48(%rsp), %r9
	movq	40(%rsp), %r8
.L91:
	movq	%r9, 4176(%rsp)
	leaq	(%r9,%r8), %rcx
	addq	%rsi, %r9
	movq	%r9, 4192(%rsp)
	movq	%r9, 40(%rsp)
	movq	%rbp, %r8
	movq	%r14, %rdx
	call	memcpy
	addq	%rax, %rbp
	movq	40(%rsp), %r9
	jmp	.L92
.L120:
	subq	4176(%rsp), %r9
	leaq	(%r9,%r9), %rsi
	cmpq	%rbx, %rsi
	cmovb	%rbx, %rsi
	testq	%rsi, %rsi
	js	.L86
	movq	%rsi, %rcx
	call	_Znwy
	movq	%rax, %r14
	movq	4176(%rsp), %rbp
	movq	4184(%rsp), %rdi
	subq	%rbp, %rdi
	movq	%rdi, %r8
	movq	%rbp, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%r13, %rbp
	je	.L95
	movq	4192(%rsp), %rdx
	subq	%rbp, %rdx
	movq	%rbp, %rcx
	call	_ZdlPvy
.L95:
	movq	%r14, 4176(%rsp)
	addq	%r14, %rdi
	addq	%r14, %rsi
	movq	%rsi, 4192(%rsp)
	movq	%rbx, %r8
	movq	%r15, %rdx
	movq	%rdi, %rcx
	call	memcpy
	leaq	(%rdi,%rbx), %r8
	movq	%r8, 4184(%rsp)
	jmp	.L96
.L98:
	movq	%r8, 40(%rsp)
	movq	%r12, %rcx
	call	clearerr
	movq	%r12, %r9
	movq	40(%rsp), %r8
	movl	$1, %edx
	movq	%r14, %rcx
	call	fwrite
	movq	%r12, %rcx
	call	ferror
	movl	%eax, %ebx
	call	*__imp__errno(%rip)
	movq	%r12, %rcx
	call	clearerr
	testl	%ebx, %ebx
	jne	.L123
	movq	4176(%rsp), %r14
	jmp	.L100
.L114:
	cmpq	$8192, %rsi
	movl	$8192, %eax
	cmovnb	%rsi, %rax
	movq	%rax, 48(%rsp)
	testq	%rax, %rax
	js	.L86
	movq	%rax, %rcx
	movq	%r11, 72(%rsp)
	call	_Znwy
	movq	4176(%rsp), %r9
	movq	4184(%rsp), %r8
	subq	%r9, %r8
	movq	%r8, 64(%rsp)
	movq	%r9, %rdx
	movq	%r9, 56(%rsp)
	movq	%rax, %rcx
	call	memcpy
	movq	%rax, %r10
	movq	56(%rsp), %r9
	cmpq	%r13, %r9
	movq	64(%rsp), %r8
	movq	72(%rsp), %r11
	je	.L82
	movq	%rax, 64(%rsp)
	movq	%r8, 56(%rsp)
	movq	4192(%rsp), %rdx
	subq	%r9, %rdx
	movq	%r9, %rcx
	call	_ZdlPvy
	movq	72(%rsp), %r11
	movq	64(%rsp), %r10
	movq	56(%rsp), %r8
.L82:
	movq	%r10, 4176(%rsp)
	leaq	(%r10,%r8), %rcx
	movq	48(%rsp), %r9
	addq	%r10, %r9
	movq	%r9, 4192(%rsp)
	movq	%r9, 48(%rsp)
	movq	%rsi, %r8
	movq	%r11, %rdx
	call	memcpy
	addq	%rax, %rsi
	movq	48(%rsp), %r9
	jmp	.L83
.L86:
	call	_ZSt17__throw_bad_allocv
.L123:
	ud2
	.seh_endproc
	.def	__main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
.LC0:
	.ascii "Hello1\0"
.LC1:
	.ascii "Hello12\0"
.LC2:
	.ascii "Hello123\0"
.LC3:
	.ascii "Hello1234\0"
.LC4:
	.ascii "Hello12345\0"
.LC5:
	.ascii "Hello123456\0"
.LC6:
	.ascii "Hello1234567\0"
.LC7:
	.ascii "Hello12345678\0"
.LC8:
	.ascii "adgsdgdsg\0"
.LC9:
	.ascii "qwrqwrwqrwqrwqr\0"
.LC10:
	.ascii "wqfwqfwqf\0"
	.section	.text.unlikely,"x"
.LCOLDB11:
	.section	.text.startup,"x"
.LHOTB11:
	.globl	main
	.def	main;	.scl	2;	.type	32;	.endef
	.seh_proc	main
main:
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$112, %rsp
	.seh_stackalloc	112
	.seh_endprologue
	call	__main
	movl	$1, %ecx
	call	*__imp___acrt_iob_func(%rip)
	movq	%rax, %rbx
	leaq	.LC0(%rip), %rax
	movq	%rax, 96(%rsp)
	movq	$6, 104(%rsp)
	leaq	96(%rsp), %r12
	movq	%r12, %rdx
	movq	%rbx, %rcx
	call	_ZN7fast_io24print_freestanding_decayINS_19basic_c_io_observerIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEEEEEvT_DpT0_
	leaq	.LC1(%rip), %rax
	movq	%rax, 96(%rsp)
	movq	$7, 104(%rsp)
	movq	%r12, %rdx
	movq	%rbx, %rcx
	call	_ZN7fast_io24print_freestanding_decayINS_19basic_c_io_observerIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEEEEEvT_DpT0_
	leaq	.LC2(%rip), %rax
	movq	%rax, 96(%rsp)
	movq	$8, 104(%rsp)
	movq	%r12, %rdx
	movq	%rbx, %rcx
	call	_ZN7fast_io24print_freestanding_decayINS_19basic_c_io_observerIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEEEEEvT_DpT0_
	leaq	.LC3(%rip), %rax
	movq	%rax, 96(%rsp)
	movq	$9, 104(%rsp)
	movq	%r12, %rdx
	movq	%rbx, %rcx
	call	_ZN7fast_io24print_freestanding_decayINS_19basic_c_io_observerIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEEEEEvT_DpT0_
	leaq	.LC4(%rip), %rax
	movq	%rax, 96(%rsp)
	movq	$10, 104(%rsp)
	movq	%r12, %rdx
	movq	%rbx, %rcx
	call	_ZN7fast_io24print_freestanding_decayINS_19basic_c_io_observerIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEEEEEvT_DpT0_
	leaq	.LC5(%rip), %rax
	movq	%rax, 96(%rsp)
	movq	$11, 104(%rsp)
	movq	%r12, %rdx
	movq	%rbx, %rcx
	call	_ZN7fast_io24print_freestanding_decayINS_19basic_c_io_observerIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEEEEEvT_DpT0_
	leaq	.LC6(%rip), %rax
	movq	%rax, 96(%rsp)
	movq	$12, 104(%rsp)
	movq	%r12, %rdx
	movq	%rbx, %rcx
	call	_ZN7fast_io24print_freestanding_decayINS_19basic_c_io_observerIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEEEEEvT_DpT0_
	leaq	.LC7(%rip), %rdi
	movq	%rdi, 96(%rsp)
	movq	$13, 104(%rsp)
	movq	%r12, %rdx
	movq	%rbx, %rcx
	call	_ZN7fast_io24print_freestanding_decayINS_19basic_c_io_observerIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEEEEEvT_DpT0_
	movq	__imp___iob_func(%rip), %rsi
	call	*%rsi
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L125
	sarq	$4, %rax
	imull	$-1431655765, %eax, %eax
	leal	16(%rax), %ecx
	call	_lock
	orl	$32768, 24(%rbx)
.L126:
	leaq	.LC8(%rip), %rbp
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L163
	movq	(%rbx), %rcx
	leaq	13(%rcx), %rax
	movslq	36(%rbx), %r8
	addq	%r8, %rdx
	cmpq	%rdx, %rax
	jnb	.L129
	movabsq	$3689065399400031560, %rdx
	movq	%rdx, (%rcx)
	movl	$926299444, 8(%rcx)
	movb	$56, 12(%rcx)
	orl	$65536, 24(%rbx)
	movq	%rax, %rdx
	subq	(%rbx), %rdx
	subl	%edx, 8(%rbx)
	movq	%rax, (%rbx)
.L130:
	leaq	9(%rax), %rcx
	movslq	36(%rbx), %rdx
	addq	16(%rbx), %rdx
	cmpq	%rdx, %rcx
	jnb	.L132
	movabsq	$8314884493163324513, %rdx
	movq	%rdx, (%rax)
	movb	$103, 8(%rax)
	orl	$65536, 24(%rbx)
	movq	%rcx, %rax
	subq	(%rbx), %rax
	subl	%eax, 8(%rbx)
	movq	%rcx, (%rbx)
.L128:
	call	*%rsi
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L135
	andl	$-32769, 24(%rbx)
	sarq	$4, %rax
	imull	$-1431655765, %eax, %eax
	leal	16(%rax), %ecx
	call	_unlock
.L136:
	call	*%rsi
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L137
	sarq	$4, %rax
	imull	$-1431655765, %eax, %eax
	leal	16(%rax), %ecx
	call	_lock
	orl	$32768, 24(%rbx)
.L138:
	leaq	.LC9(%rip), %r13
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L164
	movq	(%rbx), %rcx
	leaq	13(%rcx), %rdx
	movslq	36(%rbx), %r8
	addq	%r8, %rax
	cmpq	%rax, %rdx
	jnb	.L141
	movabsq	$3689065399400031560, %rax
	movq	%rax, (%rcx)
	movl	$926299444, 8(%rcx)
	movb	$56, 12(%rcx)
	orl	$65536, 24(%rbx)
	movq	%rdx, %rax
	subq	(%rbx), %rax
	subl	%eax, 8(%rbx)
	movq	%rdx, (%rbx)
.L142:
	leaq	9(%rdx), %rax
	movslq	36(%rbx), %rcx
	addq	16(%rbx), %rcx
	cmpq	%rcx, %rax
	jnb	.L143
	movabsq	$8314884493163324513, %rcx
	movq	%rcx, (%rdx)
	movb	$103, 8(%rdx)
	orl	$65536, 24(%rbx)
	movq	%rax, %rdx
	subq	(%rbx), %rdx
	subl	%edx, 8(%rbx)
	movq	%rax, (%rbx)
.L144:
	leaq	15(%rax), %rcx
	movslq	36(%rbx), %rdx
	addq	16(%rbx), %rdx
	cmpq	%rdx, %rcx
	jnb	.L145
	movabsq	$8176129505844426609, %rdx
	movq	%rdx, (%rax)
	movl	$1920038770, 8(%rax)
	movw	$29047, 12(%rax)
	movb	$114, 14(%rax)
	orl	$65536, 24(%rbx)
	movq	%rcx, %rax
	subq	(%rbx), %rax
	subl	%eax, 8(%rbx)
	movq	%rcx, (%rbx)
.L140:
	call	*%rsi
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L147
	andl	$-32769, 24(%rbx)
	sarq	$4, %rax
	imull	$-1431655765, %eax, %eax
	leal	16(%rax), %ecx
	call	_unlock
.L148:
	call	*%rsi
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L149
	sarq	$4, %rax
	imull	$-1431655765, %eax, %eax
	leal	16(%rax), %ecx
	call	_lock
	orl	$32768, 24(%rbx)
.L150:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L165
	movq	(%rbx), %rcx
	leaq	13(%rcx), %rdx
	movslq	36(%rbx), %r8
	addq	%r8, %rax
	cmpq	%rax, %rdx
	jnb	.L153
	movabsq	$3689065399400031560, %rax
	movq	%rax, (%rcx)
	movl	$926299444, 8(%rcx)
	movb	$56, 12(%rcx)
	orl	$65536, 24(%rbx)
	movq	%rdx, %rax
	subq	(%rbx), %rax
	subl	%eax, 8(%rbx)
	movq	%rdx, (%rbx)
.L154:
	leaq	9(%rdx), %rax
	movslq	36(%rbx), %rcx
	addq	16(%rbx), %rcx
	cmpq	%rcx, %rax
	jnb	.L155
	movabsq	$8314884493163324513, %rdi
	movq	%rdi, (%rdx)
	movb	$103, 8(%rdx)
	orl	$65536, 24(%rbx)
	movq	%rax, %rdx
	subq	(%rbx), %rdx
	subl	%edx, 8(%rbx)
	movq	%rax, (%rbx)
.L156:
	leaq	15(%rax), %rdx
	movslq	36(%rbx), %rcx
	addq	16(%rbx), %rcx
	cmpq	%rcx, %rdx
	jnb	.L157
	movabsq	$8176129505844426609, %rdi
	movq	%rdi, (%rax)
	movl	$1920038770, 8(%rax)
	movw	$29047, 12(%rax)
	movb	$114, 14(%rax)
	orl	$65536, 24(%rbx)
	movq	%rdx, %rax
	subq	(%rbx), %rax
	subl	%eax, 8(%rbx)
	movq	%rdx, (%rbx)
.L158:
	leaq	9(%rdx), %rcx
	movslq	36(%rbx), %rax
	addq	16(%rbx), %rax
	cmpq	%rax, %rcx
	jnb	.L159
	movabsq	$8176116286034964855, %rax
	movq	%rax, (%rdx)
	movb	$102, 8(%rdx)
	orl	$65536, 24(%rbx)
	movq	%rcx, %rax
	subq	(%rbx), %rax
	subl	%eax, 8(%rbx)
	movq	%rcx, (%rbx)
.L152:
	call	*%rsi
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L161
	andl	$-32769, 24(%rbx)
	sarq	$4, %rax
	imull	$-1431655765, %eax, %eax
	leal	16(%rax), %ecx
	call	_unlock
.L162:
	xorl	%eax, %eax
	addq	$112, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.L161:
	leaq	48(%rbx), %rcx
	call	LeaveCriticalSection
	jmp	.L162
.L149:
	leaq	48(%rbx), %rcx
	call	EnterCriticalSection
	.p2align 4,,4
	jmp	.L150
.L147:
	leaq	48(%rbx), %rcx
	call	LeaveCriticalSection
	.p2align 4,,4
	jmp	.L148
.L137:
	leaq	48(%rbx), %rcx
	call	EnterCriticalSection
	.p2align 4,,4
	jmp	.L138
.L135:
	leaq	48(%rbx), %rcx
	call	LeaveCriticalSection
	.p2align 4,,4
	jmp	.L136
.L125:
	leaq	48(%rbx), %rcx
	call	EnterCriticalSection
	.p2align 4,,4
	jmp	.L126
.L165:
	movq	%rdi, 96(%rsp)
	movq	$13, 104(%rsp)
	movq	%rbp, 80(%rsp)
	movq	$9, 88(%rsp)
	movq	%r13, 64(%rsp)
	movq	$15, 72(%rsp)
	leaq	.LC10(%rip), %rax
	movq	%rax, 48(%rsp)
	movq	$9, 56(%rsp)
	leaq	48(%rsp), %rax
	movq	%rax, 32(%rsp)
	leaq	64(%rsp), %r9
	leaq	80(%rsp), %r8
	movq	%r12, %rdx
	movq	%rbx, %rcx
	call	_ZN7fast_io7details5decay14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_S8_S8_EEEvT0_DpT1_
	jmp	.L152
.L163:
	movq	%rdi, 96(%rsp)
	movq	$13, 104(%rsp)
	movq	%rbp, 80(%rsp)
	movq	$9, 88(%rsp)
	leaq	80(%rsp), %r8
	movq	%r12, %rdx
	movq	%rbx, %rcx
	call	_ZN7fast_io7details5decay14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_EEEvT0_DpT1_
	jmp	.L128
.L164:
	movq	%rdi, 96(%rsp)
	movq	$13, 104(%rsp)
	movq	%rbp, 80(%rsp)
	movq	$9, 88(%rsp)
	movq	%r13, 64(%rsp)
	movq	$15, 72(%rsp)
	leaq	64(%rsp), %r9
	leaq	80(%rsp), %r8
	movq	%r12, %rdx
	movq	%rbx, %rcx
	call	_ZN7fast_io7details5decay14print_fallbackILb0ENS_28basic_c_io_observer_unlockedIcEEJNS_26io_print_define_rv_wrapperILb1ENS_18basic_io_scatter_tIcEEEES8_S8_EEEvT0_DpT1_
	jmp	.L140
.L153:
	movq	%rbx, %rcx
	call	clearerr
	movq	%rbx, %r9
	movl	$13, %r8d
	movl	$1, %edx
	leaq	.LC7(%rip), %rcx
	call	fwrite
	movq	%rbx, %rcx
	call	ferror
	movl	%eax, %edi
	call	*__imp__errno(%rip)
	movq	%rbx, %rcx
	call	clearerr
	testl	%edi, %edi
	jne	.L134
	movq	(%rbx), %rdx
	jmp	.L154
.L155:
	movq	%rbx, %rcx
	call	clearerr
	movq	%rbx, %r9
	movl	$9, %r8d
	movl	$1, %edx
	leaq	.LC8(%rip), %rcx
	call	fwrite
	movq	%rbx, %rcx
	call	ferror
	movl	%eax, %edi
	call	*__imp__errno(%rip)
	movq	%rbx, %rcx
	call	clearerr
	testl	%edi, %edi
	jne	.L134
	movq	(%rbx), %rax
	jmp	.L156
.L157:
	movq	%rbx, %rcx
	call	clearerr
	movq	%rbx, %r9
	movl	$15, %r8d
	movl	$1, %edx
	leaq	.LC9(%rip), %rcx
	call	fwrite
	movq	%rbx, %rcx
	call	ferror
	movl	%eax, %edi
	call	*__imp__errno(%rip)
	movq	%rbx, %rcx
	call	clearerr
	testl	%edi, %edi
	jne	.L134
	movq	(%rbx), %rdx
	jmp	.L158
.L143:
	movq	%rbx, %rcx
	call	clearerr
	movq	%rbx, %r9
	movl	$9, %r8d
	movl	$1, %edx
	leaq	.LC8(%rip), %rcx
	call	fwrite
	movq	%rbx, %rcx
	call	ferror
	movl	%eax, %r14d
	call	*__imp__errno(%rip)
	movq	%rbx, %rcx
	call	clearerr
	testl	%r14d, %r14d
	jne	.L134
	movq	(%rbx), %rax
	jmp	.L144
.L129:
	movq	%rbx, %rcx
	call	clearerr
	movq	%rbx, %r9
	movl	$13, %r8d
	movl	$1, %edx
	leaq	.LC7(%rip), %rcx
	call	fwrite
	movq	%rbx, %rcx
	call	ferror
	movl	%eax, %r13d
	call	*__imp__errno(%rip)
	movq	%rbx, %rcx
	call	clearerr
	testl	%r13d, %r13d
	jne	.L134
	movq	(%rbx), %rax
	jmp	.L130
.L159:
	movq	%rbx, %rcx
	call	clearerr
	movq	%rbx, %r9
	movl	$9, %r8d
	movl	$1, %edx
	leaq	.LC10(%rip), %rcx
	call	fwrite
	movq	%rbx, %rcx
	call	ferror
	movl	%eax, %edi
	call	*__imp__errno(%rip)
	movq	%rbx, %rcx
	call	clearerr
	testl	%edi, %edi
	je	.L152
	.p2align 4,,3
	jmp	.L134
.L132:
	movq	%rbx, %rcx
	.p2align 4,,6
	call	clearerr
	movq	%rbx, %r9
	movl	$9, %r8d
	movl	$1, %edx
	leaq	.LC8(%rip), %rcx
	call	fwrite
	movq	%rbx, %rcx
	call	ferror
	movl	%eax, %r13d
	call	*__imp__errno(%rip)
	movq	%rbx, %rcx
	call	clearerr
	testl	%r13d, %r13d
	je	.L128
	.p2align 4,,2
	jmp	.L134
.L145:
	movq	%rbx, %rcx
	.p2align 4,,5
	call	clearerr
	movq	%rbx, %r9
	movl	$15, %r8d
	movl	$1, %edx
	leaq	.LC9(%rip), %rcx
	call	fwrite
	movq	%rbx, %rcx
	call	ferror
	movl	%eax, %r14d
	call	*__imp__errno(%rip)
	movq	%rbx, %rcx
	call	clearerr
	testl	%r14d, %r14d
	je	.L140
	.p2align 4,,2
	jmp	.L134
.L141:
	movq	%rbx, %rcx
	.p2align 4,,5
	call	clearerr
	movq	%rbx, %r9
	movl	$13, %r8d
	movl	$1, %edx
	leaq	.LC7(%rip), %rcx
	call	fwrite
	movq	%rbx, %rcx
	call	ferror
	movl	%eax, %r14d
	call	*__imp__errno(%rip)
	movq	%rbx, %rcx
	call	clearerr
	testl	%r14d, %r14d
	jne	.L134
	movq	(%rbx), %rdx
	jmp	.L142
	.seh_endproc
	.section	.text.unlikely,"x"
	.def	main.cold;	.scl	3;	.type	32;	.endef
	.seh_proc	main.cold
	.seh_stackalloc	168
	.seh_savereg	%rbx, 112
	.seh_savereg	%rsi, 120
	.seh_savereg	%rdi, 128
	.seh_savereg	%rbp, 136
	.seh_savereg	%r12, 144
	.seh_savereg	%r13, 152
	.seh_savereg	%r14, 160
	.seh_endprologue
main.cold:
.L134:
	ud2
	.section	.text.startup,"x"
	.section	.text.unlikely,"x"
	.seh_endproc
.LCOLDE11:
	.section	.text.startup,"x"
.LHOTE11:
	.ident	"GCC: (GCC with MCF thread model, built by LH_Mouse.) 11.0.0 20200712 (experimental)"
	.def	_lock;	.scl	2;	.type	32;	.endef
	.def	_unlock;	.scl	2;	.type	32;	.endef
	.def	LeaveCriticalSection;	.scl	2;	.type	32;	.endef
	.def	EnterCriticalSection;	.scl	2;	.type	32;	.endef
	.def	memcpy;	.scl	2;	.type	32;	.endef
	.def	clearerr;	.scl	2;	.type	32;	.endef
	.def	fwrite;	.scl	2;	.type	32;	.endef
	.def	ferror;	.scl	2;	.type	32;	.endef
	.def	_ZdlPvy;	.scl	2;	.type	32;	.endef
	.def	_Znwy;	.scl	2;	.type	32;	.endef
	.def	_ZSt17__throw_bad_allocv;	.scl	2;	.type	32;	.endef
