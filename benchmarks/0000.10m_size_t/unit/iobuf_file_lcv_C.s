	.file	"iobuf_file_lcv_C.cc"
	.text
	.def	_ZN7fast_io7details3ryu15output_shortestILb0ELy0ELb1ELb0EDuLDu46EPcdEET5_NS0_27compile_time_floating_valueIXT2_ET3_XT4_EEES4_T6_.isra.0;	.scl	3;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details3ryu15output_shortestILb0ELy0ELb1ELb0EDuLDu46EPcdEET5_NS0_27compile_time_floating_valueIXT2_ET3_XT4_EEES4_T6_.isra.0
_ZN7fast_io7details3ryu15output_shortestILb0ELy0ELb1ELb0EDuLDu46EPcdEET5_NS0_27compile_time_floating_valueIXT2_ET3_XT4_EEES4_T6_.isra.0:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$72, %rsp
	.seh_stackalloc	72
	.seh_endprologue
	movq	%rcx, %r15
	movsd	%xmm1, 40(%rsp)
	movq	40(%rsp), %r12
	movabsq	$4503599627370495, %rax
	andq	%r12, %rax
	movq	%r12, %r10
	shrq	$52, %r10
	movl	%r10d, %r9d
	andl	$2047, %r9d
	cmpl	$2047, %r9d
	je	.L323
	testl	%r9d, %r9d
	je	.L324
	movabsq	$4503599627370496, %r8
	orq	%rax, %r8
	leal	-1023(%r9), %edx
	cmpl	$52, %edx
	ja	.L9
	movl	$1075, %ecx
	subl	%r9d, %ecx
	movq	$-1, %rdx
	salq	%cl, %rdx
	notq	%rdx
	testq	%r8, %rdx
	jne	.L9
	shrq	%cl, %r8
	testq	%r12, %r12
	jns	.L10
	movb	$45, (%r15)
	addq	$1, %r15
.L10:
	movabsq	$-3276141747490816367, %rax
	imulq	%r8, %rax
	rorq	$4, %rax
	movabsq	$1844674407370955, %rdx
	cmpq	%rdx, %rax
	jbe	.L11
	movabsq	$999999999999999, %rax
	cmpq	%rax, %r8
	ja	.L12
	movabsq	$99999999999999, %rax
	cmpq	%rax, %r8
	ja	.L146
	movabsq	$9999999999999, %rax
	cmpq	%rax, %r8
	ja	.L147
	movabsq	$999999999999, %rax
	cmpq	%rax, %r8
	ja	.L148
	movabsq	$99999999999, %rax
	cmpq	%rax, %r8
	ja	.L149
	movabsq	$9999999999, %rax
	cmpq	%rax, %r8
	ja	.L150
	cmpq	$999999999, %r8
	ja	.L151
	cmpq	$99999999, %r8
	ja	.L152
	cmpq	$9999999, %r8
	ja	.L153
	cmpq	$999999, %r8
	ja	.L154
	cmpq	$99999, %r8
	ja	.L155
	cmpq	$9999, %r8
	ja	.L156
	cmpq	$999, %r8
	ja	.L157
	cmpq	$99, %r8
	ja	.L14
	cmpq	$10, %r8
	sbbq	%rax, %rax
	leaq	2(%r15,%rax), %r9
	movq	%r9, %r11
.L36:
	cmpq	$9, %r8
	jbe	.L38
.L331:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%r8,2), %eax
	movw	%ax, -2(%r9)
	jmp	.L1
.L324:
	testq	%rax, %rax
	je	.L325
	movq	%r12, %rdi
	andl	$1, %edi
	movq	%rdi, 40(%rsp)
	leaq	0(,%rax,4), %r8
	leaq	2(%r8), %r14
	leaq	-2(%r8), %r10
	movl	$57, %ecx
	movl	$5200, %eax
	movl	$-325, %ebp
	movl	$751, %ebx
	movl	$1, %r11d
.L137:
	leaq	_ZN7fast_io7details3ryu4pow5IdLb1EE5splitE(%rip), %rdx
	addq	%rdx, %rax
	movq	8(%rax), %r9
	movq	(%rax), %r13
	movq	%r13, %rax
	mulq	%r14
	movq	%rdx, %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rdx, %rdi
	movq	%r14, %rax
	movq	%r9, %r14
	mulq	%r9
	addq	%rax, %rsi
	adcq	%rdx, %rdi
	shrdq	%cl, %rdi, %rsi
	shrq	%cl, %rdi
	testb	$64, %cl
	cmovne	%rdi, %rsi
	movq	%rsi, %r9
	movq	%r13, %rax
	mulq	%r10
	movq	%rdx, %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rdx, %rdi
	movq	%r10, %rax
	mulq	%r14
	addq	%rax, %rsi
	adcq	%rdx, %rdi
	shrdq	%cl, %rdi, %rsi
	shrq	%cl, %rdi
	testb	$64, %cl
	cmovne	%rdi, %rsi
	movq	%rsi, %r10
	movq	%r13, %rax
	mulq	%r8
	movq	%rdx, %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rdx, %rdi
	movq	%r14, %rax
	mulq	%r8
	addq	%rax, %rsi
	adcq	%rdx, %rdi
	shrdq	%cl, %rdi, %rsi
	shrq	%cl, %rdi
	testb	$64, %cl
	cmovne	%rdi, %rsi
	movq	%rsi, %rdi
	cmpl	$1, %ebx
	jbe	.L326
	cmpl	$62, %ebx
	ja	.L41
	movq	$-1, %rax
	movl	%ebx, %ecx
	salq	%cl, %rax
	notq	%rax
	testq	%r8, %rax
	sete	%al
.L45:
	testb	%al, %al
	je	.L41
	movabsq	$-3689348814741910323, %rcx
	movq	%r10, %rax
	mulq	%rcx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	leaq	(%rcx,%rcx,4), %rax
	addq	%rax, %rax
	movl	%r10d, %r13d
	subl	%eax, %r13d
.L54:
	movabsq	$-3689348814741910323, %rdx
	movq	%r9, %rax
	mulq	%rdx
	shrq	$3, %rdx
	movq	%rdx, %r8
	cmpq	%rdx, %rcx
	jnb	.L283
	movl	$1, %r9d
	xorl	%r11d, %r11d
.L133:
	xorl	%esi, %esi
	xorl	%ebx, %ebx
	movabsq	$-3689348814741910323, %r14
.L56:
	movq	%rdi, %r10
	movq	%rdi, %rax
	mulq	%r14
	movq	%rdx, %rdi
	shrq	$3, %rdi
	movl	%ebx, %edx
	leaq	(%rdi,%rdi,4), %rax
	addq	%rax, %rax
	movl	%r10d, %ebx
	subl	%eax, %ebx
	testb	%r13b, %r13b
	sete	%al
	andl	%eax, %r11d
	testb	%dl, %dl
	sete	%al
	andl	%eax, %r9d
	addl	$1, %esi
	movq	%r8, %rax
	mulq	%r14
	movq	%rdx, %r8
	shrq	$3, %r8
	movq	%rcx, %r10
	movq	%rcx, %rax
	mulq	%r14
	movq	%rdx, %rcx
	shrq	$3, %rcx
	leaq	(%rcx,%rcx,4), %rax
	addq	%rax, %rax
	movq	%r10, %rdx
	subq	%rax, %rdx
	movl	%edx, %r13d
	cmpq	%rcx, %r8
	ja	.L56
	movq	%rdx, %r14
.L55:
	testb	%r11b, %r11b
	je	.L57
.L134:
	movabsq	$-3689348814741910323, %r8
	movabsq	$1844674407370955161, %r11
	testq	%r14, %r14
	jne	.L319
.L58:
	movq	%rdi, %r10
	movq	%rdi, %rax
	mulq	%r8
	movq	%rdx, %rdi
	shrq	$3, %rdi
	testb	%bl, %bl
	sete	%al
	andl	%eax, %r9d
	leaq	(%rdi,%rdi,4), %rax
	addq	%rax, %rax
	movl	%r10d, %ebx
	subl	%eax, %ebx
	addl	$1, %esi
	movq	%rcx, %r10
	movq	%rcx, %rax
	mulq	%r8
	movq	%rdx, %rcx
	shrq	$3, %rcx
	leaq	(%r10,%r10,2), %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$8, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$16, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$32, %rdx
	addq	%rdx, %rax
	leaq	(%r10,%rax,4), %rax
	rorq	%rax
	cmpq	%r11, %rax
	jbe	.L58
.L319:
	addl	%esi, %ebp
	movl	$1, %r11d
.L59:
	testb	%r9b, %r9b
	je	.L60
	cmpb	$5, %bl
	jne	.L60
	movq	%rdi, %rbx
	andl	$1, %ebx
	addl	$4, %ebx
.L60:
	cmpq	%r10, %rdi
	je	.L327
.L61:
	xorl	%eax, %eax
	cmpb	$4, %bl
	seta	%al
	movq	%rax, 40(%rsp)
.L62:
	movq	40(%rsp), %rbx
	addq	%rdi, %rbx
.L63:
	testq	%r12, %r12
	jns	.L68
	movb	$45, (%r15)
	addq	$1, %r15
.L68:
	movabsq	$9999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L198
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L199
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L200
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L201
	movabsq	$999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L202
	movabsq	$99999999999, %rax
	cmpq	%rax, %rbx
	ja	.L203
	movabsq	$9999999999, %rax
	cmpq	%rax, %rbx
	ja	.L204
	cmpq	$999999999, %rbx
	ja	.L205
	cmpq	$99999999, %rbx
	ja	.L206
	cmpq	$9999999, %rbx
	ja	.L207
	cmpq	$999999, %rbx
	ja	.L208
	cmpq	$99999, %rbx
	ja	.L209
	cmpq	$9999, %rbx
	ja	.L210
	cmpq	$999, %rbx
	ja	.L211
	cmpq	$99, %rbx
	ja	.L212
	cmpq	$9, %rbx
	ja	.L328
	leal	1(%rbp), %edi
	testl	%ebp, %ebp
	jle	.L329
	cmpl	$4, %edi
	ja	.L282
	movl	$1, %esi
	movl	$1, %eax
.L95:
	leaq	(%r15,%rax), %r8
.L96:
	cmpq	$9, %rbx
	jbe	.L98
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rbx,2), %eax
	movw	%ax, -2(%r8)
.L99:
	leaq	(%r15,%rsi), %r11
	movslq	%ebp, %rbp
	movq	%rbp, %r8
	movl	$48, %edx
	movq	%r11, %rcx
	call	memset
	movq	%rax, %r11
	addq	%rbp, %r11
	jmp	.L1
.L323:
	testq	%rax, %rax
	jne	.L330
	testq	%r12, %r12
	jns	.L5
	movl	$1718511917, (%rcx)
	leaq	4(%rcx), %r11
.L1:
	movq	%r11, %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L330:
	movw	$24942, (%rcx)
	movb	$110, 2(%rcx)
	leaq	3(%rcx), %r11
	jmp	.L1
.L325:
	leaq	1(%rcx), %r11
	testq	%r12, %r12
	jns	.L8
	movb	$45, (%rcx)
	leaq	2(%rcx), %rax
	movq	%r11, %r15
	movq	%rax, %r11
.L8:
	movb	$48, (%r15)
	jmp	.L1
.L12:
	leaq	16(%r15), %r11
.L144:
	movq	%r11, %r9
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %rbx
.L18:
	movq	%r8, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%rbx
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %rcx
	salq	$2, %rcx
	movq	%r8, %rdx
	subq	%rcx, %rdx
	movq	%r8, %rcx
	movq	%rax, %r8
	subq	$2, %r9
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r9)
	cmpq	$9999, %rcx
	ja	.L18
	cmpq	$9, %r8
	ja	.L331
.L38:
	addl	$48, %r8d
	movb	%r8b, -1(%r9)
	jmp	.L1
.L5:
	movw	$28265, (%rcx)
	movb	$102, 2(%rcx)
	leaq	3(%rcx), %r11
	jmp	.L1
.L9:
	leal	-1077(%r9), %edx
	movq	%r12, %rdi
	andl	$1, %edi
	movq	%rdi, 40(%rsp)
	salq	$2, %r8
	testq	%rax, %rax
	jne	.L189
	testl	$2046, %r10d
	sete	%r11b
	sete	%al
	movzbl	%al, %eax
.L39:
	leaq	2(%r8), %r13
	movq	%r13, %r14
	leaq	-1(%r8), %rcx
	subq	%rax, %rcx
	movq	%rcx, 56(%rsp)
	movq	%rcx, %r10
	testl	%edx, %edx
	js	.L40
	movslq	%edx, %rsi
	movabsq	$169464822037455, %rax
	imulq	%rax, %rsi
	shrq	$49, %rsi
	xorl	%eax, %eax
	cmpl	$3, %edx
	setg	%al
	subl	%eax, %esi
	movl	%esi, %ebp
	movl	%esi, %eax
	salq	$4, %rax
	leaq	_ZN7fast_io7details3ryu4pow5IdLb1EE9inv_splitE(%rip), %rcx
	addq	%rcx, %rax
	movq	8(%rax), %r14
	movq	(%rax), %rdi
	leal	(%rsi,%rsi,8), %eax
	movl	%eax, %ecx
	sall	$5, %ecx
	addl	%ecx, %eax
	leal	(%rsi,%rax,8), %eax
	leal	(%rsi,%rax,2), %eax
	leal	(%rsi,%rax,4), %eax
	leal	(%rsi,%rax,4), %eax
	sall	$4, %eax
	subl	%esi, %eax
	shrl	$19, %eax
	movl	%esi, %ecx
	subl	%edx, %ecx
	leal	61(%rax,%rcx), %r11d
	movq	%rdi, %rax
	mulq	%r13
	movq	%rdx, %rax
	xorl	%edx, %edx
	movq	%rax, %rcx
	movq	%rdx, %rbx
	movq	%r14, %rax
	mulq	%r13
	addq	%rcx, %rax
	adcq	%rbx, %rdx
	movl	%r11d, 52(%rsp)
	movl	%r11d, %ecx
	shrdq	%cl, %rdx, %rax
	shrq	%cl, %rdx
	testb	$64, %r11b
	cmovne	%rdx, %rax
	movq	%rax, %r9
	movq	%r10, %rbx
	movq	%r10, %rax
	mulq	%rdi
	movq	%rdx, %r10
	xorl	%r11d, %r11d
	movq	%rbx, %rax
	mulq	%r14
	addq	%rax, %r10
	adcq	%rdx, %r11
	movzbl	52(%rsp), %ecx
	shrdq	%cl, %r11, %r10
	shrq	%cl, %r11
	testb	$64, %cl
	cmovne	%r11, %r10
	movq	%rdi, %rax
	mulq	%r8
	movq	%rdx, %rax
	xorl	%edx, %edx
	movq	%rax, %rcx
	movq	%rdx, %rbx
	movq	%r14, %rax
	mulq	%r8
	addq	%rcx, %rax
	adcq	%rbx, %rdx
	movzbl	52(%rsp), %ecx
	shrdq	%cl, %rdx, %rax
	shrq	%cl, %rdx
	testb	$64, %cl
	cmovne	%rdx, %rax
	movq	%rax, %rdi
	cmpl	$21, %esi
	jbe	.L332
.L41:
	movq	%r9, %rdx
	shrq	$2, %rdx
	movabsq	$2951479051793528259, %r11
	movq	%rdx, %rax
	mulq	%r11
	movq	%rdx, %rcx
	shrq	$2, %rcx
	movq	%r10, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%r11
	shrq	$2, %rdx
	movq	%rdx, %r8
	cmpq	%rdx, %rcx
	jbe	.L197
	movq	%rdi, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%r11
	movq	%rdx, %r11
	shrq	$2, %r11
	andq	$-4, %rdx
	leaq	(%rdx,%r11), %rax
	leaq	(%rax,%rax,4), %rax
	salq	$2, %rax
	subq	%rax, %rdi
	cmpq	$49, %rdi
	seta	%bl
	movq	%r8, %r10
	movq	%rcx, %r9
	movq	%r11, %rdi
	movl	$2, %r11d
.L64:
	movabsq	$-3689348814741910323, %r8
	movq	%r9, %rax
	mulq	%r8
	movq	%rdx, %r9
	shrq	$3, %r9
	movq	%r10, %rax
	mulq	%r8
	movq	%rdx, %rcx
	shrq	$3, %rcx
	cmpq	%r9, %rcx
	jnb	.L65
.L66:
	movq	%rdi, %rbx
	movq	%rdi, %rax
	mulq	%r8
	movq	%rdx, %rdi
	shrq	$3, %rdi
	addl	$1, %r11d
	movq	%r9, %rax
	mulq	%r8
	movq	%rdx, %r9
	shrq	$3, %r9
	movq	%rcx, %r10
	movq	%rcx, %rax
	mulq	%r8
	movq	%rdx, %rcx
	shrq	$3, %rcx
	cmpq	%rcx, %r9
	ja	.L66
	leaq	(%rdi,%rdi,4), %rax
	addq	%rax, %rax
	subq	%rax, %rbx
	cmpq	$4, %rbx
	seta	%bl
.L65:
	cmpq	%r10, %rdi
	movl	$1, %eax
	cmove	%eax, %ebx
	movzbl	%bl, %ebx
	addq	%rdi, %rbx
	addl	%r11d, %ebp
	jmp	.L63
.L146:
	movl	$15, %r11d
.L13:
	addq	%r15, %r11
	cmpq	$99, %r8
	ja	.L144
	movq	%r11, %r9
	jmp	.L36
.L11:
	movabsq	$3777893186295716171, %rdx
	movq	%r8, %rax
	mulq	%rdx
	movq	%rdx, %rcx
	shrq	$11, %rcx
	movabsq	$-3689348814741910323, %r10
.L20:
	movq	%rcx, %rax
	mulq	%r10
	shrq	$3, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %r9
	addq	%r9, %r9
	movq	%rcx, %rdx
	subq	%r9, %rdx
	movq	%rcx, %r9
	movq	%rax, %rcx
	je	.L20
	movabsq	$999999999999999, %rax
	cmpq	%rax, %r8
	ja	.L159
	movabsq	$99999999999999, %rax
	cmpq	%rax, %r8
	ja	.L160
	movabsq	$9999999999999, %rax
	cmpq	%rax, %r8
	ja	.L161
	movabsq	$999999999999, %rax
	cmpq	%rax, %r8
	ja	.L162
	movabsq	$99999999999, %rax
	cmpq	%rax, %r8
	ja	.L163
	movabsq	$9999999999, %rax
	cmpq	%rax, %r8
	ja	.L164
	cmpq	$999999999, %r8
	ja	.L165
	cmpq	$99999999, %r8
	ja	.L166
	cmpq	$9999999, %r8
	ja	.L167
	cmpq	$999999, %r8
	ja	.L168
	cmpq	$99999, %r8
	ja	.L169
	cmpq	$9999, %r8
	ja	.L170
	cmpq	$999, %r8
	ja	.L171
	cmpq	$99, %r8
	ja	.L172
	cmpq	$10, %r8
	sbbq	%r11, %r11
	addq	$2, %r11
	cmpq	$10, %r8
	sbbl	%eax, %eax
	addl	$2, %eax
.L21:
	movabsq	$99999999999, %rdx
	cmpq	%rdx, %r9
	ja	.L22
	movabsq	$9999999999, %rdx
	cmpq	%rdx, %r9
	ja	.L22
	cmpq	$999999999, %r9
	ja	.L174
	cmpq	$99999999, %r9
	ja	.L175
	cmpq	$9999999, %r9
	ja	.L176
	cmpq	$999999, %r9
	ja	.L177
	cmpq	$99999, %r9
	ja	.L178
	cmpq	$9999, %r9
	ja	.L179
	cmpq	$999, %r9
	ja	.L180
	cmpq	$99, %r9
	ja	.L181
	cmpq	$9, %r9
	ja	.L24
	movl	$5, %ecx
.L25:
	cmpl	%eax, %ecx
	jb	.L333
.L22:
	addq	%r15, %r11
	movq	%r11, %r9
	cmpq	$99, %r8
	jbe	.L36
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %rbx
.L37:
	movq	%r8, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%rbx
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %rcx
	salq	$2, %rcx
	movq	%r8, %rdx
	subq	%rcx, %rdx
	movq	%r8, %rcx
	movq	%rax, %r8
	subq	$2, %r9
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r9)
	cmpq	$9999, %rcx
	ja	.L37
	jmp	.L36
.L147:
	movl	$14, %r11d
	jmp	.L13
.L189:
	movl	$1, %eax
	movl	$1, %r11d
	jmp	.L39
.L326:
	movabsq	$-3689348814741910323, %r8
	movq	%r10, %rax
	mulq	%r8
	movq	%rdx, %rcx
	shrq	$3, %rcx
	leaq	(%rcx,%rcx,4), %rax
	addq	%rax, %rax
	movq	%r10, %rsi
	subq	%rax, %rsi
	movq	%rsi, %r14
	movl	%esi, %r13d
	cmpq	$0, 40(%rsp)
	je	.L53
	subq	$1, %r9
	jmp	.L54
.L152:
	movl	$9, %r11d
	jmp	.L13
.L57:
	addl	%esi, %ebp
	jmp	.L59
.L283:
	xorl	%r11d, %r11d
	xorl	%ebx, %ebx
	jmp	.L60
.L53:
	movq	%r9, %rax
	mulq	%r8
	movq	%rdx, %r8
	shrq	$3, %r8
	cmpq	%rcx, %r8
	jbe	.L195
	movl	$1, %r9d
	jmp	.L133
.L195:
	xorl	%esi, %esi
	movl	$1, %r9d
	xorl	%ebx, %ebx
	jmp	.L55
.L332:
	leaq	(%r8,%r8,2), %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$8, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$16, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$32, %rdx
	addq	%rdx, %rax
	leaq	(%r8,%rax,4), %rax
	movabsq	$3689348814741910323, %rcx
	cmpq	%rcx, %rax
	ja	.L42
	movabsq	$-3689348814741910323, %r11
	movq	%r8, %rax
	mulq	%r11
	movq	%rdx, %r8
	shrq	$2, %r8
	movl	$1, %ebx
	jmp	.L44
.L190:
	movq	%rdx, %r8
.L44:
	leaq	(%r8,%r8,2), %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$8, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$16, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$32, %rdx
	addq	%rdx, %rax
	leaq	(%r8,%rax,4), %rax
	cmpq	%rcx, %rax
	ja	.L43
	movq	%r8, %rax
	mulq	%r11
	shrq	$2, %rdx
	addl	$1, %ebx
	cmpq	$4, %r8
	ja	.L190
	xorl	%ebx, %ebx
.L43:
	cmpl	%ebx, %esi
	setbe	%al
	jmp	.L45
.L148:
	movl	$13, %r11d
	jmp	.L13
.L327:
	cmpq	$0, 40(%rsp)
	jne	.L62
	testb	%r11b, %r11b
	jne	.L61
	movq	$1, 40(%rsp)
	jmp	.L62
.L149:
	movl	$12, %r11d
	jmp	.L13
.L198:
	movl	$22, %edx
	movl	$18, %ecx
	movl	$18, %r12d
	movl	$17, %esi
	movl	$19, %r8d
	movl	$17, %eax
.L69:
	leal	0(%rbp,%rax), %edi
	leal	-1(%rax,%rbp), %r9d
	cmpl	%r9d, %eax
	jg	.L71
	cmpl	%edx, %edi
	jbe	.L334
.L72:
	cmpq	$9, %rbx
	jbe	.L75
	movabsq	$9999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L215
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L216
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L217
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L218
	movabsq	$999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L219
	movabsq	$99999999999, %rax
	cmpq	%rax, %rbx
	ja	.L220
	movabsq	$9999999999, %rax
	cmpq	%rax, %rbx
	ja	.L221
	cmpq	$999999999, %rbx
	ja	.L222
	cmpq	$99999999, %rbx
	ja	.L223
	cmpq	$9999999, %rbx
	ja	.L224
	cmpq	$999999, %rbx
	ja	.L225
	cmpq	$99999, %rbx
	ja	.L226
	cmpq	$9999, %rbx
	ja	.L227
	cmpq	$999, %rbx
	ja	.L228
	cmpq	$99, %rbx
	ja	.L335
	leaq	3(%r15), %r11
.L84:
	movq	%r11, %r8
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
.L131:
	movzwl	(%r10,%rbx,2), %eax
	movw	%ax, -2(%r8)
.L87:
	movzbl	1(%r15), %eax
	movb	%al, (%r15)
	movb	$46, 1(%r15)
.L88:
	movb	$101, (%r11)
	testl	%r9d, %r9d
	js	.L336
	movl	$43, %eax
.L89:
	movb	%al, 1(%r11)
	cmpl	$99, %r9d
	jg	.L337
	movslq	%r9d, %r9
	movzwl	(%r10,%r9,2), %eax
	movw	%ax, 2(%r11)
	addq	$4, %r11
	jmp	.L1
.L71:
	testl	%r9d, %r9d
	js	.L77
	cmpl	%edi, %eax
	je	.L338
	cmpl	%edx, %r8d
	.p2align 4,,3
	ja	.L72
	leaq	1(%r15), %r9
	movabsq	$9999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L109
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L256
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L257
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L258
	movabsq	$999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L259
	movabsq	$99999999999, %rax
	cmpq	%rax, %rbx
	ja	.L260
	movabsq	$9999999999, %rax
	cmpq	%rax, %rbx
	ja	.L261
	cmpq	$999999999, %rbx
	ja	.L262
	cmpq	$99999999, %rbx
	ja	.L263
	cmpq	$9999999, %rbx
	ja	.L264
	cmpq	$999999, %rbx
	ja	.L265
	cmpq	$99999, %rbx
	ja	.L266
	cmpq	$9999, %rbx
	ja	.L267
	cmpq	$999, %rbx
	ja	.L268
	cmpq	$99, %rbx
	ja	.L111
	cmpq	$10, %rbx
	sbbl	%eax, %eax
	leal	2(%rax), %r8d
	addq	%r9, %r8
.L114:
	cmpq	$9, %rbx
	jbe	.L116
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rbx,2), %eax
	movw	%ax, -2(%r8)
.L117:
	movslq	%edi, %rdi
	movq	%rdi, %r8
	movq	%r9, %rdx
	movq	%r15, %rcx
	call	memmove
	movb	$46, (%r15,%rdi)
	leaq	(%r15,%r12), %r11
	jmp	.L1
.L336:
	movl	$1, %r9d
	subl	%edi, %r9d
	movl	$45, %eax
	jmp	.L89
.L77:
	subl	%r9d, %ecx
	cmpl	%edx, %ecx
	ja	.L72
	movw	$11824, (%r15)
	leaq	2(%r15), %r11
	negl	%edi
	movq	%rdi, %r8
	movl	$48, %edx
	movq	%r11, %rcx
	call	memset
	movq	%rax, %r11
	addq	%rdi, %r11
	movabsq	$9999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L118
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L269
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L270
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L271
	movabsq	$999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L272
	movabsq	$99999999999, %rax
	cmpq	%rax, %rbx
	ja	.L273
	movabsq	$9999999999, %rax
	cmpq	%rax, %rbx
	ja	.L274
	cmpq	$999999999, %rbx
	ja	.L275
	cmpq	$99999999, %rbx
	ja	.L276
	cmpq	$9999999, %rbx
	ja	.L277
	cmpq	$999999, %rbx
	ja	.L278
	cmpq	$99999, %rbx
	ja	.L279
	cmpq	$9999, %rbx
	ja	.L280
	cmpq	$999, %rbx
	ja	.L281
	cmpq	$99, %rbx
	ja	.L120
	cmpq	$10, %rbx
	sbbl	%eax, %eax
	addl	$2, %eax
.L122:
	leaq	(%r11,%rax), %r8
.L123:
	cmpq	$9, %rbx
	jbe	.L125
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rbx,2), %eax
	movw	%ax, -2(%r8)
.L126:
	addq	%rsi, %r11
	jmp	.L1
.L150:
	movl	$11, %r11d
	jmp	.L13
.L199:
	movl	$21, %edx
	movl	$17, %ecx
	movl	$17, %r12d
	movl	$16, %esi
	movl	$18, %r8d
	movl	$16, %eax
	jmp	.L69
.L334:
	movabsq	$9999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L91
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L230
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L231
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L232
	movabsq	$999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L233
	movabsq	$99999999999, %rax
	cmpq	%rax, %rbx
	ja	.L234
	movabsq	$9999999999, %rax
	cmpq	%rax, %rbx
	ja	.L235
	cmpq	$999999999, %rbx
	ja	.L236
	cmpq	$99999999, %rbx
	ja	.L237
	cmpq	$9999999, %rbx
	ja	.L238
	cmpq	$999999, %rbx
	ja	.L239
	cmpq	$99999, %rbx
	ja	.L240
	cmpq	$9999, %rbx
	ja	.L241
	cmpq	$999, %rbx
	ja	.L242
	cmpq	$99, %rbx
	jbe	.L339
	movl	$100, %ecx
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %rbx
	leaq	1(%r15), %r8
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rdx,2), %eax
	movw	%ax, 1(%r15)
.L98:
	addl	$48, %ebx
	movb	%bl, -1(%r8)
	jmp	.L99
.L42:
	cmpq	$0, 40(%rsp)
	jne	.L191
	movq	56(%rsp), %rbx
	leaq	(%rbx,%rbx,2), %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$8, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$16, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$32, %rdx
	addq	%rdx, %rax
	leaq	(%rbx,%rax,4), %rax
	cmpq	%rcx, %rax
	ja	.L192
	movq	%rbx, %rax
	movabsq	$-3689348814741910323, %r11
	mulq	%r11
	shrq	$2, %rdx
	movq	%rdx, %r8
	movl	$1, %ebx
	jmp	.L49
.L193:
	movq	%rdx, %r8
.L49:
	leaq	(%r8,%r8,2), %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$8, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$16, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$32, %rdx
	addq	%rdx, %rax
	leaq	(%r8,%rax,4), %rax
	cmpq	%rcx, %rax
	ja	.L47
	movq	%r8, %rax
	mulq	%r11
	shrq	$2, %rdx
	addl	$1, %ebx
	cmpq	$4, %r8
	ja	.L193
.L192:
	xorl	%ebx, %ebx
.L47:
	cmpl	%ebx, %esi
	ja	.L41
	movabsq	$-3689348814741910323, %rcx
	movq	%r9, %rax
	mulq	%rcx
	shrq	$3, %rdx
	movq	%rdx, %r8
	movq	%r10, %rax
	mulq	%rcx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	leaq	(%rcx,%rcx,4), %rax
	addq	%rax, %rax
	movq	%r10, %rsi
	subq	%rax, %rsi
	movq	%rsi, %r14
	movl	%esi, %r13d
	xorl	%r9d, %r9d
	cmpq	%r8, %rcx
	jnb	.L284
	movl	$1, %r11d
	jmp	.L133
.L151:
	movl	$10, %r11d
	jmp	.L13
.L200:
	movl	$20, %edx
	movl	$16, %ecx
	movl	$16, %r12d
	movl	$15, %esi
	movl	$17, %r8d
	movl	$15, %eax
	jmp	.L69
.L197:
	xorl	%ebx, %ebx
	xorl	%r11d, %r11d
	jmp	.L64
.L201:
	movl	$19, %edx
	movl	$15, %ecx
	movl	$15, %r12d
	movl	$14, %esi
	movl	$16, %r8d
	movl	$14, %eax
	jmp	.L69
.L215:
	movl	$18, %r11d
.L81:
	addq	%r15, %r11
	cmpq	$99, %rbx
	jbe	.L84
	movq	%r11, %r8
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %rsi
.L85:
	movq	%rbx, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%rsi
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %rcx
	salq	$2, %rcx
	movq	%rbx, %rdx
	subq	%rcx, %rdx
	movq	%rbx, %rcx
	movq	%rax, %rbx
	subq	$2, %r8
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r8)
	cmpq	$9999, %rcx
	ja	.L85
	cmpq	$999, %rcx
	ja	.L131
.L86:
	addl	$48, %ebx
	movb	%bl, -1(%r8)
	jmp	.L87
.L338:
	cmpl	%ecx, %edx
	jb	.L72
	movabsq	$9999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L340
.L74:
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L243
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L244
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L245
	movabsq	$999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L246
	movabsq	$99999999999, %rax
	cmpq	%rax, %rbx
	ja	.L247
	movabsq	$9999999999, %rax
	cmpq	%rax, %rbx
	ja	.L248
	cmpq	$999999999, %rbx
	ja	.L249
	cmpq	$99999999, %rbx
	ja	.L250
	cmpq	$9999999, %rbx
	ja	.L251
	cmpq	$999999, %rbx
	ja	.L252
	cmpq	$99999, %rbx
	ja	.L253
	cmpq	$9999, %rbx
	ja	.L254
	cmpq	$999, %rbx
	ja	.L255
	cmpq	$99, %rbx
	ja	.L102
	cmpq	$10, %rbx
	sbbl	%eax, %eax
	leal	2(%rax), %ecx
	addq	%r15, %rcx
.L105:
	cmpq	$9, %rbx
	jbe	.L107
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rbx,2), %eax
	movw	%ax, -2(%rcx)
.L108:
	leaq	(%r15,%rsi), %r11
	jmp	.L1
.L159:
	movl	$16, %r11d
	movl	$16, %eax
	jmp	.L21
.L120:
	movl	$100, %ecx
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %rbx
	leaq	1(%r11), %r8
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rdx,2), %eax
	movw	%ax, 1(%r11)
.L125:
	addl	$48, %ebx
	movb	%bl, -1(%r8)
	jmp	.L126
.L202:
	movl	$18, %edx
	movl	$14, %ecx
	movl	$14, %r12d
	movl	$13, %esi
	movl	$15, %r8d
	movl	$13, %eax
	jmp	.L69
.L153:
	movl	$8, %r11d
	jmp	.L13
.L216:
	movl	$17, %r11d
	jmp	.L81
.L191:
	xorl	%r8d, %r8d
	movabsq	$-3689348814741910323, %r11
	jmp	.L46
.L194:
	movq	%rdx, %r13
.L46:
	leaq	0(%r13,%r13,2), %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$8, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$16, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$32, %rdx
	addq	%rdx, %rax
	leaq	0(%r13,%rax,4), %rax
	cmpq	%rcx, %rax
	ja	.L51
	movq	%r13, %rax
	mulq	%r11
	shrq	$2, %rdx
	addl	$1, %r8d
	cmpq	$4, %r13
	ja	.L194
	xorl	%r8d, %r8d
.L51:
	xorl	%eax, %eax
	cmpl	%r8d, %esi
	setbe	%al
	subq	%rax, %r9
	jmp	.L41
.L154:
	movl	$7, %r11d
	jmp	.L13
.L203:
	movl	$17, %edx
	movl	$13, %ecx
	movl	$13, %r12d
	movl	$12, %esi
	movl	$14, %r8d
	movl	$12, %eax
	jmp	.L69
.L217:
	movl	$16, %r11d
	jmp	.L81
.L111:
	movl	$100, %ecx
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %rbx
	leaq	2(%r15), %r8
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rdx,2), %eax
	movw	%ax, 2(%r15)
.L116:
	addl	$48, %ebx
	movb	%bl, -1(%r8)
	jmp	.L117
.L337:
	movl	%r9d, %edx
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%rdx,%rax,8), %rcx
	movq	%rcx, %rax
	salq	$10, %rax
	subq	%rcx, %rax
	salq	$5, %rax
	addq	%rdx, %rax
	leaq	(%rdx,%rax,4), %rax
	leaq	(%rdx,%rax,8), %rax
	salq	$5, %rax
	subq	%rdx, %rax
	shrq	$37, %rax
	leal	48(%rax), %edx
	movb	%dl, 2(%r11)
	leal	(%rax,%rax,4), %eax
	leal	(%rax,%rax,4), %eax
	sall	$2, %eax
	subl	%eax, %r9d
	movzwl	(%r10,%r9,2), %eax
	movw	%ax, 3(%r11)
	addq	$5, %r11
	jmp	.L1
.L204:
	movl	$16, %edx
	movl	$12, %ecx
	movl	$12, %r12d
	movl	$11, %esi
	movl	$13, %r8d
	movl	$11, %eax
	jmp	.L69
.L155:
	movl	$6, %r11d
	jmp	.L13
.L160:
	movl	$15, %r11d
	movl	$15, %eax
	jmp	.L21
.L282:
	movl	%ebp, %r9d
.L75:
	addl	$48, %ebx
	movb	%bl, (%r15)
	leaq	1(%r15), %r11
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	jmp	.L88
.L218:
	movl	$15, %r11d
	jmp	.L81
.L156:
	movl	$5, %r11d
	jmp	.L13
.L161:
	movl	$14, %r11d
	movl	$14, %eax
	jmp	.L21
.L205:
	movl	$15, %edx
	movl	$11, %ecx
	movl	$11, %r12d
	movl	$10, %esi
	movl	$12, %r8d
	movl	$10, %eax
	jmp	.L69
.L118:
	leaq	17(%r11), %r8
.L142:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %r9
.L124:
	movq	%rbx, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%r9
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %rcx
	salq	$2, %rcx
	movq	%rbx, %rdx
	subq	%rcx, %rdx
	movq	%rbx, %rcx
	movq	%rax, %rbx
	subq	$2, %r8
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r8)
	cmpq	$9999, %rcx
	ja	.L124
	jmp	.L123
.L91:
	leaq	17(%r15), %r8
.L139:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %r9
.L97:
	movq	%rbx, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%r9
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %rcx
	salq	$2, %rcx
	movq	%rbx, %rdx
	subq	%rcx, %rdx
	movq	%rbx, %rcx
	movq	%rax, %rbx
	subq	$2, %r8
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r8)
	cmpq	$9999, %rcx
	ja	.L97
	jmp	.L96
.L219:
	movl	$14, %r11d
	jmp	.L81
.L162:
	movl	$13, %r11d
	movl	$13, %eax
	jmp	.L21
.L206:
	movl	$14, %edx
	movl	$10, %ecx
	movl	$10, %r12d
	movl	$9, %esi
	movl	$11, %r8d
	movl	$9, %eax
	jmp	.L69
.L157:
	movl	$4, %r11d
	jmp	.L13
.L40:
	movl	$1077, %eax
	subl	%r9d, %eax
	movslq	%eax, %rbx
	movabsq	$196742565691928, %rcx
	imulq	%rcx, %rbx
	shrq	$48, %rbx
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	seta	%cl
	subl	%ecx, %ebx
	leal	(%rdx,%rbx), %ebp
	subl	%ebx, %eax
	movl	%eax, %edx
	cltq
	salq	$4, %rax
	leal	(%rdx,%rdx,8), %ecx
	movl	%ecx, %r9d
	sall	$5, %r9d
	addl	%r9d, %ecx
	leal	(%rdx,%rcx,8), %ecx
	leal	(%rdx,%rcx,2), %ecx
	leal	(%rdx,%rcx,4), %ecx
	leal	(%rdx,%rcx,4), %ecx
	sall	$4, %ecx
	subl	%edx, %ecx
	shrl	$19, %ecx
	movl	%ecx, %edx
	movl	%ebx, %ecx
	subl	%edx, %ecx
	addl	$60, %ecx
	jmp	.L137
.L102:
	movl	$100, %ecx
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %rbx
	leaq	1(%r15), %rcx
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rdx,2), %eax
	movw	%ax, 1(%r15)
.L107:
	addl	$48, %ebx
	movb	%bl, -1(%rcx)
	jmp	.L108
.L220:
	movl	$13, %r11d
	jmp	.L81
.L269:
	movl	$16, %r8d
.L119:
	addq	%r11, %r8
	cmpq	$99, %rbx
	ja	.L142
	jmp	.L123
.L230:
	movl	$16, %r8d
.L92:
	addq	%r15, %r8
	cmpq	$99, %rbx
	ja	.L139
	jmp	.L96
.L109:
	leaq	18(%r15), %r8
.L141:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %r11
.L115:
	movq	%rbx, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%r11
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %rcx
	salq	$2, %rcx
	movq	%rbx, %rdx
	subq	%rcx, %rdx
	movq	%rbx, %rcx
	movq	%rax, %rbx
	subq	$2, %r8
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r8)
	cmpq	$9999, %rcx
	ja	.L115
	jmp	.L114
.L14:
	leaq	3(%r15), %r11
	movq	%r8, %rax
	shrq	$2, %rax
	movabsq	$2951479051793528259, %rdx
	mulq	%rdx
	movq	%rdx, %rax
	shrq	$2, %rax
	movq	%rax, %rdx
	leaq	(%rax,%rax,4), %rax
	leaq	(%rax,%rax,4), %rcx
	salq	$2, %rcx
	movq	%r8, %rax
	subq	%rcx, %rax
	movq	%rdx, %r8
	leaq	1(%r15), %r9
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rdx
	movzwl	(%rdx,%rax,2), %eax
	movw	%ax, 1(%r15)
	jmp	.L38
.L333:
	addl	$48, %r9d
	movb	%r9b, (%r15)
	leaq	1(%r15), %rax
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
.L27:
	movw	$11109, (%rax)
	movzwl	(%r10,%r11,2), %edx
	movw	%dx, 2(%rax)
	leaq	4(%rax), %r11
	jmp	.L1
.L163:
	movl	$12, %r11d
	movl	$12, %eax
	jmp	.L21
.L207:
	movl	$13, %edx
	movl	$9, %ecx
	movl	$9, %r12d
	movl	$8, %esi
	movl	$10, %r8d
	movl	$8, %eax
	jmp	.L69
.L221:
	movl	$12, %r11d
	jmp	.L81
.L231:
	movl	$15, %r8d
	jmp	.L92
.L270:
	movl	$15, %r8d
	jmp	.L119
.L222:
	movl	$11, %r11d
	jmp	.L81
.L271:
	movl	$14, %r8d
	jmp	.L119
.L208:
	movl	$12, %edx
	movl	$8, %ecx
	movl	$8, %r12d
	movl	$7, %esi
	movl	$9, %r8d
	movl	$7, %eax
	jmp	.L69
.L232:
	movl	$14, %r8d
	jmp	.L92
.L164:
	movl	$11, %r11d
	movl	$11, %eax
	jmp	.L21
.L256:
	movl	$16, %r8d
.L110:
	addq	%r9, %r8
	cmpq	$99, %rbx
	ja	.L141
	jmp	.L114
.L174:
	movl	$14, %ecx
	movl	$15, %edx
	movl	$10, %ebx
.L23:
	cmpq	$9, %r9
	jbe	.L25
	cmpl	%eax, %edx
	jnb	.L22
	cmpq	$999999999, %r9
	ja	.L28
	cmpq	$99999999, %r9
	ja	.L182
	cmpq	$9999999, %r9
	ja	.L183
	cmpq	$999999, %r9
	ja	.L184
	cmpq	$99999, %r9
	ja	.L185
	cmpq	$9999, %r9
	ja	.L186
	cmpq	$999, %r9
	ja	.L187
	cmpq	$99, %r9
	ja	.L341
.L31:
	leaq	3(%r15), %rcx
.L318:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
.L135:
	movzwl	(%r10,%r9,2), %eax
	movw	%ax, -2(%rcx)
.L35:
	movzbl	1(%r15), %eax
	movb	%al, (%r15)
	movb	$46, 1(%r15)
	leal	1(%rbx), %eax
	addq	%r15, %rax
	jmp	.L27
.L209:
	movl	$11, %edx
	movl	$7, %ecx
	movl	$7, %r12d
	movl	$6, %esi
	movl	$8, %r8d
	movl	$6, %eax
	jmp	.L69
.L165:
	movl	$10, %r11d
	movl	$10, %eax
	jmp	.L21
.L257:
	movl	$15, %r8d
	jmp	.L110
.L223:
	movl	$10, %r11d
	jmp	.L81
.L272:
	movl	$13, %r8d
	jmp	.L119
.L166:
	movl	$9, %r11d
	movl	$9, %eax
	jmp	.L21
.L258:
	movl	$14, %r8d
	jmp	.L110
.L175:
	movl	$13, %ecx
	movl	$14, %edx
	movl	$9, %ebx
	jmp	.L23
.L233:
	movl	$13, %r8d
	jmp	.L92
.L340:
	leaq	17(%r15), %rcx
.L140:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %r9
.L106:
	movq	%rbx, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%r9
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %r8
	salq	$2, %r8
	movq	%rbx, %rdx
	subq	%r8, %rdx
	movq	%rbx, %r8
	movq	%rax, %rbx
	subq	$2, %rcx
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%rcx)
	cmpq	$9999, %r8
	ja	.L106
	jmp	.L105
.L210:
	movl	$10, %edx
	movl	$6, %ecx
	movl	$6, %r12d
	movl	$5, %esi
	movl	$7, %r8d
	movl	$5, %eax
	jmp	.L69
.L259:
	movl	$13, %r8d
	jmp	.L110
.L235:
	movl	$11, %r8d
	jmp	.L92
.L234:
	movl	$12, %r8d
	jmp	.L92
.L243:
	movl	$16, %ecx
.L101:
	addq	%r15, %rcx
	cmpq	$99, %rbx
	ja	.L140
	jmp	.L105
.L211:
	movl	$9, %edx
	movl	$5, %ecx
	movl	$5, %r12d
	movl	$4, %esi
	movl	$6, %r8d
	movl	$4, %eax
	jmp	.L69
.L329:
	jne	.L342
	movl	$1, %esi
	jmp	.L74
.L176:
	movl	$12, %ecx
	movl	$13, %edx
	movl	$8, %ebx
	jmp	.L23
.L224:
	movl	$9, %r11d
	jmp	.L81
.L273:
	movl	$12, %r8d
	jmp	.L119
.L274:
	movl	$11, %r8d
	jmp	.L119
.L167:
	movl	$8, %r11d
	movl	$8, %eax
	jmp	.L21
.L225:
	movl	$8, %r11d
	jmp	.L81
.L226:
	movl	$7, %r11d
	jmp	.L81
.L227:
	movl	$6, %r11d
	jmp	.L81
.L284:
	xorl	%esi, %esi
	xorl	%ebx, %ebx
	jmp	.L134
.L178:
	movl	$10, %ecx
	movl	$11, %edx
	movl	$6, %ebx
	jmp	.L23
.L260:
	movl	$12, %r8d
	jmp	.L110
.L177:
	movl	$11, %ecx
	movl	$12, %edx
	movl	$7, %ebx
	jmp	.L23
.L245:
	movl	$14, %ecx
	jmp	.L101
.L244:
	movl	$15, %ecx
	jmp	.L101
.L328:
	movl	$7, %edx
	movl	$3, %ecx
	movl	$3, %r12d
	movl	$2, %esi
	movl	$4, %r8d
	movl	$2, %eax
	jmp	.L69
.L212:
	movl	$8, %edx
	movl	$4, %ecx
	movl	$4, %r12d
	movl	$3, %esi
	movl	$5, %r8d
	movl	$3, %eax
	jmp	.L69
.L275:
	movl	$10, %r8d
	jmp	.L119
.L168:
	movl	$7, %r11d
	movl	$7, %eax
	jmp	.L21
.L276:
	movl	$9, %r8d
	jmp	.L119
.L237:
	movl	$9, %r8d
	jmp	.L92
.L236:
	movl	$10, %r8d
	jmp	.L92
.L169:
	movl	$6, %r11d
	movl	$6, %eax
	jmp	.L21
.L261:
	movl	$11, %r8d
	jmp	.L110
.L262:
	movl	$10, %r8d
	jmp	.L110
.L179:
	movl	$9, %ecx
	movl	$10, %edx
	movl	$5, %ebx
	jmp	.L23
.L342:
	movl	$2, %eax
	subl	%ebp, %eax
	movl	%ebp, %r9d
	cmpl	$4, %eax
	ja	.L75
	movw	$11824, (%r15)
	leaq	2(%r15), %r11
	notl	%ebp
	movq	%rbp, %r8
	movl	$48, %edx
	movq	%r11, %rcx
	call	memset
	movq	%rax, %r11
	addq	%rbp, %r11
	movl	$1, %esi
	movl	$1, %eax
	jmp	.L122
.L181:
	movl	$7, %ecx
	movl	$8, %edx
	movl	$3, %ebx
	jmp	.L23
.L180:
	movl	$8, %ecx
	movl	$9, %edx
	movl	$4, %ebx
	jmp	.L23
.L241:
	movl	$5, %r8d
	jmp	.L92
.L240:
	movl	$6, %r8d
	jmp	.L92
.L28:
	leaq	11(%r15), %rcx
.L145:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %rsi
.L33:
	movq	%r9, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%rsi
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %r8
	salq	$2, %r8
	movq	%r9, %rdx
	subq	%r8, %rdx
	movq	%r9, %r8
	movq	%rax, %r9
	subq	$2, %rcx
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%rcx)
	cmpq	$9999, %r8
	ja	.L33
	cmpq	$999, %r8
	ja	.L135
.L34:
	addl	$48, %r9d
	movb	%r9b, -1(%rcx)
	jmp	.L35
.L239:
	movl	$7, %r8d
	jmp	.L92
.L238:
	movl	$8, %r8d
	jmp	.L92
.L264:
	movl	$8, %r8d
	jmp	.L110
.L263:
	movl	$9, %r8d
	jmp	.L110
.L266:
	movl	$6, %r8d
	jmp	.L110
.L265:
	movl	$7, %r8d
	jmp	.L110
.L277:
	movl	$8, %r8d
	jmp	.L119
.L170:
	movl	$5, %r11d
	movl	$5, %eax
	jmp	.L21
.L172:
	movl	$3, %r11d
	movl	$3, %eax
	jmp	.L21
.L171:
	movl	$4, %r11d
	movl	$4, %eax
	jmp	.L21
.L250:
	movl	$9, %ecx
	jmp	.L101
.L280:
	movl	$5, %r8d
	jmp	.L119
.L279:
	movl	$6, %r8d
	jmp	.L119
.L278:
	movl	$7, %r8d
	jmp	.L119
.L249:
	movl	$10, %ecx
	jmp	.L101
.L248:
	movl	$11, %ecx
	jmp	.L101
.L247:
	movl	$12, %ecx
	jmp	.L101
.L246:
	movl	$13, %ecx
	jmp	.L101
.L228:
	movl	$5, %r11d
	jmp	.L81
.L24:
	cmpl	$7, %eax
	jbe	.L22
	movl	$2, %ebx
	jmp	.L31
.L335:
	leaq	4(%r15), %r11
	movl	$100, %ecx
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %rbx
	leaq	2(%r15), %r8
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, 2(%r15)
	jmp	.L86
.L182:
	movl	$9, %eax
.L29:
	leal	1(%rax), %ecx
	addq	%r15, %rcx
	cmpq	$99, %r9
	ja	.L145
	jmp	.L318
.L251:
	movl	$8, %ecx
	jmp	.L101
.L253:
	movl	$6, %ecx
	jmp	.L101
.L252:
	movl	$7, %ecx
	jmp	.L101
.L268:
	movl	$4, %r8d
	jmp	.L110
.L267:
	movl	$5, %r8d
	jmp	.L110
.L281:
	movl	$4, %r8d
	jmp	.L119
.L255:
	movl	$4, %ecx
	jmp	.L101
.L254:
	movl	$5, %ecx
	jmp	.L101
.L339:
	cmpq	$10, %rbx
	sbbl	%eax, %eax
	addl	$2, %eax
	jmp	.L95
.L242:
	movl	$4, %r8d
	jmp	.L92
.L184:
	movl	$7, %eax
	jmp	.L29
.L183:
	movl	$8, %eax
	jmp	.L29
.L341:
	movl	$100, %ecx
	movq	%r9, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %r9
	leaq	2(%r15), %rcx
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, 2(%r15)
	jmp	.L34
.L187:
	movl	$4, %eax
	jmp	.L29
.L186:
	movl	$5, %eax
	jmp	.L29
.L185:
	movl	$6, %eax
	jmp	.L29
	.seh_endproc
	.section	.text.unlikely,"x"
.LCOLDB1:
	.text
.LHOTB1:
	.def	_ZN7fast_io7details14print_fallbackILb1ENS_23basic_win32_io_observerIcEEJRSt17basic_string_viewIcSt11char_traitsIcEERA3_KDuNSt6chrono8durationIxSt5ratioILx1ELx1000000000EEEEEEEvRT0_DpOT1_.isra.0;	.scl	3;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details14print_fallbackILb1ENS_23basic_win32_io_observerIcEEJRSt17basic_string_viewIcSt11char_traitsIcEERA3_KDuNSt6chrono8durationIxSt5ratioILx1ELx1000000000EEEEEEEvRT0_DpOT1_.isra.0
_ZN7fast_io7details14print_fallbackILb1ENS_23basic_win32_io_observerIcEEJRSt17basic_string_viewIcSt11char_traitsIcEERA3_KDuNSt6chrono8durationIxSt5ratioILx1ELx1000000000EEEEEEEvRT0_DpOT1_.isra.0:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	movl	$4232, %eax
	call	___chkstk_ms
	subq	%rax, %rsp
	.seh_stackalloc	4232
	.seh_endprologue
	movq	%rcx, %rdi
	movq	%rdx, %rbx
	movq	%r8, %r12
	movq	%r9, %rbp
	leaq	96(%rsp), %rsi
	movq	%rsi, 4192(%rsp)
	movq	%rsi, 4200(%rsp)
	leaq	4192(%rsp), %rax
	movq	%rax, 4208(%rsp)
	cmpq	$4096, %rdx
	ja	.L366
	movq	%rdx, %r8
	movq	%r12, %rdx
	movq	%rsi, %rcx
	call	memcpy
	addq	4200(%rsp), %rbx
	movq	%rbx, 4200(%rsp)
	movq	4208(%rsp), %r13
.L347:
	movq	%r13, %rax
	subq	%rbx, %rax
	cmpq	$1, %rax
	jbe	.L367
	movzwl	0(%rbp), %eax
	movw	%ax, (%rbx)
	movq	4200(%rsp), %rax
	leaq	2(%rax), %rcx
	movq	%rcx, 4200(%rsp)
	movq	4208(%rsp), %rax
.L351:
	movq	4336(%rsp), %rdx
	pxor	%xmm1, %xmm1
	cvtsi2sdq	(%rdx), %xmm1
	mulsd	.LC0(%rip), %xmm1
	leaq	30(%rcx), %rdx
	cmpq	%rdx, %rax
	jbe	.L352
	call	_ZN7fast_io7details3ryu15output_shortestILb0ELy0ELb1ELb0EDuLDu46EPcdEET5_NS0_27compile_time_floating_valueIXT2_ET3_XT4_EEES4_T6_.isra.0
	movq	%rax, 4200(%rsp)
	movq	4208(%rsp), %rbp
.L353:
	cmpq	%rax, %rbp
	je	.L368
	movb	$115, (%rax)
	leaq	1(%rax), %r8
	movq	%r8, 4200(%rsp)
	movq	4208(%rsp), %rax
.L359:
	cmpq	%rax, %r8
	je	.L369
	movb	$10, (%r8)
	addq	$1, %r8
	movq	%r8, 4200(%rsp)
	movq	4192(%rsp), %r12
.L362:
	movq	(%rdi), %rcx
	movl	$0, 64(%rsp)
	subq	%r12, %r8
	movl	$4294967295, %eax
	cmpq	%rax, %r8
	movq	$0, 32(%rsp)
	leaq	64(%rsp), %r9
	cmova	%rax, %r8
	movq	%r12, %rdx
	call	WriteFile
	testl	%eax, %eax
	je	.L365
	movq	4192(%rsp), %rcx
	cmpq	%rsi, %rcx
	je	.L343
	movq	4208(%rsp), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
	nop
.L343:
	addq	$4232, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L366:
	cmpq	$8192, %rdx
	movl	$8192, %r15d
	cmovnb	%rdx, %r15
	testq	%r15, %r15
	js	.L349
	movq	%r15, %rcx
	call	_Znwy
	movq	%rax, %r13
	movq	4192(%rsp), %r9
	movq	4200(%rsp), %r14
	subq	%r9, %r14
	movq	%r14, %r8
	movq	%r9, %rdx
	movq	%r9, 56(%rsp)
	movq	%rax, %rcx
	call	memcpy
	movq	56(%rsp), %r9
	cmpq	%rsi, %r9
	je	.L346
	movq	4208(%rsp), %rdx
	subq	%r9, %rdx
	movq	%r9, %rcx
	call	_ZdlPvy
.L346:
	movq	%r13, 4192(%rsp)
	addq	%r13, %r14
	addq	%r15, %r13
	movq	%r13, 4208(%rsp)
	movq	%rbx, %r8
	movq	%r12, %rdx
	movq	%r14, %rcx
	call	memcpy
	addq	%r14, %rbx
	movq	%rbx, 4200(%rsp)
	jmp	.L347
.L367:
	subq	4192(%rsp), %r13
	addq	%r13, %r13
	cmpq	$2, %r13
	movl	$2, %eax
	cmovb	%rax, %r13
	testq	%r13, %r13
	js	.L349
	movq	%r13, %rcx
	call	_Znwy
	movq	%rax, %r12
	movq	4192(%rsp), %r14
	movq	4200(%rsp), %rbx
	subq	%r14, %rbx
	movq	%rbx, %r8
	movq	%r14, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%rsi, %r14
	je	.L350
	movq	4208(%rsp), %rdx
	subq	%r14, %rdx
	movq	%r14, %rcx
	call	_ZdlPvy
.L350:
	movq	%r12, 4192(%rsp)
	leaq	(%r12,%rbx), %rcx
	leaq	(%r12,%r13), %rax
	movq	%rax, 4208(%rsp)
	movzwl	0(%rbp), %edx
	movw	%dx, (%rcx)
	addq	$2, %rcx
	movq	%rcx, 4200(%rsp)
	jmp	.L351
.L352:
	leaq	64(%rsp), %r12
	movq	%r12, %rcx
	call	_ZN7fast_io7details3ryu15output_shortestILb0ELy0ELb1ELb0EDuLDu46EPcdEET5_NS0_27compile_time_floating_valueIXT2_ET3_XT4_EEES4_T6_.isra.0
	subq	%r12, %rax
	movq	%rax, %rbx
	movq	4208(%rsp), %rax
	movq	4200(%rsp), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %rbx
	ja	.L370
	movq	%rbx, %r8
	movq	%r12, %rdx
	call	memcpy
	movq	4200(%rsp), %rax
	addq	%rbx, %rax
	movq	%rax, 4200(%rsp)
	movq	4208(%rsp), %rbp
	jmp	.L353
.L368:
	subq	4192(%rsp), %rbp
	addq	%rbp, %rbp
	movq	%rbp, %r12
	js	.L349
	movq	%rbp, %rcx
	call	_Znwy
	movq	%rax, %rbp
	movq	4192(%rsp), %r13
	movq	4200(%rsp), %rbx
	subq	%r13, %rbx
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%rsi, %r13
	je	.L358
	movq	4208(%rsp), %rdx
	subq	%r13, %rdx
	movq	%r13, %rcx
	call	_ZdlPvy
.L358:
	movq	%rbp, 4192(%rsp)
	leaq	0(%rbp,%rbx), %r8
	leaq	0(%rbp,%r12), %rax
	movq	%rax, 4208(%rsp)
	movb	$115, (%r8)
	addq	$1, %r8
	movq	%r8, 4200(%rsp)
	jmp	.L359
.L369:
	subq	4192(%rsp), %r8
	addq	%r8, %r8
	movq	%r8, %rbp
	js	.L349
	movq	%r8, %rcx
	call	_Znwy
	movq	%rax, %r12
	movq	4192(%rsp), %r13
	movq	4200(%rsp), %rbx
	subq	%r13, %rbx
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%rsi, %r13
	je	.L361
	movq	4208(%rsp), %rdx
	subq	%r13, %rdx
	movq	%r13, %rcx
	call	_ZdlPvy
.L361:
	movq	%r12, 4192(%rsp)
	leaq	(%r12,%rbx), %r8
	addq	%r12, %rbp
	movq	%rbp, 4208(%rsp)
	movb	$10, (%r8)
	addq	$1, %r8
	movq	%r8, 4200(%rsp)
	jmp	.L362
.L370:
	subq	4192(%rsp), %rax
	leaq	(%rax,%rax), %r13
	cmpq	%rbx, %r13
	cmovb	%rbx, %r13
	testq	%r13, %r13
	js	.L349
	movq	%r13, %rcx
	call	_Znwy
	movq	%rax, %rbp
	movq	4192(%rsp), %r15
	movq	4200(%rsp), %r14
	subq	%r15, %r14
	movq	%r14, %r8
	movq	%r15, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%rsi, %r15
	je	.L355
	movq	4208(%rsp), %rdx
	subq	%r15, %rdx
	movq	%r15, %rcx
	call	_ZdlPvy
.L355:
	movq	%rbp, 4192(%rsp)
	addq	%rbp, %r14
	addq	%r13, %rbp
	movq	%rbp, 4208(%rsp)
	movq	%rbx, %r8
	movq	%r12, %rdx
	movq	%r14, %rcx
	call	memcpy
	leaq	(%r14,%rbx), %rax
	movq	%rax, 4200(%rsp)
	jmp	.L353
.L349:
	call	_ZSt17__throw_bad_allocv
	.seh_endproc
	.section	.text.unlikely,"x"
	.def	_ZN7fast_io7details14print_fallbackILb1ENS_23basic_win32_io_observerIcEEJRSt17basic_string_viewIcSt11char_traitsIcEERA3_KDuNSt6chrono8durationIxSt5ratioILx1ELx1000000000EEEEEEEvRT0_DpOT1_.isra.0.cold;	.scl	3;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details14print_fallbackILb1ENS_23basic_win32_io_observerIcEEJRSt17basic_string_viewIcSt11char_traitsIcEERA3_KDuNSt6chrono8durationIxSt5ratioILx1ELx1000000000EEEEEEEvRT0_DpOT1_.isra.0.cold
	.seh_stackalloc	4296
	.seh_savereg	%rbx, 4232
	.seh_savereg	%rsi, 4240
	.seh_savereg	%rdi, 4248
	.seh_savereg	%rbp, 4256
	.seh_savereg	%r12, 4264
	.seh_savereg	%r13, 4272
	.seh_savereg	%r14, 4280
	.seh_savereg	%r15, 4288
	.seh_endprologue
_ZN7fast_io7details14print_fallbackILb1ENS_23basic_win32_io_observerIcEEJRSt17basic_string_viewIcSt11char_traitsIcEERA3_KDuNSt6chrono8durationIxSt5ratioILx1ELx1000000000EEEEEEEvRT0_DpOT1_.isra.0.cold:
.L365:
	ud2
	.text
	.section	.text.unlikely,"x"
	.seh_endproc
.LCOLDE1:
	.text
.LHOTE1:
	.section .rdata,"dr"
.LC2:
	.ascii ":\11\0"
	.section	.text$_ZN7fast_io5timerD1Ev,"x"
	.linkonce discard
	.align 2
	.globl	_ZN7fast_io5timerD1Ev
	.def	_ZN7fast_io5timerD1Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io5timerD1Ev
_ZN7fast_io5timerD1Ev:
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$64, %rsp
	.seh_stackalloc	64
	.seh_endprologue
	movq	%rcx, %rbx
	call	_ZNSt6chrono3_V212system_clock3nowEv
	subq	16(%rbx), %rax
	movq	%rax, 48(%rsp)
	movl	$-12, %ecx
	call	GetStdHandle
	movq	%rax, 56(%rsp)
	leaq	56(%rsp), %rcx
	leaq	48(%rsp), %rax
	movq	%rax, 32(%rsp)
	leaq	.LC2(%rip), %r9
	movq	8(%rbx), %r8
	movq	(%rbx), %rdx
	call	_ZN7fast_io7details14print_fallbackILb1ENS_23basic_win32_io_observerIcEEJRSt17basic_string_viewIcSt11char_traitsIcEERA3_KDuNSt6chrono8durationIxSt5ratioILx1ELx1000000000EEEEEEEvRT0_DpOT1_.isra.0
	nop
	addq	$64, %rsp
	popq	%rbx
	ret
	.seh_endproc
	.def	__main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
.LC3:
	.ascii "obuf_file_lcv_C.txt\0"
.LC4:
	.ascii "C\0"
.LC5:
	.ascii "vector::_M_range_insert\0"
.LC6:
	.ascii "output\0"
.LC8:
	.ascii "input\0"
	.section	.text.unlikely,"x"
.LCOLDB11:
	.section	.text.startup,"x"
.LHOTB11:
	.globl	main
	.def	main;	.scl	2;	.type	32;	.endef
	.seh_proc	main
main:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$2392, %rsp
	.seh_stackalloc	2392
	movaps	%xmm6, 2352(%rsp)
	.seh_savexmm	%xmm6, 2352
	movaps	%xmm7, 2368(%rsp)
	.seh_savexmm	%xmm7, 2368
	.seh_endprologue
	call	__main
	leaq	.LC4(%rip), %rdx
	xorl	%ecx, %ecx
	call	*__imp__create_locale(%rip)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L536
	movq	(%rax), %rax
	movq	296(%rax), %rsi
	movq	8(%rsi), %rax
	movzbl	(%rax), %eax
	movb	%al, 112(%rsp)
	movq	16(%rsi), %r12
	movq	%r12, %rcx
	call	strlen
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L374
	movq	%rax, %rcx
	call	_Znwy
	movq	%rax, %r14
	movq	%rbx, %r8
	movq	%r12, %rdx
	movq	%rax, %rcx
	call	memmove
	leaq	(%r14,%rbx), %r12
	movq	%rbx, 80(%rsp)
	movq	56(%rsi), %rdi
	movq	%rdi, %rcx
	call	strlen
	movq	%rax, %rbp
	testq	%rax, %rax
	je	.L556
	movabsq	$9223372036854775807, %rdx
	movq	%rdx, %rax
	subq	%rbx, %rax
	cmpq	%rax, %rbp
	ja	.L386
	cmpq	%rbx, %rbp
	movq	%rbx, %rax
	cmovnb	%rbp, %rax
	addq	%rbx, %rax
	movq	%rax, 72(%rsp)
	jnc	.L555
	movq	%rdx, 72(%rsp)
.L379:
	movq	72(%rsp), %rcx
	call	_Znwy
	movq	%rax, %r13
.L380:
	cmpq	$0, 80(%rsp)
	jne	.L751
	movq	%rbp, %r8
	movq	%rdi, %rdx
	movq	%r13, %rcx
	call	memmove
	leaq	0(%r13,%rbp), %r12
	testq	%r14, %r14
	jne	.L752
.L383:
	movq	72(%rsp), %rax
	addq	%r13, %rax
	movq	%rax, 80(%rsp)
	movq	%rbx, 72(%rsp)
	movq	%r13, %r14
	jmp	.L375
.L747:
	call	GetLastError
.L536:
	ud2
.L556:
	movq	%rbx, 72(%rsp)
	movq	%r12, 80(%rsp)
.L375:
	movq	64(%rsi), %r13
	movq	%r13, %rcx
	call	strlen
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L384
	movq	80(%rsp), %rax
	subq	%r12, %rax
	cmpq	%rax, %rdi
	ja	.L385
	movq	%rdi, %r8
	movq	%r13, %rdx
	movq	%r12, %rcx
	call	memmove
	addq	%rdi, %r12
.L384:
	movq	72(%rsi), %r13
	movq	%r13, %rcx
	call	strlen
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L394
	movq	80(%rsp), %rax
	subq	%r12, %rax
	cmpq	%rax, %rdi
	ja	.L395
	movq	%rdi, %r8
	movq	%r13, %rdx
	movq	%r12, %rcx
	call	memmove
	addq	%rdi, %r12
.L394:
	movq	32(%rsi), %r13
	movq	%r13, %rcx
	call	strlen
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L403
	movq	80(%rsp), %rax
	subq	%r12, %rax
	cmpq	%rax, %rdi
	ja	.L404
	movq	%rdi, %r8
	movq	%r13, %rdx
	movq	%r12, %rcx
	call	memmove
	addq	%rdi, %r12
.L403:
	movq	24(%rsi), %r13
	movq	%r13, %rcx
	call	strlen
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L412
	movq	80(%rsp), %rax
	subq	%r12, %rax
	cmpq	%rax, %rdi
	ja	.L413
	movq	%rdi, %r8
	movq	%r13, %rdx
	movq	%r12, %rcx
	call	memmove
.L412:
	movq	$6, 160(%rsp)
	leaq	.LC6(%rip), %rax
	movq	%rax, 168(%rsp)
	call	_ZNSt6chrono3_V212system_clock3nowEv
	movq	%rax, 176(%rsp)
	movdqa	.LC7(%rip), %xmm6
	pxor	%xmm0, %xmm0
	movdqa	%xmm6, %xmm7
	punpcklbw	%xmm0, %xmm7
	movdqa	%xmm7, 304(%rsp)
	punpckhbw	%xmm0, %xmm6
	movdqa	%xmm6, 320(%rsp)
	movl	$116, %eax
	leaq	16+.LC3(%rip), %rdx
	leaq	336(%rsp), %r10
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE15first_unit_infoE(%rip), %r11
	leaq	3(%rdx), %rbx
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE11transitionsE(%rip), %r9
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE14octet_categoryE(%rip), %r8
.L421:
	addq	$1, %rdx
	leaq	2(%r10), %rcx
	testb	%al, %al
	jns	.L424
	movzbl	%al, %eax
	leaq	(%r11,%rax,2), %rcx
	movzbl	(%rcx), %eax
	movzbl	1(%rcx), %esi
	movzbl	%al, %ecx
	cmpl	$12, %esi
	jle	.L425
	cmpq	%rbx, %rdx
	jnb	.L536
.L426:
	movzbl	(%rdx), %eax
	addq	$1, %rdx
	sall	$6, %ecx
	movl	%eax, %edi
	andl	$63, %edi
	orl	%edi, %ecx
	movzbl	(%r8,%rax), %eax
	addl	%esi, %eax
	cltq
	movzbl	(%r9,%rax), %esi
	cmpl	$12, %esi
	jle	.L753
	cmpq	%rbx, %rdx
	jne	.L426
	jmp	.L536
.L753:
	cmpb	$12, %sil
	.p2align 4,,4
	je	.L536
	movl	%ecx, %eax
	cmpl	$65535, %ecx
	ja	.L429
.L554:
	movl	$2, %ecx
.L430:
	addq	%r10, %rcx
.L424:
	movw	%ax, (%r10)
	cmpq	%rbx, %rdx
	jnb	.L422
	movsbw	(%rdx), %ax
	movq	%rcx, %r10
	jmp	.L421
.L429:
	movl	%ecx, %esi
	shrl	$10, %esi
	andw	$1023, %cx
	leal	-9216(%rcx), %eax
	movw	%ax, 2(%r10)
	leal	-10304(%rsi), %eax
	movl	$4, %ecx
	jmp	.L430
.L422:
	movw	$0, (%rcx)
	leaq	304(%rsp), %rcx
	movq	$0, 48(%rsp)
	movl	$268435584, 40(%rsp)
	movl	$2, 32(%rsp)
	xorl	%r9d, %r9d
	movl	$3, %r8d
	movl	$1073741824, %edx
	call	CreateFileW
	movq	%rax, 96(%rsp)
	cmpq	$-1, %rax
	je	.L536
	movq	$0, 88(%rsp)
	xorl	%r13d, %r13d
	xorl	%edi, %edi
	xorl	%ebp, %ebp
	movq	72(%rsp), %rax
	leaq	-1(%r14,%rax), %r12
	movq	%r14, 104(%rsp)
	movzbl	112(%rsp), %r14d
	movq	%r15, 120(%rsp)
	movq	%rdi, %r15
.L501:
	leaq	45(%r13), %rax
	cmpq	%rax, 88(%rsp)
	jbe	.L432
	cmpq	$0, 72(%rsp)
	jne	.L433
	cmpq	$999999, %rbp
	ja	.L567
	cmpq	$99999, %rbp
	ja	.L568
	cmpq	$9999, %rbp
	ja	.L569
	cmpq	$999, %rbp
	ja	.L570
	cmpq	$99, %rbp
	ja	.L435
	cmpq	$10, %rbp
	sbbl	%eax, %eax
	leal	2(%rax), %r8d
	addq	%r13, %r8
	movq	%r8, %r9
	movq	%rbp, %rcx
.L438:
	cmpq	$9, %rcx
	jbe	.L440
.L779:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r11
	movzwl	(%r11,%rcx,2), %eax
	movw	%ax, -2(%r8)
.L441:
	movb	$10, (%r9)
	leaq	1(%r9), %r13
	jmp	.L464
.L432:
	cmpq	$0, 72(%rsp)
	jne	.L465
	cmpq	$999999, %rbp
	ja	.L577
	cmpq	$99999, %rbp
	ja	.L578
	cmpq	$9999, %rbp
	ja	.L579
	cmpq	$999, %rbp
	ja	.L580
	cmpq	$99, %rbp
	ja	.L467
	cmpq	$10, %rbp
	sbbl	%eax, %eax
	leaq	256(%rsp), %rsi
	leal	2(%rax), %r9d
	addq	%rsi, %r9
	movq	%r9, %rcx
	movq	%rbp, %r8
.L470:
	cmpq	$9, %r8
	jbe	.L472
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r11
	movzwl	(%r11,%r8,2), %eax
	movw	%ax, -2(%r9)
.L473:
	movb	$10, (%rcx)
	leaq	1(%rcx), %rdi
	movq	%rdi, %r8
	subq	%rsi, %r8
	leaq	0(%r13,%r8), %r9
	cmpq	88(%rsp), %r9
	movq	%r9, 112(%rsp)
	jnb	.L496
	movq	%rsi, %rdx
	movq	%r13, %rcx
	call	memcpy
	movq	112(%rsp), %r9
	movq	%r9, %r13
.L464:
	addq	$1, %rbp
	cmpq	$10000000, %rbp
	jne	.L501
	movq	104(%rsp), %r14
	movq	%r15, %rdx
	movq	%r15, %rdi
	movq	120(%rsp), %r15
	cmpq	%r13, %rdx
	je	.L502
	movl	$0, 144(%rsp)
	movq	%r13, %rax
	subq	%rdx, %rax
	movl	$4294967295, %r8d
	cmpq	%r8, %rax
	movq	$0, 32(%rsp)
	leaq	144(%rsp), %r9
	cmovbe	%rax, %r8
	movq	96(%rsp), %rcx
	call	WriteFile
	testl	%eax, %eax
	je	.L536
.L502:
	testq	%rdi, %rdi
	je	.L503
	movl	$4096, %r8d
	movl	$1048576, %edx
	movq	%rdi, %rcx
	call	_ZdlPvySt11align_val_t
.L503:
	movq	96(%rsp), %rcx
	call	CloseHandle
	leaq	160(%rsp), %rcx
	call	_ZN7fast_io5timerD1Ev
	testq	%r14, %r14
	je	.L504
	movq	80(%rsp), %rdx
	subq	%r14, %rdx
	movq	%r14, %rcx
	call	_ZdlPvy
.L504:
	movl	$80000000, %ecx
	call	_Znwy
	movq	%rax, 96(%rsp)
	movq	$0, (%rax)
	leaq	8(%rax), %rcx
	leaq	80000000(%rax), %rbp
	cmpq	%rbp, %rcx
	je	.L508
	movl	$79999992, %r8d
	xorl	%edx, %edx
	call	memset
.L508:
	movl	$32, %ecx
	call	_Znwy
	movq	%rax, %r12
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	(%r15), %rax
	cmpl	$1, 268(%rax)
	jg	.L754
	movq	320(%rax), %rdx
	movzwl	(%rdx), %edx
	andl	$8, %edx
.L509:
	testl	%edx, %edx
	je	.L510
	orl	$1, (%r12)
.L510:
	movl	$1, %esi
	movq	__imp__isctype_l(%rip), %r13
	movl	$1, %edi
	movl	268(%rax), %r14d
	cmpl	$1, %r14d
	jg	.L755
.L511:
	movzbl	%sil, %edx
	movq	320(%rax), %rcx
	movzwl	(%rcx,%rdx,2), %eax
	andl	$8, %eax
	testl	%eax, %eax
	jne	.L756
.L513:
	addb	$1, %sil
	je	.L514
.L757:
	movq	(%r15), %rax
	cmpl	$1, %r14d
	jle	.L738
	movl	268(%rax), %r14d
	cmpl	$1, %r14d
	jle	.L511
.L755:
	movzbl	%sil, %ecx
	movq	%r15, %r8
	movl	$8, %edx
	call	*%r13
	testl	%eax, %eax
	je	.L513
.L756:
	movzbl	%sil, %eax
	shrq	$5, %rax
	movl	%edi, %edx
	movl	%esi, %ecx
	sall	%cl, %edx
	orl	%edx, (%r12,%rax,4)
	addb	$1, %sil
	jne	.L757
.L514:
	movq	$5, 192(%rsp)
	leaq	.LC8(%rip), %rax
	movq	%rax, 200(%rsp)
	call	_ZNSt6chrono3_V212system_clock3nowEv
	movq	%rax, 208(%rsp)
	movq	$0, 224(%rsp)
	movq	$0, 232(%rsp)
	movq	$0, 240(%rsp)
	movdqa	%xmm7, 1328(%rsp)
	movdqa	%xmm6, 1344(%rsp)
	movl	$116, %eax
	leaq	16+.LC3(%rip), %rdx
	leaq	1360(%rsp), %r10
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE15first_unit_infoE(%rip), %r11
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE11transitionsE(%rip), %r9
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE14octet_categoryE(%rip), %r8
.L518:
	addq	$1, %rdx
	leaq	2(%r10), %rcx
	testb	%al, %al
	jns	.L521
	movzbl	%al, %eax
	leaq	(%r11,%rax,2), %rcx
	movzbl	(%rcx), %eax
	movzbl	1(%rcx), %esi
	movzbl	%al, %ecx
	cmpl	$12, %esi
	jle	.L522
	cmpq	%rbx, %rdx
	jnb	.L536
.L523:
	movzbl	(%rdx), %eax
	addq	$1, %rdx
	sall	$6, %ecx
	movl	%eax, %edi
	andl	$63, %edi
	orl	%edi, %ecx
	movzbl	(%r8,%rax), %eax
	addl	%esi, %eax
	cltq
	movzbl	(%r9,%rax), %esi
	cmpl	$12, %esi
	jle	.L758
	cmpq	%rbx, %rdx
	jne	.L523
	jmp	.L536
.L425:
	cmpb	$12, %sil
	.p2align 4,,4
	jne	.L554
	ud2
.L374:
	movq	56(%rsi), %rdi
	movq	%rdi, %rcx
	call	strlen
	movq	%rax, 72(%rsp)
	testq	%rax, %rax
	je	.L759
	movq	72(%rsp), %rbp
	xorl	%r14d, %r14d
	movq	$0, 80(%rsp)
.L555:
	xorl	%r13d, %r13d
	cmpq	$0, 72(%rsp)
	je	.L380
	movabsq	$9223372036854775807, %rax
	movq	72(%rsp), %rcx
	cmpq	%rax, %rcx
	cmovbe	%rcx, %rax
	movq	%rax, 72(%rsp)
	jmp	.L379
.L413:
	subq	%r14, %r12
	movabsq	$9223372036854775807, %rdx
	movq	%rdx, %rax
	subq	%r12, %rax
	cmpq	%rax, %rdi
	ja	.L386
	cmpq	%r12, %rdi
	movq	%r12, %rax
	cmovnb	%rdi, %rax
	addq	%r12, %rax
	movq	%rax, %rbx
	jc	.L565
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L760
.L417:
	testq	%r12, %r12
	jne	.L761
	movq	%rdi, %r8
	movq	%r13, %rdx
	movq	%rsi, %rcx
	call	memmove
	testq	%r14, %r14
	jne	.L419
.L420:
	leaq	(%rsi,%rbx), %rax
	movq	%rax, 80(%rsp)
	movq	%rsi, %r14
	jmp	.L412
.L385:
	subq	%r14, %r12
	movabsq	$9223372036854775807, %rdx
	movq	%rdx, %rax
	subq	%r12, %rax
	cmpq	%rax, %rdi
	ja	.L386
	cmpq	%r12, %rdi
	movq	%r12, %rax
	cmovnb	%rdi, %rax
	addq	%r12, %rax
	movq	%rax, %rbx
	jc	.L559
	xorl	%ebp, %ebp
	testq	%rax, %rax
	jne	.L762
.L390:
	testq	%r12, %r12
	jne	.L763
	movq	%rdi, %r8
	movq	%r13, %rdx
	movq	%rbp, %rcx
	call	memmove
	leaq	0(%rbp,%rdi), %r12
	testq	%r14, %r14
	jne	.L392
.L393:
	leaq	0(%rbp,%rbx), %rax
	movq	%rax, 80(%rsp)
	movq	%rbp, %r14
	jmp	.L384
.L395:
	subq	%r14, %r12
	movabsq	$9223372036854775807, %rax
	movq	%rax, %rdx
	subq	%r12, %rdx
	cmpq	%rdx, %rdi
	ja	.L386
	cmpq	%r12, %rdi
	movq	%r12, %rdx
	cmovnb	%rdi, %rdx
	addq	%r12, %rdx
	movq	%rdx, %rbx
	jc	.L561
	xorl	%ebp, %ebp
	testq	%rdx, %rdx
	jne	.L764
.L399:
	testq	%r12, %r12
	jne	.L765
	movq	%rdi, %r8
	movq	%r13, %rdx
	movq	%rbp, %rcx
	call	memmove
	leaq	0(%rbp,%rdi), %r12
	testq	%r14, %r14
	jne	.L401
.L402:
	leaq	0(%rbp,%rbx), %rax
	movq	%rax, 80(%rsp)
	movq	%rbp, %r14
	jmp	.L394
.L404:
	subq	%r14, %r12
	movabsq	$9223372036854775807, %rax
	movq	%rax, %rdx
	subq	%r12, %rdx
	cmpq	%rdx, %rdi
	ja	.L386
	cmpq	%r12, %rdi
	movq	%r12, %rdx
	cmovnb	%rdi, %rdx
	addq	%r12, %rdx
	movq	%rdx, %rbx
	jc	.L563
	xorl	%ebp, %ebp
	testq	%rdx, %rdx
	jne	.L766
.L408:
	testq	%r12, %r12
	jne	.L767
	movq	%rdi, %r8
	movq	%r13, %rdx
	movq	%rbp, %rcx
	call	memmove
	leaq	0(%rbp,%rdi), %r12
	testq	%r14, %r14
	jne	.L410
.L411:
	leaq	0(%rbp,%rbx), %rax
	movq	%rax, 80(%rsp)
	movq	%rbp, %r14
	jmp	.L403
.L563:
	movq	%rax, %rbx
.L407:
	movq	%rbx, %rcx
	call	_Znwy
	movq	%rax, %rbp
	jmp	.L408
.L751:
	movq	%rbx, %r8
	movq	%r14, %rdx
	movq	%r13, %rcx
	call	memcpy
	leaq	0(%r13,%rbx), %r12
	movq	%rbp, %r8
	movq	%rdi, %rdx
	movq	%r12, %rcx
	call	memmove
	addq	%rbp, %r12
	movq	%rbx, %rdx
.L382:
	movq	%r14, %rcx
	call	_ZdlPvy
	jmp	.L383
.L561:
	movq	%rax, %rbx
.L398:
	movq	%rbx, %rcx
	call	_Znwy
	movq	%rax, %rbp
	jmp	.L399
.L559:
	movq	%rdx, %rbx
.L389:
	movq	%rbx, %rcx
	call	_Znwy
	movq	%rax, %rbp
	jmp	.L390
.L565:
	movq	%rdx, %rbx
.L416:
	movq	%rbx, %rcx
	call	_Znwy
	movq	%rax, %rsi
	jmp	.L417
.L433:
	cmpq	$999999, %rbp
	ja	.L572
	cmpq	$99999, %rbp
	ja	.L573
	cmpq	$9999, %rbp
	ja	.L574
	cmpq	$999, %rbp
	ja	.L575
	cmpq	$99, %rbp
	ja	.L443
	cmpq	$10, %rbp
	sbbl	%eax, %eax
	leaq	1328(%rsp), %r9
	leal	2(%rax), %r10d
	addq	%r9, %r10
	movq	%r10, %r8
	movq	%rbp, %rcx
.L446:
	cmpq	$9, %rcx
	jbe	.L448
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r11
	movzwl	(%r11,%rcx,2), %eax
	movw	%ax, -2(%r10)
.L449:
	leaq	44(%r13), %r11
	movq	104(%rsp), %rax
	movq	%r11, %rsi
	jmp	.L450
.L455:
	testb	$4, %dl
	je	.L459
	movsl
.L459:
	testb	$2, %dl
	je	.L460
	movsw
.L460:
	andl	$1, %edx
	je	.L461
	movsb
.L461:
	leaq	-1(%r8), %rsi
	movb	%r14b, -1(%r8)
	movq	%r10, %r8
.L451:
	cmpq	%rax, %r12
	je	.L450
	addq	$1, %rax
.L450:
	movzbl	(%rax), %edx
	testb	%dl, %dl
	je	.L451
	movzbl	%dl, %ecx
	movq	%rcx, %rdx
	negq	%rdx
	movq	%r8, %r10
	subq	%rcx, %r10
	cmpq	%r9, %r10
	jb	.L768
	leaq	(%rsi,%rdx), %r8
	movl	%ecx, %edx
	movq	%r8, %rdi
	movq	%r10, %rsi
	cmpl	$8, %ecx
	jb	.L455
	testb	$1, %r8b
	jne	.L769
.L456:
	testb	$2, %dil
	jne	.L770
	testb	$4, %dil
	jne	.L771
.L458:
	movl	%edx, %ecx
	shrl	$3, %ecx
	rep movsq
	jmp	.L455
.L761:
	movq	%r12, %r8
	movq	%r14, %rdx
	movq	%rsi, %rcx
	call	memcpy
	leaq	(%rsi,%r12), %rcx
	movq	%rdi, %r8
	movq	%r13, %rdx
	call	memmove
.L419:
	movq	80(%rsp), %rdx
	subq	%r14, %rdx
	movq	%r14, %rcx
	call	_ZdlPvy
	jmp	.L420
.L763:
	movq	%r12, %r8
	movq	%r14, %rdx
	movq	%rbp, %rcx
	call	memcpy
	addq	%rbp, %r12
	movq	%rdi, %r8
	movq	%r13, %rdx
	movq	%r12, %rcx
	call	memmove
	addq	%rdi, %r12
.L392:
	movq	80(%rsp), %rdx
	subq	%r14, %rdx
	movq	%r14, %rcx
	call	_ZdlPvy
	jmp	.L393
.L765:
	movq	%r12, %r8
	movq	%r14, %rdx
	movq	%rbp, %rcx
	call	memcpy
	addq	%rbp, %r12
	movq	%rdi, %r8
	movq	%r13, %rdx
	movq	%r12, %rcx
	call	memmove
	addq	%rdi, %r12
.L401:
	movq	80(%rsp), %rdx
	subq	%r14, %rdx
	movq	%r14, %rcx
	call	_ZdlPvy
	jmp	.L402
.L767:
	movq	%r12, %r8
	movq	%r14, %rdx
	movq	%rbp, %rcx
	call	memcpy
	addq	%rbp, %r12
	movq	%rdi, %r8
	movq	%r13, %rdx
	movq	%r12, %rcx
	call	memmove
	addq	%rdi, %r12
.L410:
	movq	80(%rsp), %rdx
	subq	%r14, %rdx
	movq	%r14, %rcx
	call	_ZdlPvy
	jmp	.L411
.L759:
	movq	$0, 80(%rsp)
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	jmp	.L375
.L577:
	movl	$7, %eax
.L466:
	leaq	256(%rsp), %rsi
	leaq	(%rsi,%rax), %rcx
	movq	%rcx, %r9
	movq	%rbp, %r8
	cmpq	$99, %rbp
	jbe	.L470
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r11
	movabsq	$2951479051793528259, %rdi
.L471:
	movq	%r8, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%rdi
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %r10
	salq	$2, %r10
	movq	%r8, %rdx
	subq	%r10, %rdx
	movq	%r8, %r10
	movq	%rax, %r8
	subq	$2, %r9
	movzwl	(%r11,%rdx,2), %eax
	movw	%ax, (%r9)
	cmpq	$9999, %r10
	ja	.L471
	jmp	.L470
.L758:
	cmpb	$12, %sil
	je	.L536
	movl	%ecx, %eax
	cmpl	$65535, %ecx
	ja	.L525
.L553:
	movl	$2, %ecx
.L526:
	addq	%r10, %rcx
.L521:
	movw	%ax, (%r10)
	cmpq	%rbx, %rdx
	jnb	.L519
	movsbw	(%rdx), %ax
	movq	%rcx, %r10
	jmp	.L518
.L525:
	movl	%ecx, %esi
	shrl	$10, %esi
	andw	$1023, %cx
	leal	-9216(%rcx), %eax
	movw	%ax, 2(%r10)
	leal	-10304(%rsi), %eax
	movl	$4, %ecx
	jmp	.L526
.L519:
	movw	$0, (%rcx)
	leaq	1328(%rsp), %rcx
	movq	$0, 48(%rsp)
	movl	$268435584, 40(%rsp)
	movl	$3, 32(%rsp)
	xorl	%r9d, %r9d
	movl	$3, %r8d
	movl	$-2147483648, %edx
	call	CreateFileW
	movq	%rax, 88(%rsp)
	cmpq	$-1, %rax
	je	.L536
	movq	%rax, 248(%rsp)
	movq	96(%rsp), %r14
	xorl	%r8d, %r8d
	xorl	%ebx, %ebx
	leaq	152(%rsp), %rax
	movq	%rax, 80(%rsp)
	movq	%r15, 104(%rsp)
	movq	%r14, %r15
	cmpq	%r8, %rbx
	je	.L527
.L529:
	movl	$1, %r9d
	jmp	.L680
.L772:
	addq	$1, %rbx
	cmpq	%rbx, %r8
	je	.L527
.L680:
	movzbl	(%rbx), %edx
	movq	%rdx, %rcx
	shrq	$5, %rdx
	movl	%r9d, %eax
	sall	%cl, %eax
	andl	(%r12,%rdx,4), %eax
	jne	.L772
	movq	%rbx, 232(%rsp)
	cmpq	%r8, %rbx
	je	.L552
	leaq	224(%rsp), %rbx
.L541:
	testq	%rbx, %rbx
	je	.L536
.L538:
	movq	8(%rbx), %r13
	movzbl	0(%r13), %eax
	cmpb	$48, %al
	jne	.L589
	addq	$1, %r13
	movq	%r13, 8(%rbx)
	cmpq	16(%rbx), %r13
	jne	.L538
	movq	(%rbx), %rsi
	movq	24(%rbx), %rcx
	movl	$0, 152(%rsp)
	movq	$0, 32(%rsp)
	movq	80(%rsp), %r9
	movl	$1048576, %r8d
	movq	%rsi, %rdx
	call	ReadFile
	testl	%eax, %eax
	je	.L747
	movl	152(%rsp), %eax
	addq	%rsi, %rax
	movq	%rax, 16(%rbx)
	movq	%rsi, 8(%rbx)
	cmpq	%rax, %rsi
	jne	.L541
	ud2
.L589:
	xorl	%ecx, %ecx
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	leaq	156(%rsp), %r9
	movq	%r9, %rdi
	jmp	.L537
.L774:
	leaq	(%rsi,%rsi,4), %rdx
	movzbl	%al, %eax
	leaq	(%rax,%rdx,2), %r10
	addq	$1, %r14
	addq	$1, %r13
	movq	%r13, 8(%rbx)
	cmpq	16(%rbx), %r13
	je	.L773
.L543:
	movzbl	0(%r13), %eax
	movq	%rsi, %rcx
	movq	%r10, %rsi
.L537:
	subl	$48, %eax
	cmpb	$9, %al
	jbe	.L774
.L542:
	testq	%r14, %r14
	je	.L536
	cmpq	$19, %r14
	ja	.L775
	movq	%rsi, (%r15)
	addq	$8, %r15
	cmpq	%rbp, %r15
	je	.L547
.L777:
	movq	232(%rsp), %rbx
	movq	240(%rsp), %r8
	cmpq	%r8, %rbx
	jne	.L529
.L527:
	movq	%rbx, 232(%rsp)
.L552:
	testq	%r8, %r8
	je	.L531
	movq	224(%rsp), %rbx
.L532:
	movl	$0, 148(%rsp)
	movq	$0, 32(%rsp)
	leaq	148(%rsp), %r9
	movl	$1048576, %r8d
	movq	%rbx, %rdx
	movq	88(%rsp), %rcx
	call	ReadFile
	testl	%eax, %eax
	je	.L747
	movl	148(%rsp), %r8d
	addq	%rbx, %r8
	movq	%r8, 240(%rsp)
	movq	%rbx, 232(%rsp)
	cmpq	%r8, %rbx
	jne	.L529
	ud2
.L773:
	movq	%r10, 72(%rsp)
	movq	(%rbx), %r13
	movq	24(%rbx), %rcx
	movl	$0, 156(%rsp)
	movq	$0, 32(%rsp)
	movq	%rdi, %r9
	movl	$1048576, %r8d
	movq	%r13, %rdx
	call	ReadFile
	testl	%eax, %eax
	movq	72(%rsp), %r10
	je	.L776
	movl	156(%rsp), %eax
	addq	%r13, %rax
	movq	%rax, 16(%rbx)
	movq	%r13, 8(%rbx)
	cmpq	%rax, %r13
	jne	.L543
	movq	%r10, %rdi
.L551:
	movq	%rsi, %rcx
	movq	%rdi, %rsi
	jmp	.L542
.L776:
	movq	%r10, %rdi
	call	GetLastError
	cmpl	$109, %eax
	jne	.L536
	movq	%r13, 16(%rbx)
	movq	%r13, 8(%rbx)
	jmp	.L551
.L775:
	cmpq	$20, %r14
	seta	%dl
	cmpq	$9, %rsi
	setbe	%al
	orb	%al, %dl
	jne	.L536
	movabsq	$1844674407370955161, %rax
	cmpq	%rax, %rcx
	ja	.L536
	movq	%rsi, (%r15)
	addq	$8, %r15
	cmpq	%rbp, %r15
	jne	.L777
.L547:
	movq	104(%rsp), %r15
	movq	88(%rsp), %rcx
	call	CloseHandle
	movq	224(%rsp), %rcx
	testq	%rcx, %rcx
	je	.L549
	movl	$4096, %r8d
	movl	$1048576, %edx
	call	_ZdlPvySt11align_val_t
.L549:
	leaq	192(%rsp), %rcx
	call	_ZN7fast_io5timerD1Ev
	movl	$32, %edx
	movq	%r12, %rcx
	call	_ZdlPvy
	movl	$80000000, %edx
	movq	96(%rsp), %rcx
	call	_ZdlPvy
	movq	%r15, %rcx
	call	*__imp__free_locale(%rip)
	xorl	%eax, %eax
	movaps	2352(%rsp), %xmm6
	movaps	2368(%rsp), %xmm7
	addq	$2392, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L531:
	movl	$4096, %edx
	movl	$1048576, %ecx
	call	_ZnwySt11align_val_t
	movq	%rax, %rbx
	movq	%rax, 224(%rsp)
	movq	%rax, 232(%rsp)
	movq	%rax, 240(%rsp)
	jmp	.L532
.L522:
	cmpb	$12, %sil
	jne	.L553
	ud2
.L754:
	movq	%r15, %r8
	movl	$8, %edx
	xorl	%ecx, %ecx
	call	*__imp__isctype_l(%rip)
	movl	%eax, %edx
	movq	(%r15), %rax
	jmp	.L509
.L467:
	movl	$100, %ecx
	movq	%rbp, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %r8
	leaq	256(%rsp), %rsi
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r11
	movzwl	(%r11,%rdx,2), %eax
	movw	%ax, 257(%rsp)
	leaq	259(%rsp), %rcx
	leaq	257(%rsp), %r9
.L472:
	addl	$48, %r8d
	movb	%r8b, -1(%r9)
	jmp	.L473
.L496:
	cmpq	$0, 88(%rsp)
	je	.L778
	movq	88(%rsp), %r8
	subq	%r13, %r8
	movq	%r8, 112(%rsp)
	movq	%rsi, %rdx
	movq	%r13, %rcx
	call	memcpy
	movq	112(%rsp), %r8
	leaq	(%rsi,%r8), %r13
	movl	$0, 136(%rsp)
	movq	88(%rsp), %r8
	subq	%r15, %r8
	movl	$4294967295, %eax
	cmpq	%rax, %r8
	movq	$0, 32(%rsp)
	leaq	136(%rsp), %r9
	cmova	%rax, %r8
	movq	%r15, %rdx
	movq	96(%rsp), %rcx
	call	WriteFile
	testl	%eax, %eax
	je	.L536
	movq	%rdi, %rsi
	subq	%r13, %rsi
	leaq	1048576(%r13), %rax
	cmpq	%rax, %rdi
	jbe	.L500
	movl	$0, 140(%rsp)
	movl	$4294967295, %eax
	cmpq	%rax, %rsi
	movq	$0, 32(%rsp)
	leaq	140(%rsp), %r9
	movq	%rax, %r8
	cmovbe	%rsi, %r8
	movq	%r13, %rdx
	movq	96(%rsp), %rcx
	call	WriteFile
	testl	%eax, %eax
	je	.L536
.L746:
	movq	%r15, %r13
	jmp	.L464
.L500:
	movq	%rsi, %r8
	movq	%r13, %rdx
	movq	%r15, %rcx
	call	memcpy
	leaq	(%r15,%rsi), %r13
	jmp	.L464
.L778:
	cmpq	%rsi, %rdi
	je	.L464
	cmpq	$1048575, %r8
	jbe	.L499
	movl	$0, 132(%rsp)
	movl	$4294967295, %eax
	cmpq	%rax, %r8
	movq	$0, 32(%rsp)
	leaq	132(%rsp), %r9
	cmova	%rax, %r8
	movq	%rsi, %rdx
	movq	96(%rsp), %rcx
	call	WriteFile
	testl	%eax, %eax
	jne	.L746
	ud2
.L567:
	movl	$7, %r9d
.L434:
	addq	%r13, %r9
	movq	%r9, %r8
	movq	%rbp, %rcx
	cmpq	$99, %rbp
	jbe	.L438
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r11
	movabsq	$2951479051793528259, %r10
.L439:
	movq	%rcx, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%r10
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %rsi
	salq	$2, %rsi
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	movq	%rcx, %rsi
	movq	%rax, %rcx
	subq	$2, %r8
	movzwl	(%r11,%rdx,2), %eax
	movw	%ax, (%r8)
	cmpq	$9999, %rsi
	ja	.L439
	cmpq	$9, %rcx
	ja	.L779
.L440:
	addl	$48, %ecx
	movb	%cl, -1(%r8)
	jmp	.L441
.L465:
	cmpq	$999999, %rbp
	ja	.L582
	cmpq	$99999, %rbp
	ja	.L583
	cmpq	$9999, %rbp
	ja	.L584
	cmpq	$999, %rbp
	ja	.L585
	cmpq	$99, %rbp
	ja	.L475
	cmpq	$10, %rbp
	sbbl	%eax, %eax
	leaq	1328(%rsp), %r9
	leal	2(%rax), %r10d
	addq	%r9, %r10
	movq	%r10, %r8
	movq	%rbp, %rcx
.L478:
	cmpq	$9, %rcx
	jbe	.L480
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r11
	movzwl	(%r11,%rcx,2), %eax
	movw	%ax, -2(%r10)
.L481:
	movq	104(%rsp), %rax
	movzbl	(%rax), %r11d
	leaq	300(%rsp), %rdi
	movq	%rdi, 112(%rsp)
	movq	%rdi, %rsi
	jmp	.L482
.L487:
	testb	$4, %dl
	je	.L491
	movsl
.L491:
	testb	$2, %dl
	je	.L492
	movsw
.L492:
	andl	$1, %edx
	je	.L493
	movsb
.L493:
	leaq	-1(%r8), %rsi
	movb	%r14b, -1(%r8)
	movq	%r10, %r8
.L483:
	cmpq	%rax, %r12
	je	.L482
	movzbl	1(%rax), %r11d
	addq	$1, %rax
.L482:
	testb	%r11b, %r11b
	je	.L483
	movzbl	%r11b, %ecx
	movq	%rcx, %rdx
	negq	%rdx
	movq	%r8, %r10
	subq	%rcx, %r10
	cmpq	%r9, %r10
	jb	.L780
	leaq	(%rsi,%rdx), %r8
	movl	%ecx, %edx
	movq	%r8, %rdi
	movq	%r10, %rsi
	cmpl	$8, %ecx
	jb	.L487
	testb	$1, %r8b
	jne	.L781
.L488:
	testb	$2, %dil
	jne	.L782
.L489:
	testb	$4, %dil
	jne	.L783
.L490:
	movl	%edx, %ecx
	shrl	$3, %ecx
	rep movsq
	jmp	.L487
.L780:
	subq	%r9, %r8
	leaq	1(%rsi), %r10
	jne	.L784
.L495:
	leaq	256(%rsp), %rsi
	movq	112(%rsp), %rdi
	subq	%r10, %rdi
	movq	%rdi, %r8
	movq	%r10, %rdx
	movq	%rsi, %rcx
	call	memmove
	leaq	(%rsi,%rdi), %rcx
	jmp	.L473
.L475:
	movl	$100, %ecx
	movq	%rbp, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %rcx
	leaq	1328(%rsp), %r9
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r11
	movzwl	(%r11,%rdx,2), %eax
	movw	%ax, 1329(%rsp)
	leaq	1331(%rsp), %r8
	leaq	1329(%rsp), %r10
.L480:
	addl	$48, %ecx
	movb	%cl, -1(%r10)
	jmp	.L481
.L782:
	movzwl	(%rsi), %ecx
	movw	%cx, (%rdi)
	addq	$2, %rdi
	addq	$2, %rsi
	subl	$2, %edx
	jmp	.L489
.L781:
	movzbl	(%r10), %edx
	movb	%dl, (%r8)
	leaq	1(%r8), %rdi
	leaq	1(%r10), %rsi
	leal	-1(%rcx), %edx
	jmp	.L488
.L783:
	movl	(%rsi), %ecx
	movl	%ecx, (%rdi)
	addq	$4, %rdi
	addq	$4, %rsi
	subl	$4, %edx
	jmp	.L490
.L784:
	subq	%r8, %rsi
	movq	%r9, %rdx
	movq	%rsi, %rcx
	call	memcpy
	movq	%rsi, %r10
	jmp	.L495
.L585:
	movl	$4, %eax
.L474:
	leaq	1328(%rsp), %r9
	leaq	(%r9,%rax), %r8
	movq	%r8, %r10
	movq	%rbp, %rcx
	cmpq	$99, %rbp
	jbe	.L478
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r11
	movabsq	$2951479051793528259, %rdi
.L479:
	movq	%rcx, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%rdi
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %rsi
	salq	$2, %rsi
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	movq	%rcx, %rsi
	movq	%rax, %rcx
	subq	$2, %r10
	movzwl	(%r11,%rdx,2), %eax
	movw	%ax, (%r10)
	cmpq	$9999, %rsi
	ja	.L479
	jmp	.L478
.L584:
	movl	$5, %eax
	jmp	.L474
.L583:
	movl	$6, %eax
	jmp	.L474
.L582:
	movl	$7, %eax
	jmp	.L474
.L386:
	leaq	.LC5(%rip), %rcx
	call	_ZSt20__throw_length_errorPKc
.L579:
	movl	$5, %eax
	jmp	.L466
.L578:
	movl	$6, %eax
	jmp	.L466
.L580:
	movl	$4, %eax
	jmp	.L466
.L499:
	movq	%r8, 112(%rsp)
	movl	$4096, %edx
	movl	$1048576, %ecx
	call	_ZnwySt11align_val_t
	movq	%rax, %r15
	leaq	1048576(%rax), %rax
	movq	%rax, 88(%rsp)
	movq	112(%rsp), %r8
	movq	%rsi, %rdx
	movq	%r15, %rcx
	call	memcpy
	movq	112(%rsp), %r8
	leaq	(%r15,%r8), %r13
	jmp	.L464
.L768:
	subq	%r9, %r8
	jne	.L785
	leaq	1(%rsi), %r10
.L463:
	movq	%r11, %rsi
	subq	%r10, %rsi
	movq	%rsi, %r8
	movq	%r10, %rdx
	movq	%r13, %rcx
	call	memmove
	leaq	0(%r13,%rsi), %r9
	jmp	.L441
.L443:
	movl	$100, %ecx
	movq	%rbp, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %rcx
	leaq	1328(%rsp), %r9
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r11
	movzwl	(%r11,%rdx,2), %eax
	movw	%ax, 1329(%rsp)
	leaq	1329(%rsp), %r10
	leaq	1331(%rsp), %r8
.L448:
	addl	$48, %ecx
	movb	%cl, -1(%r10)
	jmp	.L449
.L770:
	movzwl	(%rsi), %ecx
	movw	%cx, (%rdi)
	addq	$2, %rdi
	addq	$2, %rsi
	subl	$2, %edx
	testb	$4, %dil
	je	.L458
.L771:
	movl	(%rsi), %ecx
	movl	%ecx, (%rdi)
	addq	$4, %rdi
	addq	$4, %rsi
	subl	$4, %edx
	jmp	.L458
.L769:
	movzbl	(%r10), %edx
	movb	%dl, (%r8)
	leaq	1(%r8), %rdi
	leaq	1(%r10), %rsi
	leal	-1(%rcx), %edx
	jmp	.L456
.L785:
	movq	%r11, 112(%rsp)
	subq	%r8, %rsi
	movq	%r9, %rdx
	movq	%rsi, %rcx
	call	memcpy
	movq	%rsi, %r10
	movq	112(%rsp), %r11
	jmp	.L463
.L575:
	movl	$4, %eax
.L442:
	leaq	1328(%rsp), %r9
	leaq	1328(%rsp,%rax), %r8
	movq	%r8, %r10
	movq	%rbp, %rcx
	cmpq	$99, %rbp
	jbe	.L446
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r11
	movabsq	$2951479051793528259, %rsi
.L447:
	movq	%rcx, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%rsi
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %rdi
	salq	$2, %rdi
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	movq	%rcx, %rdi
	movq	%rax, %rcx
	subq	$2, %r10
	movzwl	(%r11,%rdx,2), %eax
	movw	%ax, (%r10)
	cmpq	$9999, %rdi
	ja	.L447
	jmp	.L446
.L574:
	movl	$5, %eax
	jmp	.L442
.L573:
	movl	$6, %eax
	jmp	.L442
.L572:
	movl	$7, %eax
	jmp	.L442
.L760:
	cmpq	%rdx, %rax
	cmova	%rdx, %rbx
	jmp	.L416
.L762:
	cmpq	%rdx, %rax
	cmova	%rdx, %rbx
	jmp	.L389
.L764:
	cmpq	%rax, %rdx
	cmova	%rax, %rbx
	jmp	.L398
.L435:
	leaq	3(%r13), %r9
	movl	$100, %ecx
	movq	%rbp, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %rcx
	leaq	1(%r13), %r8
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r11
	movzwl	(%r11,%rdx,2), %eax
	movw	%ax, 1(%r13)
	jmp	.L440
.L570:
	movl	$4, %r9d
	jmp	.L434
.L569:
	movl	$5, %r9d
	jmp	.L434
.L568:
	movl	$6, %r9d
	jmp	.L434
.L766:
	cmpq	%rax, %rdx
	cmova	%rax, %rbx
	jmp	.L407
.L752:
	xorl	%edx, %edx
	jmp	.L382
	.seh_endproc
	.section	.text.unlikely,"x"
	.def	main.cold;	.scl	3;	.type	32;	.endef
	.seh_proc	main.cold
	.seh_stackalloc	2456
	.seh_savereg	%rbx, 2392
	.seh_savereg	%rsi, 2400
	.seh_savereg	%rdi, 2408
	.seh_savereg	%rbp, 2416
	.seh_savexmm	%xmm6, 2352
	.seh_savexmm	%xmm7, 2368
	.seh_savereg	%r12, 2424
	.seh_savereg	%r13, 2432
	.seh_savereg	%r14, 2440
	.seh_savereg	%r15, 2448
	.seh_endprologue
main.cold:
.L738:
	movq	320(%rax), %r8
	movl	$1, %edx
.L517:
	movzbl	%sil, %eax
	testb	$8, (%r8,%rax,2)
	jne	.L786
.L516:
	addb	$1, %sil
	jne	.L517
	jmp	.L514
.L786:
	shrq	$5, %rax
	movl	%edx, %edi
	movl	%esi, %ecx
	sall	%cl, %edi
	orl	%edi, (%r12,%rax,4)
	jmp	.L516
	.section	.text.startup,"x"
	.section	.text.unlikely,"x"
	.seh_endproc
.LCOLDE11:
	.section	.text.startup,"x"
.LHOTE11:
	.globl	_ZN7fast_io7details3ryu4pow5IdLb1EE5splitE
	.section	.rdata$_ZN7fast_io7details3ryu4pow5IdLb1EE5splitE,"dr"
	.linkonce same_size
	.align 32
_ZN7fast_io7details3ryu4pow5IdLb1EE5splitE:
	.quad	0
	.quad	1152921504606846976
	.quad	0
	.quad	1441151880758558720
	.quad	0
	.quad	1801439850948198400
	.quad	0
	.quad	2251799813685248000
	.quad	0
	.quad	1407374883553280000
	.quad	0
	.quad	1759218604441600000
	.quad	0
	.quad	2199023255552000000
	.quad	0
	.quad	1374389534720000000
	.quad	0
	.quad	1717986918400000000
	.quad	0
	.quad	2147483648000000000
	.quad	0
	.quad	1342177280000000000
	.quad	0
	.quad	1677721600000000000
	.quad	0
	.quad	2097152000000000000
	.quad	0
	.quad	1310720000000000000
	.quad	0
	.quad	1638400000000000000
	.quad	0
	.quad	2048000000000000000
	.quad	0
	.quad	1280000000000000000
	.quad	0
	.quad	1600000000000000000
	.quad	0
	.quad	2000000000000000000
	.quad	0
	.quad	1250000000000000000
	.quad	0
	.quad	1562500000000000000
	.quad	0
	.quad	1953125000000000000
	.quad	0
	.quad	1220703125000000000
	.quad	0
	.quad	1525878906250000000
	.quad	0
	.quad	1907348632812500000
	.quad	0
	.quad	1192092895507812500
	.quad	0
	.quad	1490116119384765625
	.quad	4611686018427387904
	.quad	1862645149230957031
	.quad	-8646911284551352320
	.quad	1164153218269348144
	.quad	-6196953087261802496
	.quad	1455191522836685180
	.quad	-3134505340649865216
	.quad	1818989403545856475
	.quad	-3918131675812331520
	.quad	2273736754432320594
	.quad	-4754675306596401152
	.quad	1421085471520200371
	.quad	-5943344133245501440
	.quad	1776356839400250464
	.quad	-2817494148129488896
	.quad	2220446049250313080
	.quad	-8678462870222012416
	.quad	1387778780781445675
	.quad	7598665485932036096
	.quad	1734723475976807094
	.quad	274959820560269312
	.quad	2168404344971008868
	.quad	-9051522149004607488
	.quad	1355252715606880542
	.quad	2520655369026404352
	.quad	1694065894508600678
	.quad	-6072552825571770368
	.quad	2117582368135750847
	.quad	-3795345515982356480
	.quad	1323488980084844279
	.quad	-4744181894977945600
	.quad	1654361225106055349
	.quad	3293144668132343808
	.quad	2067951531382569187
	.quad	-247627591630979072
	.quad	1292469707114105741
	.quad	8913837547316051968
	.quad	1615587133892632177
	.quad	-2692761121137098752
	.quad	2019483917365790221
	.quad	-6294661719138074624
	.quad	1262177448353618888
	.quad	-3256641130495205376
	.quad	1577721810442023610
	.quad	-8682487431546394624
	.quad	1972152263052529513
	.quad	-814868626289108736
	.quad	1232595164407830945
	.quad	8204786253993389888
	.quad	1540743955509788682
	.quad	1032610780636961552
	.quad	1925929944387235853
	.quad	2951224747111794922
	.quad	1203706215242022408
	.quad	3689030933889743652
	.quad	1504632769052528010
	.quad	-4612083369492596243
	.quad	1880790961315660012
	.quad	-576709096719178700
	.quad	1175494350822287507
	.quad	-720886370898973375
	.quad	1469367938527859384
	.quad	3710578054803671186
	.quad	1836709923159824231
	.quad	26536550077201078
	.quad	2295887403949780289
	.quad	-6900943683842831182
	.quad	1434929627468612680
	.quad	-4014493586376151074
	.quad	1793662034335765850
	.quad	8816941072311974870
	.quad	2242077542919707313
	.quad	-1406940857446097563
	.quad	1401298464324817070
	.quad	-6370362090235009857
	.quad	1751623080406021338
	.quad	5872105442488401391
	.quad	2189528850507526673
	.quad	-3247463126085830987
	.quad	1368455531567204170
	.quad	-8671014926034676638
	.quad	1710569414459005213
	.quad	-1615396620688569989
	.quad	2138211768073756516
	.quad	1296220121283337709
	.quad	1336382355046097823
	.quad	-2991410866823215768
	.quad	1670477943807622278
	.quad	-8350949601956407614
	.quad	2088097429759527848
	.quad	6309871544845715001
	.quad	1305060893599704905
	.quad	-5947718624225019960
	.quad	1631326116999631131
	.quad	-7434648280281274950
	.quad	2039157646249538914
	.quad	-6952498184389490796
	.quad	1274473528905961821
	.quad	532749306367912313
	.quad	1593091911132452277
	.quad	5277622651387278295
	.quad	1991364888915565346
	.quad	7910200175544436838
	.quad	1244603055572228341
	.quad	-3947307835851617664
	.quad	1555753819465285426
	.quad	8900923260467641632
	.quad	1944692274331606783
	.quad	-5966138008276193740
	.quad	1215432671457254239
	.quad	-7457672510345242175
	.quad	1519290839321567799
	.quad	9124653435777998898
	.quad	1899113549151959749
	.quad	8008751406574943263
	.quad	1186945968219974843
	.quad	5399253239791291175
	.quad	1483682460274968554
	.quad	-2474305487115661840
	.quad	1854603075343710692
	.quad	759402079766405302
	.quad	1159126922089819183
	.quad	-3662433418719381276
	.quad	1448908652612273978
	.quad	-9189727791826614499
	.quad	1811135815765342473
	.quad	-2263787702928492316
	.quad	2263919769706678091
	.quad	7808504722524468110
	.quad	1414949856066673807
	.quad	5148944884728197234
	.quad	1768687320083342259
	.quad	1824495087482858639
	.quad	2210859150104177824
	.quad	1140309429676786649
	.quad	1381786968815111140
	.quad	1425386787095983311
	.quad	1727233711018888925
	.quad	6393419502297367043
	.quad	2159042138773611156
	.quad	-5227484847918921406
	.quad	1349401336733506972
	.quad	-1922670041471263854
	.quad	1686751670916883715
	.quad	-2403337551839079817
	.quad	2108439588646104644
	.quad	803757039314269066
	.quad	1317774742903815403
	.quad	-3606989719284551571
	.quad	1647218428629769253
	.quad	4714634887749086344
	.quad	2059023035787211567
	.quad	-8582568241225290795
	.quad	1286889397367007229
	.quad	-1504838264676837686
	.quad	1608611746708759036
	.quad	2730638187581340797
	.quad	2010764683385948796
	.quad	-7516723169616437810
	.quad	1256727927116217997
	.quad	-172531925165771454
	.quad	1570909908895272496
	.quad	4396021111970173586
	.quad	1963637386119090621
	.quad	5053356204195052443
	.quad	1227273366324431638
	.quad	-2906676781610960254
	.quad	1534091707905539547
	.quad	-3633345977013700317
	.quad	1917614634881924434
	.quad	-4576684244847256650
	.quad	1198509146801202771
	.quad	-5720855306059070813
	.quad	1498136433501503464
	.quad	-2539383114146450612
	.quad	1872670541876879330
	.quad	-3892957455555225585
	.quad	1170419088673049581
	.quad	4357175217410743827
	.quad	1463023860841311977
	.quad	-8388589033518733928
	.quad	1828779826051639971
	.quad	7961007781811134206
	.quad	2285974782564549964
	.quad	-4247742173222816929
	.quad	1428734239102843727
	.quad	-5309677716528521161
	.quad	1785917798878554659
	.quad	-6637097145660651452
	.quad	2232397248598193324
	.quad	-1842342706824213205
	.quad	1395248280373870827
	.quad	-2302928383530266507
	.quad	1744060350467338534
	.quad	-7490346497840221037
	.quad	2180075438084173168
	.quad	6847748484918331612
	.quad	1362547148802608230
	.quad	-663686430706861293
	.quad	1703183936003260287
	.quad	-829608038383576617
	.quad	2128979920004075359
	.quad	-518505023989735386
	.quad	1330612450002547099
	.quad	-648131279987169232
	.quad	1663265562503183874
	.quad	-5421850118411349444
	.quad	2079081953128979843
	.quad	5834715712847682405
	.quad	1299426220705612402
	.quad	-1929977395795172801
	.quad	1624282775882015502
	.quad	-7024157763171353905
	.quad	2030353469852519378
	.quad	-6695941611195790143
	.quad	1268970918657824611
	.quad	-8369927013994737679
	.quad	1586213648322280764
	.quad	-5850722749066034194
	.quad	1982767060402850955
	.quad	5566670318688504437
	.quad	1239229412751781847
	.quad	2346651879933242642
	.quad	1549036765939727309
	.quad	7545000868343941206
	.quad	1936295957424659136
	.quad	4715625542714963254
	.quad	1210184973390411960
	.quad	5894531928393704067
	.quad	1512731216738014950
	.quad	-1855207126362645724
	.quad	1890914020922518687
	.quad	-1159504453976653577
	.quad	1181821263076574179
	.quad	-1449380567470816972
	.quad	1477276578845717724
	.quad	2799960309088866689
	.quad	1846595723557147156
	.quad	-7473396843674234127
	.quad	1154122327223216972
	.quad	-4730060036165404755
	.quad	1442652909029021215
	.quad	-5912575045206755944
	.quad	1803316136286276519
	.quad	-7390718806508444929
	.quad	2254145170357845649
	.quad	-7513235640390177
	.quad	1408840731473653530
	.quad	-4621077562977875625
	.quad	1761050914342066913
	.quad	3447025083132431277
	.quad	2201313642927583642
	.quad	6766076695385157452
	.quad	1375821026829739776
	.quad	8457595869231446815
	.quad	1719776283537174720
	.quad	-7874749237170243097
	.quad	2149720354421468400
	.quad	6607496772837067824
	.quad	1343575221513417750
	.quad	-964001070808441028
	.quad	1679469026891772187
	.quad	-1205001338510551285
	.quad	2099336283614715234
	.quad	-3058968845782788505
	.quad	1312085177259197021
	.quad	5399660979626290177
	.quad	1640106471573996277
	.quad	-7085481830749300991
	.quad	2050133089467495346
	.quad	-6734269153432007072
	.quad	1281333180917184591
	.quad	-8417836441790008839
	.quad	1601666476146480739
	.quad	7924448521472040567
	.quad	2002083095183100924
	.quad	-4270591710934750454
	.quad	1251301934489438077
	.quad	3885132398186337741
	.quad	1564127418111797597
	.quad	-8978642557549241536
	.quad	1955159272639746996
	.quad	-3305808589254582008
	.quad	1221974545399841872
	.quad	479425281859160394
	.quad	1527468181749802341
	.quad	5210967620751338397
	.quad	1909335227187252926
	.quad	-1354831255457801406
	.quad	1193334516992033078
	.quad	-6305225087749639662
	.quad	1491668146240041348
	.quad	-3269845341259661673
	.quad	1864585182800051685
	.quad	-6655339356714676450
	.quad	1165365739250032303
	.quad	-8319174195893345562
	.quad	1456707174062540379
	.quad	8047776328842869663
	.quad	1820883967578175474
	.quad	836348374198811271
	.quad	2276104959472719343
	.quad	7440246761515338900
	.quad	1422565599670449589
	.quad	-4534749603387990086
	.quad	1778206999588061986
	.quad	8166621051047176104
	.quad	2222758749485077483
	.quad	2798295147690791113
	.quad	1389224218428173427
	.quad	-1113817083813899013
	.quad	1736530273035216783
	.quad	-1392271354767373766
	.quad	2170662841294020979
	.quad	8353202440125167204
	.quad	1356664275808763112
	.quad	-8005241023553092611
	.quad	1695830344760953890
	.quad	3828506775840797949
	.quad	2119787930951192363
	.quad	86973725686804766
	.quad	1324867456844495227
	.quad	-4502968861318881947
	.quad	1656084321055619033
	.quad	3594660960206173375
	.quad	2070105401319523792
	.quad	2246663100128858359
	.quad	1293815875824702370
	.quad	-6415043161693702859
	.quad	1617269844780877962
	.quad	5816254103165035138
	.quad	2021587305976097453
	.quad	5941001823691840913
	.quad	1263492066235060908
	.quad	7426252279614801142
	.quad	1579365082793826135
	.quad	4671129331091113523
	.quad	1974206353492282669
	.quad	5225298841145639904
	.quad	1233878970932676668
	.quad	6531623551432049880
	.quad	1542348713665845835
	.quad	3552843420862674446
	.quad	1927935892082307294
	.quad	-2391158880388216375
	.quad	1204959932551442058
	.quad	-7600634618912658373
	.quad	1506199915689302573
	.quad	-277421236786047158
	.quad	1882749894611628216
	.quad	-7090917300632361330
	.quad	1176718684132267635
	.quad	-8863646625790451662
	.quad	1470898355165334544
	.quad	-6467872263810676674
	.quad	1838622943956668180
	.quad	-3473154311335957938
	.quad	2298278679945835225
	.quad	2440964573842414192
	.quad	1436424174966147016
	.quad	3051205717303017741
	.quad	1795530218707683770
	.quad	-5409364890226003632
	.quad	2244412773384604712
	.quad	8148361989677217490
	.quad	1402757983365377945
	.quad	-3649605568185641850
	.quad	1753447479206722431
	.quad	-4562006960232052312
	.quad	2191809349008403039
	.quad	-2851254350145032695
	.quad	1369880843130251899
	.quad	-3564067937681290869
	.quad	1712351053912814874
	.quad	-9066770940529001490
	.quad	2140438817391018593
	.quad	-1055045819403238027
	.quad	1337774260869386620
	.quad	3292878744173340370
	.quad	1672217826086733276
	.quad	4116098430216675462
	.quad	2090272282608416595
	.quad	266718509671728212
	.quad	1306420176630260372
	.quad	333398137089660265
	.quad	1633025220787825465
	.quad	5028433689789463235
	.quad	2041281525984781831
	.quad	-8386443989950055238
	.quad	1275800953740488644
	.quad	-5871368969010181144
	.quad	1594751192175610805
	.quad	1884160825592049379
	.quad	1993438990219513507
	.quad	-1128242493218663091
	.quad	1245899368887195941
	.quad	7813068920331446945
	.quad	1557374211108994927
	.quad	5154650131986920777
	.quad	1946717763886243659
	.quad	915813323278131534
	.quad	1216698602428902287
	.quad	-3466919364329723487
	.quad	1520873253036127858
	.quad	-8945335223839542262
	.quad	1901091566295159823
	.quad	-5590834514899713914
	.quad	1188182228934474889
	.quad	2234828893230133415
	.quad	1485227786168093612
	.quad	2793536116537666769
	.quad	1856534732710117015
	.quad	8663489100477123587
	.quad	1160334207943823134
	.quad	1605989338741628675
	.quad	1450417759929778918
	.quad	-7215885363427739964
	.quad	1813022199912223647
	.quad	-9019856704284674954
	.quad	2266277749890279559
	.quad	-5637410440177921847
	.quad	1416423593681424724
	.quad	-2435077031795014404
	.quad	1770529492101780905
	.quad	6179525747111007803
	.quad	2213161865127226132
	.quad	-5361168444910395931
	.quad	1383226165704516332
	.quad	-2089774537710607010
	.quad	1729032707130645415
	.quad	-2612218172138258762
	.quad	2161290883913306769
	.quad	2979049660840976177
	.quad	1350806802445816731
	.quad	-887873942376167682
	.quad	1688508503057270913
	.quad	8113529608884566205
	.quad	2110635628821588642
	.quad	-8764102049729309834
	.quad	1319147268013492901
	.quad	-1731755525306861484
	.quad	1648934085016866126
	.quad	-6776380425060964759
	.quad	2061167606271082658
	.quad	-6541080774876796927
	.quad	1288229753919426661
	.quad	1047021068258779650
	.quad	1610287192399283327
	.quad	-3302909683103913342
	.quad	2012858990499104158
	.quad	4853210475701136017
	.quad	1258036869061940099
	.quad	1454827076199032118
	.quad	1572546086327425124
	.quad	1818533845248790147
	.quad	1965682607909281405
	.quad	3442426662494187794
	.quad	1228551629943300878
	.quad	-4920338708737041066
	.quad	1535689537429126097
	.quad	3072948650933474476
	.quad	1919611921786407622
	.quad	-2691093111593966357
	.quad	1199757451116504763
	.quad	-3363866389492457946
	.quad	1499696813895630954
	.quad	-8816519005292960336
	.quad	1874621017369538693
	.quad	8324733676974063502
	.quad	1171638135855961683
	.quad	5794231077790191473
	.quad	1464547669819952104
	.quad	7242788847237739342
	.quad	1830684587274940130
	.quad	-169885977807601630
	.quad	2288355734093675162
	.quad	-2412021745343444971
	.quad	1430222333808546976
	.quad	1596658836748081690
	.quad	1787777917260683721
	.quad	6607509564362490017
	.quad	2234722396575854651
	.quad	1823850468512862308
	.quad	1396701497859909157
	.quad	6891499104068465790
	.quad	1745876872324886446
	.quad	-608998156769193571
	.quad	2182346090406108057
	.quad	4231062170446641922
	.quad	1363966306503817536
	.quad	5288827713058302403
	.quad	1704957883129771920
	.quad	6611034641322878003
	.quad	2131197353912214900
	.quad	-5091475386027977056
	.quad	1331998346195134312
	.quad	-1752658214107583416
	.quad	1664997932743917890
	.quad	-6802508786061867174
	.quad	2081247415929897363
	.quad	4971804045566108824
	.quad	1300779634956185852
	.quad	6214755056957636030
	.quad	1625974543695232315
	.quad	3156757802769657134
	.quad	2032468179619040394
	.quad	6584659645158423613
	.quad	1270292612261900246
	.quad	-992547480406746292
	.quad	1587865765327375307
	.quad	-1240684350508432865
	.quad	1984832206659219134
	.quad	6142101308573311315
	.quad	1240520129162011959
	.quad	3065940617289251240
	.quad	1550650161452514949
	.quad	8444111790038951954
	.quad	1938312701815643686
	.quad	665883850346957067
	.quad	1211445438634777304
	.quad	832354812933696334
	.quad	1514306798293471630
	.quad	-8182928520687655390
	.quad	1892883497866839537
	.quad	-502644307002396715
	.quad	1183052186166774710
	.quad	-5239991402180383798
	.quad	1478815232708468388
	.quad	-1938303234298091843
	.quad	1848519040885585485
	.quad	-5823125539863695306
	.quad	1155324400553490928
	.quad	-2667220906402231229
	.quad	1444155500691863660
	.quad	1277659885424598868
	.quad	1805194375864829576
	.quad	1597074856780748586
	.quad	2256492969831036970
	.quad	5609857803915355770
	.quad	1410308106144398106
	.quad	-2211049781960581095
	.quad	1762885132680497632
	.quad	1847873790976661535
	.quad	2203606415850622041
	.quad	-5762607908280668397
	.quad	1377254009906638775
	.quad	-7203259885350835496
	.quad	1721567512383298469
	.quad	219297180166231438
	.quad	2151959390479123087
	.quad	7054589765244976505
	.quad	1344974619049451929
	.quad	-5016820848725943081
	.quad	1681218273811814911
	.quad	-6271026060907428851
	.quad	2101522842264768639
	.quad	-3919391288067143032
	.quad	1313451776415480399
	.quad	-4899239110083928790
	.quad	1641814720519350499
	.quad	-6124048887604910988
	.quad	2052268400649188124
	.quad	-1521687545539375415
	.quad	1282667750405742577
	.quad	7321262604930556539
	.quad	1603334688007178222
	.quad	-71793780691580134
	.quad	2004168360008972777
	.quad	4566814905495150320
	.quad	1252605225005607986
	.quad	-3514853404985837908
	.quad	1565756531257009982
	.quad	-9005252774659685289
	.quad	1957195664071262478
	.quad	1289246043478778550
	.quad	1223247290044539049
	.quad	6223243572775861092
	.quad	1529059112555673811
	.quad	3167368447542438461
	.quad	1911323890694592264
	.quad	1979605279714024038
	.quad	1194577431684120165
	.quad	7086192618069917952
	.quad	1493221789605150206
	.quad	-365631264267378368
	.quad	1866527237006437757
	.quad	-4840205558594499384
	.quad	1166579523129023598
	.quad	7784801107039039482
	.quad	1458224403911279498
	.quad	507629346944023544
	.quad	1822780504889099373
	.quad	5246222702107417334
	.quad	2278475631111374216
	.quad	3278889188817135834
	.quad	1424047269444608885
	.quad	8710297504448807696
	.quad	1780059086805761106
	.globl	_ZN7fast_io7details3ryu4pow5IdLb1EE9inv_splitE
	.section	.rdata$_ZN7fast_io7details3ryu4pow5IdLb1EE9inv_splitE,"dr"
	.linkonce same_size
	.align 32
_ZN7fast_io7details3ryu4pow5IdLb1EE9inv_splitE:
	.quad	1
	.quad	2305843009213693952
	.quad	-7378697629483820646
	.quad	1844674407370955161
	.quad	5165088340638674453
	.quad	1475739525896764129
	.quad	7821419487252849886
	.quad	1180591620717411303
	.quad	8824922364862649494
	.quad	1888946593147858085
	.quad	7059937891890119595
	.quad	1511157274518286468
	.quad	-5420096130713635294
	.quad	1208925819614629174
	.quad	-8672153809141816470
	.quad	1934281311383406679
	.quad	-6937723047313453176
	.quad	1547425049106725343
	.quad	-1860829623108852217
	.quad	1237940039285380274
	.quad	-2977327396974163548
	.quad	1980704062856608439
	.quad	-2381861917579330838
	.quad	1584563250285286751
	.quad	9162556910162266299
	.quad	1267650600228229401
	.quad	7281393426775805432
	.quad	2028240960365167042
	.quad	-1553582888063176301
	.quad	1622592768292133633
	.quad	2446482504291369283
	.quad	1298074214633706907
	.quad	7603720821608101175
	.quad	2076918743413931051
	.quad	2393627842544570617
	.quad	1661534994731144841
	.quad	-1774446540706253830
	.quad	1329227995784915872
	.quad	-6528463279871916451
	.quad	2126764793255865396
	.quad	5845275820328197809
	.quad	1701411834604692317
	.quad	-2702476973221262399
	.quad	1361129467683753853
	.quad	3054734472329800808
	.quad	2177807148294006166
	.quad	-1245561236878069677
	.quad	1742245718635204932
	.quad	6382248639981364905
	.quad	1393796574908163946
	.quad	2832900194486363201
	.quad	2230074519853062314
	.quad	5955668970331000884
	.quad	1784059615882449851
	.quad	1075186361522890384
	.quad	1427247692705959881
	.quad	-5658399451047196032
	.quad	2283596308329535809
	.quad	-4526719560837756825
	.quad	1826877046663628647
	.quad	3757321980813615186
	.quad	1461501637330902918
	.quad	-8062188859574838821
	.quad	1169201309864722334
	.quad	5547241898389809503
	.quad	1870722095783555735
	.quad	4437793518711847602
	.quad	1496577676626844588
	.quad	-7517811629256252888
	.quad	1197262141301475670
	.quad	-960452162584273651
	.quad	1915619426082361072
	.quad	6610335899416401726
	.quad	1532495540865888858
	.quad	-5779777724692609589
	.quad	1225996432692711086
	.quad	-5558295544766265019
	.quad	1961594292308337738
	.quad	-757287621071101692
	.quad	1569275433846670190
	.quad	-4295178911598791677
	.quad	1255420347077336152
	.quad	7885109000409574610
	.quad	2008672555323737844
	.quad	-8449308058639981605
	.quad	1606938044258990275
	.quad	7997948812055656009
	.quad	1285550435407192220
	.quad	-5650025974420502002
	.quad	2056880696651507552
	.quad	2858676849947419045
	.quad	1645504557321206042
	.quad	-5091756149525885410
	.quad	1316403645856964833
	.quad	-768112209757596011
	.quad	2106245833371143733
	.quad	3074859046935833515
	.quad	1684996666696914987
	.quad	-4918810391935153834
	.quad	1347997333357531989
	.quad	-7870096627096246135
	.quad	2156795733372051183
	.quad	-2606728486935086585
	.quad	1725436586697640946
	.quad	8982663654677661702
	.quad	1380349269358112757
	.quad	-385133411483382570
	.quad	2208558830972980411
	.quad	-7686804358670526703
	.quad	1766847064778384329
	.quad	-6149443486936421362
	.quad	1413477651822707463
	.quad	-2460411949614453533
	.quad	2261564242916331941
	.quad	9099716884534168143
	.quad	1809251394333065553
	.quad	-3788272936598396455
	.quad	1447401115466452442
	.quad	4348079280205103483
	.quad	1157920892373161954
	.quad	-4111119595897565398
	.quad	1852673427797059126
	.quad	7779150767507678651
	.quad	1482138742237647301
	.quad	2533971799264232598
	.quad	1185710993790117841
	.quad	-3324342750661048490
	.quad	1897137590064188545
	.quad	-6348823015270749115
	.quad	1517710072051350836
	.quad	5988988032009131678
	.quad	1214168057641080669
	.quad	-1485665593011120286
	.quad	1942668892225729070
	.quad	-4877881289150806552
	.quad	1554135113780583256
	.quad	7165741412905085728
	.quad	1243308091024466605
	.quad	-6981557813061414451
	.quad	1989292945639146568
	.quad	-1895897435707221237
	.quad	1591434356511317254
	.quad	-1516717948565776990
	.quad	1273147485209053803
	.quad	4951948911778577463
	.quad	2037035976334486086
	.quad	272210314680951647
	.quad	1629628781067588869
	.quad	3907117066486671641
	.quad	1303703024854071095
	.quad	6251387306378674625
	.quad	2085924839766513752
	.quad	-2377587784380880946
	.quad	1668739871813211001
	.quad	9165976216721026213
	.quad	1334991897450568801
	.quad	7286864317269821294
	.quad	2135987035920910082
	.quad	-1549206175667963611
	.quad	1708789628736728065
	.quad	-4928713755276281212
	.quad	1367031702989382452
	.quad	6871453250525591353
	.quad	2187250724783011924
	.quad	9186511415162383406
	.quad	1749800579826409539
	.quad	-7408186126837734568
	.quad	1399840463861127631
	.quad	-8163748988198464986
	.quad	2239744742177804210
	.quad	8226396068408869304
	.quad	1791795793742243368
	.quad	-4486929589498635526
	.quad	1433436634993794694
	.quad	-7179087343197816842
	.quad	2293498615990071511
	.quad	5324776569667477496
	.quad	1834798892792057209
	.quad	7949170070475892320
	.quad	1467839114233645767
	.quad	-1019361573103106790
	.quad	1174271291386916613
	.quad	5747719112518849781
	.quad	1878834066219066582
	.quad	-2780522339468740821
	.quad	1503067252975253265
	.quad	-5913766686316902980
	.quad	1202453802380202612
	.quad	5295368560860596524
	.quad	1923926083808324180
	.quad	4236294848688477220
	.quad	1539140867046659344
	.quad	7078384693692692099
	.quad	1231312693637327475
	.quad	-7121328563801244258
	.quad	1970100309819723960
	.quad	9060332407926645887
	.quad	1576080247855779168
	.quad	-3819780517884414260
	.quad	1260864198284623334
	.quad	-6111648828615062817
	.quad	2017382717255397335
	.quad	-8578667877633960576
	.quad	1613906173804317868
	.quad	-3173585487365258138
	.quad	1291124939043454294
	.quad	-5077736779784413021
	.quad	2065799902469526871
	.quad	7005857020398200553
	.quad	1652639921975621497
	.quad	-1774012013165260204
	.quad	1322111937580497197
	.quad	-6527768035806326650
	.quad	2115379100128795516
	.quad	5845832015580669650
	.quad	1692303280103036413
	.quad	-6391380831761195250
	.quad	1353842624082429130
	.quad	841837113407818570
	.quad	2166148198531886609
	.quad	4362818505468165179
	.quad	1732918558825509287
	.quad	-3888442825109288503
	.quad	1386334847060407429
	.quad	-6221508520174861605
	.quad	2218135755296651887
	.quad	2401490813343931363
	.quad	1774508604237321510
	.quad	1921192650675145090
	.quad	1419606883389857208
	.quad	-615440573661678179
	.quad	2271371013423771532
	.quad	6886345170554478103
	.quad	1817096810739017226
	.quad	1819727321701672159
	.quad	1453677448591213781
	.quad	-2233566957380572596
	.quad	1162941958872971024
	.quad	-3573707131808916153
	.quad	1860707134196753639
	.quad	-2858965705447132922
	.quad	1488565707357402911
	.quad	8780873879868024632
	.quad	1190852565885922329
	.quad	2981351763563108441
	.quad	1905364105417475727
	.quad	-4993616218633333894
	.quad	1524291284333980581
	.quad	7073153469319063855
	.quad	1219433027467184465
	.quad	-7129698522799049449
	.quad	1951092843947495144
	.quad	-5703758818239239559
	.quad	1560874275157996115
	.quad	-8252355869333301970
	.quad	1248699420126396892
	.quad	1553625868034358140
	.quad	1997919072202235028
	.quad	8621598323911307159
	.quad	1598335257761788022
	.quad	-481418970354774919
	.quad	1278668206209430417
	.quad	-4459619167309550194
	.quad	2045869129935088668
	.quad	121653480894270168
	.quad	1636695303948070935
	.quad	97322784715416134
	.quad	1309356243158456748
	.quad	-3533632359197244509
	.quad	2094969989053530796
	.quad	8241140556867935363
	.quad	1675975991242824637
	.quad	-785785183989472356
	.quad	1340780792994259709
	.quad	-1257256294383155770
	.quad	2145249268790815535
	.quad	-4695153850248434939
	.quad	1716199415032652428
	.quad	-66774265456837628
	.quad	1372959532026121942
	.quad	-3796187639472850528
	.quad	2196735251241795108
	.quad	652398703163629901
	.quad	1757388200993436087
	.quad	-6856778666952916726
	.quad	1405910560794748869
	.quad	7475898206584884855
	.quad	2249456897271598191
	.quad	2291369750525997561
	.quad	1799565517817278553
	.quad	9211793429904618695
	.quad	1439652414253822842
	.quad	-18525771120251381
	.quad	2303443862806116547
	.quad	7363877012587619542
	.quad	1842755090244893238
	.quad	-5176944834155635336
	.quad	1474204072195914590
	.quad	-7830904682066418592
	.quad	1179363257756731672
	.quad	2227947767661371545
	.quad	1886981212410770676
	.quad	-1906990600612813087
	.quad	1509584969928616540
	.quad	-5214941295232160793
	.quad	1207667975942893232
	.quad	6413489186596184024
	.quad	1932268761508629172
	.quad	-2247906280206873427
	.quad	1545815009206903337
	.quad	5580372605318321905
	.quad	1236652007365522670
	.quad	8928596168509315048
	.quad	1978643211784836272
	.quad	-235820694676368608
	.quad	1582914569427869017
	.quad	7190041073742725760
	.quad	1266331655542295214
	.quad	436019273762630246
	.quad	2026130648867672343
	.quad	7727513048493924843
	.quad	1620904519094137874
	.quad	-8575384820172501418
	.quad	1296723615275310299
	.quad	4726128361433549347
	.quad	2074757784440496479
	.quad	7470251503888749801
	.quad	1659806227552397183
	.quad	-5091845241114731129
	.quad	1327844982041917746
	.quad	-4457603571041659483
	.quad	2124551971267068394
	.quad	-3566082856833327587
	.quad	1699641577013654715
	.quad	-6542215100208572392
	.quad	1359713261610923772
	.quad	4289851098633925465
	.quad	2175541218577478036
	.quad	-257467935834769951
	.quad	1740432974861982428
	.quad	3483374466074094362
	.quad	1392346379889585943
	.quad	1884050330976640656
	.quad	2227754207823337509
	.quad	5196589079523222848
	.quad	1782203366258670007
	.quad	-3221426365865242368
	.quad	1425762693006936005
	.quad	5913764258841343181
	.quad	2281220308811097609
	.quad	8420360221814984868
	.quad	1824976247048878087
	.quad	-642409452031832752
	.quad	1459980997639102469
	.quad	-513927561625466201
	.quad	1167984798111281975
	.quad	-8200981728084566569
	.quad	1868775676978051161
	.quad	4507261061758077715
	.quad	1495020541582440929
	.quad	7295157664148372495
	.quad	1196016433265952743
	.quad	7982903447895485668
	.quad	1913626293225524389
	.quad	-8371072500651252758
	.quad	1530901034580419511
	.quad	4371188443704728763
	.quad	1224720827664335609
	.quad	-4074144934298164949
	.quad	1959553324262936974
	.quad	-3259315947438531959
	.quad	1567642659410349579
	.quad	-2607452757950825567
	.quad	1254114127528279663
	.quad	3206773216762499739
	.quad	2006582604045247462
	.quad	-4813279056073820855
	.quad	1605266083236197969
	.quad	-3850623244859056684
	.quad	1284212866588958375
	.quad	4907049252451240275
	.quad	2054740586542333401
	.quad	236290587219081897
	.quad	1643792469233866721
	.quad	-3500316344966644806
	.quad	1315033975387093376
	.quad	-1911157337204721366
	.quad	2104054360619349402
	.quad	5849771759720043554
	.quad	1683243488495479522
	.quad	-2698880221707785803
	.quad	1346594790796383617
	.quad	-8007557169474367609
	.quad	2154551665274213788
	.quad	-2716696920837583764
	.quad	1723641332219371030
	.quad	-5862706351411977334
	.quad	1378913065775496824
	.quad	9066413911450387881
	.quad	2206260905240794919
	.quad	-7504264129807330988
	.quad	1765008724192635935
	.quad	8753983955121776503
	.quad	1412006979354108748
	.quad	-8129718560256619535
	.quad	2259211166966573997
	.quad	874922781278525018
	.quad	1807368933573259198
	.quad	8078635854506640661
	.quad	1445895146858607358
	.quad	-4605137760620418441
	.quad	1156716117486885886
	.quad	-3678871602250759182
	.quad	1850745787979017418
	.quad	746251532941302978
	.quad	1480596630383213935
	.quad	597001226353042382
	.quad	1184477304306571148
	.quad	-2734146852577042512
	.quad	1895163686890513836
	.quad	8880728962164096960
	.quad	1516130949512411069
	.quad	-7652812089236363725
	.quad	1212904759609928855
	.quad	-1176452898552450990
	.quad	1940647615375886168
	.quad	2748186495899949531
	.quad	1552518092300708935
	.quad	2198549196719959625
	.quad	1242014473840567148
	.quad	-171670099989974923
	.quad	1987223158144907436
	.quad	-7516033709475800585
	.quad	1589778526515925949
	.quad	-6012826967580640468
	.quad	1271822821212740759
	.quad	8826220925580526867
	.quad	2034916513940385215
	.quad	7060976740464421494
	.quad	1627933211152308172
	.quad	-1729916237112283451
	.quad	1302346568921846537
	.quad	-6457214794121563846
	.quad	2083754510274954460
	.quad	-8855120650039161400
	.quad	1667003608219963568
	.quad	-3394747705289418796
	.quad	1333602886575970854
	.quad	-5431596328463070074
	.quad	2133764618521553367
	.quad	3033420566713364587
	.quad	1707011694817242694
	.quad	6116085268112601993
	.quad	1365609355853794155
	.quad	-8661007644729388428
	.quad	2184974969366070648
	.quad	-3239457301041600419
	.quad	1747979975492856518
	.quad	1097782973908629988
	.quad	1398383980394285215
	.quad	1756452758253807981
	.quad	2237414368630856344
	.quad	5094511021344956708
	.quad	1789931494904685075
	.quad	4075608817075965366
	.quad	1431945195923748060
	.quad	6520974107321544586
	.quad	2291112313477996896
	.quad	1527430471115325346
	.quad	1832889850782397517
	.quad	-6156753252591560370
	.quad	1466311880625918013
	.quad	-1236053787331337972
	.quad	1173049504500734410
	.quad	9090360384495590213
	.quad	1876879207201175057
	.quad	-106409321887348476
	.quad	1501503365760940045
	.quad	-3774476272251789104
	.quad	1201202692608752036
	.quad	-2349813220860952243
	.quad	1921924308174003258
	.quad	1809498238053148529
	.quad	1537539446539202607
	.quad	-5931099039041301823
	.quad	1230031557231362085
	.quad	1578287981759648052
	.quad	1968050491570179337
	.quad	-6116067244076102204
	.quad	1574440393256143469
	.quad	-4892853795260881763
	.quad	1259552314604914775
	.quad	3239480371808320148
	.quad	2015283703367863641
	.quad	-1097764517295254205
	.quad	1612226962694290912
	.quad	6500486015647617283
	.quad	1289781570155432730
	.quad	-8045966448673363964
	.quad	2063650512248692368
	.quad	-2747424344196780848
	.quad	1650920409798953894
	.quad	-2197939475357424678
	.quad	1320736327839163115
	.quad	7551343283653851484
	.quad	2113178124542660985
	.quad	6041074626923081187
	.quad	1690542499634128788
	.quad	-6235186742687266020
	.quad	1352433999707303030
	.quad	1091747655926105338
	.quad	2163894399531684849
	.quad	4562746939482794594
	.quad	1731115519625347879
	.quad	7339546366328145998
	.quad	1384892415700278303
	.quad	8053925371383123274
	.quad	2215827865120445285
	.quad	6443140297106498619
	.quad	1772662292096356228
	.quad	-5913534206540532074
	.quad	1418129833677084982
	.quad	5295740528502789974
	.quad	2269007733883335972
	.quad	-3142105206681588667
	.quad	1815206187106668777
	.quad	4865013464138549713
	.quad	1452164949685335022
	.quad	-3486686858172980876
	.quad	1161731959748268017
	.quad	9178696285890871890
	.quad	1858771135597228828
	.quad	-3725089415513033457
	.quad	1487016908477783062
	.quad	4398626097073393881
	.quad	1189613526782226450
	.quad	7037801755317430209
	.quad	1903381642851562320
	.quad	5630241404253944167
	.quad	1522705314281249856
	.quad	814844308661245011
	.quad	1218164251424999885
	.quad	1303750893857992017
	.quad	1949062802279999816
	.quad	-2646348099655516710
	.quad	1559250241823999852
	.quad	5261619149759407279
	.quad	1247400193459199882
	.quad	-6338804619352589647
	.quad	1995840309534719811
	.quad	5997002748743659252
	.quad	1596672247627775849
	.quad	8486951013736837725
	.quad	1277337798102220679
	.quad	2511075177753209390
	.quad	2043740476963553087
	.quad	-5369837487281253134
	.quad	1634992381570842469
	.quad	-4295869989825002507
	.quad	1307993905256673975
	.quad	4194654460505726958
	.quad	2092790248410678361
	.quad	-333625246337328757
	.quad	1674232198728542688
	.quad	3422448617672047318
	.quad	1339385758982834151
	.quad	-1902779841208544938
	.quad	2143017214372534641
	.quad	-8900921502450656597
	.quad	1714413771498027713
	.quad	-3431388387218614954
	.quad	1371531017198422170
	.quad	5577825024675947042
	.quad	2194449627517475473
	.quad	-6605786424484973336
	.quad	1755559702013980378
	.quad	-1595280324846068345
	.quad	1404447761611184302
	.quad	-6241797334495619676
	.quad	2247116418577894884
	.quad	-4993437867596495741
	.quad	1797693134862315907
	.quad	3383947335406624054
	.quad	1438154507889852726
	.quad	-1964381892833222160
	.quad	2301047212623764361
	.quad	-8950203143750398374
	.quad	1840837770099011489
	.quad	-7160162515000318699
	.quad	1472670216079209191
	.quad	5339916432225476010
	.quad	1178136172863367353
	.quad	4854517476818851293
	.quad	1885017876581387765
	.quad	3883613981455081034
	.quad	1508014301265110212
	.quad	-4271806444319755819
	.quad	1206411441012088169
	.quad	-6834890310911609310
	.quad	1930258305619341071
	.quad	5600134195496443521
	.quad	1544206644495472857
	.quad	-2898590273086665829
	.quad	1235365315596378285
	.quad	6430302007287065643
	.quad	1976584504954205257
	.quad	-2234456023654168132
	.quad	1581267603963364205
	.quad	-5476913633665244829
	.quad	1265014083170691364
	.quad	-8763061813864391727
	.quad	2024022533073106183
	.quad	-3321100636349603058
	.quad	1619218026458484946
	.quad	8411165935146048523
	.quad	1295374421166787957
	.quad	-1299529762733963656
	.quad	2072599073866860731
	.quad	-8418321439670991571
	.quad	1658079259093488585
	.quad	8022738107230848036
	.quad	1326463407274790868
	.quad	9147032156827446534
	.quad	2122341451639665389
	.quad	-7439769533505684065
	.quad	1697873161311732311
	.quad	5116230817421183718
	.quad	1358298529049385849
	.quad	-2882077136351837022
	.quad	2173277646479017358
	.quad	1383687105660440706
	.quad	1738622117183213887
	.quad	-6271747944955468082
	.quad	1390897693746571109
	.quad	8411947361780802685
	.quad	2225436309994513775
	.quad	6729557889424642148
	.quad	1780349047995611020
	.quad	5383646311539713719
	.quad	1424279238396488816
	.quad	1235136468979721303
	.quad	2278846781434382106
	.quad	-2701239639558133281
	.quad	1823077425147505684
	.quad	-2160991711646506624
	.quad	1458461940118004547
	.quad	5649904260166615347
	.quad	1166769552094403638
	.quad	5350498001524674232
	.quad	1866831283351045821
	.quad	591049586477829062
	.quad	1493465026680836657
	.quad	-6905857960301557397
	.quad	1194772021344669325
	.quad	18673707743239135
	.quad	1911635234151470921
	.quad	-3674409848547319015
	.quad	1529308187321176736
	.quad	8128518565387875758
	.quad	1223446549856941389
	.quad	1937583260394870242
	.quad	1957514479771106223
	.quad	8928764237799716840
	.quad	1566011583816884978
	.quad	-3925035053985957497
	.quad	1252809267053507982
	.quad	8477339172590109297
	.quad	2004494827285612772
	.quad	-596826291411733209
	.quad	1603595861828490217
	.quad	6901236596354434079
	.quad	1282876689462792174
	.quad	-26067890058636443
	.quad	2052602703140467478
	.quad	3668494502695001169
	.quad	1642082162512373983
	.quad	-8133250842069730034
	.quad	1313665730009899186
	.quad	9122891541139893884
	.quad	2101865168015838698
	.quad	-3769733211313815862
	.quad	1681492134412670958
	.quad	673562245690857633
	.quad	1345193707530136767
	.globl	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE
	.section	.rdata$_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE,"dr"
	.linkonce same_size
	.align 32
_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE:
	.byte	48
	.byte	48
	.byte	48
	.byte	49
	.byte	48
	.byte	50
	.byte	48
	.byte	51
	.byte	48
	.byte	52
	.byte	48
	.byte	53
	.byte	48
	.byte	54
	.byte	48
	.byte	55
	.byte	48
	.byte	56
	.byte	48
	.byte	57
	.byte	49
	.byte	48
	.byte	49
	.byte	49
	.byte	49
	.byte	50
	.byte	49
	.byte	51
	.byte	49
	.byte	52
	.byte	49
	.byte	53
	.byte	49
	.byte	54
	.byte	49
	.byte	55
	.byte	49
	.byte	56
	.byte	49
	.byte	57
	.byte	50
	.byte	48
	.byte	50
	.byte	49
	.byte	50
	.byte	50
	.byte	50
	.byte	51
	.byte	50
	.byte	52
	.byte	50
	.byte	53
	.byte	50
	.byte	54
	.byte	50
	.byte	55
	.byte	50
	.byte	56
	.byte	50
	.byte	57
	.byte	51
	.byte	48
	.byte	51
	.byte	49
	.byte	51
	.byte	50
	.byte	51
	.byte	51
	.byte	51
	.byte	52
	.byte	51
	.byte	53
	.byte	51
	.byte	54
	.byte	51
	.byte	55
	.byte	51
	.byte	56
	.byte	51
	.byte	57
	.byte	52
	.byte	48
	.byte	52
	.byte	49
	.byte	52
	.byte	50
	.byte	52
	.byte	51
	.byte	52
	.byte	52
	.byte	52
	.byte	53
	.byte	52
	.byte	54
	.byte	52
	.byte	55
	.byte	52
	.byte	56
	.byte	52
	.byte	57
	.byte	53
	.byte	48
	.byte	53
	.byte	49
	.byte	53
	.byte	50
	.byte	53
	.byte	51
	.byte	53
	.byte	52
	.byte	53
	.byte	53
	.byte	53
	.byte	54
	.byte	53
	.byte	55
	.byte	53
	.byte	56
	.byte	53
	.byte	57
	.byte	54
	.byte	48
	.byte	54
	.byte	49
	.byte	54
	.byte	50
	.byte	54
	.byte	51
	.byte	54
	.byte	52
	.byte	54
	.byte	53
	.byte	54
	.byte	54
	.byte	54
	.byte	55
	.byte	54
	.byte	56
	.byte	54
	.byte	57
	.byte	55
	.byte	48
	.byte	55
	.byte	49
	.byte	55
	.byte	50
	.byte	55
	.byte	51
	.byte	55
	.byte	52
	.byte	55
	.byte	53
	.byte	55
	.byte	54
	.byte	55
	.byte	55
	.byte	55
	.byte	56
	.byte	55
	.byte	57
	.byte	56
	.byte	48
	.byte	56
	.byte	49
	.byte	56
	.byte	50
	.byte	56
	.byte	51
	.byte	56
	.byte	52
	.byte	56
	.byte	53
	.byte	56
	.byte	54
	.byte	56
	.byte	55
	.byte	56
	.byte	56
	.byte	56
	.byte	57
	.byte	57
	.byte	48
	.byte	57
	.byte	49
	.byte	57
	.byte	50
	.byte	57
	.byte	51
	.byte	57
	.byte	52
	.byte	57
	.byte	53
	.byte	57
	.byte	54
	.byte	57
	.byte	55
	.byte	57
	.byte	56
	.byte	57
	.byte	57
	.globl	_ZN7fast_io7details3utf14utf_util_tableILb1EE11transitionsE
	.section	.rdata$_ZN7fast_io7details3utf14utf_util_tableILb1EE11transitionsE,"dr"
	.linkonce same_size
	.align 32
_ZN7fast_io7details3utf14utf_util_tableILb1EE11transitionsE:
	.byte	12
	.byte	0
	.byte	12
	.byte	12
	.byte	12
	.byte	24
	.byte	60
	.byte	36
	.byte	72
	.byte	84
	.byte	48
	.byte	96
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	0
	.byte	0
	.byte	0
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	24
	.byte	24
	.byte	24
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	36
	.byte	36
	.byte	36
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	24
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	24
	.byte	24
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	36
	.byte	36
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	36
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.globl	_ZN7fast_io7details3utf14utf_util_tableILb1EE14octet_categoryE
	.section	.rdata$_ZN7fast_io7details3utf14utf_util_tableILb1EE14octet_categoryE,"dr"
	.linkonce same_size
	.align 32
_ZN7fast_io7details3utf14utf_util_tableILb1EE14octet_categoryE:
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	0
	.byte	0
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	6
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	8
	.byte	7
	.byte	7
	.byte	9
	.byte	10
	.byte	10
	.byte	10
	.byte	11
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.globl	_ZN7fast_io7details3utf14utf_util_tableILb1EE15first_unit_infoE
	.section	.rdata$_ZN7fast_io7details3utf14utf_util_tableILb1EE15first_unit_infoE,"dr"
	.linkonce same_size
	.align 32
_ZN7fast_io7details3utf14utf_util_tableILb1EE15first_unit_infoE:
	.byte	0
	.byte	0
	.byte	1
	.byte	0
	.byte	2
	.byte	0
	.byte	3
	.byte	0
	.byte	4
	.byte	0
	.byte	5
	.byte	0
	.byte	6
	.byte	0
	.byte	7
	.byte	0
	.byte	8
	.byte	0
	.byte	9
	.byte	0
	.byte	10
	.byte	0
	.byte	11
	.byte	0
	.byte	12
	.byte	0
	.byte	13
	.byte	0
	.byte	14
	.byte	0
	.byte	15
	.byte	0
	.byte	16
	.byte	0
	.byte	17
	.byte	0
	.byte	18
	.byte	0
	.byte	19
	.byte	0
	.byte	20
	.byte	0
	.byte	21
	.byte	0
	.byte	22
	.byte	0
	.byte	23
	.byte	0
	.byte	24
	.byte	0
	.byte	25
	.byte	0
	.byte	26
	.byte	0
	.byte	27
	.byte	0
	.byte	28
	.byte	0
	.byte	29
	.byte	0
	.byte	30
	.byte	0
	.byte	31
	.byte	0
	.byte	32
	.byte	0
	.byte	33
	.byte	0
	.byte	34
	.byte	0
	.byte	35
	.byte	0
	.byte	36
	.byte	0
	.byte	37
	.byte	0
	.byte	38
	.byte	0
	.byte	39
	.byte	0
	.byte	40
	.byte	0
	.byte	41
	.byte	0
	.byte	42
	.byte	0
	.byte	43
	.byte	0
	.byte	44
	.byte	0
	.byte	45
	.byte	0
	.byte	46
	.byte	0
	.byte	47
	.byte	0
	.byte	48
	.byte	0
	.byte	49
	.byte	0
	.byte	50
	.byte	0
	.byte	51
	.byte	0
	.byte	52
	.byte	0
	.byte	53
	.byte	0
	.byte	54
	.byte	0
	.byte	55
	.byte	0
	.byte	56
	.byte	0
	.byte	57
	.byte	0
	.byte	58
	.byte	0
	.byte	59
	.byte	0
	.byte	60
	.byte	0
	.byte	61
	.byte	0
	.byte	62
	.byte	0
	.byte	63
	.byte	0
	.byte	64
	.byte	0
	.byte	65
	.byte	0
	.byte	66
	.byte	0
	.byte	67
	.byte	0
	.byte	68
	.byte	0
	.byte	69
	.byte	0
	.byte	70
	.byte	0
	.byte	71
	.byte	0
	.byte	72
	.byte	0
	.byte	73
	.byte	0
	.byte	74
	.byte	0
	.byte	75
	.byte	0
	.byte	76
	.byte	0
	.byte	77
	.byte	0
	.byte	78
	.byte	0
	.byte	79
	.byte	0
	.byte	80
	.byte	0
	.byte	81
	.byte	0
	.byte	82
	.byte	0
	.byte	83
	.byte	0
	.byte	84
	.byte	0
	.byte	85
	.byte	0
	.byte	86
	.byte	0
	.byte	87
	.byte	0
	.byte	88
	.byte	0
	.byte	89
	.byte	0
	.byte	90
	.byte	0
	.byte	91
	.byte	0
	.byte	92
	.byte	0
	.byte	93
	.byte	0
	.byte	94
	.byte	0
	.byte	95
	.byte	0
	.byte	96
	.byte	0
	.byte	97
	.byte	0
	.byte	98
	.byte	0
	.byte	99
	.byte	0
	.byte	100
	.byte	0
	.byte	101
	.byte	0
	.byte	102
	.byte	0
	.byte	103
	.byte	0
	.byte	104
	.byte	0
	.byte	105
	.byte	0
	.byte	106
	.byte	0
	.byte	107
	.byte	0
	.byte	108
	.byte	0
	.byte	109
	.byte	0
	.byte	110
	.byte	0
	.byte	111
	.byte	0
	.byte	112
	.byte	0
	.byte	113
	.byte	0
	.byte	114
	.byte	0
	.byte	115
	.byte	0
	.byte	116
	.byte	0
	.byte	117
	.byte	0
	.byte	118
	.byte	0
	.byte	119
	.byte	0
	.byte	120
	.byte	0
	.byte	121
	.byte	0
	.byte	122
	.byte	0
	.byte	123
	.byte	0
	.byte	124
	.byte	0
	.byte	125
	.byte	0
	.byte	126
	.byte	0
	.byte	127
	.byte	0
	.byte	0
	.byte	12
	.byte	1
	.byte	12
	.byte	2
	.byte	12
	.byte	3
	.byte	12
	.byte	4
	.byte	12
	.byte	5
	.byte	12
	.byte	6
	.byte	12
	.byte	7
	.byte	12
	.byte	8
	.byte	12
	.byte	9
	.byte	12
	.byte	10
	.byte	12
	.byte	11
	.byte	12
	.byte	12
	.byte	12
	.byte	13
	.byte	12
	.byte	14
	.byte	12
	.byte	15
	.byte	12
	.byte	16
	.byte	12
	.byte	17
	.byte	12
	.byte	18
	.byte	12
	.byte	19
	.byte	12
	.byte	20
	.byte	12
	.byte	21
	.byte	12
	.byte	22
	.byte	12
	.byte	23
	.byte	12
	.byte	24
	.byte	12
	.byte	25
	.byte	12
	.byte	26
	.byte	12
	.byte	27
	.byte	12
	.byte	28
	.byte	12
	.byte	29
	.byte	12
	.byte	30
	.byte	12
	.byte	31
	.byte	12
	.byte	32
	.byte	12
	.byte	33
	.byte	12
	.byte	34
	.byte	12
	.byte	35
	.byte	12
	.byte	36
	.byte	12
	.byte	37
	.byte	12
	.byte	38
	.byte	12
	.byte	39
	.byte	12
	.byte	40
	.byte	12
	.byte	41
	.byte	12
	.byte	42
	.byte	12
	.byte	43
	.byte	12
	.byte	44
	.byte	12
	.byte	45
	.byte	12
	.byte	46
	.byte	12
	.byte	47
	.byte	12
	.byte	48
	.byte	12
	.byte	49
	.byte	12
	.byte	50
	.byte	12
	.byte	51
	.byte	12
	.byte	52
	.byte	12
	.byte	53
	.byte	12
	.byte	54
	.byte	12
	.byte	55
	.byte	12
	.byte	56
	.byte	12
	.byte	57
	.byte	12
	.byte	58
	.byte	12
	.byte	59
	.byte	12
	.byte	60
	.byte	12
	.byte	61
	.byte	12
	.byte	62
	.byte	12
	.byte	63
	.byte	12
	.byte	-64
	.byte	12
	.byte	-63
	.byte	12
	.byte	2
	.byte	24
	.byte	3
	.byte	24
	.byte	4
	.byte	24
	.byte	5
	.byte	24
	.byte	6
	.byte	24
	.byte	7
	.byte	24
	.byte	8
	.byte	24
	.byte	9
	.byte	24
	.byte	10
	.byte	24
	.byte	11
	.byte	24
	.byte	12
	.byte	24
	.byte	13
	.byte	24
	.byte	14
	.byte	24
	.byte	15
	.byte	24
	.byte	16
	.byte	24
	.byte	17
	.byte	24
	.byte	18
	.byte	24
	.byte	19
	.byte	24
	.byte	20
	.byte	24
	.byte	21
	.byte	24
	.byte	22
	.byte	24
	.byte	23
	.byte	24
	.byte	24
	.byte	24
	.byte	25
	.byte	24
	.byte	26
	.byte	24
	.byte	27
	.byte	24
	.byte	28
	.byte	24
	.byte	29
	.byte	24
	.byte	30
	.byte	24
	.byte	31
	.byte	24
	.byte	0
	.byte	60
	.byte	1
	.byte	36
	.byte	2
	.byte	36
	.byte	3
	.byte	36
	.byte	4
	.byte	36
	.byte	5
	.byte	36
	.byte	6
	.byte	36
	.byte	7
	.byte	36
	.byte	8
	.byte	36
	.byte	9
	.byte	36
	.byte	10
	.byte	36
	.byte	11
	.byte	36
	.byte	12
	.byte	36
	.byte	13
	.byte	72
	.byte	14
	.byte	36
	.byte	15
	.byte	36
	.byte	0
	.byte	84
	.byte	1
	.byte	48
	.byte	2
	.byte	48
	.byte	3
	.byte	48
	.byte	4
	.byte	96
	.byte	-11
	.byte	12
	.byte	-10
	.byte	12
	.byte	-9
	.byte	12
	.byte	-8
	.byte	12
	.byte	-7
	.byte	12
	.byte	-6
	.byte	12
	.byte	-5
	.byte	12
	.byte	-4
	.byte	12
	.byte	-3
	.byte	12
	.byte	-2
	.byte	12
	.byte	-1
	.byte	12
	.section .rdata,"dr"
	.align 8
.LC0:
	.long	-400107883
	.long	1041313291
	.align 16
.LC7:
	.byte	111
	.byte	98
	.byte	117
	.byte	102
	.byte	95
	.byte	102
	.byte	105
	.byte	108
	.byte	101
	.byte	95
	.byte	108
	.byte	99
	.byte	118
	.byte	95
	.byte	67
	.byte	46
	.ident	"GCC: (GCC with MCF thread model, built by LH_Mouse.) 11.0.0 20200712 (experimental)"
	.def	memset;	.scl	2;	.type	32;	.endef
	.def	memmove;	.scl	2;	.type	32;	.endef
	.def	memcpy;	.scl	2;	.type	32;	.endef
	.def	WriteFile;	.scl	2;	.type	32;	.endef
	.def	_ZdlPvy;	.scl	2;	.type	32;	.endef
	.def	_Znwy;	.scl	2;	.type	32;	.endef
	.def	_ZSt17__throw_bad_allocv;	.scl	2;	.type	32;	.endef
	.def	_ZNSt6chrono3_V212system_clock3nowEv;	.scl	2;	.type	32;	.endef
	.def	GetStdHandle;	.scl	2;	.type	32;	.endef
	.def	strlen;	.scl	2;	.type	32;	.endef
	.def	memmove;	.scl	2;	.type	32;	.endef
	.def	GetLastError;	.scl	2;	.type	32;	.endef
	.def	CreateFileW;	.scl	2;	.type	32;	.endef
	.def	_ZdlPvySt11align_val_t;	.scl	2;	.type	32;	.endef
	.def	CloseHandle;	.scl	2;	.type	32;	.endef
	.def	memset;	.scl	2;	.type	32;	.endef
	.def	ReadFile;	.scl	2;	.type	32;	.endef
	.def	_ZnwySt11align_val_t;	.scl	2;	.type	32;	.endef
	.def	_ZSt20__throw_length_errorPKc;	.scl	2;	.type	32;	.endef
