#include"../../timer.h"
#include"../../../include/fast_io.h"
#include"../../../include/fast_io_device.h"
#include"../../../include/fast_io_legacy.h"

int main()
{
	constexpr std::size_t N(10000000);
	{
	fast_io::timer t("output");
	fast_io::obuf_file obf("iobuf_file_w.txt");
	for(std::size_t i{};i!=N;++i)
		write(obf,std::addressof(i),std::addressof(i)+1);
	}
}