#include<fmt/format.h>
#include<chrono>
#include<windows.h>
#include"../../timer.h"

int main()
{
fast_io::timer t("fmt");
using namespace std::chrono;


fmt::memory_buffer buf;
buf.reserve(1000000000);

for (std::uint_fast32_t i = 0; i < 10000000; ++i)
{
    fmt::format_to(buf, "{}\n", i);
}

auto begin = high_resolution_clock::now();

HANDLE hfile = CreateFileW(L"output.txt", GENERIC_WRITE, 0, nullptr, CREATE_ALWAYS, 0, nullptr);
WriteFile(hfile, nums, sizeof(nums), nullptr, nullptr);
CloseHandle(hfile);

auto end = high_resolution_clock::now();

printf("%lld", duration_cast<milliseconds>(end - begin).count());
}