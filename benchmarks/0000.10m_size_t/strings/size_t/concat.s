	.file	"concat.cc"
	.text
	.section	.text$_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy,"x"
	.linkonce discard
	.align 2
	.globl	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy
	.def	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy
_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy:
.LFB16438:
	.seh_endprologue
	movq	8(%rcx), %rcx
	movq	4104(%rcx), %rax
	addq	%rax, %rdx
	cmpq	%rdx, 4112(%rcx)
	movl	$0, %edx
	cmovbe	%rdx, %rax
	ret
	.seh_endproc
	.section	.text$_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc,"x"
	.linkonce discard
	.align 2
	.globl	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc
	.def	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc
_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc:
.LFB16440:
	.seh_endprologue
	movq	8(%rcx), %rax
	movq	%rdx, 4104(%rax)
	ret
	.seh_endproc
	.section	.text$_ZN7fast_io11posix_errorD0Ev,"x"
	.linkonce discard
	.align 2
	.globl	_ZN7fast_io11posix_errorD0Ev
	.def	_ZN7fast_io11posix_errorD0Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io11posix_errorD0Ev
_ZN7fast_io11posix_errorD0Ev:
.LFB16398:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	%rcx, %r12
	leaq	16+_ZTVN7fast_io13fast_io_errorE(%rip), %rax
	movq	%rax, (%rcx)
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L6
	call	_ZdaPv
.L6:
	movq	%r12, %rcx
	call	_ZNSt9exceptionD2Ev
	movl	$24, %edx
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZdlPvy
	.seh_endproc
	.section	.text$_ZN7fast_io11win32_errorD1Ev,"x"
	.linkonce discard
	.align 2
	.globl	_ZN7fast_io11win32_errorD1Ev
	.def	_ZN7fast_io11win32_errorD1Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io11win32_errorD1Ev
_ZN7fast_io11win32_errorD1Ev:
.LFB16393:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	%rcx, %r12
	leaq	16+_ZTVN7fast_io13fast_io_errorE(%rip), %rax
	movq	%rax, (%rcx)
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L11
	call	_ZdaPv
.L11:
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZNSt9exceptionD2Ev
	.seh_endproc
	.section	.text$_ZN7fast_io11posix_errorD1Ev,"x"
	.linkonce discard
	.align 2
	.globl	_ZN7fast_io11posix_errorD1Ev
	.def	_ZN7fast_io11posix_errorD1Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io11posix_errorD1Ev
_ZN7fast_io11posix_errorD1Ev:
.LFB16397:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	%rcx, %r12
	leaq	16+_ZTVN7fast_io13fast_io_errorE(%rip), %rax
	movq	%rax, (%rcx)
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L16
	call	_ZdaPv
.L16:
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZNSt9exceptionD2Ev
	.seh_endproc
	.section	.text$_ZN7fast_io11win32_errorD0Ev,"x"
	.linkonce discard
	.align 2
	.globl	_ZN7fast_io11win32_errorD0Ev
	.def	_ZN7fast_io11win32_errorD0Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io11win32_errorD0Ev
_ZN7fast_io11win32_errorD0Ev:
.LFB16394:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	%rcx, %r12
	leaq	16+_ZTVN7fast_io13fast_io_errorE(%rip), %rax
	movq	%rax, (%rcx)
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L21
	call	_ZdaPv
.L21:
	movq	%r12, %rcx
	call	_ZNSt9exceptionD2Ev
	movl	$24, %edx
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZdlPvy
	.seh_endproc
	.section	.text$_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_,"x"
	.linkonce discard
	.align 2
	.globl	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_
	.def	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_
_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_:
.LFB16437:
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	%rdx, %r13
	movq	%r8, %r12
	movq	8(%rcx), %rsi
	movq	%r8, %rbx
	subq	%rdx, %rbx
	movq	4112(%rsi), %rax
	movq	4104(%rsi), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %rbx
	ja	.L34
	testq	%rbx, %rbx
	jne	.L35
.L30:
	addq	%rcx, %rbx
.L29:
	movq	%rbx, 4104(%rsi)
	movq	%r12, %rax
	addq	$32, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.L35:
	movq	%rbx, %r8
	movq	%r13, %rdx
	call	memcpy
	movq	%rax, %rcx
	jmp	.L30
.L34:
	subq	4096(%rsi), %rax
	leaq	(%rax,%rax), %rdi
	cmpq	%rbx, %rdi
	cmovb	%rbx, %rdi
	testq	%rdi, %rdi
	js	.L36
	movq	%rdi, %rcx
	call	_Znwy
	movq	%rax, %rbp
	movq	4096(%rsi), %rdx
	movq	4104(%rsi), %r8
	subq	%rdx, %r8
	movq	%rax, %rcx
	call	memcpy
	movq	4096(%rsi), %rcx
	movq	4104(%rsi), %r14
	subq	%rcx, %r14
	cmpq	%rsi, %rcx
	je	.L28
	movq	4112(%rsi), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
.L28:
	movq	%rbp, 4096(%rsi)
	leaq	0(%rbp,%r14), %rcx
	movq	%rcx, 4104(%rsi)
	addq	%rdi, %rbp
	movq	%rbp, 4112(%rsi)
	movq	%rbx, %r8
	movq	%r13, %rdx
	call	memcpy
	addq	4104(%rsi), %rbx
	jmp	.L29
.L36:
	call	_ZSt17__throw_bad_allocv
	nop
	.seh_endproc
	.section .rdata,"dr"
.LC0:
	.ascii "unknown fast_io_error\0"
	.section	.text$_ZNK7fast_io13fast_io_error4whatEv,"x"
	.linkonce discard
	.align 2
	.globl	_ZNK7fast_io13fast_io_error4whatEv
	.def	_ZNK7fast_io13fast_io_error4whatEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK7fast_io13fast_io_error4whatEv
_ZNK7fast_io13fast_io_error4whatEv:
.LFB5324:
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	movl	$4184, %eax
	call	___chkstk_ms
	subq	%rax, %rsp
	.seh_stackalloc	4184
	.seh_endprologue
	movq	%rcx, %rsi
	leaq	48(%rsp), %rdi
	movq	%rdi, 4144(%rsp)
	movq	%rdi, 4152(%rsp)
	leaq	4144(%rsp), %rax
	movq	%rax, 4160(%rsp)
	leaq	16+_ZTVN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE(%rip), %rax
	movq	%rax, 32(%rsp)
	movq	%rdi, 40(%rsp)
	leaq	32(%rsp), %rdx
	movq	(%rcx), %rax
.LEHB0:
	call	*24(%rax)
	movq	4152(%rsp), %rcx
	cmpq	4160(%rsp), %rcx
	je	.L59
	movb	$0, (%rcx)
	addq	$1, %rcx
	movq	%rcx, 4152(%rsp)
	movq	4144(%rsp), %r12
	cmpq	%rdi, %r12
	je	.L60
.L41:
	movq	8(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L44
	call	_ZdaPv
.L44:
	movq	%r12, 8(%rsi)
.L37:
	movq	%r12, %rax
	addq	$4184, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.L60:
	subq	%rdi, %rcx
	call	_Znay
	movq	%rax, %r12
	movq	8(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L42
	call	_ZdaPv
.L42:
	movq	%r12, 8(%rsi)
	movq	4144(%rsp), %r13
	movq	4152(%rsp), %r8
	subq	%r13, %r8
	jne	.L61
.L43:
	cmpq	%rdi, %r13
	je	.L37
	movq	4160(%rsp), %rdx
	subq	%r13, %rdx
	movq	%r13, %rcx
	call	_ZdlPvy
	jmp	.L37
.L59:
	subq	4144(%rsp), %rcx
	movq	%rcx, %rbx
	addq	%rbx, %rbx
	js	.L62
	movq	%rbx, %rcx
	call	_Znwy
	movq	%rax, %r12
	movq	4144(%rsp), %r14
	movq	4152(%rsp), %r13
	subq	%r14, %r13
	movq	%r13, %r8
	movq	%r14, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%rdi, %r14
	je	.L40
	movq	4160(%rsp), %rdx
	subq	%r14, %rdx
	movq	%r14, %rcx
	call	_ZdlPvy
.L40:
	movq	%r12, 4144(%rsp)
	addq	%r12, %r13
	addq	%r12, %rbx
	movq	%rbx, 4160(%rsp)
	movb	$0, 0(%r13)
	addq	$1, %r13
	movq	%r13, 4152(%rsp)
	jmp	.L41
.L61:
	movq	%r13, %rdx
	movq	%r12, %rcx
	call	memmove
	jmp	.L43
.L62:
	call	_ZSt17__throw_bad_allocv
.LEHE0:
.L49:
	movq	%rax, %r12
	movq	4144(%rsp), %rcx
	cmpq	%rdi, %rcx
	je	.L47
	movq	4160(%rsp), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
.L47:
	movq	%r12, %rcx
	call	__cxa_begin_catch
	call	__cxa_end_catch
	leaq	.LC0(%rip), %r12
	jmp	.L37
	.def	__gxx_personality_seh0;	.scl	2;	.type	32;	.endef
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
	.align 4
.LLSDA5324:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT5324-.LLSDATTD5324
.LLSDATTD5324:
	.byte	0x1
	.uleb128 .LLSDACSE5324-.LLSDACSB5324
.LLSDACSB5324:
	.uleb128 .LEHB0-.LFB5324
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L49-.LFB5324
	.uleb128 0x3
.LLSDACSE5324:
	.byte	0x1
	.byte	0
	.byte	0
	.byte	0x7d
	.align 4
	.long	0

.LLSDATT5324:
	.section	.text$_ZNK7fast_io13fast_io_error4whatEv,"x"
	.linkonce discard
	.seh_endproc
	.text
	.def	_ZN7fast_io7details3ryu15output_shortestILb0ELy0ELb1ELb0EDuLDu46EPcdEET5_NS0_27compile_time_floating_valueIXT2_ET3_XT4_EEES4_T6_.isra.0;	.scl	3;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details3ryu15output_shortestILb0ELy0ELb1ELb0EDuLDu46EPcdEET5_NS0_27compile_time_floating_valueIXT2_ET3_XT4_EEES4_T6_.isra.0
_ZN7fast_io7details3ryu15output_shortestILb0ELy0ELb1ELb0EDuLDu46EPcdEET5_NS0_27compile_time_floating_valueIXT2_ET3_XT4_EEES4_T6_.isra.0:
.LFB16459:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$72, %rsp
	.seh_stackalloc	72
	.seh_endprologue
	movq	%rcx, %r15
	movsd	%xmm1, 40(%rsp)
	movq	40(%rsp), %r12
	movabsq	$4503599627370495, %rax
	andq	%r12, %rax
	movq	%r12, %r10
	shrq	$52, %r10
	movl	%r10d, %r9d
	andl	$2047, %r9d
	cmpl	$2047, %r9d
	je	.L389
	testl	%r9d, %r9d
	je	.L390
	movabsq	$4503599627370496, %r8
	orq	%rax, %r8
	leal	-1023(%r9), %edx
	cmpl	$52, %edx
	ja	.L71
	movl	$1075, %ecx
	subl	%r9d, %ecx
	movq	$-1, %rdx
	salq	%cl, %rdx
	notq	%rdx
	testq	%r8, %rdx
	jne	.L71
	shrq	%cl, %r8
	testq	%r12, %r12
	jns	.L72
	movb	$45, (%r15)
	addq	$1, %r15
.L72:
	movabsq	$-3276141747490816367, %rax
	imulq	%r8, %rax
	rorq	$4, %rax
	movabsq	$1844674407370955, %rdx
	cmpq	%rdx, %rax
	jbe	.L73
	movabsq	$999999999999999, %rax
	cmpq	%rax, %r8
	ja	.L74
	movabsq	$99999999999999, %rax
	cmpq	%rax, %r8
	ja	.L209
	movabsq	$9999999999999, %rax
	cmpq	%rax, %r8
	ja	.L210
	movabsq	$999999999999, %rax
	cmpq	%rax, %r8
	ja	.L211
	movabsq	$99999999999, %rax
	cmpq	%rax, %r8
	ja	.L212
	movabsq	$9999999999, %rax
	cmpq	%rax, %r8
	ja	.L213
	cmpq	$999999999, %r8
	ja	.L214
	cmpq	$99999999, %r8
	ja	.L215
	cmpq	$9999999, %r8
	ja	.L216
	cmpq	$999999, %r8
	ja	.L217
	cmpq	$99999, %r8
	ja	.L218
	cmpq	$9999, %r8
	ja	.L219
	cmpq	$999, %r8
	ja	.L220
	cmpq	$99, %r8
	ja	.L76
	cmpq	$10, %r8
	sbbq	%rax, %rax
	leaq	2(%r15,%rax), %r9
	movq	%r9, %r11
.L98:
	cmpq	$9, %r8
	jbe	.L100
.L397:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%r8,2), %eax
	movw	%ax, -2(%r9)
	jmp	.L63
.L390:
	testq	%rax, %rax
	je	.L391
	movq	%r12, %rdi
	andl	$1, %edi
	movq	%rdi, 40(%rsp)
	leaq	0(,%rax,4), %r8
	leaq	2(%r8), %r14
	leaq	-2(%r8), %r10
	movl	$57, %ecx
	movl	$5200, %eax
	movl	$-325, %ebp
	movl	$751, %ebx
	movl	$1, %r11d
.L200:
	leaq	_ZN7fast_io7details3ryu4pow5IdLb1EE5splitE(%rip), %rdx
	addq	%rdx, %rax
	movq	8(%rax), %r9
	movq	(%rax), %r13
	movq	%r13, %rax
	mulq	%r14
	movq	%rdx, %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rdx, %rdi
	movq	%r14, %rax
	movq	%r9, %r14
	mulq	%r9
	addq	%rax, %rsi
	adcq	%rdx, %rdi
	shrdq	%cl, %rdi, %rsi
	shrq	%cl, %rdi
	testb	$64, %cl
	cmovne	%rdi, %rsi
	movq	%rsi, %r9
	movq	%r13, %rax
	mulq	%r10
	movq	%rdx, %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rdx, %rdi
	movq	%r10, %rax
	mulq	%r14
	addq	%rax, %rsi
	adcq	%rdx, %rdi
	shrdq	%cl, %rdi, %rsi
	shrq	%cl, %rdi
	testb	$64, %cl
	cmovne	%rdi, %rsi
	movq	%rsi, %r10
	movq	%r13, %rax
	mulq	%r8
	movq	%rdx, %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rdx, %rdi
	movq	%r14, %rax
	mulq	%r8
	addq	%rax, %rsi
	adcq	%rdx, %rdi
	shrdq	%cl, %rdi, %rsi
	shrq	%cl, %rdi
	testb	$64, %cl
	cmovne	%rdi, %rsi
	movq	%rsi, %rdi
	cmpl	$1, %ebx
	jbe	.L392
	cmpl	$62, %ebx
	ja	.L103
	movq	$-1, %rax
	movl	%ebx, %ecx
	salq	%cl, %rax
	notq	%rax
	testq	%r8, %rax
	sete	%al
.L107:
	testb	%al, %al
	je	.L103
	movabsq	$-3689348814741910323, %rdx
	movq	%r10, %rax
	mulq	%rdx
	shrq	$3, %rdx
	movq	%rdx, %r8
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	movl	%r10d, %r13d
	subl	%eax, %r13d
.L116:
	movabsq	$-3689348814741910323, %rdx
	movq	%r9, %rax
	mulq	%rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	cmpq	%rcx, %r8
	jnb	.L347
	movl	$1, %r9d
	xorl	%r11d, %r11d
.L196:
	xorl	%esi, %esi
	xorl	%ebx, %ebx
	movabsq	$-3689348814741910323, %r14
.L118:
	movq	%rdi, %r10
	movq	%rdi, %rax
	mulq	%r14
	movq	%rdx, %rdi
	shrq	$3, %rdi
	movl	%ebx, %edx
	leaq	(%rdi,%rdi,4), %rax
	addq	%rax, %rax
	movl	%r10d, %ebx
	subl	%eax, %ebx
	testb	%r13b, %r13b
	sete	%al
	andl	%eax, %r11d
	testb	%dl, %dl
	sete	%al
	andl	%eax, %r9d
	addl	$1, %esi
	movq	%rcx, %rax
	mulq	%r14
	movq	%rdx, %rcx
	shrq	$3, %rcx
	movq	%r8, %r10
	movq	%r8, %rax
	mulq	%r14
	shrq	$3, %rdx
	movq	%rdx, %r8
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	movq	%r10, %rdx
	subq	%rax, %rdx
	movl	%edx, %r13d
	cmpq	%r8, %rcx
	ja	.L118
	movq	%rdx, %r14
.L117:
	testb	%r11b, %r11b
	je	.L119
.L197:
	movabsq	$-3689348814741910323, %rcx
	movabsq	$1844674407370955161, %r11
	testq	%r14, %r14
	jne	.L386
.L120:
	movq	%rdi, %r10
	movq	%rdi, %rax
	mulq	%rcx
	movq	%rdx, %rdi
	shrq	$3, %rdi
	testb	%bl, %bl
	sete	%al
	andl	%eax, %r9d
	leaq	(%rdi,%rdi,4), %rax
	addq	%rax, %rax
	movl	%r10d, %ebx
	subl	%eax, %ebx
	addl	$1, %esi
	movq	%r8, %r10
	movq	%r8, %rax
	mulq	%rcx
	shrq	$3, %rdx
	movq	%rdx, %r8
	leaq	(%r10,%r10,2), %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$8, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$16, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$32, %rdx
	addq	%rdx, %rax
	leaq	(%r10,%rax,4), %rax
	rorq	%rax
	cmpq	%r11, %rax
	jbe	.L120
.L386:
	addl	%esi, %ebp
	movl	$1, %r11d
.L121:
	testb	%r9b, %r9b
	je	.L122
	cmpb	$5, %bl
	jne	.L122
	movq	%rdi, %rbx
	andl	$1, %ebx
	addl	$4, %ebx
.L122:
	cmpq	%rdi, %r10
	je	.L393
.L123:
	xorl	%eax, %eax
	cmpb	$4, %bl
	seta	%al
	movq	%rax, 40(%rsp)
.L124:
	movq	40(%rsp), %rbx
	addq	%rdi, %rbx
.L125:
	testq	%r12, %r12
	jns	.L130
	movb	$45, (%r15)
	addq	$1, %r15
.L130:
	movabsq	$9999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L261
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L262
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L263
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L264
	movabsq	$999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L265
	movabsq	$99999999999, %rax
	cmpq	%rax, %rbx
	ja	.L266
	movabsq	$9999999999, %rax
	cmpq	%rax, %rbx
	ja	.L267
	cmpq	$999999999, %rbx
	ja	.L268
	cmpq	$99999999, %rbx
	ja	.L269
	cmpq	$9999999, %rbx
	ja	.L270
	cmpq	$999999, %rbx
	ja	.L271
	cmpq	$99999, %rbx
	ja	.L272
	cmpq	$9999, %rbx
	ja	.L273
	cmpq	$999, %rbx
	ja	.L274
	cmpq	$99, %rbx
	ja	.L275
	cmpq	$9, %rbx
	ja	.L394
	leal	1(%rbp), %edi
	testl	%ebp, %ebp
	jle	.L395
	cmpl	$4, %edi
	ja	.L346
	movl	$1, %esi
	movl	$1, %eax
.L157:
	leaq	(%r15,%rax), %r8
.L158:
	cmpq	$9, %rbx
	jbe	.L160
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rbx,2), %eax
	movw	%ax, -2(%r8)
.L161:
	leaq	(%r15,%rsi), %r11
	movslq	%ebp, %rbp
	movq	%rbp, %r8
	movl	$48, %edx
	movq	%r11, %rcx
	call	memset
	movq	%rax, %r11
	addq	%rbp, %r11
	jmp	.L63
.L389:
	testq	%rax, %rax
	jne	.L396
	testq	%r12, %r12
	jns	.L67
	movl	$1718511917, (%rcx)
	leaq	4(%rcx), %r11
.L63:
	movq	%r11, %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L396:
	movw	$24942, (%rcx)
	movb	$110, 2(%rcx)
	leaq	3(%rcx), %r11
	jmp	.L63
.L391:
	leaq	1(%rcx), %r11
	testq	%r12, %r12
	jns	.L70
	movb	$45, (%rcx)
	leaq	2(%rcx), %rax
	movq	%r11, %r15
	movq	%rax, %r11
.L70:
	movb	$48, (%r15)
	jmp	.L63
.L74:
	leaq	16(%r15), %r11
.L207:
	movq	%r11, %r9
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %rbx
.L80:
	movq	%r8, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%rbx
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %rcx
	salq	$2, %rcx
	movq	%r8, %rdx
	subq	%rcx, %rdx
	movq	%r8, %rcx
	movq	%rax, %r8
	subq	$2, %r9
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r9)
	cmpq	$9999, %rcx
	ja	.L80
	cmpq	$9, %r8
	ja	.L397
.L100:
	addl	$48, %r8d
	movb	%r8b, -1(%r9)
	jmp	.L63
.L67:
	movw	$28265, (%rcx)
	movb	$102, 2(%rcx)
	leaq	3(%rcx), %r11
	jmp	.L63
.L71:
	leal	-1077(%r9), %edx
	movq	%r12, %rdi
	andl	$1, %edi
	movq	%rdi, 40(%rsp)
	salq	$2, %r8
	testq	%rax, %rax
	jne	.L252
	testl	$2046, %r10d
	sete	%r11b
	sete	%al
	movzbl	%al, %eax
.L101:
	leaq	2(%r8), %r13
	movq	%r13, %r14
	leaq	-1(%r8), %rcx
	subq	%rax, %rcx
	movq	%rcx, 56(%rsp)
	movq	%rcx, %r10
	testl	%edx, %edx
	js	.L102
	movslq	%edx, %rsi
	movabsq	$169464822037455, %rax
	imulq	%rax, %rsi
	shrq	$49, %rsi
	xorl	%eax, %eax
	cmpl	$3, %edx
	setg	%al
	subl	%eax, %esi
	movl	%esi, %ebp
	movl	%esi, %eax
	salq	$4, %rax
	leaq	_ZN7fast_io7details3ryu4pow5IdLb1EE9inv_splitE(%rip), %rcx
	addq	%rcx, %rax
	movq	8(%rax), %r14
	movq	(%rax), %rdi
	leal	(%rsi,%rsi,8), %eax
	movl	%eax, %ecx
	sall	$5, %ecx
	addl	%ecx, %eax
	leal	(%rsi,%rax,8), %eax
	leal	(%rsi,%rax,2), %eax
	leal	(%rsi,%rax,4), %eax
	leal	(%rsi,%rax,4), %eax
	sall	$4, %eax
	subl	%esi, %eax
	shrl	$19, %eax
	movl	%esi, %ecx
	subl	%edx, %ecx
	leal	61(%rax,%rcx), %r11d
	movq	%r13, %rax
	mulq	%rdi
	movq	%rdx, %rax
	xorl	%edx, %edx
	movq	%rax, %rcx
	movq	%rdx, %rbx
	movq	%r13, %rax
	mulq	%r14
	addq	%rcx, %rax
	adcq	%rbx, %rdx
	movl	%r11d, 52(%rsp)
	movl	%r11d, %ecx
	shrdq	%cl, %rdx, %rax
	shrq	%cl, %rdx
	testb	$64, %r11b
	cmovne	%rdx, %rax
	movq	%rax, %r9
	movq	%r10, %rbx
	movq	%r10, %rax
	mulq	%rdi
	movq	%rdx, %r10
	xorl	%r11d, %r11d
	movq	%rbx, %rax
	mulq	%r14
	addq	%rax, %r10
	adcq	%rdx, %r11
	movzbl	52(%rsp), %ecx
	shrdq	%cl, %r11, %r10
	shrq	%cl, %r11
	testb	$64, %cl
	cmovne	%r11, %r10
	movq	%rdi, %rax
	mulq	%r8
	movq	%rdx, %rax
	xorl	%edx, %edx
	movq	%rax, %rcx
	movq	%rdx, %rbx
	movq	%r14, %rax
	mulq	%r8
	addq	%rcx, %rax
	adcq	%rbx, %rdx
	movzbl	52(%rsp), %ecx
	shrdq	%cl, %rdx, %rax
	shrq	%cl, %rdx
	testb	$64, %cl
	cmovne	%rdx, %rax
	movq	%rax, %rdi
	cmpl	$21, %esi
	jbe	.L398
.L103:
	movq	%r9, %rdx
	shrq	$2, %rdx
	movabsq	$2951479051793528259, %r11
	movq	%rdx, %rax
	mulq	%r11
	movq	%rdx, %rcx
	shrq	$2, %rcx
	movq	%r10, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%r11
	shrq	$2, %rdx
	movq	%rdx, %r8
	cmpq	%rdx, %rcx
	jbe	.L260
	movq	%rdi, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%r11
	movq	%rdx, %r11
	shrq	$2, %r11
	andq	$-4, %rdx
	leaq	(%rdx,%r11), %rax
	leaq	(%rax,%rax,4), %rax
	salq	$2, %rax
	subq	%rax, %rdi
	cmpq	$49, %rdi
	seta	%bl
	movq	%r8, %r10
	movq	%rcx, %r9
	movq	%r11, %rdi
	movl	$2, %r11d
.L126:
	movabsq	$-3689348814741910323, %r8
	movq	%r9, %rax
	mulq	%r8
	movq	%rdx, %r9
	shrq	$3, %r9
	movq	%r10, %rax
	mulq	%r8
	movq	%rdx, %rcx
	shrq	$3, %rcx
	cmpq	%r9, %rcx
	jnb	.L127
.L128:
	movq	%rdi, %rbx
	movq	%rdi, %rax
	mulq	%r8
	movq	%rdx, %rdi
	shrq	$3, %rdi
	addl	$1, %r11d
	movq	%r9, %rax
	mulq	%r8
	movq	%rdx, %r9
	shrq	$3, %r9
	movq	%rcx, %r10
	movq	%rcx, %rax
	mulq	%r8
	movq	%rdx, %rcx
	shrq	$3, %rcx
	cmpq	%rcx, %r9
	ja	.L128
	leaq	(%rdi,%rdi,4), %rax
	addq	%rax, %rax
	subq	%rax, %rbx
	cmpq	$4, %rbx
	seta	%bl
.L127:
	cmpq	%r10, %rdi
	movl	$1, %eax
	cmove	%eax, %ebx
	movzbl	%bl, %ebx
	addq	%rdi, %rbx
	addl	%r11d, %ebp
	jmp	.L125
.L209:
	movl	$15, %r11d
.L75:
	addq	%r15, %r11
	cmpq	$99, %r8
	ja	.L207
	movq	%r11, %r9
	jmp	.L98
.L73:
	movabsq	$3777893186295716171, %rdx
	movq	%r8, %rax
	mulq	%rdx
	movq	%rdx, %rcx
	shrq	$11, %rcx
	movabsq	$-3689348814741910323, %r10
.L82:
	movq	%rcx, %rax
	mulq	%r10
	shrq	$3, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %r9
	addq	%r9, %r9
	movq	%rcx, %rdx
	subq	%r9, %rdx
	movq	%rcx, %r9
	movq	%rax, %rcx
	je	.L82
	movabsq	$999999999999999, %rax
	cmpq	%rax, %r8
	ja	.L222
	movabsq	$99999999999999, %rax
	cmpq	%rax, %r8
	ja	.L223
	movabsq	$9999999999999, %rax
	cmpq	%rax, %r8
	ja	.L224
	movabsq	$999999999999, %rax
	cmpq	%rax, %r8
	ja	.L225
	movabsq	$99999999999, %rax
	cmpq	%rax, %r8
	ja	.L226
	movabsq	$9999999999, %rax
	cmpq	%rax, %r8
	ja	.L227
	cmpq	$999999999, %r8
	ja	.L228
	cmpq	$99999999, %r8
	ja	.L229
	cmpq	$9999999, %r8
	ja	.L230
	cmpq	$999999, %r8
	ja	.L231
	cmpq	$99999, %r8
	ja	.L232
	cmpq	$9999, %r8
	ja	.L233
	cmpq	$999, %r8
	ja	.L234
	cmpq	$99, %r8
	ja	.L235
	cmpq	$10, %r8
	sbbq	%r11, %r11
	addq	$2, %r11
	cmpq	$10, %r8
	sbbl	%eax, %eax
	addl	$2, %eax
.L83:
	movabsq	$99999999999, %rdx
	cmpq	%rdx, %r9
	ja	.L84
	movabsq	$9999999999, %rdx
	cmpq	%rdx, %r9
	ja	.L84
	cmpq	$999999999, %r9
	ja	.L237
	cmpq	$99999999, %r9
	ja	.L238
	cmpq	$9999999, %r9
	ja	.L239
	cmpq	$999999, %r9
	ja	.L240
	cmpq	$99999, %r9
	ja	.L241
	cmpq	$9999, %r9
	ja	.L242
	cmpq	$999, %r9
	ja	.L243
	cmpq	$99, %r9
	ja	.L244
	cmpq	$9, %r9
	ja	.L86
	movl	$5, %ecx
.L87:
	cmpl	%eax, %ecx
	jb	.L399
.L84:
	addq	%r15, %r11
	movq	%r11, %r9
	cmpq	$99, %r8
	jbe	.L98
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %rbx
.L99:
	movq	%r8, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%rbx
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %rcx
	salq	$2, %rcx
	movq	%r8, %rdx
	subq	%rcx, %rdx
	movq	%r8, %rcx
	movq	%rax, %r8
	subq	$2, %r9
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r9)
	cmpq	$9999, %rcx
	ja	.L99
	jmp	.L98
.L210:
	movl	$14, %r11d
	jmp	.L75
.L252:
	movl	$1, %eax
	movl	$1, %r11d
	jmp	.L101
.L392:
	movabsq	$-3689348814741910323, %rcx
	movq	%r10, %rax
	mulq	%rcx
	shrq	$3, %rdx
	movq	%rdx, %r8
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	movq	%r10, %rsi
	subq	%rax, %rsi
	movq	%rsi, %r14
	movl	%esi, %r13d
	cmpq	$0, 40(%rsp)
	je	.L115
	subq	$1, %r9
	jmp	.L116
.L215:
	movl	$9, %r11d
	jmp	.L75
.L119:
	addl	%esi, %ebp
	jmp	.L121
.L347:
	xorl	%r11d, %r11d
	xorl	%ebx, %ebx
	jmp	.L122
.L115:
	movq	%r9, %rax
	mulq	%rcx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	cmpq	%r8, %rcx
	jbe	.L258
	movl	$1, %r9d
	jmp	.L196
.L258:
	xorl	%esi, %esi
	movl	$1, %r9d
	xorl	%ebx, %ebx
	jmp	.L117
.L398:
	leaq	(%r8,%r8,2), %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$8, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$16, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$32, %rdx
	addq	%rdx, %rax
	leaq	(%r8,%rax,4), %rax
	movabsq	$3689348814741910323, %rcx
	cmpq	%rcx, %rax
	ja	.L104
	movabsq	$-3689348814741910323, %r11
	movq	%r8, %rax
	mulq	%r11
	movq	%rdx, %r8
	shrq	$2, %r8
	movl	$1, %ebx
	jmp	.L106
.L253:
	movq	%rdx, %r8
.L106:
	leaq	(%r8,%r8,2), %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$8, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$16, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$32, %rdx
	addq	%rdx, %rax
	leaq	(%r8,%rax,4), %rax
	cmpq	%rcx, %rax
	ja	.L105
	movq	%r8, %rax
	mulq	%r11
	shrq	$2, %rdx
	addl	$1, %ebx
	cmpq	$4, %r8
	ja	.L253
	xorl	%ebx, %ebx
.L105:
	cmpl	%ebx, %esi
	setbe	%al
	jmp	.L107
.L211:
	movl	$13, %r11d
	jmp	.L75
.L393:
	cmpq	$0, 40(%rsp)
	jne	.L124
	testb	%r11b, %r11b
	jne	.L123
	movq	$1, 40(%rsp)
	jmp	.L124
.L212:
	movl	$12, %r11d
	jmp	.L75
.L261:
	movl	$22, %edx
	movl	$18, %ecx
	movl	$18, %r12d
	movl	$17, %esi
	movl	$19, %r8d
	movl	$17, %eax
.L131:
	leal	(%rax,%rbp), %edi
	leal	-1(%rax,%rbp), %r9d
	cmpl	%eax, %r9d
	jl	.L133
	cmpl	%edx, %edi
	jbe	.L400
.L134:
	cmpq	$9, %rbx
	jbe	.L137
	movabsq	$9999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L279
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L280
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L281
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L282
	movabsq	$999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L283
	movabsq	$99999999999, %rax
	cmpq	%rax, %rbx
	ja	.L284
	movabsq	$9999999999, %rax
	cmpq	%rax, %rbx
	ja	.L285
	cmpq	$999999999, %rbx
	ja	.L286
	cmpq	$99999999, %rbx
	ja	.L287
	cmpq	$9999999, %rbx
	ja	.L288
	cmpq	$999999, %rbx
	ja	.L289
	cmpq	$99999, %rbx
	ja	.L290
	cmpq	$9999, %rbx
	ja	.L291
	cmpq	$999, %rbx
	ja	.L292
	cmpq	$99, %rbx
	ja	.L401
	leaq	3(%r15), %r11
.L146:
	movq	%r11, %r8
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
.L194:
	movzwl	(%r10,%rbx,2), %eax
	movw	%ax, -2(%r8)
.L149:
	movzbl	1(%r15), %eax
	movb	%al, (%r15)
	movb	$46, 1(%r15)
.L150:
	movb	$101, (%r11)
	testl	%r9d, %r9d
	js	.L402
	movl	$43, %eax
.L151:
	movb	%al, 1(%r11)
	cmpl	$99, %r9d
	jg	.L403
	movslq	%r9d, %r9
	movzwl	(%r10,%r9,2), %eax
	movw	%ax, 2(%r11)
	addq	$4, %r11
	jmp	.L63
.L133:
	testl	%r9d, %r9d
	js	.L139
	cmpl	%eax, %edi
	je	.L404
	cmpl	%edx, %r8d
	.p2align 4,,3
	ja	.L134
	leaq	1(%r15), %r9
	movabsq	$9999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L171
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L320
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L321
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L322
	movabsq	$999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L323
	movabsq	$99999999999, %rax
	cmpq	%rax, %rbx
	ja	.L324
	movabsq	$9999999999, %rax
	cmpq	%rax, %rbx
	ja	.L325
	cmpq	$999999999, %rbx
	ja	.L326
	cmpq	$99999999, %rbx
	ja	.L327
	cmpq	$9999999, %rbx
	ja	.L328
	cmpq	$999999, %rbx
	ja	.L329
	cmpq	$99999, %rbx
	ja	.L330
	cmpq	$9999, %rbx
	ja	.L331
	cmpq	$999, %rbx
	ja	.L332
	cmpq	$99, %rbx
	ja	.L173
	cmpq	$10, %rbx
	sbbl	%eax, %eax
	leal	2(%rax), %r8d
	addq	%r9, %r8
.L176:
	cmpq	$9, %rbx
	jbe	.L178
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rbx,2), %eax
	movw	%ax, -2(%r8)
.L179:
	movslq	%edi, %rbx
	testl	%edi, %edi
	jne	.L405
.L180:
	movb	$46, (%r15,%rbx)
	leaq	(%r15,%r12), %r11
	jmp	.L63
.L402:
	movl	$1, %r9d
	subl	%edi, %r9d
	movl	$45, %eax
	jmp	.L151
.L139:
	subl	%r9d, %ecx
	cmpl	%edx, %ecx
	ja	.L134
	movw	$11824, (%r15)
	leaq	2(%r15), %r11
	negl	%edi
	movq	%rdi, %r8
	movl	$48, %edx
	movq	%r11, %rcx
	call	memset
	movq	%rax, %r11
	addq	%rdi, %r11
	movabsq	$9999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L181
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L333
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L334
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L335
	movabsq	$999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L336
	movabsq	$99999999999, %rax
	cmpq	%rax, %rbx
	ja	.L337
	movabsq	$9999999999, %rax
	cmpq	%rax, %rbx
	ja	.L338
	cmpq	$999999999, %rbx
	ja	.L339
	cmpq	$99999999, %rbx
	ja	.L340
	cmpq	$9999999, %rbx
	ja	.L341
	cmpq	$999999, %rbx
	ja	.L342
	cmpq	$99999, %rbx
	ja	.L343
	cmpq	$9999, %rbx
	ja	.L344
	cmpq	$999, %rbx
	ja	.L345
	cmpq	$99, %rbx
	ja	.L183
	cmpq	$10, %rbx
	sbbl	%eax, %eax
	addl	$2, %eax
.L185:
	leaq	(%r11,%rax), %r8
.L186:
	cmpq	$9, %rbx
	jbe	.L188
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rbx,2), %eax
	movw	%ax, -2(%r8)
.L189:
	addq	%rsi, %r11
	jmp	.L63
.L213:
	movl	$11, %r11d
	jmp	.L75
.L262:
	movl	$21, %edx
	movl	$17, %ecx
	movl	$17, %r12d
	movl	$16, %esi
	movl	$18, %r8d
	movl	$16, %eax
	jmp	.L131
.L400:
	movabsq	$9999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L153
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L294
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L295
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L296
	movabsq	$999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L297
	movabsq	$99999999999, %rax
	cmpq	%rax, %rbx
	ja	.L298
	movabsq	$9999999999, %rax
	cmpq	%rax, %rbx
	ja	.L299
	cmpq	$999999999, %rbx
	ja	.L300
	cmpq	$99999999, %rbx
	ja	.L301
	cmpq	$9999999, %rbx
	ja	.L302
	cmpq	$999999, %rbx
	ja	.L303
	cmpq	$99999, %rbx
	ja	.L304
	cmpq	$9999, %rbx
	ja	.L305
	cmpq	$999, %rbx
	ja	.L306
	cmpq	$99, %rbx
	jbe	.L406
	movl	$100, %ecx
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %rbx
	leaq	1(%r15), %r8
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rdx,2), %eax
	movw	%ax, 1(%r15)
.L160:
	addl	$48, %ebx
	movb	%bl, -1(%r8)
	jmp	.L161
.L104:
	cmpq	$0, 40(%rsp)
	jne	.L254
	movq	56(%rsp), %rbx
	leaq	(%rbx,%rbx,2), %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$8, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$16, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$32, %rdx
	addq	%rdx, %rax
	leaq	(%rbx,%rax,4), %rax
	cmpq	%rcx, %rax
	ja	.L255
	movq	%rbx, %rax
	movabsq	$-3689348814741910323, %r11
	mulq	%r11
	shrq	$2, %rdx
	movq	%rdx, %r8
	movl	$1, %ebx
	jmp	.L111
.L256:
	movq	%rdx, %r8
.L111:
	leaq	(%r8,%r8,2), %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$8, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$16, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$32, %rdx
	addq	%rdx, %rax
	leaq	(%r8,%rax,4), %rax
	cmpq	%rcx, %rax
	ja	.L109
	movq	%r8, %rax
	mulq	%r11
	shrq	$2, %rdx
	addl	$1, %ebx
	cmpq	$4, %r8
	ja	.L256
.L255:
	xorl	%ebx, %ebx
.L109:
	cmpl	%ebx, %esi
	ja	.L103
	movabsq	$-3689348814741910323, %r8
	movq	%r9, %rax
	mulq	%r8
	movq	%rdx, %rcx
	shrq	$3, %rcx
	movq	%r10, %rax
	mulq	%r8
	movq	%rdx, %r8
	shrq	$3, %r8
	leaq	(%r8,%r8,4), %rax
	addq	%rax, %rax
	movq	%r10, %rsi
	subq	%rax, %rsi
	movq	%rsi, %r14
	movl	%esi, %r13d
	xorl	%r9d, %r9d
	cmpq	%rcx, %r8
	jnb	.L348
	movl	$1, %r11d
	jmp	.L196
.L214:
	movl	$10, %r11d
	jmp	.L75
.L263:
	movl	$20, %edx
	movl	$16, %ecx
	movl	$16, %r12d
	movl	$15, %esi
	movl	$17, %r8d
	movl	$15, %eax
	jmp	.L131
.L260:
	xorl	%ebx, %ebx
	xorl	%r11d, %r11d
	jmp	.L126
.L264:
	movl	$19, %edx
	movl	$15, %ecx
	movl	$15, %r12d
	movl	$14, %esi
	movl	$16, %r8d
	movl	$14, %eax
	jmp	.L131
.L279:
	movl	$18, %r11d
.L143:
	addq	%r15, %r11
	cmpq	$99, %rbx
	jbe	.L146
	movq	%r11, %r8
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %rsi
.L147:
	movq	%rbx, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%rsi
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %rcx
	salq	$2, %rcx
	movq	%rbx, %rdx
	subq	%rcx, %rdx
	movq	%rbx, %rcx
	movq	%rax, %rbx
	subq	$2, %r8
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r8)
	cmpq	$9999, %rcx
	ja	.L147
	cmpq	$999, %rcx
	ja	.L194
.L148:
	addl	$48, %ebx
	movb	%bl, -1(%r8)
	jmp	.L149
.L404:
	cmpl	%ecx, %edx
	jb	.L134
	movabsq	$9999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L407
.L136:
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L307
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L308
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L309
	movabsq	$999999999999, %rax
	cmpq	%rax, %rbx
	ja	.L310
	movabsq	$99999999999, %rax
	cmpq	%rax, %rbx
	ja	.L311
	movabsq	$9999999999, %rax
	cmpq	%rax, %rbx
	ja	.L312
	cmpq	$999999999, %rbx
	ja	.L313
	cmpq	$99999999, %rbx
	ja	.L314
	cmpq	$9999999, %rbx
	ja	.L315
	cmpq	$999999, %rbx
	ja	.L316
	cmpq	$99999, %rbx
	ja	.L317
	cmpq	$9999, %rbx
	ja	.L318
	cmpq	$999, %rbx
	ja	.L319
	cmpq	$99, %rbx
	ja	.L164
	cmpq	$10, %rbx
	sbbl	%eax, %eax
	leal	2(%rax), %ecx
	addq	%r15, %rcx
.L167:
	cmpq	$9, %rbx
	jbe	.L169
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rbx,2), %eax
	movw	%ax, -2(%rcx)
.L170:
	leaq	(%r15,%rsi), %r11
	jmp	.L63
.L222:
	movl	$16, %r11d
	movl	$16, %eax
	jmp	.L83
.L183:
	movl	$100, %ecx
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %rbx
	leaq	1(%r11), %r8
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rdx,2), %eax
	movw	%ax, 1(%r11)
.L188:
	addl	$48, %ebx
	movb	%bl, -1(%r8)
	jmp	.L189
.L265:
	movl	$18, %edx
	movl	$14, %ecx
	movl	$14, %r12d
	movl	$13, %esi
	movl	$15, %r8d
	movl	$13, %eax
	jmp	.L131
.L216:
	movl	$8, %r11d
	jmp	.L75
.L280:
	movl	$17, %r11d
	jmp	.L143
.L254:
	xorl	%r8d, %r8d
	movabsq	$-3689348814741910323, %r11
	jmp	.L108
.L257:
	movq	%rdx, %r13
.L108:
	leaq	0(%r13,%r13,2), %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$8, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$16, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$32, %rdx
	addq	%rdx, %rax
	leaq	0(%r13,%rax,4), %rax
	cmpq	%rcx, %rax
	ja	.L113
	movq	%r13, %rax
	mulq	%r11
	shrq	$2, %rdx
	addl	$1, %r8d
	cmpq	$4, %r13
	ja	.L257
	xorl	%r8d, %r8d
.L113:
	xorl	%eax, %eax
	cmpl	%r8d, %esi
	setbe	%al
	subq	%rax, %r9
	jmp	.L103
.L217:
	movl	$7, %r11d
	jmp	.L75
.L266:
	movl	$17, %edx
	movl	$13, %ecx
	movl	$13, %r12d
	movl	$12, %esi
	movl	$14, %r8d
	movl	$12, %eax
	jmp	.L131
.L281:
	movl	$16, %r11d
	jmp	.L143
.L173:
	movl	$100, %ecx
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %rbx
	leaq	2(%r15), %r8
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rdx,2), %eax
	movw	%ax, 2(%r15)
.L178:
	addl	$48, %ebx
	movb	%bl, -1(%r8)
	jmp	.L179
.L267:
	movl	$16, %edx
	movl	$12, %ecx
	movl	$12, %r12d
	movl	$11, %esi
	movl	$13, %r8d
	movl	$11, %eax
	jmp	.L131
.L223:
	movl	$15, %r11d
	movl	$15, %eax
	jmp	.L83
.L218:
	movl	$6, %r11d
	jmp	.L75
.L403:
	movl	%r9d, %edx
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%rdx,%rax,8), %rcx
	movq	%rcx, %rax
	salq	$10, %rax
	subq	%rcx, %rax
	salq	$5, %rax
	addq	%rdx, %rax
	leaq	(%rdx,%rax,4), %rax
	leaq	(%rdx,%rax,8), %rax
	salq	$5, %rax
	subq	%rdx, %rax
	shrq	$37, %rax
	leal	48(%rax), %edx
	movb	%dl, 2(%r11)
	leal	(%rax,%rax,4), %eax
	leal	(%rax,%rax,4), %eax
	sall	$2, %eax
	subl	%eax, %r9d
	movzwl	(%r10,%r9,2), %eax
	movw	%ax, 3(%r11)
	addq	$5, %r11
	jmp	.L63
.L346:
	movl	%ebp, %r9d
.L137:
	addl	$48, %ebx
	movb	%bl, (%r15)
	leaq	1(%r15), %r11
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	jmp	.L150
.L282:
	movl	$15, %r11d
	jmp	.L143
.L219:
	movl	$5, %r11d
	jmp	.L75
.L224:
	movl	$14, %r11d
	movl	$14, %eax
	jmp	.L83
.L283:
	movl	$14, %r11d
	jmp	.L143
.L153:
	leaq	17(%r15), %r8
.L202:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %r9
.L159:
	movq	%rbx, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%r9
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %rcx
	salq	$2, %rcx
	movq	%rbx, %rdx
	subq	%rcx, %rdx
	movq	%rbx, %rcx
	movq	%rax, %rbx
	subq	$2, %r8
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r8)
	cmpq	$9999, %rcx
	ja	.L159
	jmp	.L158
.L268:
	movl	$15, %edx
	movl	$11, %ecx
	movl	$11, %r12d
	movl	$10, %esi
	movl	$12, %r8d
	movl	$10, %eax
	jmp	.L131
.L405:
	movq	%rbx, %r8
	movq	%r9, %rdx
	movq	%r15, %rcx
	call	memmove
	jmp	.L180
.L181:
	leaq	17(%r11), %r8
.L205:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %r9
.L187:
	movq	%rbx, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%r9
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %rcx
	salq	$2, %rcx
	movq	%rbx, %rdx
	subq	%rcx, %rdx
	movq	%rbx, %rcx
	movq	%rax, %rbx
	subq	$2, %r8
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r8)
	cmpq	$9999, %rcx
	ja	.L187
	jmp	.L186
.L102:
	movl	$1077, %eax
	subl	%r9d, %eax
	movslq	%eax, %rbx
	movabsq	$196742565691928, %rcx
	imulq	%rcx, %rbx
	shrq	$48, %rbx
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	seta	%cl
	subl	%ecx, %ebx
	leal	(%rdx,%rbx), %ebp
	subl	%ebx, %eax
	movl	%eax, %edx
	cltq
	salq	$4, %rax
	leal	(%rdx,%rdx,8), %ecx
	movl	%ecx, %r9d
	sall	$5, %r9d
	addl	%r9d, %ecx
	leal	(%rdx,%rcx,8), %ecx
	leal	(%rdx,%rcx,2), %ecx
	leal	(%rdx,%rcx,4), %ecx
	leal	(%rdx,%rcx,4), %ecx
	sall	$4, %ecx
	subl	%edx, %ecx
	shrl	$19, %ecx
	movl	%ecx, %edx
	movl	%ebx, %ecx
	subl	%edx, %ecx
	addl	$60, %ecx
	jmp	.L200
.L220:
	movl	$4, %r11d
	jmp	.L75
.L225:
	movl	$13, %r11d
	movl	$13, %eax
	jmp	.L83
.L269:
	movl	$14, %edx
	movl	$10, %ecx
	movl	$10, %r12d
	movl	$9, %esi
	movl	$11, %r8d
	movl	$9, %eax
	jmp	.L131
.L164:
	movl	$100, %ecx
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %rbx
	leaq	1(%r15), %rcx
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rdx,2), %eax
	movw	%ax, 1(%r15)
.L169:
	addl	$48, %ebx
	movb	%bl, -1(%rcx)
	jmp	.L170
.L284:
	movl	$13, %r11d
	jmp	.L143
.L294:
	movl	$16, %r8d
.L154:
	addq	%r15, %r8
	cmpq	$99, %rbx
	ja	.L202
	jmp	.L158
.L333:
	movl	$16, %r8d
.L182:
	addq	%r11, %r8
	cmpq	$99, %rbx
	ja	.L205
	jmp	.L186
.L171:
	leaq	18(%r15), %r8
.L204:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %r11
.L177:
	movq	%rbx, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%r11
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %rcx
	salq	$2, %rcx
	movq	%rbx, %rdx
	subq	%rcx, %rdx
	movq	%rbx, %rcx
	movq	%rax, %rbx
	subq	$2, %r8
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%r8)
	cmpq	$9999, %rcx
	ja	.L177
	jmp	.L176
.L76:
	leaq	3(%r15), %r11
	movq	%r8, %rax
	shrq	$2, %rax
	movabsq	$2951479051793528259, %rdx
	mulq	%rdx
	movq	%rdx, %rax
	shrq	$2, %rax
	movq	%rax, %rdx
	leaq	(%rax,%rax,4), %rax
	leaq	(%rax,%rax,4), %rcx
	salq	$2, %rcx
	movq	%r8, %rax
	subq	%rcx, %rax
	movq	%rdx, %r8
	leaq	1(%r15), %r9
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rdx
	movzwl	(%rdx,%rax,2), %eax
	movw	%ax, 1(%r15)
	jmp	.L100
.L270:
	movl	$13, %edx
	movl	$9, %ecx
	movl	$9, %r12d
	movl	$8, %esi
	movl	$10, %r8d
	movl	$8, %eax
	jmp	.L131
.L226:
	movl	$12, %r11d
	movl	$12, %eax
	jmp	.L83
.L399:
	addl	$48, %r9d
	movb	%r9b, (%r15)
	leaq	1(%r15), %rax
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
.L89:
	movw	$11109, (%rax)
	movzwl	(%r10,%r11,2), %edx
	movw	%dx, 2(%rax)
	leaq	4(%rax), %r11
	jmp	.L63
.L334:
	movl	$15, %r8d
	jmp	.L182
.L285:
	movl	$12, %r11d
	jmp	.L143
.L295:
	movl	$15, %r8d
	jmp	.L154
.L335:
	movl	$14, %r8d
	jmp	.L182
.L286:
	movl	$11, %r11d
	jmp	.L143
.L296:
	movl	$14, %r8d
	jmp	.L154
.L320:
	movl	$16, %r8d
.L172:
	addq	%r9, %r8
	cmpq	$99, %rbx
	ja	.L204
	jmp	.L176
.L227:
	movl	$11, %r11d
	movl	$11, %eax
	jmp	.L83
.L271:
	movl	$12, %edx
	movl	$8, %ecx
	movl	$8, %r12d
	movl	$7, %esi
	movl	$9, %r8d
	movl	$7, %eax
	jmp	.L131
.L272:
	movl	$11, %edx
	movl	$7, %ecx
	movl	$7, %r12d
	movl	$6, %esi
	movl	$8, %r8d
	movl	$6, %eax
	jmp	.L131
.L228:
	movl	$10, %r11d
	movl	$10, %eax
	jmp	.L83
.L321:
	movl	$15, %r8d
	jmp	.L172
.L237:
	movl	$14, %ecx
	movl	$15, %edx
	movl	$10, %ebx
.L85:
	cmpq	$9, %r9
	jbe	.L87
	cmpl	%eax, %edx
	jnb	.L84
	cmpq	$999999999, %r9
	ja	.L90
	cmpq	$99999999, %r9
	ja	.L245
	cmpq	$9999999, %r9
	ja	.L246
	cmpq	$999999, %r9
	ja	.L247
	cmpq	$99999, %r9
	ja	.L248
	cmpq	$9999, %r9
	ja	.L249
	movl	$4, %eax
	cmpq	$999, %r9
	jbe	.L408
.L91:
	leal	1(%rax), %ecx
	addq	%r15, %rcx
	cmpq	$99, %r9
	jbe	.L385
.L208:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %rsi
.L95:
	movq	%r9, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%rsi
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %r8
	salq	$2, %r8
	movq	%r9, %rdx
	subq	%r8, %rdx
	movq	%r9, %r8
	movq	%rax, %r9
	subq	$2, %rcx
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%rcx)
	cmpq	$9999, %r8
	ja	.L95
	cmpq	$999, %r8
	ja	.L198
.L96:
	addl	$48, %r9d
	movb	%r9b, -1(%rcx)
.L97:
	movzbl	1(%r15), %eax
	movb	%al, (%r15)
	movb	$46, 1(%r15)
	leal	1(%rbx), %eax
	addq	%r15, %rax
	jmp	.L89
.L297:
	movl	$13, %r8d
	jmp	.L154
.L322:
	movl	$14, %r8d
	jmp	.L172
.L238:
	movl	$13, %ecx
	movl	$14, %edx
	movl	$9, %ebx
	jmp	.L85
.L407:
	leaq	17(%r15), %rcx
.L203:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %r9
.L168:
	movq	%rbx, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%r9
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %r8
	salq	$2, %r8
	movq	%rbx, %rdx
	subq	%r8, %rdx
	movq	%rbx, %r8
	movq	%rax, %rbx
	subq	$2, %rcx
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%rcx)
	cmpq	$9999, %r8
	ja	.L168
	jmp	.L167
.L287:
	movl	$10, %r11d
	jmp	.L143
.L229:
	movl	$9, %r11d
	movl	$9, %eax
	jmp	.L83
.L336:
	movl	$13, %r8d
	jmp	.L182
.L273:
	movl	$10, %edx
	movl	$6, %ecx
	movl	$6, %r12d
	movl	$5, %esi
	movl	$7, %r8d
	movl	$5, %eax
	jmp	.L131
.L274:
	movl	$9, %edx
	movl	$5, %ecx
	movl	$5, %r12d
	movl	$4, %esi
	movl	$6, %r8d
	movl	$4, %eax
	jmp	.L131
.L337:
	movl	$12, %r8d
	jmp	.L182
.L338:
	movl	$11, %r8d
	jmp	.L182
.L230:
	movl	$8, %r11d
	movl	$8, %eax
	jmp	.L83
.L323:
	movl	$13, %r8d
	jmp	.L172
.L395:
	jne	.L409
	movl	$1, %esi
	jmp	.L136
.L299:
	movl	$11, %r8d
	jmp	.L154
.L239:
	movl	$12, %ecx
	movl	$13, %edx
	movl	$8, %ebx
	jmp	.L85
.L307:
	movl	$16, %ecx
.L163:
	addq	%r15, %rcx
	cmpq	$99, %rbx
	ja	.L203
	jmp	.L167
.L288:
	movl	$9, %r11d
	jmp	.L143
.L289:
	movl	$8, %r11d
	jmp	.L143
.L298:
	movl	$12, %r8d
	jmp	.L154
.L348:
	xorl	%esi, %esi
	xorl	%ebx, %ebx
	jmp	.L197
.L291:
	movl	$6, %r11d
	jmp	.L143
.L290:
	movl	$7, %r11d
	jmp	.L143
.L86:
	cmpl	$7, %eax
	jbe	.L84
	movl	$2, %ebx
.L93:
	leaq	3(%r15), %rcx
.L385:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
.L198:
	movzwl	(%r10,%r9,2), %eax
	movw	%ax, -2(%rcx)
	jmp	.L97
.L301:
	movl	$9, %r8d
	jmp	.L154
.L300:
	movl	$10, %r8d
	jmp	.L154
.L309:
	movl	$14, %ecx
	jmp	.L163
.L308:
	movl	$15, %ecx
	jmp	.L163
.L231:
	movl	$7, %r11d
	movl	$7, %eax
	jmp	.L83
.L324:
	movl	$12, %r8d
	jmp	.L172
.L339:
	movl	$10, %r8d
	jmp	.L182
.L394:
	movl	$7, %edx
	movl	$3, %ecx
	movl	$3, %r12d
	movl	$2, %esi
	movl	$4, %r8d
	movl	$2, %eax
	jmp	.L131
.L275:
	movl	$8, %edx
	movl	$4, %ecx
	movl	$4, %r12d
	movl	$3, %esi
	movl	$5, %r8d
	movl	$3, %eax
	jmp	.L131
.L325:
	movl	$11, %r8d
	jmp	.L172
.L240:
	movl	$11, %ecx
	movl	$12, %edx
	movl	$7, %ebx
	jmp	.L85
.L241:
	movl	$10, %ecx
	movl	$11, %edx
	movl	$6, %ebx
	jmp	.L85
.L232:
	movl	$6, %r11d
	movl	$6, %eax
	jmp	.L83
.L340:
	movl	$9, %r8d
	jmp	.L182
.L341:
	movl	$8, %r8d
	jmp	.L182
.L326:
	movl	$10, %r8d
	jmp	.L172
.L328:
	movl	$8, %r8d
	jmp	.L172
.L327:
	movl	$9, %r8d
	jmp	.L172
.L330:
	movl	$6, %r8d
	jmp	.L172
.L329:
	movl	$7, %r8d
	jmp	.L172
.L292:
	movl	$5, %r11d
	jmp	.L143
.L401:
	leaq	4(%r15), %r11
	movl	$100, %ecx
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %rbx
	leaq	2(%r15), %r8
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, 2(%r15)
	jmp	.L148
.L302:
	movl	$8, %r8d
	jmp	.L154
.L409:
	movl	$2, %eax
	subl	%ebp, %eax
	movl	%ebp, %r9d
	cmpl	$4, %eax
	ja	.L137
	movw	$11824, (%r15)
	leaq	2(%r15), %r11
	notl	%ebp
	movq	%rbp, %r8
	movl	$48, %edx
	movq	%r11, %rcx
	call	memset
	movq	%rax, %r11
	addq	%rbp, %r11
	movl	$1, %esi
	movl	$1, %eax
	jmp	.L185
.L244:
	movl	$7, %ecx
	movl	$8, %edx
	movl	$3, %ebx
	jmp	.L85
.L243:
	movl	$8, %ecx
	movl	$9, %edx
	movl	$4, %ebx
	jmp	.L85
.L343:
	movl	$6, %r8d
	jmp	.L182
.L342:
	movl	$7, %r8d
	jmp	.L182
.L314:
	movl	$9, %ecx
	jmp	.L163
.L313:
	movl	$10, %ecx
	jmp	.L163
.L312:
	movl	$11, %ecx
	jmp	.L163
.L311:
	movl	$12, %ecx
	jmp	.L163
.L310:
	movl	$13, %ecx
	jmp	.L163
.L242:
	movl	$9, %ecx
	movl	$10, %edx
	movl	$5, %ebx
	jmp	.L85
.L233:
	movl	$5, %r11d
	movl	$5, %eax
	jmp	.L83
.L235:
	movl	$3, %r11d
	movl	$3, %eax
	jmp	.L83
.L234:
	movl	$4, %r11d
	movl	$4, %eax
	jmp	.L83
.L305:
	movl	$5, %r8d
	jmp	.L154
.L304:
	movl	$6, %r8d
	jmp	.L154
.L303:
	movl	$7, %r8d
	jmp	.L154
.L344:
	movl	$5, %r8d
	jmp	.L182
.L90:
	leaq	11(%r15), %rcx
	jmp	.L208
.L245:
	movl	$9, %eax
	jmp	.L91
.L319:
	movl	$4, %ecx
	jmp	.L163
.L318:
	movl	$5, %ecx
	jmp	.L163
.L249:
	movl	$5, %eax
	jmp	.L91
.L248:
	movl	$6, %eax
	jmp	.L91
.L247:
	movl	$7, %eax
	jmp	.L91
.L246:
	movl	$8, %eax
	jmp	.L91
.L406:
	cmpq	$10, %rbx
	sbbl	%eax, %eax
	addl	$2, %eax
	jmp	.L157
.L306:
	movl	$4, %r8d
	jmp	.L154
.L345:
	movl	$4, %r8d
	jmp	.L182
.L408:
	cmpq	$99, %r9
	jbe	.L93
	movl	$100, %ecx
	movq	%r9, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %r9
	leaq	2(%r15), %rcx
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, 2(%r15)
	jmp	.L96
.L315:
	movl	$8, %ecx
	jmp	.L163
.L317:
	movl	$6, %ecx
	jmp	.L163
.L316:
	movl	$7, %ecx
	jmp	.L163
.L332:
	movl	$4, %r8d
	jmp	.L172
.L331:
	movl	$5, %r8d
	jmp	.L172
	.seh_endproc
	.def	_ZN7fast_io7details5decay14print_fallbackILb1ENS_23basic_win32_io_observerIcEEJNS_26io_print_define_rv_wrapperILb1ESt17basic_string_viewIcSt11char_traitsIcEEEENS5_ILb1ENS_18basic_io_scatter_tIDuEEEENS5_ILb1ENSt6chrono8durationIxSt5ratioILx1ELx1000000000EEEEEEEEEvT0_DpT1_.isra.0;	.scl	3;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details5decay14print_fallbackILb1ENS_23basic_win32_io_observerIcEEJNS_26io_print_define_rv_wrapperILb1ESt17basic_string_viewIcSt11char_traitsIcEEEENS5_ILb1ENS_18basic_io_scatter_tIDuEEEENS5_ILb1ENSt6chrono8durationIxSt5ratioILx1ELx1000000000EEEEEEEEEvT0_DpT1_.isra.0
_ZN7fast_io7details5decay14print_fallbackILb1ENS_23basic_win32_io_observerIcEEJNS_26io_print_define_rv_wrapperILb1ESt17basic_string_viewIcSt11char_traitsIcEEEENS5_ILb1ENS_18basic_io_scatter_tIDuEEEENS5_ILb1ENSt6chrono8durationIxSt5ratioILx1ELx1000000000EEEEEEEEEvT0_DpT1_.isra.0:
.LFB16454:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	movl	$4232, %eax
	call	___chkstk_ms
	subq	%rax, %rsp
	.seh_stackalloc	4232
	.seh_endprologue
	movq	%rcx, %r12
	movq	(%rdx), %rbx
	movq	8(%rdx), %r13
	movq	%r8, %rbp
	leaq	96(%rsp), %rsi
	movq	%rsi, 4192(%rsp)
	movq	%rsi, 4200(%rsp)
	leaq	4192(%rsp), %rax
	movq	%rax, 4208(%rsp)
	cmpq	$4096, %rbx
	ja	.L440
	testq	%rbx, %rbx
	jne	.L441
	movq	%rax, %rdi
.L415:
	leaq	(%rsi,%rbx), %rcx
.L414:
	movq	%rcx, 4200(%rsp)
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rbp, %xmm1
	mulsd	.LC1(%rip), %xmm1
	leaq	30(%rcx), %rax
	cmpq	%rax, %rdi
	jbe	.L416
	call	_ZN7fast_io7details3ryu15output_shortestILb0ELy0ELb1ELb0EDuLDu46EPcdEET5_NS0_27compile_time_floating_valueIXT2_ET3_XT4_EEES4_T6_.isra.0
	movq	%rax, 4200(%rsp)
	movq	4208(%rsp), %rdi
.L417:
	cmpq	%rdi, %rax
	je	.L442
	movb	$115, (%rax)
	addq	$1, %rax
	movq	%rax, 4200(%rsp)
	movq	4208(%rsp), %rdx
.L426:
	cmpq	%rax, %rdx
	je	.L443
	movb	$10, (%rax)
	addq	$1, %rax
	movq	%rax, 4200(%rsp)
	movq	4192(%rsp), %r13
.L430:
	movl	$0, 64(%rsp)
	subq	%r13, %rax
	movl	$4294967295, %r8d
	cmpq	%r8, %rax
	movq	$0, 32(%rsp)
	leaq	64(%rsp), %r9
	cmovbe	%rax, %r8
	movq	%r13, %rdx
	movq	%r12, %rcx
	call	WriteFile
	testl	%eax, %eax
	je	.L444
	movq	4192(%rsp), %rcx
	cmpq	%rsi, %rcx
	je	.L410
	movq	4208(%rsp), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
	nop
.L410:
	addq	$4232, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L441:
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	%rsi, %rcx
	call	memcpy
	movq	4208(%rsp), %rdi
	jmp	.L415
.L440:
	cmpq	$8192, %rbx
	movl	$8192, %r14d
	cmovnb	%rbx, %r14
	testq	%r14, %r14
	js	.L445
	movq	%r14, %rcx
.LEHB1:
	call	_Znwy
	movq	%rax, %rdi
	movq	4192(%rsp), %r15
	movq	4200(%rsp), %r8
	subq	%r15, %r8
	movq	%r8, 56(%rsp)
	movq	%r15, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%rsi, %r15
	movq	56(%rsp), %r8
	je	.L413
	movq	4208(%rsp), %rdx
	subq	%r15, %rdx
	movq	%r15, %rcx
	call	_ZdlPvy
	movq	56(%rsp), %r8
.L413:
	movq	%rdi, 4192(%rsp)
	leaq	(%rdi,%r8), %rcx
	addq	%r14, %rdi
	movq	%rdi, 4208(%rsp)
	movq	%rbx, %r8
	movq	%r13, %rdx
	call	memcpy
	movq	%rax, %rcx
	addq	%rbx, %rcx
	jmp	.L414
.L416:
	leaq	64(%rsp), %r13
	movq	%r13, %rcx
	call	_ZN7fast_io7details3ryu15output_shortestILb0ELy0ELb1ELb0EDuLDu46EPcdEET5_NS0_27compile_time_floating_valueIXT2_ET3_XT4_EEES4_T6_.isra.0
	subq	%r13, %rax
	movq	%rax, %rbx
	movq	4208(%rsp), %rdi
	movq	4200(%rsp), %rcx
	movq	%rdi, %rax
	subq	%rcx, %rax
	cmpq	%rax, %rbx
	ja	.L446
	testq	%rbx, %rbx
	jne	.L447
.L422:
	leaq	(%rcx,%rbx), %rax
.L421:
	movq	%rax, 4200(%rsp)
	jmp	.L417
.L442:
	subq	4192(%rsp), %rax
	addq	%rax, %rax
	movq	%rax, %rbp
	js	.L448
	movq	%rax, %rcx
	call	_Znwy
	movq	%rax, %rdi
	movq	4192(%rsp), %r13
	movq	4200(%rsp), %rbx
	subq	%r13, %rbx
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%rsi, %r13
	je	.L425
	movq	4208(%rsp), %rdx
	subq	%r13, %rdx
	movq	%r13, %rcx
	call	_ZdlPvy
.L425:
	movq	%rdi, 4192(%rsp)
	leaq	(%rdi,%rbx), %rax
	leaq	(%rdi,%rbp), %rdx
	movq	%rdx, 4208(%rsp)
	movb	$115, (%rax)
	addq	$1, %rax
	movq	%rax, 4200(%rsp)
	jmp	.L426
.L443:
	subq	4192(%rsp), %rdx
	movq	%rdx, %rdi
	addq	%rdi, %rdi
	js	.L449
	movq	%rdi, %rcx
	call	_Znwy
	movq	%rax, %r13
	movq	4192(%rsp), %r14
	movq	4200(%rsp), %rbx
	subq	%r14, %rbx
	movq	%rbx, %r8
	movq	%r14, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%rsi, %r14
	je	.L429
	movq	4208(%rsp), %rdx
	subq	%r14, %rdx
	movq	%r14, %rcx
	call	_ZdlPvy
.L429:
	movq	%r13, 4192(%rsp)
	leaq	0(%r13,%rbx), %rax
	addq	%r13, %rdi
	movq	%rdi, 4208(%rsp)
	movb	$10, (%rax)
	addq	$1, %rax
	movq	%rax, 4200(%rsp)
	jmp	.L430
.L447:
	movq	%rbx, %r8
	movq	%r13, %rdx
	call	memcpy
	movq	%rax, %rcx
	movq	4208(%rsp), %rdi
	jmp	.L422
.L446:
	subq	4192(%rsp), %rdi
	leaq	(%rdi,%rdi), %rax
	cmpq	%rbx, %rax
	cmovb	%rbx, %rax
	movq	%rax, %r14
	testq	%rax, %rax
	js	.L450
	movq	%rax, %rcx
	call	_Znwy
	movq	%rax, %rdi
	movq	4192(%rsp), %r15
	movq	4200(%rsp), %rbp
	subq	%r15, %rbp
	movq	%rbp, %r8
	movq	%r15, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%rsi, %r15
	je	.L420
	movq	4208(%rsp), %rdx
	subq	%r15, %rdx
	movq	%r15, %rcx
	call	_ZdlPvy
.L420:
	movq	%rdi, 4192(%rsp)
	addq	%rdi, %rbp
	addq	%r14, %rdi
	movq	%rdi, 4208(%rsp)
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	%rbp, %rcx
	call	memcpy
	leaq	0(%rbp,%rbx), %rax
	jmp	.L421
.L450:
	call	_ZSt17__throw_bad_allocv
.L445:
	call	_ZSt17__throw_bad_allocv
.L449:
	.p2align 4,,5
	call	_ZSt17__throw_bad_allocv
.L448:
	.p2align 4,,5
	call	_ZSt17__throw_bad_allocv
.LEHE1:
.L436:
	movq	%rax, %r12
	movq	4192(%rsp), %rcx
	cmpq	%rsi, %rcx
	je	.L434
	movq	4208(%rsp), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
.L434:
	movq	%r12, %rcx
.LEHB2:
	call	_Unwind_Resume
.LEHE2:
.L444:
	movl	$24, %ecx
	call	__cxa_allocate_exception
	movq	%rax, %r12
	call	GetLastError
	movq	$0, 8(%r12)
	leaq	16+_ZTVN7fast_io11win32_errorE(%rip), %rdi
	movq	%rdi, (%r12)
	movl	%eax, 16(%r12)
	leaq	_ZN7fast_io11win32_errorD1Ev(%rip), %r8
	leaq	_ZTIN7fast_io11win32_errorE(%rip), %rdx
	movq	%r12, %rcx
.LEHB3:
	call	__cxa_throw
	nop
.LEHE3:
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA16454:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE16454-.LLSDACSB16454
.LLSDACSB16454:
	.uleb128 .LEHB1-.LFB16454
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L436-.LFB16454
	.uleb128 0
	.uleb128 .LEHB2-.LFB16454
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB3-.LFB16454
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L436-.LFB16454
	.uleb128 0
.LLSDACSE16454:
	.text
	.seh_endproc
	.section	.text$_ZN7fast_io5timerD1Ev,"x"
	.linkonce discard
	.align 2
	.globl	_ZN7fast_io5timerD1Ev
	.def	_ZN7fast_io5timerD1Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io5timerD1Ev
_ZN7fast_io5timerD1Ev:
.LFB14424:
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$48, %rsp
	.seh_stackalloc	48
	.seh_endprologue
	movq	%rcx, %rbx
	call	_ZNSt6chrono3_V212system_clock3nowEv
	subq	16(%rbx), %rax
	movq	%rax, %rsi
	movq	(%rbx), %rdi
	movq	8(%rbx), %rbx
	movl	$-12, %ecx
	call	GetStdHandle
	movq	%rax, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rbx, 40(%rsp)
	leaq	32(%rsp), %rdx
	movq	%rsi, %r8
	call	_ZN7fast_io7details5decay14print_fallbackILb1ENS_23basic_win32_io_observerIcEEJNS_26io_print_define_rv_wrapperILb1ESt17basic_string_viewIcSt11char_traitsIcEEEENS5_ILb1ENS_18basic_io_scatter_tIDuEEEENS5_ILb1ENSt6chrono8durationIxSt5ratioILx1ELx1000000000EEEEEEEEEvT0_DpT1_.isra.0
	nop
	addq	$48, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	ret
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA14424:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE14424-.LLSDACSB14424
.LLSDACSB14424:
.LLSDACSE14424:
	.section	.text$_ZN7fast_io5timerD1Ev,"x"
	.linkonce discard
	.seh_endproc
	.def	__main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
.LC2:
	.ascii "fast_io::concat\0"
	.section	.text.startup,"x"
	.globl	main
	.def	main;	.scl	2;	.type	32;	.endef
	.seh_proc	main
main:
.LFB14425:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$152, %rsp
	.seh_stackalloc	152
	.seh_endprologue
	call	__main
	movq	$15, 80(%rsp)
	leaq	.LC2(%rip), %rax
	movq	%rax, 88(%rsp)
	call	_ZNSt6chrono3_V212system_clock3nowEv
	movq	%rax, 96(%rsp)
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	leaq	128(%rsp), %r13
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rbx
	movabsq	$2951479051793528259, %rbp
	leaq	48(%rsp), %r15
.L472:
	cmpq	$99999999, %r12
	ja	.L453
.L580:
	cmpq	$9999999, %r12
	ja	.L508
	cmpq	$999999, %r12
	ja	.L509
	cmpq	$99999, %r12
	ja	.L510
	cmpq	$9999, %r12
	ja	.L511
	cmpq	$999, %r12
	ja	.L512
	cmpq	$99, %r12
	ja	.L455
	cmpq	$10, %r12
	sbbq	%r8, %r8
	addq	$2, %r8
	cmpq	$10, %r12
	sbbl	%r10d, %r10d
	addl	$2, %r10d
	leaq	(%r15,%r8), %r9
	movq	%r12, %rcx
.L458:
	cmpq	$9, %rcx
	jbe	.L460
	movzwl	(%rbx,%rcx,2), %eax
	movw	%ax, -2(%r9)
	movq	%r13, 112(%rsp)
	cmpl	$1, %r10d
	je	.L577
.L575:
	movl	%r8d, %ecx
	movq	%r13, %rdi
	movq	%r15, %rsi
	cmpl	$8, %r8d
	jnb	.L578
.L463:
	testb	$4, %cl
	je	.L466
	movsl
.L466:
	testb	$2, %cl
	je	.L467
	movsw
.L467:
	andl	$1, %ecx
	je	.L462
	movsb
.L462:
	movq	%r8, 120(%rsp)
	movb	$0, 128(%rsp,%r8)
	movq	120(%rsp), %rsi
	addq	%r14, %rsi
	movq	%rsi, %r14
	movq	112(%rsp), %rcx
	cmpq	%r13, %rcx
	je	.L469
	movq	128(%rsp), %rax
	leaq	1(%rax), %rdx
	call	_ZdlPvy
	addq	$1, %r12
	cmpq	$200000000, %r12
	jne	.L472
.L470:
	leaq	80(%rsp), %rcx
	call	_ZN7fast_io5timerD1Ev
	movl	$1, %ecx
.LEHB4:
	call	*__imp___acrt_iob_func(%rip)
.LEHE4:
	movq	%rax, %rbx
	movq	__imp___iob_func(%rip), %rdi
	call	*%rdi
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	jbe	.L579
	leaq	48(%rbx), %rcx
	call	EnterCriticalSection
.L474:
	movq	(%rbx), %r8
	leaq	21(%r8), %rdx
	movslq	36(%rbx), %rax
	addq	16(%rbx), %rax
	cmpq	%rax, %rdx
	movabsq	$-8446744073709551617, %rax
	jnb	.L475
	cmpq	%rax, %rsi
	ja	.L476
	movabsq	$999999999999999999, %rax
	cmpq	%rax, %rsi
	ja	.L514
	movabsq	$99999999999999999, %rax
	cmpq	%rax, %rsi
	ja	.L515
	movabsq	$9999999999999999, %rax
	cmpq	%rax, %rsi
	ja	.L516
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rsi
	ja	.L517
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rsi
	ja	.L518
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rsi
	ja	.L519
	movabsq	$999999999999, %rax
	cmpq	%rax, %rsi
	ja	.L520
	movabsq	$99999999999, %rax
	cmpq	%rax, %rsi
	ja	.L521
	movabsq	$9999999999, %rax
	cmpq	%rax, %rsi
	ja	.L522
	cmpq	$999999999, %rsi
	ja	.L523
	cmpq	$99999999, %rsi
	ja	.L524
	cmpq	$9999999, %rsi
	ja	.L525
	cmpq	$999999, %rsi
	ja	.L526
	cmpq	$99999, %rsi
	ja	.L527
	cmpq	$9999, %rsi
	ja	.L528
	cmpq	$999, %rsi
	ja	.L529
	cmpq	$99, %rsi
	ja	.L478
	cmpq	$10, %rsi
	sbbl	%eax, %eax
	leal	2(%rax), %ecx
	addq	%r8, %rcx
	movq	%rcx, %r9
.L481:
	cmpq	$9, %r14
	jbe	.L483
.L581:
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%r14,2), %eax
	movw	%ax, -2(%rcx)
.L484:
	movb	$10, (%r9)
	addq	$1, %r9
	orl	$65536, 24(%rbx)
	movq	%r9, %rax
	subq	(%rbx), %rax
	subl	%eax, 8(%rbx)
	movq	%r9, (%rbx)
.L485:
	call	*%rdi
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L498
	andl	$-32769, 24(%rbx)
	sarq	$4, %rax
	imull	$-1431655765, %eax, %eax
	leal	16(%rax), %ecx
	call	_unlock
.L574:
	xorl	%eax, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L455:
	movl	$100, %ecx
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %rcx
	movzwl	(%rbx,%rdx,2), %eax
	movw	%ax, 1(%r15)
	leaq	49(%rsp), %r9
	movl	$3, %r10d
	movl	$3, %r8d
.L460:
	addl	$48, %ecx
	movb	%cl, -1(%r9)
	movq	%r13, 112(%rsp)
	cmpl	$1, %r10d
	jne	.L575
.L577:
	movzbl	48(%rsp), %eax
	movb	%al, 128(%rsp)
	jmp	.L462
.L469:
	addq	$1, %r12
	cmpq	$200000000, %r12
	je	.L470
	cmpq	$99999999, %r12
	jbe	.L580
.L453:
	leaq	57(%rsp), %r9
	movl	$9, %r8d
	movl	$9, %r10d
.L507:
	movq	%r12, %rcx
.L459:
	movq	%rcx, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%rbp
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %r11
	salq	$2, %r11
	movq	%rcx, %rdx
	subq	%r11, %rdx
	movq	%rcx, %r11
	movq	%rax, %rcx
	subq	$2, %r9
	movzwl	(%rbx,%rdx,2), %eax
	movw	%ax, (%r9)
	cmpq	$9999, %r11
	ja	.L459
	jmp	.L458
.L578:
	movl	%r8d, %r10d
	andl	$-8, %r10d
	xorl	%eax, %eax
.L464:
	movl	%eax, %edx
	movq	(%r15,%rdx), %r9
	movq	%r9, 0(%r13,%rdx)
	addl	$8, %eax
	cmpl	%r10d, %eax
	jb	.L464
	leaq	0(%r13,%rax), %rdi
	leaq	(%r15,%rax), %rsi
	jmp	.L463
.L508:
	movl	$8, %r8d
	movl	$8, %r10d
.L454:
	leaq	(%r15,%r8), %r9
	cmpq	$99, %r12
	ja	.L507
	movq	%r12, %rcx
	jmp	.L458
.L509:
	movl	$7, %r8d
	movl	$7, %r10d
	jmp	.L454
.L510:
	movl	$6, %r8d
	movl	$6, %r10d
	jmp	.L454
.L511:
	movl	$5, %r8d
	movl	$5, %r10d
	jmp	.L454
.L579:
	sarq	$4, %rax
	imull	$-1431655765, %eax, %eax
	leal	16(%rax), %ecx
	call	_lock
	orl	$32768, 24(%rbx)
	jmp	.L474
.L512:
	movl	$4, %r8d
	movl	$4, %r10d
	jmp	.L454
.L514:
	movl	$19, %eax
.L477:
	leaq	(%r8,%rax), %rcx
	movq	%rcx, %r9
	cmpq	$99, %rsi
	jbe	.L481
.L505:
	movq	%r9, %rcx
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %r11
.L482:
	movq	%r14, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%r11
	shrq	$2, %rdx
	movq	%rdx, %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	(%rdx,%rdx,4), %r8
	salq	$2, %r8
	movq	%r14, %rdx
	subq	%r8, %rdx
	movq	%r14, %r8
	movq	%rax, %r14
	subq	$2, %rcx
	movzwl	(%r10,%rdx,2), %eax
	movw	%ax, (%rcx)
	cmpq	$9999, %r8
	ja	.L482
	cmpq	$9, %r14
	ja	.L581
.L483:
	addl	$48, %r14d
	movb	%r14b, -1(%rcx)
	jmp	.L484
.L498:
	leaq	48(%rbx), %rcx
	call	LeaveCriticalSection
	jmp	.L574
.L476:
	leaq	20(%r8), %r9
	jmp	.L505
.L515:
	movl	$18, %eax
	.p2align 4,,2
	jmp	.L477
.L475:
	cmpq	%rax, %rsi
	ja	.L486
	movabsq	$999999999999999999, %rax
	cmpq	%rax, %rsi
	ja	.L531
	movabsq	$99999999999999999, %rax
	cmpq	%rax, %rsi
	ja	.L532
	movabsq	$9999999999999999, %rax
	cmpq	%rax, %rsi
	ja	.L533
	movabsq	$999999999999999, %rax
	cmpq	%rax, %rsi
	ja	.L534
	movabsq	$99999999999999, %rax
	cmpq	%rax, %rsi
	ja	.L535
	movabsq	$9999999999999, %rax
	cmpq	%rax, %rsi
	ja	.L536
	movabsq	$999999999999, %rax
	cmpq	%rax, %rsi
	ja	.L537
	movabsq	$99999999999, %rax
	cmpq	%rax, %rsi
	ja	.L538
	movabsq	$9999999999, %rax
	cmpq	%rax, %rsi
	ja	.L539
	cmpq	$999999999, %rsi
	ja	.L540
	cmpq	$99999999, %rsi
	ja	.L541
	cmpq	$9999999, %rsi
	ja	.L542
	cmpq	$999999, %rsi
	ja	.L543
	cmpq	$99999, %rsi
	ja	.L544
	cmpq	$9999, %rsi
	ja	.L545
	cmpq	$999, %rsi
	ja	.L546
	cmpq	$99, %rsi
	ja	.L488
	cmpq	$10, %rsi
	sbbl	%eax, %eax
	leaq	112(%rsp), %r12
	leal	2(%rax), %ecx
	addq	%r12, %rcx
	movq	%rcx, %rax
.L491:
	cmpq	$9, %r14
	jbe	.L493
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rdx
	movzwl	(%rdx,%r14,2), %edx
	movw	%dx, -2(%rcx)
.L494:
	movb	$10, (%rax)
	addq	$1, %rax
	subq	%r12, %rax
	movq	%rax, %r8
	movq	(%rbx), %rcx
	leaq	(%rcx,%rax), %rsi
	movslq	36(%rbx), %rax
	addq	16(%rbx), %rax
	cmpq	%rax, %rsi
	jnb	.L495
	testq	%r8, %r8
	jne	.L582
.L496:
	orl	$65536, 24(%rbx)
	movq	%rsi, %rax
	subq	(%rbx), %rax
	subl	%eax, 8(%rbx)
	movq	%rsi, (%rbx)
	jmp	.L485
.L518:
	movl	$15, %eax
	jmp	.L477
.L517:
	movl	$16, %eax
	jmp	.L477
.L516:
	movl	$17, %eax
	jmp	.L477
.L488:
	movl	$100, %ecx
	movq	%rsi, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %r14
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rdx,2), %eax
	movw	%ax, 113(%rsp)
	leaq	115(%rsp), %rax
	leaq	113(%rsp), %rcx
	leaq	112(%rsp), %r12
.L493:
	addl	$48, %r14d
	movb	%r14b, -1(%rcx)
	jmp	.L494
.L522:
	movl	$11, %eax
	jmp	.L477
.L521:
	movl	$12, %eax
	jmp	.L477
.L520:
	movl	$13, %eax
	jmp	.L477
.L519:
	movl	$14, %eax
	jmp	.L477
.L524:
	movl	$9, %eax
	jmp	.L477
.L523:
	movl	$10, %eax
	jmp	.L477
.L582:
	movq	%r12, %rdx
	call	memcpy
	jmp	.L496
.L495:
	movq	%r8, 40(%rsp)
	movq	%rbx, %rcx
	call	clearerr
	movq	%rbx, %r9
	movq	40(%rsp), %r8
	movl	$1, %edx
	movq	%r12, %rcx
.LEHB5:
	call	fwrite
	movq	%rbx, %rcx
	call	ferror
	movl	%eax, %esi
	call	*__imp__errno(%rip)
	movl	(%rax), %ebp
	movq	%rbx, %rcx
	call	clearerr
	testl	%esi, %esi
	je	.L485
	movl	$24, %ecx
	call	__cxa_allocate_exception
	movq	%rax, %rcx
	movq	$0, 8(%rax)
	leaq	16+_ZTVN7fast_io11posix_errorE(%rip), %rax
	movq	%rax, (%rcx)
	movl	%ebp, 16(%rcx)
	leaq	_ZN7fast_io11posix_errorD1Ev(%rip), %r8
	leaq	_ZTIN7fast_io11posix_errorE(%rip), %rdx
	call	__cxa_throw
.LEHE5:
.L532:
	movl	$18, %eax
.L487:
	leaq	112(%rsp), %r12
	leaq	(%r12,%rax), %rcx
	cmpq	$99, %rsi
	jbe	.L547
.L506:
	movq	%rcx, %r8
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %r10
	movabsq	$2951479051793528259, %r11
.L492:
	movq	%r14, %rax
	shrq	$2, %rax
	mulq	%r11
	movq	%rdx, %rax
	shrq	$2, %rax
	movq	%rax, %rdx
	leaq	(%rax,%rax,4), %rax
	leaq	(%rax,%rax,4), %r9
	salq	$2, %r9
	movq	%r14, %rax
	subq	%r9, %rax
	movq	%r14, %r9
	movq	%rdx, %r14
	subq	$2, %r8
	movzwl	(%r10,%rax,2), %eax
	movw	%ax, (%r8)
	cmpq	$9999, %r9
	ja	.L492
	movq	%rcx, %rax
	movq	%r8, %rcx
	jmp	.L491
.L547:
	movq	%rcx, %rax
	jmp	.L491
.L531:
	movl	$19, %eax
	jmp	.L487
.L544:
	movl	$6, %eax
	jmp	.L487
.L543:
	movl	$7, %eax
	jmp	.L487
.L542:
	movl	$8, %eax
	jmp	.L487
.L541:
	movl	$9, %eax
	jmp	.L487
.L486:
	leaq	132(%rsp), %rcx
	leaq	112(%rsp), %r12
	jmp	.L506
.L540:
	movl	$10, %eax
	jmp	.L487
.L539:
	movl	$11, %eax
	jmp	.L487
.L538:
	movl	$12, %eax
	jmp	.L487
.L537:
	movl	$13, %eax
	jmp	.L487
.L536:
	movl	$14, %eax
	jmp	.L487
.L535:
	movl	$15, %eax
	jmp	.L487
.L534:
	movl	$16, %eax
	jmp	.L487
.L533:
	movl	$17, %eax
	jmp	.L487
.L546:
	movl	$4, %eax
	jmp	.L487
.L545:
	movl	$5, %eax
	jmp	.L487
.L478:
	leaq	3(%r8), %r9
	movl	$100, %ecx
	movq	%rsi, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %r14
	leaq	1(%r8), %rcx
	leaq	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE(%rip), %rax
	movzwl	(%rax,%rdx,2), %eax
	movw	%ax, 1(%r8)
	jmp	.L483
.L529:
	movl	$4, %eax
	jmp	.L477
.L528:
	movl	$5, %eax
	jmp	.L477
.L527:
	movl	$6, %eax
	jmp	.L477
.L526:
	movl	$7, %eax
	jmp	.L477
.L525:
	movl	$8, %eax
	jmp	.L477
.L548:
	movq	%rax, %r12
	call	*%rdi
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	movq	%rax, %rcx
	sarq	$4, %rcx
	movabsq	$-6148914691236517205, %rdx
	imulq	%rdx, %rcx
	cmpq	$912, %rax
	ja	.L501
	andl	$-32769, 24(%rbx)
	addl	$16, %ecx
	call	_unlock
.L502:
	movq	%r12, %rcx
.LEHB6:
	call	_Unwind_Resume
.LEHE6:
.L501:
	leaq	48(%rbx), %rcx
	call	LeaveCriticalSection
	jmp	.L502
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA14425:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE14425-.LLSDACSB14425
.LLSDACSB14425:
	.uleb128 .LEHB4-.LFB14425
	.uleb128 .LEHE4-.LEHB4
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB5-.LFB14425
	.uleb128 .LEHE5-.LEHB5
	.uleb128 .L548-.LFB14425
	.uleb128 0
	.uleb128 .LEHB6-.LFB14425
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0
	.uleb128 0
.LLSDACSE14425:
	.section	.text.startup,"x"
	.seh_endproc
	.section	.text$_ZN7fast_io7details5decay14print_fallbackILb0ENS_20io_reference_wrapperINS_14error_reporterEEEJNS_26io_print_define_rv_wrapperILb1ENS_5manip4chvwIPcEEEEEEEvT0_DpT1_,"x"
	.linkonce discard
	.globl	_ZN7fast_io7details5decay14print_fallbackILb0ENS_20io_reference_wrapperINS_14error_reporterEEEJNS_26io_print_define_rv_wrapperILb1ENS_5manip4chvwIPcEEEEEEEvT0_DpT1_
	.def	_ZN7fast_io7details5decay14print_fallbackILb0ENS_20io_reference_wrapperINS_14error_reporterEEEJNS_26io_print_define_rv_wrapperILb1ENS_5manip4chvwIPcEEEEEEEvT0_DpT1_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details5decay14print_fallbackILb0ENS_20io_reference_wrapperINS_14error_reporterEEEJNS_26io_print_define_rv_wrapperILb1ENS_5manip4chvwIPcEEEEEEEvT0_DpT1_
_ZN7fast_io7details5decay14print_fallbackILb0ENS_20io_reference_wrapperINS_14error_reporterEEEJNS_26io_print_define_rv_wrapperILb1ENS_5manip4chvwIPcEEEEEEEvT0_DpT1_:
.LFB15657:
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbx
	.seh_pushreg	%rbx
	movl	$4168, %eax
	call	___chkstk_ms
	subq	%rax, %rsp
	.seh_stackalloc	4168
	.seh_endprologue
	leaq	32(%rsp), %rbx
	movq	%rbx, 4128(%rsp)
	movq	%rbx, 4136(%rsp)
	leaq	4128(%rsp), %rax
	movq	%rax, 4144(%rsp)
	movq	(%rcx), %rax
	movq	(%rax), %rax
	leaq	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L590
.L583:
	addq	$4168, %rsp
	popq	%rbx
	popq	%r12
	ret
.L590:
	movq	%rbx, %r8
	movq	%rbx, %rdx
.LEHB7:
	call	*%rax
.LEHE7:
	movq	4128(%rsp), %rcx
	cmpq	%rbx, %rcx
	je	.L583
	movq	4144(%rsp), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
	nop
	addq	$4168, %rsp
	popq	%rbx
	popq	%r12
	ret
.L589:
	movq	%rax, %r12
	movq	4128(%rsp), %rcx
	cmpq	%rbx, %rcx
	je	.L588
	movq	4144(%rsp), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
.L588:
	movq	%r12, %rcx
.LEHB8:
	call	_Unwind_Resume
	nop
.LEHE8:
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA15657:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE15657-.LLSDACSB15657
.LLSDACSB15657:
	.uleb128 .LEHB7-.LFB15657
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L589-.LFB15657
	.uleb128 0
	.uleb128 .LEHB8-.LFB15657
	.uleb128 .LEHE8-.LEHB8
	.uleb128 0
	.uleb128 0
.LLSDACSE15657:
	.section	.text$_ZN7fast_io7details5decay14print_fallbackILb0ENS_20io_reference_wrapperINS_14error_reporterEEEJNS_26io_print_define_rv_wrapperILb1ENS_5manip4chvwIPcEEEEEEEvT0_DpT1_,"x"
	.linkonce discard
	.seh_endproc
	.section	.text$_ZNK7fast_io11posix_error6reportERNS_14error_reporterE,"x"
	.linkonce discard
	.align 2
	.globl	_ZNK7fast_io11posix_error6reportERNS_14error_reporterE
	.def	_ZNK7fast_io11posix_error6reportERNS_14error_reporterE;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK7fast_io11posix_error6reportERNS_14error_reporterE
_ZNK7fast_io11posix_error6reportERNS_14error_reporterE:
.LFB11482:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	%rdx, %r12
	movl	16(%rcx), %ecx
	call	strerror
	movq	%rax, %rdx
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZN7fast_io7details5decay14print_fallbackILb0ENS_20io_reference_wrapperINS_14error_reporterEEEJNS_26io_print_define_rv_wrapperILb1ENS_5manip4chvwIPcEEEEEEEvT0_DpT1_
	.seh_endproc
	.section	.text$_ZN7fast_io7details33reserve_write_extremely_cold_pathINS_14error_reporterEZNS0_18report_win32_errorERS2_jEUlT_E_EEvRS4_yRT0_,"x"
	.linkonce discard
	.globl	_ZN7fast_io7details33reserve_write_extremely_cold_pathINS_14error_reporterEZNS0_18report_win32_errorERS2_jEUlT_E_EEvRS4_yRT0_
	.def	_ZN7fast_io7details33reserve_write_extremely_cold_pathINS_14error_reporterEZNS0_18report_win32_errorERS2_jEUlT_E_EEvRS4_yRT0_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details33reserve_write_extremely_cold_pathINS_14error_reporterEZNS0_18report_win32_errorERS2_jEUlT_E_EEvRS4_yRT0_
_ZN7fast_io7details33reserve_write_extremely_cold_pathINS_14error_reporterEZNS0_18report_win32_errorERS2_jEUlT_E_EEvRS4_yRT0_:
.LFB15730:
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$1104, %rsp
	.seh_stackalloc	1104
	.seh_endprologue
	movq	%rcx, %rsi
	cmpq	$1024, %rdx
	ja	.L593
	movq	(%r8), %rax
	movq	$0, 48(%rsp)
	movl	$32768, 40(%rsp)
	leaq	80(%rsp), %r12
	movq	%r12, 32(%rsp)
	movl	$1024, %r9d
	movl	(%rax), %r8d
	xorl	%edx, %edx
	movl	$4608, %ecx
	call	FormatMessageA
	movl	%eax, %ebx
	addq	%r12, %rbx
	leaq	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_(%rip), %rdi
.L600:
	movq	(%rsi), %rax
	movq	(%rax), %rax
	cmpq	%rdi, %rax
	jne	.L594
	movq	8(%rsi), %rsi
	subq	%r12, %rbx
	movq	4112(%rsi), %rax
	movq	4104(%rsi), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %rbx
	ja	.L619
	testq	%rbx, %rbx
	jne	.L620
.L599:
	addq	%rbx, %rcx
	movq	%rcx, 4104(%rsi)
.L592:
	addq	$1104, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.L594:
	movq	%rbx, %r8
	movq	%r12, %rdx
	movq	%rsi, %rcx
.LEHB9:
	call	*%rax
	movq	%rax, %r12
	cmpq	%rbx, %rax
	jne	.L600
	jmp	.L592
.L620:
	movq	%rbx, %r8
	movq	%r12, %rdx
	call	memcpy
	movq	%rax, %rcx
	jmp	.L599
.L619:
	subq	4096(%rsi), %rax
	leaq	(%rax,%rax), %rdi
	cmpq	%rbx, %rdi
	cmovb	%rbx, %rdi
	testq	%rdi, %rdi
	js	.L621
	movq	%rdi, %rcx
	call	_Znwy
	movq	%rax, %rbp
	movq	4096(%rsi), %rdx
	movq	4104(%rsi), %r8
	subq	%rdx, %r8
	movq	%rax, %rcx
	call	memcpy
	movq	4096(%rsi), %rcx
	movq	4104(%rsi), %r13
	subq	%rcx, %r13
	cmpq	%rsi, %rcx
	je	.L597
	movq	4112(%rsi), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
.L597:
	movq	%rbp, 4096(%rsi)
	leaq	0(%rbp,%r13), %rcx
	movq	%rcx, 4104(%rsi)
	addq	%rdi, %rbp
	movq	%rbp, 4112(%rsi)
	movq	%rbx, %r8
	movq	%r12, %rdx
	call	memcpy
	addq	%rbx, 4104(%rsi)
	jmp	.L592
.L593:
	movq	%r8, 72(%rsp)
	movq	%rdx, %rcx
	call	_Znay
.LEHE9:
	movq	%rax, %r12
	movq	72(%rsp), %r8
	movq	(%r8), %rax
	movq	$0, 48(%rsp)
	movl	$32768, 40(%rsp)
	movq	%r12, 32(%rsp)
	movl	$1024, %r9d
	movl	(%rax), %r8d
	xorl	%edx, %edx
	movl	$4608, %ecx
	call	FormatMessageA
	movl	%eax, %ebx
	addq	%r12, %rbx
	movq	%r12, %r13
	leaq	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_(%rip), %rdi
.L608:
	movq	(%rsi), %rax
	movq	(%rax), %rax
	cmpq	%rdi, %rax
	jne	.L602
	movq	8(%rsi), %rsi
	subq	%r13, %rbx
	movq	4112(%rsi), %rax
	movq	4104(%rsi), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %rbx
	ja	.L622
	testq	%rbx, %rbx
	jne	.L623
.L607:
	addq	%rbx, %rcx
	movq	%rcx, 4104(%rsi)
.L606:
	movq	%r12, %rcx
	addq	$1104, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	jmp	_ZdaPv
.L623:
	movq	%rbx, %r8
	movq	%r13, %rdx
	call	memcpy
	movq	%rax, %rcx
	jmp	.L607
.L602:
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	%rsi, %rcx
.LEHB10:
	call	*%rax
	movq	%rax, %r13
	cmpq	%rbx, %rax
	jne	.L608
	jmp	.L606
.L622:
	subq	4096(%rsi), %rax
	leaq	(%rax,%rax), %rdi
	cmpq	%rbx, %rdi
	cmovb	%rbx, %rdi
	testq	%rdi, %rdi
	js	.L624
	movq	%rdi, %rcx
	call	_Znwy
	movq	%rax, %rbp
	movq	4096(%rsi), %rdx
	movq	4104(%rsi), %r8
	subq	%rdx, %r8
	movq	%rax, %rcx
	call	memcpy
	movq	4096(%rsi), %rcx
	movq	4104(%rsi), %r14
	subq	%rcx, %r14
	cmpq	%rsi, %rcx
	je	.L605
	movq	4112(%rsi), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
.L605:
	movq	%rbp, 4096(%rsi)
	leaq	0(%rbp,%r14), %rcx
	movq	%rcx, 4104(%rsi)
	addq	%rdi, %rbp
	movq	%rbp, 4112(%rsi)
	movq	%rbx, %r8
	movq	%r13, %rdx
	call	memcpy
	addq	%rbx, 4104(%rsi)
	jmp	.L606
.L624:
	call	_ZSt17__throw_bad_allocv
.LEHE10:
.L621:
.LEHB11:
	call	_ZSt17__throw_bad_allocv
.L610:
	movq	%rax, %r13
	movq	%r12, %rcx
	call	_ZdaPv
	movq	%r13, %rcx
	call	_Unwind_Resume
	nop
.LEHE11:
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA15730:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE15730-.LLSDACSB15730
.LLSDACSB15730:
	.uleb128 .LEHB9-.LFB15730
	.uleb128 .LEHE9-.LEHB9
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB10-.LFB15730
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L610-.LFB15730
	.uleb128 0
	.uleb128 .LEHB11-.LFB15730
	.uleb128 .LEHE11-.LEHB11
	.uleb128 0
	.uleb128 0
.LLSDACSE15730:
	.section	.text$_ZN7fast_io7details33reserve_write_extremely_cold_pathINS_14error_reporterEZNS0_18report_win32_errorERS2_jEUlT_E_EEvRS4_yRT0_,"x"
	.linkonce discard
	.seh_endproc
	.section	.text$_ZNK7fast_io11win32_error6reportERNS_14error_reporterE,"x"
	.linkonce discard
	.align 2
	.globl	_ZNK7fast_io11win32_error6reportERNS_14error_reporterE
	.def	_ZNK7fast_io11win32_error6reportERNS_14error_reporterE;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK7fast_io11win32_error6reportERNS_14error_reporterE
_ZNK7fast_io11win32_error6reportERNS_14error_reporterE:
.LFB12792:
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$104, %rsp
	.seh_stackalloc	104
	.seh_endprologue
	movq	%rdx, %r12
	movl	16(%rcx), %eax
	movl	%eax, 68(%rsp)
	movq	$32768, 72(%rsp)
	leaq	68(%rsp), %rax
	movq	%rax, 80(%rsp)
	leaq	72(%rsp), %rax
	movq	%rax, 88(%rsp)
	movq	(%rdx), %rax
	movq	8(%rax), %rax
	leaq	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L626
	movq	8(%r12), %rax
	movq	4104(%rax), %rbx
	leaq	32768(%rbx), %rdx
	cmpq	%rdx, 4112(%rax)
	jbe	.L629
.L627:
	testq	%rbx, %rbx
	je	.L629
	movq	$0, 48(%rsp)
	movl	$32768, 40(%rsp)
	movq	%rbx, 32(%rsp)
	movl	$1024, %r9d
	movq	80(%rsp), %rax
	movl	(%rax), %r8d
	xorl	%edx, %edx
	movl	$4608, %ecx
	call	FormatMessageA
	movl	%eax, %eax
	leaq	(%rbx,%rax), %rdx
	movq	(%r12), %rax
	movq	16(%rax), %rax
	leaq	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc(%rip), %rcx
	cmpq	%rcx, %rax
	jne	.L630
	movq	8(%r12), %rax
	movq	%rdx, 4104(%rax)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	ret
.L626:
	movl	$32768, %edx
	movq	%r12, %rcx
	call	*%rax
	movq	%rax, %rbx
	jmp	.L627
.L630:
	movq	%r12, %rcx
	call	*%rax
	nop
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	ret
.L629:
	leaq	80(%rsp), %r8
	movl	$32768, %edx
	movq	%r12, %rcx
	call	_ZN7fast_io7details33reserve_write_extremely_cold_pathINS_14error_reporterEZNS0_18report_win32_errorERS2_jEUlT_E_EEvRS4_yRT0_
	nop
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	ret
	.seh_endproc
	.globl	_ZTSSt9exception
	.section	.rdata$_ZTSSt9exception,"dr"
	.linkonce same_size
	.align 8
_ZTSSt9exception:
	.ascii "St9exception\0"
	.globl	_ZTISt9exception
	.section	.rdata$_ZTISt9exception,"dr"
	.linkonce same_size
	.align 8
_ZTISt9exception:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSSt9exception
	.globl	_ZTSN7fast_io14error_reporterE
	.section	.rdata$_ZTSN7fast_io14error_reporterE,"dr"
	.linkonce same_size
	.align 16
_ZTSN7fast_io14error_reporterE:
	.ascii "N7fast_io14error_reporterE\0"
	.globl	_ZTIN7fast_io14error_reporterE
	.section	.rdata$_ZTIN7fast_io14error_reporterE,"dr"
	.linkonce same_size
	.align 8
_ZTIN7fast_io14error_reporterE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN7fast_io14error_reporterE
	.globl	_ZTSN7fast_io13fast_io_errorE
	.section	.rdata$_ZTSN7fast_io13fast_io_errorE,"dr"
	.linkonce same_size
	.align 16
_ZTSN7fast_io13fast_io_errorE:
	.ascii "N7fast_io13fast_io_errorE\0"
	.globl	_ZTIN7fast_io13fast_io_errorE
	.section	.rdata$_ZTIN7fast_io13fast_io_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTIN7fast_io13fast_io_errorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN7fast_io13fast_io_errorE
	.quad	_ZTISt9exception
	.globl	_ZTSN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE
	.section	.rdata$_ZTSN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE,"dr"
	.linkonce same_size
	.align 32
_ZTSN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE:
	.ascii "N7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE\0"
	.globl	_ZTIN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE
	.section	.rdata$_ZTIN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE,"dr"
	.linkonce same_size
	.align 8
_ZTIN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE
	.quad	_ZTIN7fast_io14error_reporterE
	.globl	_ZTSN7fast_io11posix_errorE
	.section	.rdata$_ZTSN7fast_io11posix_errorE,"dr"
	.linkonce same_size
	.align 16
_ZTSN7fast_io11posix_errorE:
	.ascii "N7fast_io11posix_errorE\0"
	.globl	_ZTIN7fast_io11posix_errorE
	.section	.rdata$_ZTIN7fast_io11posix_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTIN7fast_io11posix_errorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN7fast_io11posix_errorE
	.quad	_ZTIN7fast_io13fast_io_errorE
	.globl	_ZTSN7fast_io11win32_errorE
	.section	.rdata$_ZTSN7fast_io11win32_errorE,"dr"
	.linkonce same_size
	.align 16
_ZTSN7fast_io11win32_errorE:
	.ascii "N7fast_io11win32_errorE\0"
	.globl	_ZTIN7fast_io11win32_errorE
	.section	.rdata$_ZTIN7fast_io11win32_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTIN7fast_io11win32_errorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN7fast_io11win32_errorE
	.quad	_ZTIN7fast_io13fast_io_errorE
	.globl	_ZTVN7fast_io13fast_io_errorE
	.section	.rdata$_ZTVN7fast_io13fast_io_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTVN7fast_io13fast_io_errorE:
	.quad	0
	.quad	_ZTIN7fast_io13fast_io_errorE
	.quad	0
	.quad	0
	.quad	_ZNK7fast_io13fast_io_error4whatEv
	.quad	__cxa_pure_virtual
	.globl	_ZTVN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE
	.section	.rdata$_ZTVN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE,"dr"
	.linkonce same_size
	.align 8
_ZTVN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE:
	.quad	0
	.quad	_ZTIN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE
	.quad	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_
	.quad	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy
	.quad	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc
	.globl	_ZTVN7fast_io11posix_errorE
	.section	.rdata$_ZTVN7fast_io11posix_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTVN7fast_io11posix_errorE:
	.quad	0
	.quad	_ZTIN7fast_io11posix_errorE
	.quad	_ZN7fast_io11posix_errorD1Ev
	.quad	_ZN7fast_io11posix_errorD0Ev
	.quad	_ZNK7fast_io13fast_io_error4whatEv
	.quad	_ZNK7fast_io11posix_error6reportERNS_14error_reporterE
	.globl	_ZTVN7fast_io11win32_errorE
	.section	.rdata$_ZTVN7fast_io11win32_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTVN7fast_io11win32_errorE:
	.quad	0
	.quad	_ZTIN7fast_io11win32_errorE
	.quad	_ZN7fast_io11win32_errorD1Ev
	.quad	_ZN7fast_io11win32_errorD0Ev
	.quad	_ZNK7fast_io13fast_io_error4whatEv
	.quad	_ZNK7fast_io11win32_error6reportERNS_14error_reporterE
	.globl	_ZN7fast_io7details3ryu4pow5IdLb1EE5splitE
	.section	.rdata$_ZN7fast_io7details3ryu4pow5IdLb1EE5splitE,"dr"
	.linkonce same_size
	.align 32
_ZN7fast_io7details3ryu4pow5IdLb1EE5splitE:
	.quad	0
	.quad	1152921504606846976
	.quad	0
	.quad	1441151880758558720
	.quad	0
	.quad	1801439850948198400
	.quad	0
	.quad	2251799813685248000
	.quad	0
	.quad	1407374883553280000
	.quad	0
	.quad	1759218604441600000
	.quad	0
	.quad	2199023255552000000
	.quad	0
	.quad	1374389534720000000
	.quad	0
	.quad	1717986918400000000
	.quad	0
	.quad	2147483648000000000
	.quad	0
	.quad	1342177280000000000
	.quad	0
	.quad	1677721600000000000
	.quad	0
	.quad	2097152000000000000
	.quad	0
	.quad	1310720000000000000
	.quad	0
	.quad	1638400000000000000
	.quad	0
	.quad	2048000000000000000
	.quad	0
	.quad	1280000000000000000
	.quad	0
	.quad	1600000000000000000
	.quad	0
	.quad	2000000000000000000
	.quad	0
	.quad	1250000000000000000
	.quad	0
	.quad	1562500000000000000
	.quad	0
	.quad	1953125000000000000
	.quad	0
	.quad	1220703125000000000
	.quad	0
	.quad	1525878906250000000
	.quad	0
	.quad	1907348632812500000
	.quad	0
	.quad	1192092895507812500
	.quad	0
	.quad	1490116119384765625
	.quad	4611686018427387904
	.quad	1862645149230957031
	.quad	-8646911284551352320
	.quad	1164153218269348144
	.quad	-6196953087261802496
	.quad	1455191522836685180
	.quad	-3134505340649865216
	.quad	1818989403545856475
	.quad	-3918131675812331520
	.quad	2273736754432320594
	.quad	-4754675306596401152
	.quad	1421085471520200371
	.quad	-5943344133245501440
	.quad	1776356839400250464
	.quad	-2817494148129488896
	.quad	2220446049250313080
	.quad	-8678462870222012416
	.quad	1387778780781445675
	.quad	7598665485932036096
	.quad	1734723475976807094
	.quad	274959820560269312
	.quad	2168404344971008868
	.quad	-9051522149004607488
	.quad	1355252715606880542
	.quad	2520655369026404352
	.quad	1694065894508600678
	.quad	-6072552825571770368
	.quad	2117582368135750847
	.quad	-3795345515982356480
	.quad	1323488980084844279
	.quad	-4744181894977945600
	.quad	1654361225106055349
	.quad	3293144668132343808
	.quad	2067951531382569187
	.quad	-247627591630979072
	.quad	1292469707114105741
	.quad	8913837547316051968
	.quad	1615587133892632177
	.quad	-2692761121137098752
	.quad	2019483917365790221
	.quad	-6294661719138074624
	.quad	1262177448353618888
	.quad	-3256641130495205376
	.quad	1577721810442023610
	.quad	-8682487431546394624
	.quad	1972152263052529513
	.quad	-814868626289108736
	.quad	1232595164407830945
	.quad	8204786253993389888
	.quad	1540743955509788682
	.quad	1032610780636961552
	.quad	1925929944387235853
	.quad	2951224747111794922
	.quad	1203706215242022408
	.quad	3689030933889743652
	.quad	1504632769052528010
	.quad	-4612083369492596243
	.quad	1880790961315660012
	.quad	-576709096719178700
	.quad	1175494350822287507
	.quad	-720886370898973375
	.quad	1469367938527859384
	.quad	3710578054803671186
	.quad	1836709923159824231
	.quad	26536550077201078
	.quad	2295887403949780289
	.quad	-6900943683842831182
	.quad	1434929627468612680
	.quad	-4014493586376151074
	.quad	1793662034335765850
	.quad	8816941072311974870
	.quad	2242077542919707313
	.quad	-1406940857446097563
	.quad	1401298464324817070
	.quad	-6370362090235009857
	.quad	1751623080406021338
	.quad	5872105442488401391
	.quad	2189528850507526673
	.quad	-3247463126085830987
	.quad	1368455531567204170
	.quad	-8671014926034676638
	.quad	1710569414459005213
	.quad	-1615396620688569989
	.quad	2138211768073756516
	.quad	1296220121283337709
	.quad	1336382355046097823
	.quad	-2991410866823215768
	.quad	1670477943807622278
	.quad	-8350949601956407614
	.quad	2088097429759527848
	.quad	6309871544845715001
	.quad	1305060893599704905
	.quad	-5947718624225019960
	.quad	1631326116999631131
	.quad	-7434648280281274950
	.quad	2039157646249538914
	.quad	-6952498184389490796
	.quad	1274473528905961821
	.quad	532749306367912313
	.quad	1593091911132452277
	.quad	5277622651387278295
	.quad	1991364888915565346
	.quad	7910200175544436838
	.quad	1244603055572228341
	.quad	-3947307835851617664
	.quad	1555753819465285426
	.quad	8900923260467641632
	.quad	1944692274331606783
	.quad	-5966138008276193740
	.quad	1215432671457254239
	.quad	-7457672510345242175
	.quad	1519290839321567799
	.quad	9124653435777998898
	.quad	1899113549151959749
	.quad	8008751406574943263
	.quad	1186945968219974843
	.quad	5399253239791291175
	.quad	1483682460274968554
	.quad	-2474305487115661840
	.quad	1854603075343710692
	.quad	759402079766405302
	.quad	1159126922089819183
	.quad	-3662433418719381276
	.quad	1448908652612273978
	.quad	-9189727791826614499
	.quad	1811135815765342473
	.quad	-2263787702928492316
	.quad	2263919769706678091
	.quad	7808504722524468110
	.quad	1414949856066673807
	.quad	5148944884728197234
	.quad	1768687320083342259
	.quad	1824495087482858639
	.quad	2210859150104177824
	.quad	1140309429676786649
	.quad	1381786968815111140
	.quad	1425386787095983311
	.quad	1727233711018888925
	.quad	6393419502297367043
	.quad	2159042138773611156
	.quad	-5227484847918921406
	.quad	1349401336733506972
	.quad	-1922670041471263854
	.quad	1686751670916883715
	.quad	-2403337551839079817
	.quad	2108439588646104644
	.quad	803757039314269066
	.quad	1317774742903815403
	.quad	-3606989719284551571
	.quad	1647218428629769253
	.quad	4714634887749086344
	.quad	2059023035787211567
	.quad	-8582568241225290795
	.quad	1286889397367007229
	.quad	-1504838264676837686
	.quad	1608611746708759036
	.quad	2730638187581340797
	.quad	2010764683385948796
	.quad	-7516723169616437810
	.quad	1256727927116217997
	.quad	-172531925165771454
	.quad	1570909908895272496
	.quad	4396021111970173586
	.quad	1963637386119090621
	.quad	5053356204195052443
	.quad	1227273366324431638
	.quad	-2906676781610960254
	.quad	1534091707905539547
	.quad	-3633345977013700317
	.quad	1917614634881924434
	.quad	-4576684244847256650
	.quad	1198509146801202771
	.quad	-5720855306059070813
	.quad	1498136433501503464
	.quad	-2539383114146450612
	.quad	1872670541876879330
	.quad	-3892957455555225585
	.quad	1170419088673049581
	.quad	4357175217410743827
	.quad	1463023860841311977
	.quad	-8388589033518733928
	.quad	1828779826051639971
	.quad	7961007781811134206
	.quad	2285974782564549964
	.quad	-4247742173222816929
	.quad	1428734239102843727
	.quad	-5309677716528521161
	.quad	1785917798878554659
	.quad	-6637097145660651452
	.quad	2232397248598193324
	.quad	-1842342706824213205
	.quad	1395248280373870827
	.quad	-2302928383530266507
	.quad	1744060350467338534
	.quad	-7490346497840221037
	.quad	2180075438084173168
	.quad	6847748484918331612
	.quad	1362547148802608230
	.quad	-663686430706861293
	.quad	1703183936003260287
	.quad	-829608038383576617
	.quad	2128979920004075359
	.quad	-518505023989735386
	.quad	1330612450002547099
	.quad	-648131279987169232
	.quad	1663265562503183874
	.quad	-5421850118411349444
	.quad	2079081953128979843
	.quad	5834715712847682405
	.quad	1299426220705612402
	.quad	-1929977395795172801
	.quad	1624282775882015502
	.quad	-7024157763171353905
	.quad	2030353469852519378
	.quad	-6695941611195790143
	.quad	1268970918657824611
	.quad	-8369927013994737679
	.quad	1586213648322280764
	.quad	-5850722749066034194
	.quad	1982767060402850955
	.quad	5566670318688504437
	.quad	1239229412751781847
	.quad	2346651879933242642
	.quad	1549036765939727309
	.quad	7545000868343941206
	.quad	1936295957424659136
	.quad	4715625542714963254
	.quad	1210184973390411960
	.quad	5894531928393704067
	.quad	1512731216738014950
	.quad	-1855207126362645724
	.quad	1890914020922518687
	.quad	-1159504453976653577
	.quad	1181821263076574179
	.quad	-1449380567470816972
	.quad	1477276578845717724
	.quad	2799960309088866689
	.quad	1846595723557147156
	.quad	-7473396843674234127
	.quad	1154122327223216972
	.quad	-4730060036165404755
	.quad	1442652909029021215
	.quad	-5912575045206755944
	.quad	1803316136286276519
	.quad	-7390718806508444929
	.quad	2254145170357845649
	.quad	-7513235640390177
	.quad	1408840731473653530
	.quad	-4621077562977875625
	.quad	1761050914342066913
	.quad	3447025083132431277
	.quad	2201313642927583642
	.quad	6766076695385157452
	.quad	1375821026829739776
	.quad	8457595869231446815
	.quad	1719776283537174720
	.quad	-7874749237170243097
	.quad	2149720354421468400
	.quad	6607496772837067824
	.quad	1343575221513417750
	.quad	-964001070808441028
	.quad	1679469026891772187
	.quad	-1205001338510551285
	.quad	2099336283614715234
	.quad	-3058968845782788505
	.quad	1312085177259197021
	.quad	5399660979626290177
	.quad	1640106471573996277
	.quad	-7085481830749300991
	.quad	2050133089467495346
	.quad	-6734269153432007072
	.quad	1281333180917184591
	.quad	-8417836441790008839
	.quad	1601666476146480739
	.quad	7924448521472040567
	.quad	2002083095183100924
	.quad	-4270591710934750454
	.quad	1251301934489438077
	.quad	3885132398186337741
	.quad	1564127418111797597
	.quad	-8978642557549241536
	.quad	1955159272639746996
	.quad	-3305808589254582008
	.quad	1221974545399841872
	.quad	479425281859160394
	.quad	1527468181749802341
	.quad	5210967620751338397
	.quad	1909335227187252926
	.quad	-1354831255457801406
	.quad	1193334516992033078
	.quad	-6305225087749639662
	.quad	1491668146240041348
	.quad	-3269845341259661673
	.quad	1864585182800051685
	.quad	-6655339356714676450
	.quad	1165365739250032303
	.quad	-8319174195893345562
	.quad	1456707174062540379
	.quad	8047776328842869663
	.quad	1820883967578175474
	.quad	836348374198811271
	.quad	2276104959472719343
	.quad	7440246761515338900
	.quad	1422565599670449589
	.quad	-4534749603387990086
	.quad	1778206999588061986
	.quad	8166621051047176104
	.quad	2222758749485077483
	.quad	2798295147690791113
	.quad	1389224218428173427
	.quad	-1113817083813899013
	.quad	1736530273035216783
	.quad	-1392271354767373766
	.quad	2170662841294020979
	.quad	8353202440125167204
	.quad	1356664275808763112
	.quad	-8005241023553092611
	.quad	1695830344760953890
	.quad	3828506775840797949
	.quad	2119787930951192363
	.quad	86973725686804766
	.quad	1324867456844495227
	.quad	-4502968861318881947
	.quad	1656084321055619033
	.quad	3594660960206173375
	.quad	2070105401319523792
	.quad	2246663100128858359
	.quad	1293815875824702370
	.quad	-6415043161693702859
	.quad	1617269844780877962
	.quad	5816254103165035138
	.quad	2021587305976097453
	.quad	5941001823691840913
	.quad	1263492066235060908
	.quad	7426252279614801142
	.quad	1579365082793826135
	.quad	4671129331091113523
	.quad	1974206353492282669
	.quad	5225298841145639904
	.quad	1233878970932676668
	.quad	6531623551432049880
	.quad	1542348713665845835
	.quad	3552843420862674446
	.quad	1927935892082307294
	.quad	-2391158880388216375
	.quad	1204959932551442058
	.quad	-7600634618912658373
	.quad	1506199915689302573
	.quad	-277421236786047158
	.quad	1882749894611628216
	.quad	-7090917300632361330
	.quad	1176718684132267635
	.quad	-8863646625790451662
	.quad	1470898355165334544
	.quad	-6467872263810676674
	.quad	1838622943956668180
	.quad	-3473154311335957938
	.quad	2298278679945835225
	.quad	2440964573842414192
	.quad	1436424174966147016
	.quad	3051205717303017741
	.quad	1795530218707683770
	.quad	-5409364890226003632
	.quad	2244412773384604712
	.quad	8148361989677217490
	.quad	1402757983365377945
	.quad	-3649605568185641850
	.quad	1753447479206722431
	.quad	-4562006960232052312
	.quad	2191809349008403039
	.quad	-2851254350145032695
	.quad	1369880843130251899
	.quad	-3564067937681290869
	.quad	1712351053912814874
	.quad	-9066770940529001490
	.quad	2140438817391018593
	.quad	-1055045819403238027
	.quad	1337774260869386620
	.quad	3292878744173340370
	.quad	1672217826086733276
	.quad	4116098430216675462
	.quad	2090272282608416595
	.quad	266718509671728212
	.quad	1306420176630260372
	.quad	333398137089660265
	.quad	1633025220787825465
	.quad	5028433689789463235
	.quad	2041281525984781831
	.quad	-8386443989950055238
	.quad	1275800953740488644
	.quad	-5871368969010181144
	.quad	1594751192175610805
	.quad	1884160825592049379
	.quad	1993438990219513507
	.quad	-1128242493218663091
	.quad	1245899368887195941
	.quad	7813068920331446945
	.quad	1557374211108994927
	.quad	5154650131986920777
	.quad	1946717763886243659
	.quad	915813323278131534
	.quad	1216698602428902287
	.quad	-3466919364329723487
	.quad	1520873253036127858
	.quad	-8945335223839542262
	.quad	1901091566295159823
	.quad	-5590834514899713914
	.quad	1188182228934474889
	.quad	2234828893230133415
	.quad	1485227786168093612
	.quad	2793536116537666769
	.quad	1856534732710117015
	.quad	8663489100477123587
	.quad	1160334207943823134
	.quad	1605989338741628675
	.quad	1450417759929778918
	.quad	-7215885363427739964
	.quad	1813022199912223647
	.quad	-9019856704284674954
	.quad	2266277749890279559
	.quad	-5637410440177921847
	.quad	1416423593681424724
	.quad	-2435077031795014404
	.quad	1770529492101780905
	.quad	6179525747111007803
	.quad	2213161865127226132
	.quad	-5361168444910395931
	.quad	1383226165704516332
	.quad	-2089774537710607010
	.quad	1729032707130645415
	.quad	-2612218172138258762
	.quad	2161290883913306769
	.quad	2979049660840976177
	.quad	1350806802445816731
	.quad	-887873942376167682
	.quad	1688508503057270913
	.quad	8113529608884566205
	.quad	2110635628821588642
	.quad	-8764102049729309834
	.quad	1319147268013492901
	.quad	-1731755525306861484
	.quad	1648934085016866126
	.quad	-6776380425060964759
	.quad	2061167606271082658
	.quad	-6541080774876796927
	.quad	1288229753919426661
	.quad	1047021068258779650
	.quad	1610287192399283327
	.quad	-3302909683103913342
	.quad	2012858990499104158
	.quad	4853210475701136017
	.quad	1258036869061940099
	.quad	1454827076199032118
	.quad	1572546086327425124
	.quad	1818533845248790147
	.quad	1965682607909281405
	.quad	3442426662494187794
	.quad	1228551629943300878
	.quad	-4920338708737041066
	.quad	1535689537429126097
	.quad	3072948650933474476
	.quad	1919611921786407622
	.quad	-2691093111593966357
	.quad	1199757451116504763
	.quad	-3363866389492457946
	.quad	1499696813895630954
	.quad	-8816519005292960336
	.quad	1874621017369538693
	.quad	8324733676974063502
	.quad	1171638135855961683
	.quad	5794231077790191473
	.quad	1464547669819952104
	.quad	7242788847237739342
	.quad	1830684587274940130
	.quad	-169885977807601630
	.quad	2288355734093675162
	.quad	-2412021745343444971
	.quad	1430222333808546976
	.quad	1596658836748081690
	.quad	1787777917260683721
	.quad	6607509564362490017
	.quad	2234722396575854651
	.quad	1823850468512862308
	.quad	1396701497859909157
	.quad	6891499104068465790
	.quad	1745876872324886446
	.quad	-608998156769193571
	.quad	2182346090406108057
	.quad	4231062170446641922
	.quad	1363966306503817536
	.quad	5288827713058302403
	.quad	1704957883129771920
	.quad	6611034641322878003
	.quad	2131197353912214900
	.quad	-5091475386027977056
	.quad	1331998346195134312
	.quad	-1752658214107583416
	.quad	1664997932743917890
	.quad	-6802508786061867174
	.quad	2081247415929897363
	.quad	4971804045566108824
	.quad	1300779634956185852
	.quad	6214755056957636030
	.quad	1625974543695232315
	.quad	3156757802769657134
	.quad	2032468179619040394
	.quad	6584659645158423613
	.quad	1270292612261900246
	.quad	-992547480406746292
	.quad	1587865765327375307
	.quad	-1240684350508432865
	.quad	1984832206659219134
	.quad	6142101308573311315
	.quad	1240520129162011959
	.quad	3065940617289251240
	.quad	1550650161452514949
	.quad	8444111790038951954
	.quad	1938312701815643686
	.quad	665883850346957067
	.quad	1211445438634777304
	.quad	832354812933696334
	.quad	1514306798293471630
	.quad	-8182928520687655390
	.quad	1892883497866839537
	.quad	-502644307002396715
	.quad	1183052186166774710
	.quad	-5239991402180383798
	.quad	1478815232708468388
	.quad	-1938303234298091843
	.quad	1848519040885585485
	.quad	-5823125539863695306
	.quad	1155324400553490928
	.quad	-2667220906402231229
	.quad	1444155500691863660
	.quad	1277659885424598868
	.quad	1805194375864829576
	.quad	1597074856780748586
	.quad	2256492969831036970
	.quad	5609857803915355770
	.quad	1410308106144398106
	.quad	-2211049781960581095
	.quad	1762885132680497632
	.quad	1847873790976661535
	.quad	2203606415850622041
	.quad	-5762607908280668397
	.quad	1377254009906638775
	.quad	-7203259885350835496
	.quad	1721567512383298469
	.quad	219297180166231438
	.quad	2151959390479123087
	.quad	7054589765244976505
	.quad	1344974619049451929
	.quad	-5016820848725943081
	.quad	1681218273811814911
	.quad	-6271026060907428851
	.quad	2101522842264768639
	.quad	-3919391288067143032
	.quad	1313451776415480399
	.quad	-4899239110083928790
	.quad	1641814720519350499
	.quad	-6124048887604910988
	.quad	2052268400649188124
	.quad	-1521687545539375415
	.quad	1282667750405742577
	.quad	7321262604930556539
	.quad	1603334688007178222
	.quad	-71793780691580134
	.quad	2004168360008972777
	.quad	4566814905495150320
	.quad	1252605225005607986
	.quad	-3514853404985837908
	.quad	1565756531257009982
	.quad	-9005252774659685289
	.quad	1957195664071262478
	.quad	1289246043478778550
	.quad	1223247290044539049
	.quad	6223243572775861092
	.quad	1529059112555673811
	.quad	3167368447542438461
	.quad	1911323890694592264
	.quad	1979605279714024038
	.quad	1194577431684120165
	.quad	7086192618069917952
	.quad	1493221789605150206
	.quad	-365631264267378368
	.quad	1866527237006437757
	.quad	-4840205558594499384
	.quad	1166579523129023598
	.quad	7784801107039039482
	.quad	1458224403911279498
	.quad	507629346944023544
	.quad	1822780504889099373
	.quad	5246222702107417334
	.quad	2278475631111374216
	.quad	3278889188817135834
	.quad	1424047269444608885
	.quad	8710297504448807696
	.quad	1780059086805761106
	.globl	_ZN7fast_io7details3ryu4pow5IdLb1EE9inv_splitE
	.section	.rdata$_ZN7fast_io7details3ryu4pow5IdLb1EE9inv_splitE,"dr"
	.linkonce same_size
	.align 32
_ZN7fast_io7details3ryu4pow5IdLb1EE9inv_splitE:
	.quad	1
	.quad	2305843009213693952
	.quad	-7378697629483820646
	.quad	1844674407370955161
	.quad	5165088340638674453
	.quad	1475739525896764129
	.quad	7821419487252849886
	.quad	1180591620717411303
	.quad	8824922364862649494
	.quad	1888946593147858085
	.quad	7059937891890119595
	.quad	1511157274518286468
	.quad	-5420096130713635294
	.quad	1208925819614629174
	.quad	-8672153809141816470
	.quad	1934281311383406679
	.quad	-6937723047313453176
	.quad	1547425049106725343
	.quad	-1860829623108852217
	.quad	1237940039285380274
	.quad	-2977327396974163548
	.quad	1980704062856608439
	.quad	-2381861917579330838
	.quad	1584563250285286751
	.quad	9162556910162266299
	.quad	1267650600228229401
	.quad	7281393426775805432
	.quad	2028240960365167042
	.quad	-1553582888063176301
	.quad	1622592768292133633
	.quad	2446482504291369283
	.quad	1298074214633706907
	.quad	7603720821608101175
	.quad	2076918743413931051
	.quad	2393627842544570617
	.quad	1661534994731144841
	.quad	-1774446540706253830
	.quad	1329227995784915872
	.quad	-6528463279871916451
	.quad	2126764793255865396
	.quad	5845275820328197809
	.quad	1701411834604692317
	.quad	-2702476973221262399
	.quad	1361129467683753853
	.quad	3054734472329800808
	.quad	2177807148294006166
	.quad	-1245561236878069677
	.quad	1742245718635204932
	.quad	6382248639981364905
	.quad	1393796574908163946
	.quad	2832900194486363201
	.quad	2230074519853062314
	.quad	5955668970331000884
	.quad	1784059615882449851
	.quad	1075186361522890384
	.quad	1427247692705959881
	.quad	-5658399451047196032
	.quad	2283596308329535809
	.quad	-4526719560837756825
	.quad	1826877046663628647
	.quad	3757321980813615186
	.quad	1461501637330902918
	.quad	-8062188859574838821
	.quad	1169201309864722334
	.quad	5547241898389809503
	.quad	1870722095783555735
	.quad	4437793518711847602
	.quad	1496577676626844588
	.quad	-7517811629256252888
	.quad	1197262141301475670
	.quad	-960452162584273651
	.quad	1915619426082361072
	.quad	6610335899416401726
	.quad	1532495540865888858
	.quad	-5779777724692609589
	.quad	1225996432692711086
	.quad	-5558295544766265019
	.quad	1961594292308337738
	.quad	-757287621071101692
	.quad	1569275433846670190
	.quad	-4295178911598791677
	.quad	1255420347077336152
	.quad	7885109000409574610
	.quad	2008672555323737844
	.quad	-8449308058639981605
	.quad	1606938044258990275
	.quad	7997948812055656009
	.quad	1285550435407192220
	.quad	-5650025974420502002
	.quad	2056880696651507552
	.quad	2858676849947419045
	.quad	1645504557321206042
	.quad	-5091756149525885410
	.quad	1316403645856964833
	.quad	-768112209757596011
	.quad	2106245833371143733
	.quad	3074859046935833515
	.quad	1684996666696914987
	.quad	-4918810391935153834
	.quad	1347997333357531989
	.quad	-7870096627096246135
	.quad	2156795733372051183
	.quad	-2606728486935086585
	.quad	1725436586697640946
	.quad	8982663654677661702
	.quad	1380349269358112757
	.quad	-385133411483382570
	.quad	2208558830972980411
	.quad	-7686804358670526703
	.quad	1766847064778384329
	.quad	-6149443486936421362
	.quad	1413477651822707463
	.quad	-2460411949614453533
	.quad	2261564242916331941
	.quad	9099716884534168143
	.quad	1809251394333065553
	.quad	-3788272936598396455
	.quad	1447401115466452442
	.quad	4348079280205103483
	.quad	1157920892373161954
	.quad	-4111119595897565398
	.quad	1852673427797059126
	.quad	7779150767507678651
	.quad	1482138742237647301
	.quad	2533971799264232598
	.quad	1185710993790117841
	.quad	-3324342750661048490
	.quad	1897137590064188545
	.quad	-6348823015270749115
	.quad	1517710072051350836
	.quad	5988988032009131678
	.quad	1214168057641080669
	.quad	-1485665593011120286
	.quad	1942668892225729070
	.quad	-4877881289150806552
	.quad	1554135113780583256
	.quad	7165741412905085728
	.quad	1243308091024466605
	.quad	-6981557813061414451
	.quad	1989292945639146568
	.quad	-1895897435707221237
	.quad	1591434356511317254
	.quad	-1516717948565776990
	.quad	1273147485209053803
	.quad	4951948911778577463
	.quad	2037035976334486086
	.quad	272210314680951647
	.quad	1629628781067588869
	.quad	3907117066486671641
	.quad	1303703024854071095
	.quad	6251387306378674625
	.quad	2085924839766513752
	.quad	-2377587784380880946
	.quad	1668739871813211001
	.quad	9165976216721026213
	.quad	1334991897450568801
	.quad	7286864317269821294
	.quad	2135987035920910082
	.quad	-1549206175667963611
	.quad	1708789628736728065
	.quad	-4928713755276281212
	.quad	1367031702989382452
	.quad	6871453250525591353
	.quad	2187250724783011924
	.quad	9186511415162383406
	.quad	1749800579826409539
	.quad	-7408186126837734568
	.quad	1399840463861127631
	.quad	-8163748988198464986
	.quad	2239744742177804210
	.quad	8226396068408869304
	.quad	1791795793742243368
	.quad	-4486929589498635526
	.quad	1433436634993794694
	.quad	-7179087343197816842
	.quad	2293498615990071511
	.quad	5324776569667477496
	.quad	1834798892792057209
	.quad	7949170070475892320
	.quad	1467839114233645767
	.quad	-1019361573103106790
	.quad	1174271291386916613
	.quad	5747719112518849781
	.quad	1878834066219066582
	.quad	-2780522339468740821
	.quad	1503067252975253265
	.quad	-5913766686316902980
	.quad	1202453802380202612
	.quad	5295368560860596524
	.quad	1923926083808324180
	.quad	4236294848688477220
	.quad	1539140867046659344
	.quad	7078384693692692099
	.quad	1231312693637327475
	.quad	-7121328563801244258
	.quad	1970100309819723960
	.quad	9060332407926645887
	.quad	1576080247855779168
	.quad	-3819780517884414260
	.quad	1260864198284623334
	.quad	-6111648828615062817
	.quad	2017382717255397335
	.quad	-8578667877633960576
	.quad	1613906173804317868
	.quad	-3173585487365258138
	.quad	1291124939043454294
	.quad	-5077736779784413021
	.quad	2065799902469526871
	.quad	7005857020398200553
	.quad	1652639921975621497
	.quad	-1774012013165260204
	.quad	1322111937580497197
	.quad	-6527768035806326650
	.quad	2115379100128795516
	.quad	5845832015580669650
	.quad	1692303280103036413
	.quad	-6391380831761195250
	.quad	1353842624082429130
	.quad	841837113407818570
	.quad	2166148198531886609
	.quad	4362818505468165179
	.quad	1732918558825509287
	.quad	-3888442825109288503
	.quad	1386334847060407429
	.quad	-6221508520174861605
	.quad	2218135755296651887
	.quad	2401490813343931363
	.quad	1774508604237321510
	.quad	1921192650675145090
	.quad	1419606883389857208
	.quad	-615440573661678179
	.quad	2271371013423771532
	.quad	6886345170554478103
	.quad	1817096810739017226
	.quad	1819727321701672159
	.quad	1453677448591213781
	.quad	-2233566957380572596
	.quad	1162941958872971024
	.quad	-3573707131808916153
	.quad	1860707134196753639
	.quad	-2858965705447132922
	.quad	1488565707357402911
	.quad	8780873879868024632
	.quad	1190852565885922329
	.quad	2981351763563108441
	.quad	1905364105417475727
	.quad	-4993616218633333894
	.quad	1524291284333980581
	.quad	7073153469319063855
	.quad	1219433027467184465
	.quad	-7129698522799049449
	.quad	1951092843947495144
	.quad	-5703758818239239559
	.quad	1560874275157996115
	.quad	-8252355869333301970
	.quad	1248699420126396892
	.quad	1553625868034358140
	.quad	1997919072202235028
	.quad	8621598323911307159
	.quad	1598335257761788022
	.quad	-481418970354774919
	.quad	1278668206209430417
	.quad	-4459619167309550194
	.quad	2045869129935088668
	.quad	121653480894270168
	.quad	1636695303948070935
	.quad	97322784715416134
	.quad	1309356243158456748
	.quad	-3533632359197244509
	.quad	2094969989053530796
	.quad	8241140556867935363
	.quad	1675975991242824637
	.quad	-785785183989472356
	.quad	1340780792994259709
	.quad	-1257256294383155770
	.quad	2145249268790815535
	.quad	-4695153850248434939
	.quad	1716199415032652428
	.quad	-66774265456837628
	.quad	1372959532026121942
	.quad	-3796187639472850528
	.quad	2196735251241795108
	.quad	652398703163629901
	.quad	1757388200993436087
	.quad	-6856778666952916726
	.quad	1405910560794748869
	.quad	7475898206584884855
	.quad	2249456897271598191
	.quad	2291369750525997561
	.quad	1799565517817278553
	.quad	9211793429904618695
	.quad	1439652414253822842
	.quad	-18525771120251381
	.quad	2303443862806116547
	.quad	7363877012587619542
	.quad	1842755090244893238
	.quad	-5176944834155635336
	.quad	1474204072195914590
	.quad	-7830904682066418592
	.quad	1179363257756731672
	.quad	2227947767661371545
	.quad	1886981212410770676
	.quad	-1906990600612813087
	.quad	1509584969928616540
	.quad	-5214941295232160793
	.quad	1207667975942893232
	.quad	6413489186596184024
	.quad	1932268761508629172
	.quad	-2247906280206873427
	.quad	1545815009206903337
	.quad	5580372605318321905
	.quad	1236652007365522670
	.quad	8928596168509315048
	.quad	1978643211784836272
	.quad	-235820694676368608
	.quad	1582914569427869017
	.quad	7190041073742725760
	.quad	1266331655542295214
	.quad	436019273762630246
	.quad	2026130648867672343
	.quad	7727513048493924843
	.quad	1620904519094137874
	.quad	-8575384820172501418
	.quad	1296723615275310299
	.quad	4726128361433549347
	.quad	2074757784440496479
	.quad	7470251503888749801
	.quad	1659806227552397183
	.quad	-5091845241114731129
	.quad	1327844982041917746
	.quad	-4457603571041659483
	.quad	2124551971267068394
	.quad	-3566082856833327587
	.quad	1699641577013654715
	.quad	-6542215100208572392
	.quad	1359713261610923772
	.quad	4289851098633925465
	.quad	2175541218577478036
	.quad	-257467935834769951
	.quad	1740432974861982428
	.quad	3483374466074094362
	.quad	1392346379889585943
	.quad	1884050330976640656
	.quad	2227754207823337509
	.quad	5196589079523222848
	.quad	1782203366258670007
	.quad	-3221426365865242368
	.quad	1425762693006936005
	.quad	5913764258841343181
	.quad	2281220308811097609
	.quad	8420360221814984868
	.quad	1824976247048878087
	.quad	-642409452031832752
	.quad	1459980997639102469
	.quad	-513927561625466201
	.quad	1167984798111281975
	.quad	-8200981728084566569
	.quad	1868775676978051161
	.quad	4507261061758077715
	.quad	1495020541582440929
	.quad	7295157664148372495
	.quad	1196016433265952743
	.quad	7982903447895485668
	.quad	1913626293225524389
	.quad	-8371072500651252758
	.quad	1530901034580419511
	.quad	4371188443704728763
	.quad	1224720827664335609
	.quad	-4074144934298164949
	.quad	1959553324262936974
	.quad	-3259315947438531959
	.quad	1567642659410349579
	.quad	-2607452757950825567
	.quad	1254114127528279663
	.quad	3206773216762499739
	.quad	2006582604045247462
	.quad	-4813279056073820855
	.quad	1605266083236197969
	.quad	-3850623244859056684
	.quad	1284212866588958375
	.quad	4907049252451240275
	.quad	2054740586542333401
	.quad	236290587219081897
	.quad	1643792469233866721
	.quad	-3500316344966644806
	.quad	1315033975387093376
	.quad	-1911157337204721366
	.quad	2104054360619349402
	.quad	5849771759720043554
	.quad	1683243488495479522
	.quad	-2698880221707785803
	.quad	1346594790796383617
	.quad	-8007557169474367609
	.quad	2154551665274213788
	.quad	-2716696920837583764
	.quad	1723641332219371030
	.quad	-5862706351411977334
	.quad	1378913065775496824
	.quad	9066413911450387881
	.quad	2206260905240794919
	.quad	-7504264129807330988
	.quad	1765008724192635935
	.quad	8753983955121776503
	.quad	1412006979354108748
	.quad	-8129718560256619535
	.quad	2259211166966573997
	.quad	874922781278525018
	.quad	1807368933573259198
	.quad	8078635854506640661
	.quad	1445895146858607358
	.quad	-4605137760620418441
	.quad	1156716117486885886
	.quad	-3678871602250759182
	.quad	1850745787979017418
	.quad	746251532941302978
	.quad	1480596630383213935
	.quad	597001226353042382
	.quad	1184477304306571148
	.quad	-2734146852577042512
	.quad	1895163686890513836
	.quad	8880728962164096960
	.quad	1516130949512411069
	.quad	-7652812089236363725
	.quad	1212904759609928855
	.quad	-1176452898552450990
	.quad	1940647615375886168
	.quad	2748186495899949531
	.quad	1552518092300708935
	.quad	2198549196719959625
	.quad	1242014473840567148
	.quad	-171670099989974923
	.quad	1987223158144907436
	.quad	-7516033709475800585
	.quad	1589778526515925949
	.quad	-6012826967580640468
	.quad	1271822821212740759
	.quad	8826220925580526867
	.quad	2034916513940385215
	.quad	7060976740464421494
	.quad	1627933211152308172
	.quad	-1729916237112283451
	.quad	1302346568921846537
	.quad	-6457214794121563846
	.quad	2083754510274954460
	.quad	-8855120650039161400
	.quad	1667003608219963568
	.quad	-3394747705289418796
	.quad	1333602886575970854
	.quad	-5431596328463070074
	.quad	2133764618521553367
	.quad	3033420566713364587
	.quad	1707011694817242694
	.quad	6116085268112601993
	.quad	1365609355853794155
	.quad	-8661007644729388428
	.quad	2184974969366070648
	.quad	-3239457301041600419
	.quad	1747979975492856518
	.quad	1097782973908629988
	.quad	1398383980394285215
	.quad	1756452758253807981
	.quad	2237414368630856344
	.quad	5094511021344956708
	.quad	1789931494904685075
	.quad	4075608817075965366
	.quad	1431945195923748060
	.quad	6520974107321544586
	.quad	2291112313477996896
	.quad	1527430471115325346
	.quad	1832889850782397517
	.quad	-6156753252591560370
	.quad	1466311880625918013
	.quad	-1236053787331337972
	.quad	1173049504500734410
	.quad	9090360384495590213
	.quad	1876879207201175057
	.quad	-106409321887348476
	.quad	1501503365760940045
	.quad	-3774476272251789104
	.quad	1201202692608752036
	.quad	-2349813220860952243
	.quad	1921924308174003258
	.quad	1809498238053148529
	.quad	1537539446539202607
	.quad	-5931099039041301823
	.quad	1230031557231362085
	.quad	1578287981759648052
	.quad	1968050491570179337
	.quad	-6116067244076102204
	.quad	1574440393256143469
	.quad	-4892853795260881763
	.quad	1259552314604914775
	.quad	3239480371808320148
	.quad	2015283703367863641
	.quad	-1097764517295254205
	.quad	1612226962694290912
	.quad	6500486015647617283
	.quad	1289781570155432730
	.quad	-8045966448673363964
	.quad	2063650512248692368
	.quad	-2747424344196780848
	.quad	1650920409798953894
	.quad	-2197939475357424678
	.quad	1320736327839163115
	.quad	7551343283653851484
	.quad	2113178124542660985
	.quad	6041074626923081187
	.quad	1690542499634128788
	.quad	-6235186742687266020
	.quad	1352433999707303030
	.quad	1091747655926105338
	.quad	2163894399531684849
	.quad	4562746939482794594
	.quad	1731115519625347879
	.quad	7339546366328145998
	.quad	1384892415700278303
	.quad	8053925371383123274
	.quad	2215827865120445285
	.quad	6443140297106498619
	.quad	1772662292096356228
	.quad	-5913534206540532074
	.quad	1418129833677084982
	.quad	5295740528502789974
	.quad	2269007733883335972
	.quad	-3142105206681588667
	.quad	1815206187106668777
	.quad	4865013464138549713
	.quad	1452164949685335022
	.quad	-3486686858172980876
	.quad	1161731959748268017
	.quad	9178696285890871890
	.quad	1858771135597228828
	.quad	-3725089415513033457
	.quad	1487016908477783062
	.quad	4398626097073393881
	.quad	1189613526782226450
	.quad	7037801755317430209
	.quad	1903381642851562320
	.quad	5630241404253944167
	.quad	1522705314281249856
	.quad	814844308661245011
	.quad	1218164251424999885
	.quad	1303750893857992017
	.quad	1949062802279999816
	.quad	-2646348099655516710
	.quad	1559250241823999852
	.quad	5261619149759407279
	.quad	1247400193459199882
	.quad	-6338804619352589647
	.quad	1995840309534719811
	.quad	5997002748743659252
	.quad	1596672247627775849
	.quad	8486951013736837725
	.quad	1277337798102220679
	.quad	2511075177753209390
	.quad	2043740476963553087
	.quad	-5369837487281253134
	.quad	1634992381570842469
	.quad	-4295869989825002507
	.quad	1307993905256673975
	.quad	4194654460505726958
	.quad	2092790248410678361
	.quad	-333625246337328757
	.quad	1674232198728542688
	.quad	3422448617672047318
	.quad	1339385758982834151
	.quad	-1902779841208544938
	.quad	2143017214372534641
	.quad	-8900921502450656597
	.quad	1714413771498027713
	.quad	-3431388387218614954
	.quad	1371531017198422170
	.quad	5577825024675947042
	.quad	2194449627517475473
	.quad	-6605786424484973336
	.quad	1755559702013980378
	.quad	-1595280324846068345
	.quad	1404447761611184302
	.quad	-6241797334495619676
	.quad	2247116418577894884
	.quad	-4993437867596495741
	.quad	1797693134862315907
	.quad	3383947335406624054
	.quad	1438154507889852726
	.quad	-1964381892833222160
	.quad	2301047212623764361
	.quad	-8950203143750398374
	.quad	1840837770099011489
	.quad	-7160162515000318699
	.quad	1472670216079209191
	.quad	5339916432225476010
	.quad	1178136172863367353
	.quad	4854517476818851293
	.quad	1885017876581387765
	.quad	3883613981455081034
	.quad	1508014301265110212
	.quad	-4271806444319755819
	.quad	1206411441012088169
	.quad	-6834890310911609310
	.quad	1930258305619341071
	.quad	5600134195496443521
	.quad	1544206644495472857
	.quad	-2898590273086665829
	.quad	1235365315596378285
	.quad	6430302007287065643
	.quad	1976584504954205257
	.quad	-2234456023654168132
	.quad	1581267603963364205
	.quad	-5476913633665244829
	.quad	1265014083170691364
	.quad	-8763061813864391727
	.quad	2024022533073106183
	.quad	-3321100636349603058
	.quad	1619218026458484946
	.quad	8411165935146048523
	.quad	1295374421166787957
	.quad	-1299529762733963656
	.quad	2072599073866860731
	.quad	-8418321439670991571
	.quad	1658079259093488585
	.quad	8022738107230848036
	.quad	1326463407274790868
	.quad	9147032156827446534
	.quad	2122341451639665389
	.quad	-7439769533505684065
	.quad	1697873161311732311
	.quad	5116230817421183718
	.quad	1358298529049385849
	.quad	-2882077136351837022
	.quad	2173277646479017358
	.quad	1383687105660440706
	.quad	1738622117183213887
	.quad	-6271747944955468082
	.quad	1390897693746571109
	.quad	8411947361780802685
	.quad	2225436309994513775
	.quad	6729557889424642148
	.quad	1780349047995611020
	.quad	5383646311539713719
	.quad	1424279238396488816
	.quad	1235136468979721303
	.quad	2278846781434382106
	.quad	-2701239639558133281
	.quad	1823077425147505684
	.quad	-2160991711646506624
	.quad	1458461940118004547
	.quad	5649904260166615347
	.quad	1166769552094403638
	.quad	5350498001524674232
	.quad	1866831283351045821
	.quad	591049586477829062
	.quad	1493465026680836657
	.quad	-6905857960301557397
	.quad	1194772021344669325
	.quad	18673707743239135
	.quad	1911635234151470921
	.quad	-3674409848547319015
	.quad	1529308187321176736
	.quad	8128518565387875758
	.quad	1223446549856941389
	.quad	1937583260394870242
	.quad	1957514479771106223
	.quad	8928764237799716840
	.quad	1566011583816884978
	.quad	-3925035053985957497
	.quad	1252809267053507982
	.quad	8477339172590109297
	.quad	2004494827285612772
	.quad	-596826291411733209
	.quad	1603595861828490217
	.quad	6901236596354434079
	.quad	1282876689462792174
	.quad	-26067890058636443
	.quad	2052602703140467478
	.quad	3668494502695001169
	.quad	1642082162512373983
	.quad	-8133250842069730034
	.quad	1313665730009899186
	.quad	9122891541139893884
	.quad	2101865168015838698
	.quad	-3769733211313815862
	.quad	1681492134412670958
	.quad	673562245690857633
	.quad	1345193707530136767
	.globl	_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE
	.section	.rdata$_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE,"dr"
	.linkonce same_size
	.align 32
_ZN7fast_io7details24shared_static_base_tableIcLy10ELb0ELb0EE5tableE:
	.byte	48
	.byte	48
	.byte	48
	.byte	49
	.byte	48
	.byte	50
	.byte	48
	.byte	51
	.byte	48
	.byte	52
	.byte	48
	.byte	53
	.byte	48
	.byte	54
	.byte	48
	.byte	55
	.byte	48
	.byte	56
	.byte	48
	.byte	57
	.byte	49
	.byte	48
	.byte	49
	.byte	49
	.byte	49
	.byte	50
	.byte	49
	.byte	51
	.byte	49
	.byte	52
	.byte	49
	.byte	53
	.byte	49
	.byte	54
	.byte	49
	.byte	55
	.byte	49
	.byte	56
	.byte	49
	.byte	57
	.byte	50
	.byte	48
	.byte	50
	.byte	49
	.byte	50
	.byte	50
	.byte	50
	.byte	51
	.byte	50
	.byte	52
	.byte	50
	.byte	53
	.byte	50
	.byte	54
	.byte	50
	.byte	55
	.byte	50
	.byte	56
	.byte	50
	.byte	57
	.byte	51
	.byte	48
	.byte	51
	.byte	49
	.byte	51
	.byte	50
	.byte	51
	.byte	51
	.byte	51
	.byte	52
	.byte	51
	.byte	53
	.byte	51
	.byte	54
	.byte	51
	.byte	55
	.byte	51
	.byte	56
	.byte	51
	.byte	57
	.byte	52
	.byte	48
	.byte	52
	.byte	49
	.byte	52
	.byte	50
	.byte	52
	.byte	51
	.byte	52
	.byte	52
	.byte	52
	.byte	53
	.byte	52
	.byte	54
	.byte	52
	.byte	55
	.byte	52
	.byte	56
	.byte	52
	.byte	57
	.byte	53
	.byte	48
	.byte	53
	.byte	49
	.byte	53
	.byte	50
	.byte	53
	.byte	51
	.byte	53
	.byte	52
	.byte	53
	.byte	53
	.byte	53
	.byte	54
	.byte	53
	.byte	55
	.byte	53
	.byte	56
	.byte	53
	.byte	57
	.byte	54
	.byte	48
	.byte	54
	.byte	49
	.byte	54
	.byte	50
	.byte	54
	.byte	51
	.byte	54
	.byte	52
	.byte	54
	.byte	53
	.byte	54
	.byte	54
	.byte	54
	.byte	55
	.byte	54
	.byte	56
	.byte	54
	.byte	57
	.byte	55
	.byte	48
	.byte	55
	.byte	49
	.byte	55
	.byte	50
	.byte	55
	.byte	51
	.byte	55
	.byte	52
	.byte	55
	.byte	53
	.byte	55
	.byte	54
	.byte	55
	.byte	55
	.byte	55
	.byte	56
	.byte	55
	.byte	57
	.byte	56
	.byte	48
	.byte	56
	.byte	49
	.byte	56
	.byte	50
	.byte	56
	.byte	51
	.byte	56
	.byte	52
	.byte	56
	.byte	53
	.byte	56
	.byte	54
	.byte	56
	.byte	55
	.byte	56
	.byte	56
	.byte	56
	.byte	57
	.byte	57
	.byte	48
	.byte	57
	.byte	49
	.byte	57
	.byte	50
	.byte	57
	.byte	51
	.byte	57
	.byte	52
	.byte	57
	.byte	53
	.byte	57
	.byte	54
	.byte	57
	.byte	55
	.byte	57
	.byte	56
	.byte	57
	.byte	57
	.section .rdata,"dr"
	.align 8
.LC1:
	.long	-400107883
	.long	1041313291
	.weak	__cxa_pure_virtual
	.ident	"GCC: (GCC with MCF thread model, built by LH_Mouse.) 11.0.0 20200712 (experimental)"
	.def	_ZdaPv;	.scl	2;	.type	32;	.endef
	.def	_ZNSt9exceptionD2Ev;	.scl	2;	.type	32;	.endef
	.def	_ZdlPvy;	.scl	2;	.type	32;	.endef
	.def	memcpy;	.scl	2;	.type	32;	.endef
	.def	_Znwy;	.scl	2;	.type	32;	.endef
	.def	_ZSt17__throw_bad_allocv;	.scl	2;	.type	32;	.endef
	.def	_Znay;	.scl	2;	.type	32;	.endef
	.def	memmove;	.scl	2;	.type	32;	.endef
	.def	__cxa_begin_catch;	.scl	2;	.type	32;	.endef
	.def	__cxa_end_catch;	.scl	2;	.type	32;	.endef
	.def	memset;	.scl	2;	.type	32;	.endef
	.def	memmove;	.scl	2;	.type	32;	.endef
	.def	WriteFile;	.scl	2;	.type	32;	.endef
	.def	_Unwind_Resume;	.scl	2;	.type	32;	.endef
	.def	__cxa_allocate_exception;	.scl	2;	.type	32;	.endef
	.def	GetLastError;	.scl	2;	.type	32;	.endef
	.def	__cxa_throw;	.scl	2;	.type	32;	.endef
	.def	_ZNSt6chrono3_V212system_clock3nowEv;	.scl	2;	.type	32;	.endef
	.def	GetStdHandle;	.scl	2;	.type	32;	.endef
	.def	EnterCriticalSection;	.scl	2;	.type	32;	.endef
	.def	_unlock;	.scl	2;	.type	32;	.endef
	.def	_lock;	.scl	2;	.type	32;	.endef
	.def	LeaveCriticalSection;	.scl	2;	.type	32;	.endef
	.def	clearerr;	.scl	2;	.type	32;	.endef
	.def	fwrite;	.scl	2;	.type	32;	.endef
	.def	ferror;	.scl	2;	.type	32;	.endef
	.def	strerror;	.scl	2;	.type	32;	.endef
	.def	FormatMessageA;	.scl	2;	.type	32;	.endef
	.def	__cxa_pure_virtual;	.scl	2;	.type	32;	.endef
