
#include"../../include/fast_io.h"
#include"../../include/fast_io_device.h"
#include<vector>
#include"../timer.h"
#include"thread_local_pool.h"

int main()
{
	fast_io::timer t("vector");
	std::size_t sum{};
	for(std::size_t i{};i!=10000000;++i)
	{
		std::vector<char8_t> vec{1,2,3,4,5,6,7,8,9,10};
		std::vector<char8_t> vec2{1,2,3,4,5,6,7,8,9,10};
		sum+=vec.size();
		sum+=vec2.size();
	}
	println("sum is: ",sum);
}