	.file	"fast.cc"
	.text
	.def	__main;	.scl	2;	.type	32;	.endef
	.section	.text.startup,"x"
	.globl	main
	.def	main;	.scl	2;	.type	32;	.endef
	.seh_proc	main
main:
.LFB14375:
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	call	__main
	movq	64+_ZN7fast_io7details10allocation7bucketsE(%rip), %rsi
	xorl	%ebx, %ebx
	jmp	.L7
.L9:
	movq	%rax, %rsi
.L7:
	testq	%rsi, %rsi
	je	.L15
	movq	(%rsi), %rax
	movq	%rax, 64+_ZN7fast_io7details10allocation7bucketsE(%rip)
	movq	%rsi, %rax
.L5:
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	$0, 72(%rax)
	movq	64+_ZN7fast_io7details10allocation7bucketsE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 64+_ZN7fast_io7details10allocation7bucketsE(%rip)
	addq	$1, %rbx
	cmpq	$10000000, %rbx
	je	.L6
	testq	%rsi, %rsi
	je	.L9
.L8:
	movq	(%rax), %rdx
	movq	%rdx, 64+_ZN7fast_io7details10allocation7bucketsE(%rip)
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	$0, 72(%rax)
	movq	64+_ZN7fast_io7details10allocation7bucketsE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 64+_ZN7fast_io7details10allocation7bucketsE(%rip)
	addq	$1, %rbx
	cmpq	$10000000, %rbx
	jne	.L8
.L6:
	xorl	%eax, %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	ret
.L15:
	movq	72+_ZN7fast_io7details10allocation7bucketsE(%rip), %rax
	cmpq	80+_ZN7fast_io7details10allocation7bucketsE(%rip), %rax
	je	.L16
	leaq	128(%rax), %rdx
	movq	%rdx, 72+_ZN7fast_io7details10allocation7bucketsE(%rip)
	jmp	.L5
.L16:
	cmpq	$0, 88+_ZN7fast_io7details10allocation7bucketsE(%rip)
	jne	.L4
	movq	$4096, 88+_ZN7fast_io7details10allocation7bucketsE(%rip)
.L4:
	movq	88+_ZN7fast_io7details10allocation7bucketsE(%rip), %rdi
	call	GetProcessHeap
	movq	%rax, %rcx
	movq	%rdi, %r8
	xorl	%edx, %edx
	call	HeapAlloc
	movq	88+_ZN7fast_io7details10allocation7bucketsE(%rip), %rdx
	leaq	(%rax,%rdx), %rcx
	movq	%rcx, 80+_ZN7fast_io7details10allocation7bucketsE(%rip)
	addq	%rdx, %rdx
	movq	%rdx, 88+_ZN7fast_io7details10allocation7bucketsE(%rip)
	leaq	128(%rax), %rdx
	movq	%rdx, 72+_ZN7fast_io7details10allocation7bucketsE(%rip)
	jmp	.L5
	.seh_endproc
	.globl	_ZN7fast_io7details10allocation7bucketsE
	.section	.data$_ZN7fast_io7details10allocation7bucketsE,"w"
	.linkonce same_size
	.align 32
_ZN7fast_io7details10allocation7bucketsE:
	.space 1888
	.ident	"GCC: (GCC with MCF thread model, built by LH_Mouse.) 11.0.0 20200712 (experimental)"
	.def	GetProcessHeap;	.scl	2;	.type	32;	.endef
	.def	HeapAlloc;	.scl	2;	.type	32;	.endef
