	.file	"fast_thread_local.cc"
	.text
	.def	__main;	.scl	2;	.type	32;	.endef
	.section	.text.startup,"x"
	.globl	main
	.def	main;	.scl	2;	.type	32;	.endef
	.seh_proc	main
main:
.LFB14375:
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	call	__main
	leaq	__emutls_v._ZN7fast_io7details10allocation7bucketsE(%rip), %rcx
	call	__emutls_get_address
	movq	%rax, %rbx
	movq	64(%rax), %rdi
	xorl	%esi, %esi
	jmp	.L7
.L9:
	movq	%rax, %rdi
.L7:
	testq	%rdi, %rdi
	je	.L15
	movq	(%rdi), %rax
	movq	%rax, 64(%rbx)
	movq	%rdi, %rax
.L5:
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	$0, 72(%rax)
	movq	64(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 64(%rbx)
	addq	$1, %rsi
	cmpq	$10000000, %rsi
	je	.L6
	testq	%rdi, %rdi
	je	.L9
.L8:
	movq	(%rax), %rdx
	movq	%rdx, 64(%rbx)
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	$0, 72(%rax)
	movq	64(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 64(%rbx)
	addq	$1, %rsi
	cmpq	$10000000, %rsi
	jne	.L8
.L6:
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	ret
.L15:
	movq	72(%rbx), %rax
	cmpq	80(%rbx), %rax
	je	.L16
	leaq	128(%rax), %rdx
	movq	%rdx, 72(%rbx)
	jmp	.L5
.L16:
	cmpq	$0, 88(%rbx)
	jne	.L4
	movq	$4096, 88(%rbx)
.L4:
	movq	88(%rbx), %rbp
	call	GetProcessHeap
	movq	%rax, %rcx
	movq	%rbp, %r8
	xorl	%edx, %edx
	call	HeapAlloc
	movq	88(%rbx), %rdx
	leaq	(%rax,%rdx), %rcx
	movq	%rcx, 80(%rbx)
	addq	%rdx, %rdx
	movq	%rdx, 88(%rbx)
	leaq	128(%rax), %rdx
	movq	%rdx, 72(%rbx)
	jmp	.L5
	.seh_endproc
	.globl	__emutls_t._ZN7fast_io7details10allocation7bucketsE
	.section	.rdata$__emutls_t._ZN7fast_io7details10allocation7bucketsE,"dr"
	.linkonce same_size
	.align 32
__emutls_t._ZN7fast_io7details10allocation7bucketsE:
	.space 1888
	.globl	__emutls_v._ZN7fast_io7details10allocation7bucketsE
	.section	.data$__emutls_v._ZN7fast_io7details10allocation7bucketsE,"w"
	.linkonce same_size
	.align 32
__emutls_v._ZN7fast_io7details10allocation7bucketsE:
	.quad	1888
	.quad	8
	.quad	0
	.quad	__emutls_t._ZN7fast_io7details10allocation7bucketsE
	.ident	"GCC: (GCC with MCF thread model, built by LH_Mouse.) 11.0.0 20200712 (experimental)"
	.def	__emutls_get_address;	.scl	2;	.type	32;	.endef
	.def	GetProcessHeap;	.scl	2;	.type	32;	.endef
	.def	HeapAlloc;	.scl	2;	.type	32;	.endef
