#include"../../include/fast_io.h"


namespace fast_io
{

class win32_memory_map_io_observer
{
public:
	using native_handle_type = void*;
	void* handle{};
	std::byte *address_begin{},*address_end{};
	constexpr auto& native_handle() const noexcept
	{
		return handle;
	}
	constexpr auto& native_handle() noexcept
	{
		return handle;
	}
	constexpr std::size_t bytes() const noexcept
	{
		return address_end-address_begin;
	}
};

namespace win32::details
{

inline void* create_file_mapping_impl(void* handle,file_map_attribute attr)
{
	void* addr{win32::CreateFileMappingW(handle,nullptr,static_cast<std::uint32_t>(attr),0,0,nullptr)};
	if(addr==nullptr)
		throw_win32_error();
	return addr;
}

}


class win32_memory_map_file:public win32_memory_map_io_observer
{
public:
	using native_handle_type = void*;
	constexpr win32_memory_map_file()=default;
	win32_memory_map_file(void* handle,std::byte* address_begin,std::byte* address_end):win32_memory_map_io_observer{handle,address_begin,address_end}{}
	template<std::integral char_type>
	win32_memory_map_file(basic_win32_io_observer<char_type> bf,file_map_attribute attr,std::size_t bytes,std::size_t start_address=0):
		win32_memory_map_io_observer{win32::details::create_file_mapping_impl(bf.native_handle(),attr)}
	{
		void *base_ptr{win32::MapViewOfFile(this->native_handle(),static_cast<std::uint32_t>(to_win32_file_map_attribute(attr)),start_address>>32,static_cast<std::uint32_t>(start_address),bytes)};
		if(base_ptr==nullptr)
		{
			if(this->native_handle())
				win32::CloseHandle(this->native_handle());
			throw_win32_error();
		}
		this->address_begin=reinterpret_cast<std::byte*>(base_ptr);
		this->address_end=this->address_begin+bytes;
	}
	win32_memory_map_file(win32_memory_map_file const&)=delete;
	win32_memory_map_file& operator=(win32_memory_map_file const&)=delete;
	constexpr win32_memory_map_file(win32_memory_map_file&& other) noexcept:win32_memory_map_io_observer{other.handle,other.address_begin,other.address_end}
	{
		other.handle=nullptr;
		other.address_end=other.address_begin=nullptr;
	}
	win32_memory_map_file& operator=(win32_memory_map_file&& other) noexcept
	{
		if(std::addressof(other)==this)
			return *this;
		if(this->address_begin)[[likely]]
			win32::UnmapViewOfFile(this->address_begin);
		if(this->handle)
			win32::CloseHandle(this->handle);
		this->handle=other.handle;
		this->address_begin=other.address_begin;
		this->address_end=other.address_end;
		other.handle=nullptr;
		other.address_end=other.address_begin=nullptr;
		return *this;
	}
	void close() noexcept
	{
		if(this->address_begin)[[likely]]
		{
			win32::UnmapViewOfFile(this->address_begin);
			this->address_begin=nullptr;
		}
		if(this->handle)
		{
			win32::CloseHandle(this->handle);
			this->handle=nullptr;
		}
	}
	~win32_memory_map_file()
	{
		if(this->address_begin)[[likely]]
			win32::UnmapViewOfFile(this->address_begin);
		if(this->handle)
			win32::CloseHandle(this->handle);
	}
};

using native_memory_map_io_observer = win32_memory_map_io_observer;

template<std::integral ch_type>
class basic_omemory_map
{
public:
	using char_type = ch_type;
	char_type *begin_ptr{},*curr_ptr{},*end_ptr{};
	constexpr basic_omemory_map() = default;
	basic_omemory_map(native_memory_map_io_observer iob,std::size_t offset=0):begin_ptr(reinterpret_cast<char_type*>(iob.address_begin+offset)),curr_ptr(this->begin_ptr),end_ptr(this->begin_ptr+iob.bytes()/sizeof(char_type)){}

	constexpr std::size_t written_bytes() const noexcept
	{
		return static_cast<std::size_t>(curr_ptr-begin_ptr)*sizeof(char_type);
	}
};

namespace details
{

template<std::integral char_type>
inline constexpr void write(basic_omemory_map<char_type>& bomp,char_type const* begin,char_type const* end)
{
	std::size_t const to_write(end-begin);
	non_overlapped_copy_n(begin,to_write,bomp.curr_ptr);
	bomp.curr_ptr+=to_write;
}
}

template<std::integral char_type,std::contiguous_iterator Iter>
constexpr void write(basic_omemory_map<char_type>& bomp,Iter begin,Iter end)
{
	details::write(bomp,std::to_address(begin),std::to_address(end));
}

template<std::integral char_type>
constexpr char_type* obuffer_begin(basic_omemory_map<char_type>& bomp)
{
	return bomp.begin_ptr;
}
template<std::integral char_type>
constexpr char_type* obuffer_curr(basic_omemory_map<char_type>& bomp)
{
	return bomp.curr_ptr;
}
template<std::integral char_type>
constexpr char_type* obuffer_end(basic_omemory_map<char_type>& bomp)
{
	return bomp.end_ptr;
}

template<std::integral char_type>
constexpr void overflow(basic_omemory_map<char_type>& bomp,char_type)
{
//ub for overflow
}

template<std::integral char_type>
constexpr void obuffer_set_curr(basic_omemory_map<char_type>& bomp,char_type* ptr)
{
	bomp.curr_ptr=ptr;
}

using omemory_map = basic_omemory_map<char>;

}

int main()
{
	fast_io::native_file nfl("w.txt",fast_io::open_mode::in|fast_io::open_mode::out|fast_io::open_mode::trunc|fast_io::open_mode::creat);
	truncate(nfl,20000000ull);
	fast_io::win32_memory_map_file mmf(nfl,fast_io::file_map_attribute::read_write,20000000ull,0);
	fast_io::basic_omemory_map<char> bomp(mmf);
	print(bomp,"Hello World\n");
	mmf.close();
	truncate(nfl,bomp.written_bytes());
}