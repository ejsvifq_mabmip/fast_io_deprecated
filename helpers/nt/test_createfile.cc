#include"../../include/fast_io.h"
#include <Windows.h>
#undef interface
#undef min
#undef max
#undef unix
#include <winternl.h>
#include <string>
#include <iostream>
#include <fstream>

/*
https://github.com/cyllenius/testcreatefile/blob/master/testcreatefile.cpp
*/

extern "C" typedef NTSYSAPI NTSTATUS(NTAPI *PNtCreateFile)(
   PHANDLE            FileHandle,
   ACCESS_MASK        DesiredAccess,
   POBJECT_ATTRIBUTES ObjectAttributes,
   PIO_STATUS_BLOCK  IoStatusBlock,
   PLARGE_INTEGER     AllocationSize,
   ULONG              FileAttributes,
   ULONG              ShareAccess,
   ULONG              CreateDisposition,
   ULONG              CreateOptions,
   PVOID              EaBuffer,
   ULONG              EaLength
   );

extern "C" typedef NTSYSAPI VOID(NTAPI *PRtlInitUnicodeString)(
   PUNICODE_STRING DestinationString,
   PCWSTR SourceString
   );

inline void test_ntcreatefile(std::wstring_view filename)
{
	using namespace std;
   HMODULE hNtDll = GetModuleHandleW(L"ntdll.dll");
   auto aNtCreateFile = (PNtCreateFile) GetProcAddress(hNtDll, "NtCreateFile");
   auto aRtlInitUnicodeString = (PRtlInitUnicodeString) GetProcAddress(hNtDll, "RtlInitUnicodeString");

   PCWSTR filePath = L"\\??\\\\D:\\hg\\fast_io\\helpers\\nt\\w.txt";
   UNICODE_STRING unicodeString;
   aRtlInitUnicodeString(&unicodeString, filePath);

   OBJECT_ATTRIBUTES objAttribs = { 0 };
   InitializeObjectAttributes(&objAttribs, &unicodeString, OBJ_CASE_INSENSITIVE, NULL, NULL);

   const int allocSize = 2048;
   LARGE_INTEGER largeInteger;
   largeInteger.QuadPart = allocSize;

	fast_io::win32::nt::io_status_block ioStatusBlock = { 0 };
   HANDLE hFile = INVALID_HANDLE_VALUE;

	::debug_println(&hFile," ",STANDARD_RIGHTS_ALL, " ",&objAttribs, " ",&ioStatusBlock, " ",&largeInteger," ",
      FILE_ATTRIBUTE_NORMAL, " ",FILE_SHARE_READ," ",FILE_CREATE," ",FILE_NON_DIRECTORY_FILE," ",NULL," ",0);
static_assert(sizeof(LARGE_INTEGER)==sizeof(std::uint64_t));
   NTSTATUS status = fast_io::win32::nt::nt_create_file(&hFile, STANDARD_RIGHTS_ALL, fast_io::bit_cast<fast_io::win32::nt::object_attributes*>(&objAttribs), &ioStatusBlock, fast_io::bit_cast<std::int64_t*>(&largeInteger),
      FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ, FILE_CREATE, FILE_NON_DIRECTORY_FILE, nullptr, 0);

   if (status != 0)
	throw fast_io::nt_error(status);
/*   {
      std::cout << "Cannot create file, status: " << status << std::endl;
      return;
   }*/
}
   //  
int main()
{
	::debug_println("FILE_GENERIC_WRITE=0x",fast_io::hex(FILE_GENERIC_READ));
   test_ntcreatefile(LR"/(D:\hg\fast_io\helpers\w.txt)/");
}