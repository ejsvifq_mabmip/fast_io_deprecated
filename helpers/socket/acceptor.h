#pragma once

namespace fast_io
{

template<stream stm>
class acceptor:public stm
{
public:
	using char_type = typename stm::char_type;
	using native_handle_type = typename stm::native_handle_type;
	template<acceptance_stream lisn>
	constexpr acceptor(lisn& lsn):stm{accept(lsn)}{}
};

template<acceptance_stream lisn> acceptor(lisn& lsn) -> acceptor<decltype(accept(lsn))>;


}