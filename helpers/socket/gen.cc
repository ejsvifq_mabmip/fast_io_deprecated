#include"../../include/fast_io_device.h"

int main()
{
	fast_io::obuf_file obf("obf_sha.txt");
	for(std::size_t i{};i!=80;++i)
		if(i<16)
			print(obf,"\t\t\tdetails::sha512::round<",i,">(X,w,data);\n");
		else
			print(obf,"\t\t\tdetails::sha512::round<",i,">(X,w);\n");
}