#include"../../include/fast_io.h"
#include"../../include/fast_io_network.h"
#include"../../include/fast_io_device.h"
//#include"socket.h"
//#include"acceptor.h"

int main()
{
	fast_io::basic_ibuf<fast_io::basic_tcp<char>> client(fast_io::dns_once("www.jgjy.gov.cn"),80);
	connect(client);
	print(client,u8"GET / HTTP/1.1\r\n"
		"Host:www.jgjy.gov.cn\r\n"
		"User-agent:whatever\r\n"
		"Accept-Type:*/*\r\n"
		"Connection:close\r\n\r\n");
	skip_http_header(client);
	fast_io::obuf_file obf("test.html");
	transmit(obf,client);
}