	.file	"onative_file.cc"
	.text
	.section	.text$_ZN7fast_io7details18create_file_a_implILb0EJRKjS3_S3_S3_EEEPvSt17basic_string_viewIcSt11char_traitsIcEEDpOT0_,"x"
	.linkonce discard
	.globl	_ZN7fast_io7details18create_file_a_implILb0EJRKjS3_S3_S3_EEEPvSt17basic_string_viewIcSt11char_traitsIcEEDpOT0_
	.def	_ZN7fast_io7details18create_file_a_implILb0EJRKjS3_S3_S3_EEEPvSt17basic_string_viewIcSt11char_traitsIcEEDpOT0_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details18create_file_a_implILb0EJRKjS3_S3_S3_EEEPvSt17basic_string_viewIcSt11char_traitsIcEEDpOT0_
_ZN7fast_io7details18create_file_a_implILb0EJRKjS3_S3_S3_EEEPvSt17basic_string_viewIcSt11char_traitsIcEEDpOT0_:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$1112, %rsp
	.seh_stackalloc	1112
	.seh_endprologue
	movq	(%rcx), %rax
	movq	%rdx, %rdi
	movq	1216(%rsp), %rbp
	movq	8(%rcx), %rbx
	leaq	(%rbx,%rax), %rsi
	leaq	16(%rbx), %r13
	cmpq	$510, %rax
	ja	.L2
	leaq	80(%rsp), %rcx
	movq	%rcx, %r10
	cmpq	%r13, %rsi
	jbe	.L3
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE15first_unit_infoE(%rip), %r13
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE11transitionsE(%rip), %r12
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE14octet_categoryE(%rip), %r11
	pxor	%xmm1, %xmm1
.L4:
	movzbl	(%rbx), %eax
	testb	%al, %al
	jns	.L97
	leaq	0(%r13,%rax,2), %rdx
	movzbl	(%rdx), %eax
	movzbl	1(%rdx), %r14d
	movzbl	%al, %edx
	addq	$1, %rbx
	cmpl	$12, %r14d
	jle	.L10
	cmpq	%rsi, %rbx
	jnb	.L23
.L11:
	movzbl	(%rbx), %eax
	addq	$1, %rbx
	sall	$6, %edx
	movl	%eax, %r15d
	andl	$63, %r15d
	orl	%r15d, %edx
	movzbl	(%r11,%rax), %eax
	addl	%r14d, %eax
	cltq
	movzbl	(%r12,%rax), %r14d
	cmpl	$12, %r14d
	jle	.L98
	cmpq	%rsi, %rbx
	jne	.L11
.L23:
	ud2
.L98:
	cmpb	$12, %r14b
	je	.L23
	movl	%edx, %eax
	cmpl	$65535, %edx
	ja	.L14
.L46:
	movl	$2, %edx
.L15:
	movw	%ax, (%r10)
	addq	%rdx, %r10
.L9:
	leaq	16(%rbx), %rax
	cmpq	%rax, %rsi
	ja	.L4
.L3:
	cmpq	%rsi, %rbx
	jnb	.L50
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE15first_unit_infoE(%rip), %r13
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE11transitionsE(%rip), %r12
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE14octet_categoryE(%rip), %r11
.L6:
	movsbw	(%rbx), %ax
	addq	$1, %rbx
	leaq	2(%r10), %rdx
	testb	%al, %al
	jns	.L17
	movzbl	%al, %eax
	leaq	0(%r13,%rax,2), %rdx
	movzbl	(%rdx), %eax
	movzbl	1(%rdx), %r14d
	movzbl	%al, %edx
	cmpl	$12, %r14d
	jle	.L18
	cmpq	%rbx, %rsi
	jbe	.L23
.L19:
	movzbl	(%rbx), %eax
	addq	$1, %rbx
	sall	$6, %edx
	movl	%eax, %r15d
	andl	$63, %r15d
	orl	%r15d, %edx
	movzbl	(%r11,%rax), %eax
	addl	%r14d, %eax
	cltq
	movzbl	(%r12,%rax), %r14d
	cmpl	$12, %r14d
	jle	.L99
	cmpq	%rsi, %rbx
	jne	.L19
	jmp	.L23
.L99:
	cmpb	$12, %r14b
	.p2align 4,,4
	je	.L23
	movl	%edx, %eax
	cmpl	$65535, %edx
	ja	.L21
.L45:
	movl	$2, %edx
.L22:
	addq	%r10, %rdx
.L17:
	movw	%ax, (%r10)
	cmpq	%rsi, %rbx
	jnb	.L5
	movq	%rdx, %r10
	jmp	.L6
.L14:
	movl	%edx, %eax
	shrl	$10, %eax
	andw	$1023, %dx
	subw	$9216, %dx
	movw	%dx, 2(%r10)
	subw	$10304, %ax
	movl	$4, %edx
	jmp	.L15
.L21:
	movl	%edx, %eax
	shrl	$10, %eax
	andw	$1023, %dx
	subw	$9216, %dx
	movw	%dx, 2(%r10)
	subw	$10304, %ax
	movl	$4, %edx
	jmp	.L22
.L50:
	movq	%r10, %rdx
.L5:
	movw	$0, (%rdx)
	movq	$0, 48(%rsp)
	movl	0(%rbp), %eax
	movl	%eax, 40(%rsp)
	movl	(%r9), %eax
	movl	%eax, 32(%rsp)
	xorl	%r9d, %r9d
	movl	(%r8), %r8d
	movl	(%rdi), %edx
	call	CreateFileW
	cmpq	$-1, %rax
	je	.L23
.L1:
	addq	$1112, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L97:
	movdqu	(%rbx), %xmm0
	pmovmskb	%xmm0, %edx
	movdqa	%xmm0, %xmm2
	punpcklbw	%xmm1, %xmm2
	movdqu	%xmm2, (%r10)
	punpckhbw	%xmm1, %xmm0
	movdqu	%xmm0, 16(%r10)
	movzwl	%dx, %eax
	testw	%dx, %dx
	je	.L51
	bsfl	%eax, %eax
	cltq
	leaq	(%rax,%rax), %rdx
.L8:
	addq	%rax, %rbx
	addq	%rdx, %r10
	jmp	.L9
.L10:
	cmpb	$12, %r14b
	jne	.L46
	ud2
.L18:
	cmpb	$12, %r14b
	jne	.L45
	ud2
.L51:
	movl	$32, %edx
	movl	$16, %eax
	jmp	.L8
.L2:
	addq	$1, %rax
	movabsq	$4611686018427387900, %rdx
	cmpq	%rdx, %rax
	jbe	.L100
	movq	$-1, %rcx
.L25:
	movq	%r9, 1208(%rsp)
	movq	%r8, 1200(%rsp)
	call	_Znay
	movq	%rax, %r12
	movq	%rax, %rcx
	cmpq	%r13, %rsi
	movq	1200(%rsp), %r8
	movq	1208(%rsp), %r9
	jbe	.L26
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE15first_unit_infoE(%rip), %r13
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE11transitionsE(%rip), %r11
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE14octet_categoryE(%rip), %r10
	pxor	%xmm1, %xmm1
.L27:
	movzbl	(%rbx), %eax
	testb	%al, %al
	jns	.L101
	leaq	0(%r13,%rax,2), %rdx
	movzbl	(%rdx), %eax
	movzbl	1(%rdx), %r14d
	movzbl	%al, %edx
	addq	$1, %rbx
	cmpl	$12, %r14d
	jle	.L33
	cmpq	%rsi, %rbx
	jnb	.L23
.L34:
	movzbl	(%rbx), %eax
	addq	$1, %rbx
	sall	$6, %edx
	movl	%eax, %r15d
	andl	$63, %r15d
	orl	%r15d, %edx
	movzbl	(%r10,%rax), %eax
	addl	%r14d, %eax
	cltq
	movzbl	(%r11,%rax), %r14d
	cmpl	$12, %r14d
	jle	.L102
	cmpq	%rsi, %rbx
	jne	.L34
	jmp	.L23
.L100:
	leaq	(%rax,%rax), %rcx
	.p2align 4,,4
	jmp	.L25
.L102:
	cmpb	$12, %r14b
	.p2align 4,,3
	je	.L23
	movl	%edx, %eax
	cmpl	$65535, %edx
	ja	.L36
.L48:
	movl	$2, %edx
.L37:
	movw	%ax, (%rcx)
	addq	%rdx, %rcx
.L32:
	leaq	16(%rbx), %rax
	cmpq	%rax, %rsi
	ja	.L27
.L26:
	cmpq	%rsi, %rbx
	jnb	.L54
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE15first_unit_infoE(%rip), %r13
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE11transitionsE(%rip), %r11
	leaq	_ZN7fast_io7details3utf14utf_util_tableILb1EE14octet_categoryE(%rip), %r10
.L29:
	movsbw	(%rbx), %ax
	addq	$1, %rbx
	leaq	2(%rcx), %rdx
	testb	%al, %al
	jns	.L39
	movzbl	%al, %eax
	leaq	0(%r13,%rax,2), %rdx
	movzbl	(%rdx), %eax
	movzbl	1(%rdx), %r14d
	movzbl	%al, %edx
	cmpl	$12, %r14d
	jle	.L40
	cmpq	%rbx, %rsi
	jbe	.L23
.L41:
	movzbl	(%rbx), %eax
	addq	$1, %rbx
	sall	$6, %edx
	movl	%eax, %r15d
	andl	$63, %r15d
	orl	%r15d, %edx
	movzbl	(%r10,%rax), %eax
	addl	%r14d, %eax
	cltq
	movzbl	(%r11,%rax), %r14d
	cmpl	$12, %r14d
	jle	.L103
	cmpq	%rsi, %rbx
	jne	.L41
	jmp	.L23
.L103:
	cmpb	$12, %r14b
	.p2align 4,,4
	je	.L23
	movl	%edx, %eax
	cmpl	$65535, %edx
	ja	.L43
.L47:
	movl	$2, %edx
.L44:
	addq	%rcx, %rdx
.L39:
	movw	%ax, (%rcx)
	cmpq	%rsi, %rbx
	jnb	.L28
	movq	%rdx, %rcx
	jmp	.L29
.L36:
	movl	%edx, %r14d
	shrl	$10, %r14d
	andw	$1023, %dx
	leal	-9216(%rdx), %eax
	movw	%ax, 2(%rcx)
	leal	-10304(%r14), %eax
	movl	$4, %edx
	jmp	.L37
.L43:
	movl	%edx, %r14d
	shrl	$10, %r14d
	andw	$1023, %dx
	leal	-9216(%rdx), %eax
	movw	%ax, 2(%rcx)
	leal	-10304(%r14), %eax
	movl	$4, %edx
	jmp	.L44
.L54:
	movq	%rcx, %rdx
.L28:
	movw	$0, (%rdx)
	movq	$0, 48(%rsp)
	movl	0(%rbp), %eax
	movl	%eax, 40(%rsp)
	movl	(%r9), %eax
	movl	%eax, 32(%rsp)
	xorl	%r9d, %r9d
	movl	(%r8), %r8d
	movl	(%rdi), %edx
	movq	%r12, %rcx
	call	CreateFileW
	cmpq	$-1, %rax
	movq	%rax, 72(%rsp)
	je	.L23
	movq	%r12, %rcx
	call	_ZdaPv
	movq	72(%rsp), %rax
	jmp	.L1
.L101:
	movdqu	(%rbx), %xmm0
	pmovmskb	%xmm0, %edx
	movdqa	%xmm0, %xmm2
	punpcklbw	%xmm1, %xmm2
	movdqu	%xmm2, (%rcx)
	punpckhbw	%xmm1, %xmm0
	movdqu	%xmm0, 16(%rcx)
	movzwl	%dx, %eax
	testw	%dx, %dx
	je	.L55
	bsfl	%eax, %eax
	cltq
	leaq	(%rax,%rax), %rdx
.L31:
	addq	%rax, %rbx
	addq	%rdx, %rcx
	jmp	.L32
.L40:
	cmpb	$12, %r14b
	jne	.L47
	ud2
.L33:
	cmpb	$12, %r14b
	jne	.L48
	ud2
.L55:
	movl	$32, %edx
	movl	$16, %eax
	jmp	.L31
	.seh_endproc
	.def	__main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
.LC0:
	.ascii "w.txt\0"
.LC1:
	.ascii "m\12\0"
	.section	.text.startup,"x"
	.globl	main
	.def	main;	.scl	2;	.type	32;	.endef
	.seh_proc	main
main:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$64, %rsp
	.seh_stackalloc	64
	.seh_endprologue
	call	__main
	movq	$5, 48(%rsp)
	leaq	.LC0(%rip), %rax
	movq	%rax, 56(%rsp)
	leaq	48(%rsp), %rcx
	leaq	12+_ZN7fast_io7details26win32_file_openmode_singleILNS_9open_modeE2097160EE4modeE(%rip), %rax
	movq	%rax, 32(%rsp)
	leaq	-4(%rax), %r9
	leaq	-8(%rax), %r8
	leaq	-12(%rax), %rdx
	call	_ZN7fast_io7details18create_file_a_implILb0EJRKjS3_S3_S3_EEEPvSt17basic_string_viewIcSt11char_traitsIcEEDpOT0_
	movq	%rax, %r12
	leaq	.LC1(%rip), %rcx
	call	puts
	testq	%r12, %r12
	je	.L105
	movq	%r12, %rcx
	call	CloseHandle
.L105:
	xorl	%eax, %eax
	addq	$64, %rsp
	popq	%r12
	ret
	.seh_endproc
	.globl	_ZN7fast_io7details26win32_file_openmode_singleILNS_9open_modeE2097160EE4modeE
	.section	.rdata$_ZN7fast_io7details26win32_file_openmode_singleILNS_9open_modeE2097160EE4modeE,"dr"
	.linkonce same_size
	.align 16
_ZN7fast_io7details26win32_file_openmode_singleILNS_9open_modeE2097160EE4modeE:
	.long	1073741824
	.long	3
	.long	2
	.long	268435584
	.globl	_ZN7fast_io7details3utf14utf_util_tableILb1EE11transitionsE
	.section	.rdata$_ZN7fast_io7details3utf14utf_util_tableILb1EE11transitionsE,"dr"
	.linkonce same_size
	.align 32
_ZN7fast_io7details3utf14utf_util_tableILb1EE11transitionsE:
	.byte	12
	.byte	0
	.byte	12
	.byte	12
	.byte	12
	.byte	24
	.byte	60
	.byte	36
	.byte	72
	.byte	84
	.byte	48
	.byte	96
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	0
	.byte	0
	.byte	0
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	24
	.byte	24
	.byte	24
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	36
	.byte	36
	.byte	36
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	24
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	24
	.byte	24
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	36
	.byte	36
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	36
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.globl	_ZN7fast_io7details3utf14utf_util_tableILb1EE14octet_categoryE
	.section	.rdata$_ZN7fast_io7details3utf14utf_util_tableILb1EE14octet_categoryE,"dr"
	.linkonce same_size
	.align 32
_ZN7fast_io7details3utf14utf_util_tableILb1EE14octet_categoryE:
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	0
	.byte	0
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	6
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	8
	.byte	7
	.byte	7
	.byte	9
	.byte	10
	.byte	10
	.byte	10
	.byte	11
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.globl	_ZN7fast_io7details3utf14utf_util_tableILb1EE15first_unit_infoE
	.section	.rdata$_ZN7fast_io7details3utf14utf_util_tableILb1EE15first_unit_infoE,"dr"
	.linkonce same_size
	.align 32
_ZN7fast_io7details3utf14utf_util_tableILb1EE15first_unit_infoE:
	.byte	0
	.byte	0
	.byte	1
	.byte	0
	.byte	2
	.byte	0
	.byte	3
	.byte	0
	.byte	4
	.byte	0
	.byte	5
	.byte	0
	.byte	6
	.byte	0
	.byte	7
	.byte	0
	.byte	8
	.byte	0
	.byte	9
	.byte	0
	.byte	10
	.byte	0
	.byte	11
	.byte	0
	.byte	12
	.byte	0
	.byte	13
	.byte	0
	.byte	14
	.byte	0
	.byte	15
	.byte	0
	.byte	16
	.byte	0
	.byte	17
	.byte	0
	.byte	18
	.byte	0
	.byte	19
	.byte	0
	.byte	20
	.byte	0
	.byte	21
	.byte	0
	.byte	22
	.byte	0
	.byte	23
	.byte	0
	.byte	24
	.byte	0
	.byte	25
	.byte	0
	.byte	26
	.byte	0
	.byte	27
	.byte	0
	.byte	28
	.byte	0
	.byte	29
	.byte	0
	.byte	30
	.byte	0
	.byte	31
	.byte	0
	.byte	32
	.byte	0
	.byte	33
	.byte	0
	.byte	34
	.byte	0
	.byte	35
	.byte	0
	.byte	36
	.byte	0
	.byte	37
	.byte	0
	.byte	38
	.byte	0
	.byte	39
	.byte	0
	.byte	40
	.byte	0
	.byte	41
	.byte	0
	.byte	42
	.byte	0
	.byte	43
	.byte	0
	.byte	44
	.byte	0
	.byte	45
	.byte	0
	.byte	46
	.byte	0
	.byte	47
	.byte	0
	.byte	48
	.byte	0
	.byte	49
	.byte	0
	.byte	50
	.byte	0
	.byte	51
	.byte	0
	.byte	52
	.byte	0
	.byte	53
	.byte	0
	.byte	54
	.byte	0
	.byte	55
	.byte	0
	.byte	56
	.byte	0
	.byte	57
	.byte	0
	.byte	58
	.byte	0
	.byte	59
	.byte	0
	.byte	60
	.byte	0
	.byte	61
	.byte	0
	.byte	62
	.byte	0
	.byte	63
	.byte	0
	.byte	64
	.byte	0
	.byte	65
	.byte	0
	.byte	66
	.byte	0
	.byte	67
	.byte	0
	.byte	68
	.byte	0
	.byte	69
	.byte	0
	.byte	70
	.byte	0
	.byte	71
	.byte	0
	.byte	72
	.byte	0
	.byte	73
	.byte	0
	.byte	74
	.byte	0
	.byte	75
	.byte	0
	.byte	76
	.byte	0
	.byte	77
	.byte	0
	.byte	78
	.byte	0
	.byte	79
	.byte	0
	.byte	80
	.byte	0
	.byte	81
	.byte	0
	.byte	82
	.byte	0
	.byte	83
	.byte	0
	.byte	84
	.byte	0
	.byte	85
	.byte	0
	.byte	86
	.byte	0
	.byte	87
	.byte	0
	.byte	88
	.byte	0
	.byte	89
	.byte	0
	.byte	90
	.byte	0
	.byte	91
	.byte	0
	.byte	92
	.byte	0
	.byte	93
	.byte	0
	.byte	94
	.byte	0
	.byte	95
	.byte	0
	.byte	96
	.byte	0
	.byte	97
	.byte	0
	.byte	98
	.byte	0
	.byte	99
	.byte	0
	.byte	100
	.byte	0
	.byte	101
	.byte	0
	.byte	102
	.byte	0
	.byte	103
	.byte	0
	.byte	104
	.byte	0
	.byte	105
	.byte	0
	.byte	106
	.byte	0
	.byte	107
	.byte	0
	.byte	108
	.byte	0
	.byte	109
	.byte	0
	.byte	110
	.byte	0
	.byte	111
	.byte	0
	.byte	112
	.byte	0
	.byte	113
	.byte	0
	.byte	114
	.byte	0
	.byte	115
	.byte	0
	.byte	116
	.byte	0
	.byte	117
	.byte	0
	.byte	118
	.byte	0
	.byte	119
	.byte	0
	.byte	120
	.byte	0
	.byte	121
	.byte	0
	.byte	122
	.byte	0
	.byte	123
	.byte	0
	.byte	124
	.byte	0
	.byte	125
	.byte	0
	.byte	126
	.byte	0
	.byte	127
	.byte	0
	.byte	0
	.byte	12
	.byte	1
	.byte	12
	.byte	2
	.byte	12
	.byte	3
	.byte	12
	.byte	4
	.byte	12
	.byte	5
	.byte	12
	.byte	6
	.byte	12
	.byte	7
	.byte	12
	.byte	8
	.byte	12
	.byte	9
	.byte	12
	.byte	10
	.byte	12
	.byte	11
	.byte	12
	.byte	12
	.byte	12
	.byte	13
	.byte	12
	.byte	14
	.byte	12
	.byte	15
	.byte	12
	.byte	16
	.byte	12
	.byte	17
	.byte	12
	.byte	18
	.byte	12
	.byte	19
	.byte	12
	.byte	20
	.byte	12
	.byte	21
	.byte	12
	.byte	22
	.byte	12
	.byte	23
	.byte	12
	.byte	24
	.byte	12
	.byte	25
	.byte	12
	.byte	26
	.byte	12
	.byte	27
	.byte	12
	.byte	28
	.byte	12
	.byte	29
	.byte	12
	.byte	30
	.byte	12
	.byte	31
	.byte	12
	.byte	32
	.byte	12
	.byte	33
	.byte	12
	.byte	34
	.byte	12
	.byte	35
	.byte	12
	.byte	36
	.byte	12
	.byte	37
	.byte	12
	.byte	38
	.byte	12
	.byte	39
	.byte	12
	.byte	40
	.byte	12
	.byte	41
	.byte	12
	.byte	42
	.byte	12
	.byte	43
	.byte	12
	.byte	44
	.byte	12
	.byte	45
	.byte	12
	.byte	46
	.byte	12
	.byte	47
	.byte	12
	.byte	48
	.byte	12
	.byte	49
	.byte	12
	.byte	50
	.byte	12
	.byte	51
	.byte	12
	.byte	52
	.byte	12
	.byte	53
	.byte	12
	.byte	54
	.byte	12
	.byte	55
	.byte	12
	.byte	56
	.byte	12
	.byte	57
	.byte	12
	.byte	58
	.byte	12
	.byte	59
	.byte	12
	.byte	60
	.byte	12
	.byte	61
	.byte	12
	.byte	62
	.byte	12
	.byte	63
	.byte	12
	.byte	-64
	.byte	12
	.byte	-63
	.byte	12
	.byte	2
	.byte	24
	.byte	3
	.byte	24
	.byte	4
	.byte	24
	.byte	5
	.byte	24
	.byte	6
	.byte	24
	.byte	7
	.byte	24
	.byte	8
	.byte	24
	.byte	9
	.byte	24
	.byte	10
	.byte	24
	.byte	11
	.byte	24
	.byte	12
	.byte	24
	.byte	13
	.byte	24
	.byte	14
	.byte	24
	.byte	15
	.byte	24
	.byte	16
	.byte	24
	.byte	17
	.byte	24
	.byte	18
	.byte	24
	.byte	19
	.byte	24
	.byte	20
	.byte	24
	.byte	21
	.byte	24
	.byte	22
	.byte	24
	.byte	23
	.byte	24
	.byte	24
	.byte	24
	.byte	25
	.byte	24
	.byte	26
	.byte	24
	.byte	27
	.byte	24
	.byte	28
	.byte	24
	.byte	29
	.byte	24
	.byte	30
	.byte	24
	.byte	31
	.byte	24
	.byte	0
	.byte	60
	.byte	1
	.byte	36
	.byte	2
	.byte	36
	.byte	3
	.byte	36
	.byte	4
	.byte	36
	.byte	5
	.byte	36
	.byte	6
	.byte	36
	.byte	7
	.byte	36
	.byte	8
	.byte	36
	.byte	9
	.byte	36
	.byte	10
	.byte	36
	.byte	11
	.byte	36
	.byte	12
	.byte	36
	.byte	13
	.byte	72
	.byte	14
	.byte	36
	.byte	15
	.byte	36
	.byte	0
	.byte	84
	.byte	1
	.byte	48
	.byte	2
	.byte	48
	.byte	3
	.byte	48
	.byte	4
	.byte	96
	.byte	-11
	.byte	12
	.byte	-10
	.byte	12
	.byte	-9
	.byte	12
	.byte	-8
	.byte	12
	.byte	-7
	.byte	12
	.byte	-6
	.byte	12
	.byte	-5
	.byte	12
	.byte	-4
	.byte	12
	.byte	-3
	.byte	12
	.byte	-2
	.byte	12
	.byte	-1
	.byte	12
	.ident	"GCC: (GCC with MCF thread model, built by LH_Mouse.) 11.0.0 20200712 (experimental)"
	.def	CreateFileW;	.scl	2;	.type	32;	.endef
	.def	_Znay;	.scl	2;	.type	32;	.endef
	.def	_ZdaPv;	.scl	2;	.type	32;	.endef
	.def	puts;	.scl	2;	.type	32;	.endef
	.def	CloseHandle;	.scl	2;	.type	32;	.endef
