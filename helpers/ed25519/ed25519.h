#pragma once

namespace fast_io::details::curve25519
{

struct field_element
{
	std::uint64_t element[5];
	inline constexpr std::uint64_t& operator[](std::size_t position) noexcept
	{
		return element[position];
	}
	inline constexpr std::uint64_t const& operator[](std::size_t position) const noexcept
	{
		return element[position];
	}
};

inline constexpr field_element operator+(field_element const& a, field_element const& b) noexcept
{
	return {a[0]+b[0],a[1]+b[1],a[2]+b[2],a[3]+b[3],a[4]+b[4]};
}

template<std::uint64_t p0,std::uint64_t p1234>
inline constexpr field_element subtraction(field_element const& a,field_element const& b) noexcept
{
	return {a[0]+p0-b[0],
		a[1]+p1234-b[1],
		a[2]+p1234-b[2],
		a[3]+p1234-b[3],
		a[4]+p1234-b[4]};
}

inline constexpr field_element operator-(field_element const& a,field_element const& b) noexcept
{
	return subtraction<0x0fffffffffffda,0x0ffffffffffffe>(a,b);
}

inline constexpr field_element sub_after_basic(field_element const& a,field_element const& b) noexcept
{
	return subtraction<0x1fffffffffffb4,0x1ffffffffffffc>(a,b); 
}


inline constexpr field_element add_reduce(field_element const& a,field_element const& b) noexcept
{
	constexpr auto reduce_mask_51{(static_cast<std::uint64_t>(1) << 51) - 1};
	field_element result;
	std::uint64_t c{(a[0] + b[0])>>51};
	result[1] = a[1] + b[1] + c; c = (result[1] >> 51); result[1] &= reduce_mask_51;
	result[2] = a[2] + b[2] + c; c = (result[2] >> 51); result[2] &= reduce_mask_51;
	result[3] = a[3] + b[3] + c; c = (result[3] >> 51); result[3] &= reduce_mask_51;
	result[4] = a[4] + b[4] + c; result[0] = (result[4] >> 51)*19; result[4] &= reduce_mask_51;
	return result;
}

inline constexpr field_element sub_reduce(field_element const& a,field_element const& b) noexcept
{
	constexpr auto reduce_mask_51{(static_cast<std::uint64_t>(1) << 51) - 1};
	constexpr std::uint64_t p1234{0x1ffffffffffffc};
	field_element result;
	std::uint64_t c{(a[0] + static_cast<std::uint64_t>(0x1fffffffffffb4) - b[0])>>51};
	result[1] = a[1] + p1234 - b[1] + c; c = (result[1] >> 51); result[1] &= reduce_mask_51;
	result[2] = a[2] + p1234 - b[2] + c; c = (result[2] >> 51); result[2] &= reduce_mask_51;
	result[3] = a[3] + p1234 - b[3] + c; c = (result[3] >> 51); result[3] &= reduce_mask_51;
	result[4] = a[4] + p1234 - b[4] + c; result[0] = (result[4] >> 51) * 19; result[4] &= reduce_mask_51;
	return result;
}

inline constexpr field_element operator-(field_element const& a) noexcept
{
	constexpr auto reduce_mask_51{(static_cast<std::uint64_t>(1) << 51) - 1};
	constexpr std::uint64_t p1234{0x1ffffffffffffc};
	field_element result;
	std::uint64_t c{(static_cast<std::uint64_t>(0x1fffffffffffb4) - a[0])>>51};
	result[1] = p1234 - a[1] + c; c = (result[1] >> 51); result[1] &= reduce_mask_51;
	result[2] = p1234 - a[2] + c; c = (result[2] >> 51); result[2] &= reduce_mask_51;
	result[3] = p1234 - a[3] + c; c = (result[3] >> 51); result[3] &= reduce_mask_51;
	result[4] = p1234 - a[4] + c; result[0] = (result[4] >> 51)*19; result[4] &= reduce_mask_51;
	return result;
}

inline constexpr field_element operator*(field_element const& r,field_element const& s) noexcept
{
	auto t0{mul_extend(r[0],s[0])+mul_extend(r[4],s[1]*19)+mul_extend(r[3],s[2]*19)+mul_extend(r[2],s[3]*19)+mul_extend(r[1],s[4]*19)};
	auto t1{mul_extend(r[0],s[1])+mul_extend(r[1],s[0])+mul_extend(r[4],s[2]*19)+mul_extend(r[3],s[3]*19)+mul_extend(r[2],s[4]*19)};
	auto t2{mul_extend(r[0],s[2])+mul_extend(r[1],s[1])+mul_extend(r[2],s[0])+mul_extend(r[4],s[3]*19)+mul_extend(r[3],s[4]*19)};
	auto t3{mul_extend(r[0],s[3])+mul_extend(r[1],s[2])+mul_extend(r[2],s[1])+mul_extend(r[3],s[0])+mul_extend(r[4],s[4]*19)};
	auto t4{mul_extend(r[0],s[4])+mul_extend(r[1],s[3])+mul_extend(r[2],s[2])+mul_extend(r[3],s[1])+mul_extend(r[4],s[0])};
	
	return {{low(t0),low(t1),low(t2),low(t3),low(t4)}};
}

/*
field_element multiply(field_element const& __restrict__ r,field_element const& __restrict__ s) noexcept
{
	auto r0{r[0]};
	auto r1{r[1]};
	auto r2{r[2]};	
	auto r3{r[3]};
	auto r4{r[4]};

	auto s0{s[0]};
	auto s1{s[1]};
	auto s2{s[2]};	
	auto s3{s[3]};
	auto s4{s[4]};

	auto c1{s1*19};
	auto c2{s2*19};	
	auto c3{s3*19};
	auto c4{s4*19};

	std::array<uint128_t,5> t
	{
	mul_extend(r0,s0)+mul_extend(r4,c1)+mul_extend(r3,c2)+mul_extend(r2,c3)+mul_extend(r1,c4),
	mul_extend(r0,s1)+mul_extend(r1,s0)+mul_extend(r4,c2)+mul_extend(r3,c3)+mul_extend(r2,c4),
	mul_extend(r0,s2)+mul_extend(r1,s1)+mul_extend(r2,s0)+mul_extend(r4,c3)+mul_extend(r3,c4),
	mul_extend(r0,s3)+mul_extend(r1,s2)+mul_extend(r2,s1)+mul_extend(r3,s0)+mul_extend(r3,c4),
	mul_extend(r0,s4)+mul_extend(r1,s3)+mul_extend(r2,s2)+mul_extend(r3,s1)+mul_extend(r4,s0)
	};
	return {{low(t[0]),low(t[1]),low(t[2]),low(t[3]),low(t[4])}};
}

field_element multiply2safasf(field_element const& __restrict__ r,field_element const& __restrict__ s) noexcept
{
	auto t0=mul_extend(r[0],s[0])+mul_extend(r[4],s[1]*19)+mul_extend(r[3],s[2]*19)+mul_extend(r[2],s[3]*19)+mul_extend(r[1],s[4]*19);
	auto t1=mul_extend(r[0],s[1])+mul_extend(r[1],s[0])+mul_extend(r[4],s[2]*19)+mul_extend(r[3],s[3]*19)+mul_extend(r[2],s[4]*19);
	auto t2=mul_extend(r[0],s[2])+mul_extend(r[1],s[1])+mul_extend(r[2],s[0])+mul_extend(r[4],s[3]*19)+mul_extend(r[3],s[4]*19);
	auto t3=mul_extend(r[0],s[3])+mul_extend(r[1],s[2])+mul_extend(r[2],s[1])+mul_extend(r[3],s[0])+mul_extend(r[3],s[4]*19);
	auto t4=mul_extend(r[0],s[4])+mul_extend(r[1],s[3])+mul_extend(r[2],s[2])+mul_extend(r[3],s[1])+mul_extend(r[4],s[0]);
	return {{low(t0),low(t1),low(t2),low(t3),low(t4)}};
}
*/
}