	.file	"curve25519.cc"
	.text
	.p2align 4
	.globl	_ZN7fast_io7details10curve25519mlERKNS1_13field_elementES4_
	.def	_ZN7fast_io7details10curve25519mlERKNS1_13field_elementES4_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details10curve25519mlERKNS1_13field_elementES4_
_ZN7fast_io7details10curve25519mlERKNS1_13field_elementES4_:
.LFB14315:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$104, %rsp
	.seh_stackalloc	104
	.seh_endprologue
	movq	16(%r8), %rbp
	movq	(%r8), %rax
	movq	24(%r8), %r11
	movq	32(%r8), %rdi
	movq	32(%rdx), %r9
	movq	24(%rdx), %rbx
	movq	16(%rdx), %r12
	movq	(%rdx), %r14
	movq	%rcx, %r10
	movq	%rax, 8(%rsp)
	leaq	0(%rbp,%rbp,8), %rcx
	movq	8(%r8), %rax
	leaq	0(%rbp,%rcx,2), %rsi
	leaq	(%r11,%r11,8), %rcx
	movq	%rdi, 64(%rsp)
	leaq	(%r11,%rcx,2), %r13
	leaq	(%rdi,%rdi,8), %rcx
	movq	%r9, 32(%rsp)
	movq	%rbx, 40(%rsp)
	leaq	(%rdi,%rcx,2), %r15
	movq	%r12, 24(%rsp)
	movq	8(%rdx), %rcx
	leaq	(%rax,%rax,8), %rdx
	movq	%r15, 56(%rsp)
	leaq	(%rax,%rdx,2), %r8
	movq	%r9, %rdx
	mulx	%r8, %r8, %r9
	movq	%rbx, %rdx
	movq	%rcx, %rdi
	mulx	%rsi, %rcx, %rbx
	addq	%rcx, %r8
	movq	%rdi, %rdx
	movq	%rdi, 16(%rsp)
	movq	%r13, %rdi
	adcq	%rbx, %r9
	mulx	%r15, %rcx, %rbx
	movq	%r12, %rdx
	movq	%rdi, 88(%rsp)
	mulx	%r13, %r12, %r13
	movq	8(%rsp), %rdx
	addq	%r12, %rcx
	adcq	%r13, %rbx
	addq	%r8, %rcx
	adcq	%r9, %rbx
	movq	%r14, %r9
	mulx	%r14, %r14, %r15
	mulx	16(%rsp), %r12, %r13
	movq	%r9, %rdx
	movq	%r9, 48(%rsp)
	addq	%rcx, %r14
	adcq	%rbx, %r15
	movq	%r14, %rcx
	movabsq	$2251799813685247, %rbx
	andq	%rbx, %rcx
	movq	%rcx, 72(%rsp)
	mulx	%rax, %rcx, %rbx
	movq	%rdi, %rdx
	mulx	40(%rsp), %r8, %r9
	movq	32(%rsp), %rdx
	addq	%rcx, %r12
	adcq	%rbx, %r13
	mulx	%rsi, %rsi, %rdi
	movq	24(%rsp), %rdx
	addq	%rsi, %r8
	adcq	%rdi, %r9
	addq	%r12, %r8
	adcq	%r13, %r9
	shrdq	$51, %r15, %r14
	movq	56(%rsp), %r15
	xorl	%edi, %edi
	movq	%r14, %rsi
	mulx	%r15, %rcx, %rbx
	movq	16(%rsp), %rdx
	addq	%rcx, %rsi
	adcq	%rbx, %rdi
	addq	%rsi, %r8
	adcq	%rdi, %r9
	movq	%r8, %rsi
	movabsq	$2251799813685247, %rdi
	andq	%rdi, %rsi
	movq	%rsi, 80(%rsp)
	mulx	%rax, %rsi, %rdi
	movq	48(%rsp), %rdx
	mulx	%rbp, %rcx, %rbx
	movq	%r15, %rdx
	addq	%rcx, %rsi
	adcq	%rbx, %rdi
	mulx	40(%rsp), %rcx, %rbx
	movq	32(%rsp), %rdx
	mulx	88(%rsp), %r12, %r13
	movq	24(%rsp), %rdx
	addq	%r12, %rcx
	movq	8(%rsp), %r12
	adcq	%r13, %rbx
	addq	%rsi, %rcx
	movq	40(%rsp), %r13
	adcq	%rdi, %rbx
	shrdq	$51, %r9, %r8
	xorl	%edi, %edi
	movq	%r8, %rsi
	mulx	%r12, %r8, %r9
	addq	%r8, %rsi
	adcq	%r9, %rdi
	addq	%rsi, %rcx
	adcq	%rdi, %rbx
	movq	%rcx, %r14
	movq	%rbx, %r15
	mulx	%rax, %rcx, %rbx
	movq	%r12, %rdx
	mulx	%r13, %r8, %r9
	movq	48(%rsp), %rdx
	addq	%r8, %rcx
	adcq	%r9, %rbx
	mulx	%r11, %rsi, %rdi
	movq	16(%rsp), %rdx
	mulx	%rbp, %r8, %r9
	movq	32(%rsp), %rdx
	addq	%r8, %rsi
	movq	%r14, %r8
	adcq	%r9, %rdi
	addq	%rcx, %rsi
	adcq	%rbx, %rdi
	shrdq	$51, %r15, %r8
	xorl	%ebx, %ebx
	movq	%r8, %rcx
	mulx	56(%rsp), %r8, %r9
	movq	24(%rsp), %rdx
	addq	%r8, %rcx
	adcq	%r9, %rbx
	addq	%rsi, %rcx
	adcq	%rdi, %rbx
	mulx	%rbp, %rsi, %rdi
	movq	%r13, %rdx
	mulx	%rax, %r8, %r9
	movq	48(%rsp), %rdx
	addq	%rsi, %r8
	adcq	%rdi, %r9
	mulx	64(%rsp), %rsi, %rdi
	movq	16(%rsp), %rdx
	mulx	%r11, %r12, %r13
	movq	32(%rsp), %rdx
	addq	%rsi, %r12
	movq	%rcx, %rsi
	adcq	%rdi, %r13
	addq	%r8, %r12
	adcq	%r9, %r13
	shrdq	$51, %rbx, %rsi
	xorl	%r9d, %r9d
	movq	%rsi, %r8
	mulx	8(%rsp), %rsi, %rdi
	addq	%rsi, %r8
	adcq	%rdi, %r9
	addq	%r12, %r8
	movabsq	$2251799813685247, %rdi
	adcq	%r13, %r9
	movq	%r8, %rsi
	movq	%r8, %r11
	movq	%rdi, %r8
	shrdq	$51, %r9, %rsi
	andq	%rdi, %r14
	andq	%rdi, %rcx
	andq	%r11, %r8
	leaq	(%rsi,%rsi,8), %rax
	movq	%r14, 16(%r10)
	movq	%rcx, 24(%r10)
	movq	%r8, 32(%r10)
	leaq	(%rsi,%rax,2), %rax
	addq	72(%rsp), %rax
	movq	%rax, %r9
	shrq	$51, %rax
	addq	80(%rsp), %rax
	andq	%rdi, %r9
	movq	%r9, (%r10)
	movq	%rax, 8(%r10)
	movq	%r10, %rax
	addq	$104, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.seh_endproc
	.ident	"GCC: (GCC with MCF thread model, built by LH_Mouse.) 11.0.0 20200712 (experimental)"
