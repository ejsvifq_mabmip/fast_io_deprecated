	.file	"ed25519.cc"
	.text
	.p2align 4
	.globl	_ZN7fast_io7details7ed25519mlERKNS1_6numberES4_
	.def	_ZN7fast_io7details7ed25519mlERKNS1_6numberES4_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details7ed25519mlERKNS1_6numberES4_
_ZN7fast_io7details7ed25519mlERKNS1_6numberES4_:
.LFB14319:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	.seh_endprologue
	movq	24(%rdx), %rbp
	movq	(%rdx), %rdi
	movq	(%r8), %rsi
	movq	8(%r8), %r11
	movq	16(%r8), %rax
	movq	24(%r8), %r10
	movq	16(%rdx), %r12
	movq	32(%rdx), %rbx
	movq	%rcx, %r9
	movq	32(%r8), %rcx
	movq	8(%rdx), %r8
	movq	%r11, %r14
	movq	%r10, %r15
	imulq	%rbx, %r14
	movq	%rcx, %rdx
	imulq	%r12, %r15
	imulq	%rbp, %rdx
	leaq	(%rdx,%rdx,8), %r13
	leaq	(%rdx,%r13,2), %r13
	movq	%rax, %rdx
	imulq	%rbp, %rdx
	addq	%r14, %rdx
	movq	%rcx, %r14
	imulq	%r8, %r14
	addq	%r15, %r14
	movq	%r11, %r15
	addq	%r14, %rdx
	imulq	%rdi, %r15
	leaq	(%rdx,%rdx,8), %r14
	leaq	(%rdx,%r14,2), %rdx
	movq	%rdi, %r14
	imulq	%rsi, %r14
	addq	%r14, %rdx
	movq	%rax, %r14
	movq	%rdx, (%r9)
	imulq	%rbx, %r14
	movq	%r10, %rdx
	imulq	%rbp, %rdx
	addq	%r14, %rdx
	movq	%rcx, %r14
	imulq	%r12, %r14
	addq	%r14, %rdx
	leaq	(%rdx,%rdx,8), %r14
	leaq	(%rdx,%r14,2), %rdx
	movq	%r8, %r14
	imulq	%rsi, %r14
	addq	%r15, %r14
	movq	%r11, %r15
	addq	%r14, %rdx
	imulq	%r8, %r15
	movq	%rdx, 8(%r9)
	movq	%r10, %rdx
	imulq	%rbx, %rdx
	leaq	(%rdx,%rdx,8), %r14
	leaq	(%rdx,%r14,2), %rdx
	movq	%rax, %r14
	imulq	%rdi, %r14
	addq	%r13, %rdx
	addq	%r15, %r14
	movq	%rbp, %r15
	addq	%r14, %rdx
	imulq	%rsi, %r15
	movq	%r12, %r14
	imulq	%rsi, %r14
	addq	%r14, %rdx
	movq	%rax, %r14
	movq	%rdx, 16(%r9)
	imulq	%r8, %r14
	movq	%r10, %rdx
	imulq	%rdi, %rdx
	addq	%r14, %rdx
	movq	%r11, %r14
	imulq	%r12, %r14
	addq	%r15, %r14
	imulq	%r12, %rax
	imulq	%rbp, %r11
	addq	%r14, %rdx
	imulq	%rdi, %rcx
	addq	%r13, %rdx
	imulq	%r8, %r10
	movq	%rdx, 24(%r9)
	imulq	%rsi, %rbx
	addq	%r11, %rax
	addq	%r10, %rcx
	addq	%rcx, %rax
	addq	%rbx, %rax
	movq	%rax, 32(%r9)
	movq	%r9, %rax
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.seh_endproc
	.ident	"GCC: (GCC with MCF thread model, built by LH_Mouse.) 11.0.0 20200712 (experimental)"
