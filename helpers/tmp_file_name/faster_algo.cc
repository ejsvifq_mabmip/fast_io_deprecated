#include<fast_io_device.h>
#include<fast_io.h>
#include<vector>
#include<array>
#include<algorithm>

int main()
{
	fast_io::ibuf_file ibf("a.txt");
	std::size_t n;
	scan(ibf,n);
	std::vector<std::size_t> vec(n);
	for(std::size_t i{};i!=n;++i)
		scan(ibf,vec[i]);
	std::sort(vec.data(),vec.data()+vec.size());
	std::sort(vec.begin(),vec.end());
}