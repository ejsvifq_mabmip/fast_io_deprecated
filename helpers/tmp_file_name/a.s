	.file	"a.cc"
	.text
	.section	.text$_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy,"x"
	.linkonce discard
	.align 2
	.globl	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy
	.def	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy
_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy:
.LFB16103:
	.seh_endprologue
	movq	8(%rcx), %rcx
	movq	4104(%rcx), %rax
	addq	%rax, %rdx
	cmpq	%rdx, 4112(%rcx)
	movl	$0, %edx
	cmovbe	%rdx, %rax
	ret
	.seh_endproc
	.section	.text$_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc,"x"
	.linkonce discard
	.align 2
	.globl	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc
	.def	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc
_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc:
.LFB16105:
	.seh_endprologue
	movq	8(%rcx), %rax
	movq	%rdx, 4104(%rax)
	ret
	.seh_endproc
	.section	.text$_ZN7fast_io11win32_errorD0Ev,"x"
	.linkonce discard
	.align 2
	.globl	_ZN7fast_io11win32_errorD0Ev
	.def	_ZN7fast_io11win32_errorD0Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io11win32_errorD0Ev
_ZN7fast_io11win32_errorD0Ev:
.LFB16059:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	%rcx, %r12
	leaq	16+_ZTVN7fast_io13fast_io_errorE(%rip), %rax
	movq	%rax, (%rcx)
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L6
	call	_ZdaPv
.L6:
	movq	%r12, %rcx
	call	_ZNSt9exceptionD2Ev
	movl	$24, %edx
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZdlPvy
	.seh_endproc
	.section	.text$_ZN7fast_io11win32_errorD1Ev,"x"
	.linkonce discard
	.align 2
	.globl	_ZN7fast_io11win32_errorD1Ev
	.def	_ZN7fast_io11win32_errorD1Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io11win32_errorD1Ev
_ZN7fast_io11win32_errorD1Ev:
.LFB16058:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	%rcx, %r12
	leaq	16+_ZTVN7fast_io13fast_io_errorE(%rip), %rax
	movq	%rax, (%rcx)
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L11
	call	_ZdaPv
.L11:
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZNSt9exceptionD2Ev
	.seh_endproc
	.section	.text$_ZN7fast_io11posix_errorD1Ev,"x"
	.linkonce discard
	.align 2
	.globl	_ZN7fast_io11posix_errorD1Ev
	.def	_ZN7fast_io11posix_errorD1Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io11posix_errorD1Ev
_ZN7fast_io11posix_errorD1Ev:
.LFB16062:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	%rcx, %r12
	leaq	16+_ZTVN7fast_io13fast_io_errorE(%rip), %rax
	movq	%rax, (%rcx)
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L16
	call	_ZdaPv
.L16:
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZNSt9exceptionD2Ev
	.seh_endproc
	.section	.text$_ZN7fast_io11posix_errorD0Ev,"x"
	.linkonce discard
	.align 2
	.globl	_ZN7fast_io11posix_errorD0Ev
	.def	_ZN7fast_io11posix_errorD0Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io11posix_errorD0Ev
_ZN7fast_io11posix_errorD0Ev:
.LFB16063:
	pushq	%r12
	.seh_pushreg	%r12
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	%rcx, %r12
	leaq	16+_ZTVN7fast_io13fast_io_errorE(%rip), %rax
	movq	%rax, (%rcx)
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L21
	call	_ZdaPv
.L21:
	movq	%r12, %rcx
	call	_ZNSt9exceptionD2Ev
	movl	$24, %edx
	movq	%r12, %rcx
	addq	$32, %rsp
	popq	%r12
	jmp	_ZdlPvy
	.seh_endproc
	.section	.text$_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_,"x"
	.linkonce discard
	.align 2
	.globl	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_
	.def	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_
_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_:
.LFB16102:
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	%rdx, %r13
	movq	%r8, %r12
	movq	8(%rcx), %rsi
	movq	%r8, %rbx
	subq	%rdx, %rbx
	movq	4112(%rsi), %rax
	movq	4104(%rsi), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %rbx
	ja	.L30
	movq	%rbx, %r8
	movq	%r13, %rdx
	call	memcpy
	addq	4104(%rsi), %rbx
.L29:
	movq	%rbx, 4104(%rsi)
	movq	%r12, %rax
	addq	$32, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.L30:
	subq	4096(%rsi), %rax
	leaq	(%rax,%rax), %rdi
	cmpq	%rbx, %rdi
	cmovb	%rbx, %rdi
	testq	%rdi, %rdi
	js	.L31
	movq	%rdi, %rcx
	call	_Znwy
	movq	%rax, %rbp
	movq	4096(%rsi), %rdx
	movq	4104(%rsi), %r8
	subq	%rdx, %r8
	movq	%rax, %rcx
	call	memcpy
	movq	4096(%rsi), %rcx
	movq	4104(%rsi), %r14
	subq	%rcx, %r14
	cmpq	%rsi, %rcx
	je	.L28
	movq	4112(%rsi), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
.L28:
	movq	%rbp, 4096(%rsi)
	leaq	0(%rbp,%r14), %rcx
	movq	%rcx, 4104(%rsi)
	addq	%rdi, %rbp
	movq	%rbp, 4112(%rsi)
	movq	%rbx, %r8
	movq	%r13, %rdx
	call	memcpy
	addq	4104(%rsi), %rbx
	jmp	.L29
.L31:
	call	_ZSt17__throw_bad_allocv
	nop
	.seh_endproc
	.section	.text$_ZNK7fast_io11posix_error6reportERNS_14error_reporterE,"x"
	.linkonce discard
	.align 2
	.globl	_ZNK7fast_io11posix_error6reportERNS_14error_reporterE
	.def	_ZNK7fast_io11posix_error6reportERNS_14error_reporterE;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK7fast_io11posix_error6reportERNS_14error_reporterE
_ZNK7fast_io11posix_error6reportERNS_14error_reporterE:
.LFB11703:
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	movq	%rdx, %r13
	movl	16(%rcx), %ecx
	call	strerror
	movq	%rax, %r12
	movq	%rax, %rcx
	call	strlen
	movq	%rax, %rbx
	movq	0(%r13), %rax
	movq	(%rax), %rax
	leaq	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L33
	movq	8(%r13), %r13
	movq	4112(%r13), %rax
	movq	4104(%r13), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %rbx
	ja	.L38
	movq	%rbx, %r8
	movq	%r12, %rdx
	call	memcpy
	addq	%rbx, 4104(%r13)
.L32:
	addq	$40, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
.L33:
	leaq	(%r12,%rbx), %r8
	movq	%r12, %rdx
	movq	%r13, %rcx
	addq	$40, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	rex.W jmp	*%rax
.L38:
	subq	4096(%r13), %rax
	leaq	(%rax,%rax), %rsi
	cmpq	%rbx, %rsi
	cmovb	%rbx, %rsi
	testq	%rsi, %rsi
	js	.L39
	movq	%rsi, %rcx
	call	_Znwy
	movq	%rax, %rdi
	movq	4096(%r13), %rdx
	movq	4104(%r13), %r8
	subq	%rdx, %r8
	movq	%rax, %rcx
	call	memcpy
	movq	4096(%r13), %rcx
	movq	4104(%r13), %rbp
	subq	%rcx, %rbp
	cmpq	%r13, %rcx
	je	.L36
	movq	4112(%r13), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
.L36:
	movq	%rdi, 4096(%r13)
	leaq	(%rdi,%rbp), %rcx
	movq	%rcx, 4104(%r13)
	addq	%rsi, %rdi
	movq	%rdi, 4112(%r13)
	movq	%rbx, %r8
	movq	%r12, %rdx
	call	memcpy
	addq	%rbx, 4104(%r13)
	jmp	.L32
.L39:
	call	_ZSt17__throw_bad_allocv
	nop
	.seh_endproc
	.section .rdata,"dr"
.LC0:
	.ascii "unknown fast_io_error\0"
	.section	.text$_ZNK7fast_io13fast_io_error4whatEv,"x"
	.linkonce discard
	.align 2
	.globl	_ZNK7fast_io13fast_io_error4whatEv
	.def	_ZNK7fast_io13fast_io_error4whatEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK7fast_io13fast_io_error4whatEv
_ZNK7fast_io13fast_io_error4whatEv:
.LFB5589:
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	movl	$4184, %eax
	call	___chkstk_ms
	subq	%rax, %rsp
	.seh_stackalloc	4184
	.seh_endprologue
	movq	%rcx, %rsi
	leaq	48(%rsp), %rdi
	movq	%rdi, 4144(%rsp)
	movq	%rdi, 4152(%rsp)
	leaq	4144(%rsp), %rax
	movq	%rax, 4160(%rsp)
	leaq	16+_ZTVN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE(%rip), %rax
	movq	%rax, 32(%rsp)
	movq	%rdi, 40(%rsp)
	leaq	32(%rsp), %rdx
	movq	(%rcx), %rax
.LEHB0:
	call	*24(%rax)
	movq	4152(%rsp), %rcx
	cmpq	4160(%rsp), %rcx
	je	.L62
	movb	$0, (%rcx)
	addq	$1, %rcx
	movq	%rcx, 4152(%rsp)
	movq	4144(%rsp), %r12
	cmpq	%rdi, %r12
	je	.L63
.L44:
	movq	8(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L47
	call	_ZdaPv
.L47:
	movq	%r12, 8(%rsi)
.L40:
	movq	%r12, %rax
	addq	$4184, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.L63:
	subq	%rdi, %rcx
	call	_Znay
	movq	%rax, %r12
	movq	8(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L45
	call	_ZdaPv
.L45:
	movq	%r12, 8(%rsi)
	movq	4144(%rsp), %r13
	movq	4152(%rsp), %r8
	subq	%r13, %r8
	jne	.L64
.L46:
	cmpq	%rdi, %r13
	je	.L40
	movq	4160(%rsp), %rdx
	subq	%r13, %rdx
	movq	%r13, %rcx
	call	_ZdlPvy
	jmp	.L40
.L62:
	subq	4144(%rsp), %rcx
	movq	%rcx, %rbx
	addq	%rbx, %rbx
	js	.L65
	movq	%rbx, %rcx
	call	_Znwy
	movq	%rax, %r12
	movq	4144(%rsp), %r14
	movq	4152(%rsp), %r13
	subq	%r14, %r13
	movq	%r13, %r8
	movq	%r14, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%rdi, %r14
	je	.L43
	movq	4160(%rsp), %rdx
	subq	%r14, %rdx
	movq	%r14, %rcx
	call	_ZdlPvy
.L43:
	movq	%r12, 4144(%rsp)
	addq	%r12, %r13
	addq	%r12, %rbx
	movq	%rbx, 4160(%rsp)
	movb	$0, 0(%r13)
	addq	$1, %r13
	movq	%r13, 4152(%rsp)
	jmp	.L44
.L64:
	movq	%r13, %rdx
	movq	%r12, %rcx
	call	memmove
	jmp	.L46
.L65:
	call	_ZSt17__throw_bad_allocv
.LEHE0:
.L52:
	movq	%rax, %r12
	movq	4144(%rsp), %rcx
	cmpq	%rdi, %rcx
	je	.L50
	movq	4160(%rsp), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
.L50:
	movq	%r12, %rcx
	call	__cxa_begin_catch
	call	__cxa_end_catch
	leaq	.LC0(%rip), %r12
	jmp	.L40
	.def	__gxx_personality_seh0;	.scl	2;	.type	32;	.endef
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
	.align 4
.LLSDA5589:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT5589-.LLSDATTD5589
.LLSDATTD5589:
	.byte	0x1
	.uleb128 .LLSDACSE5589-.LLSDACSB5589
.LLSDACSB5589:
	.uleb128 .LEHB0-.LFB5589
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L52-.LFB5589
	.uleb128 0x3
.LLSDACSE5589:
	.byte	0x1
	.byte	0
	.byte	0
	.byte	0x7d
	.align 4
	.long	0

.LLSDATT5589:
	.section	.text$_ZNK7fast_io13fast_io_error4whatEv,"x"
	.linkonce discard
	.seh_endproc
	.section	.text$_ZN7fast_io17throw_posix_errorEv,"x"
	.linkonce discard
	.globl	_ZN7fast_io17throw_posix_errorEv
	.def	_ZN7fast_io17throw_posix_errorEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io17throw_posix_errorEv
_ZN7fast_io17throw_posix_errorEv:
.LFB11704:
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	movl	$24, %ecx
	call	__cxa_allocate_exception
	movq	%rax, %r12
.LEHB1:
	call	*__imp__errno(%rip)
.LEHE1:
	movl	(%rax), %eax
	movq	$0, 8(%r12)
	leaq	16+_ZTVN7fast_io11posix_errorE(%rip), %rdx
	movq	%rdx, (%r12)
	movl	%eax, 16(%r12)
	leaq	_ZN7fast_io11posix_errorD1Ev(%rip), %r8
	leaq	_ZTIN7fast_io11posix_errorE(%rip), %rdx
	movq	%r12, %rcx
.LEHB2:
	call	__cxa_throw
.L68:
	movq	%rax, %r13
	movq	%r12, %rcx
	call	__cxa_free_exception
	movq	%r13, %rcx
	call	_Unwind_Resume
	nop
.LEHE2:
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA11704:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE11704-.LLSDACSB11704
.LLSDACSB11704:
	.uleb128 .LEHB1-.LFB11704
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L68-.LFB11704
	.uleb128 0
	.uleb128 .LEHB2-.LFB11704
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
.LLSDACSE11704:
	.section	.text$_ZN7fast_io17throw_posix_errorEv,"x"
	.linkonce discard
	.seh_endproc
	.text
	.def	_ZN7fast_io7details14print_fallbackILb1ENS_28basic_c_io_observer_unlockedIcEEJNS_5manip4chvwIPcEEEEEvRT0_DpOT1_.isra.0;	.scl	3;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details14print_fallbackILb1ENS_28basic_c_io_observer_unlockedIcEEJNS_5manip4chvwIPcEEEEEvRT0_DpOT1_.isra.0
_ZN7fast_io7details14print_fallbackILb1ENS_28basic_c_io_observer_unlockedIcEEJNS_5manip4chvwIPcEEEEEvRT0_DpOT1_.isra.0:
.LFB16116:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	movl	$4168, %eax
	call	___chkstk_ms
	subq	%rax, %rsp
	.seh_stackalloc	4168
	.seh_endprologue
	movq	%rcx, %rdi
	movq	%rdx, %r12
	leaq	32(%rsp), %rsi
	movq	%rsi, 4128(%rsp)
	movq	%rsi, 4136(%rsp)
	leaq	4128(%rsp), %rax
	movq	%rax, 4144(%rsp)
	movq	%rdx, %rcx
	call	strlen
	movq	%rax, %rbx
	cmpq	$4096, %rax
	ja	.L90
	movq	%rax, %r8
	movq	%r12, %rdx
	movq	%rsi, %rcx
	call	memcpy
	addq	4136(%rsp), %rbx
	movq	%rbx, %r8
	movq	%rbx, 4136(%rsp)
	movq	4144(%rsp), %rbp
.L72:
	cmpq	%rbp, %r8
	je	.L91
	movb	$10, (%r8)
	leaq	1(%r8), %rbx
	movq	%rbx, 4136(%rsp)
	movq	4128(%rsp), %r12
.L76:
	movq	(%rdi), %rdi
	subq	%r12, %rbx
	movq	(%rdi), %rcx
	movq	16(%rdi), %rdx
	leaq	(%rcx,%rbx), %rbp
	movslq	36(%rdi), %rax
	addq	%rdx, %rax
	cmpq	%rax, %rbp
	jnb	.L77
	movq	%rbx, %r8
	movq	%r12, %rdx
	call	memcpy
	orl	$65536, 24(%rdi)
	movq	%rbp, %rax
	subq	(%rdi), %rax
	subl	%eax, 8(%rdi)
	movq	%rbp, (%rdi)
.L78:
	movq	4128(%rsp), %rcx
	cmpq	%rsi, %rcx
	je	.L69
	movq	4144(%rsp), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
	nop
.L69:
	addq	$4168, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L90:
	cmpq	$8192, %rax
	movl	$8192, %r14d
	cmovnb	%rax, %r14
	movq	%r14, %rcx
.LEHB3:
	call	_Znwy
	movq	%rax, %rbp
	movq	4128(%rsp), %r15
	movq	4136(%rsp), %r13
	subq	%r15, %r13
	movq	%r13, %r8
	movq	%r15, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%rsi, %r15
	je	.L71
	movq	4144(%rsp), %rdx
	subq	%r15, %rdx
	movq	%r15, %rcx
	call	_ZdlPvy
.L71:
	movq	%rbp, 4128(%rsp)
	addq	%rbp, %r13
	addq	%r14, %rbp
	movq	%rbp, 4144(%rsp)
	movq	%rbx, %r8
	movq	%r12, %rdx
	movq	%r13, %rcx
	call	memcpy
	leaq	0(%r13,%rbx), %r8
	movq	%r8, 4136(%rsp)
	jmp	.L72
.L91:
	subq	4128(%rsp), %r8
	addq	%r8, %r8
	movq	%r8, %rbp
	js	.L92
	movq	%r8, %rcx
	call	_Znwy
	movq	%rax, %r12
	movq	4128(%rsp), %r13
	movq	4136(%rsp), %rbx
	subq	%r13, %rbx
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%rsi, %r13
	je	.L75
	movq	4144(%rsp), %rdx
	subq	%r13, %rdx
	movq	%r13, %rcx
	call	_ZdlPvy
.L75:
	movq	%r12, 4128(%rsp)
	addq	%r12, %rbx
	addq	%r12, %rbp
	movq	%rbp, 4144(%rsp)
	movb	$10, (%rbx)
	addq	$1, %rbx
	movq	%rbx, 4136(%rsp)
	jmp	.L76
.L77:
	testq	%rdx, %rdx
	je	.L93
.L79:
	movq	%rdi, %r9
	movq	%rbx, %r8
	movl	$1, %edx
	movq	%r12, %rcx
	call	fwrite
	cmpq	%rax, %rbx
	jbe	.L78
	call	_ZN7fast_io17throw_posix_errorEv
.LEHE3:
.L93:
	movq	%rdi, %rcx
	call	*__imp__fileno(%rip)
	movl	%eax, %r13d
	movl	%eax, %ecx
.LEHB4:
	call	*__imp__isatty(%rip)
	testl	%eax, %eax
	je	.L79
	movl	%ebx, %r8d
	movq	%r12, %rdx
	movl	%r13d, %ecx
	call	write
.LEHE4:
	testl	%eax, %eax
	jns	.L78
	movl	$24, %ecx
	call	__cxa_allocate_exception
	movq	%rax, %r13
.LEHB5:
	call	*__imp__errno(%rip)
.LEHE5:
	movl	(%rax), %eax
	movq	$0, 8(%r13)
	leaq	16+_ZTVN7fast_io11posix_errorE(%rip), %rdi
	movq	%rdi, 0(%r13)
	movl	%eax, 16(%r13)
	leaq	_ZN7fast_io11posix_errorD1Ev(%rip), %r8
	leaq	_ZTIN7fast_io11posix_errorE(%rip), %rdx
	movq	%r13, %rcx
.LEHB6:
	call	__cxa_throw
.L92:
	call	_ZSt17__throw_bad_allocv
.LEHE6:
.L85:
	movq	%rax, %r12
	jmp	.L82
.L86:
	movq	%rax, %r12
	movq	%r13, %rcx
	call	__cxa_free_exception
.L82:
	movq	4128(%rsp), %rcx
	cmpq	%rsi, %rcx
	je	.L84
	movq	4144(%rsp), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
.L84:
	movq	%r12, %rcx
.LEHB7:
	call	_Unwind_Resume
	nop
.LEHE7:
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA16116:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE16116-.LLSDACSB16116
.LLSDACSB16116:
	.uleb128 .LEHB3-.LFB16116
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L85-.LFB16116
	.uleb128 0
	.uleb128 .LEHB4-.LFB16116
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L85-.LFB16116
	.uleb128 0
	.uleb128 .LEHB5-.LFB16116
	.uleb128 .LEHE5-.LEHB5
	.uleb128 .L86-.LFB16116
	.uleb128 0
	.uleb128 .LEHB6-.LFB16116
	.uleb128 .LEHE6-.LEHB6
	.uleb128 .L85-.LFB16116
	.uleb128 0
	.uleb128 .LEHB7-.LFB16116
	.uleb128 .LEHE7-.LEHB7
	.uleb128 0
	.uleb128 0
.LLSDACSE16116:
	.text
	.seh_endproc
	.section	.text$_ZN7fast_io7details13print_controlINS_5ospanIcLy200EEERNS_3shaINS_15sha256_functionELb1EEEEEvRT_OT0_,"x"
	.linkonce discard
	.globl	_ZN7fast_io7details13print_controlINS_5ospanIcLy200EEERNS_3shaINS_15sha256_functionELb1EEEEEvRT_OT0_
	.def	_ZN7fast_io7details13print_controlINS_5ospanIcLy200EEERNS_3shaINS_15sha256_functionELb1EEEEEvRT_OT0_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details13print_controlINS_5ospanIcLy200EEERNS_3shaINS_15sha256_functionELb1EEEEEvRT_OT0_
_ZN7fast_io7details13print_controlINS_5ospanIcLy200EEERNS_3shaINS_15sha256_functionELb1EEEEEvRT_OT0_:
.LFB15562:
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$256, %rsp
	.seh_stackalloc	256
	.seh_endprologue
	movq	%rcx, %r10
	movq	8(%rcx), %rcx
	movq	(%r10), %rdi
	leaq	256(%rcx), %r8
	leaq	200(%rdi), %rax
	cmpq	%rax, %r8
	jnb	.L95
	leaq	64(%rcx), %r11
	jmp	.L113
.L166:
	addl	$87, %r8d
.L97:
	movb	%r8b, 7(%rcx)
	movl	%eax, %r8d
	shrl	$8, %r8d
	andl	$15, %r9d
	cmpb	$9, %r9b
	jle	.L98
	addl	$87, %r9d
.L99:
	movb	%r9b, 6(%rcx)
	movl	%eax, %r9d
	shrl	$12, %r9d
	andl	$15, %r8d
	cmpb	$9, %r8b
	jle	.L100
	addl	$87, %r8d
.L101:
	movb	%r8b, 5(%rcx)
	movl	%eax, %r8d
	shrl	$16, %r8d
	andl	$15, %r9d
	cmpb	$9, %r9b
	jle	.L102
	addl	$87, %r9d
.L103:
	movb	%r9b, 4(%rcx)
	movl	%eax, %r9d
	shrl	$20, %r9d
	andl	$15, %r8d
	cmpb	$9, %r8b
	jle	.L104
	addl	$87, %r8d
.L105:
	movb	%r8b, 3(%rcx)
	movl	%eax, %r8d
	shrl	$24, %r8d
	andl	$15, %r9d
	cmpb	$9, %r9b
	jle	.L106
	addl	$87, %r9d
.L107:
	movb	%r9b, 2(%rcx)
	shrl	$28, %eax
	andl	$15, %r8d
	cmpb	$9, %r8b
	jle	.L108
	addl	$87, %r8d
.L109:
	movb	%r8b, 1(%rcx)
	cmpl	$9, %eax
	jbe	.L110
	addl	$87, %eax
	movb	%al, (%rcx)
	addq	$8, %rcx
	addq	$4, %rdx
	cmpq	%r11, %rcx
	je	.L165
.L113:
	movl	(%rdx), %eax
	movl	%eax, %r9d
	shrl	$4, %r9d
	movl	%eax, %r8d
	andl	$15, %r8d
	cmpb	$9, %r8b
	jg	.L166
	addl	$48, %r8d
	jmp	.L97
.L110:
	addl	$48, %eax
	movb	%al, (%rcx)
	addq	$8, %rcx
	addq	$4, %rdx
	cmpq	%r11, %rcx
	jne	.L113
.L165:
	movq	%r11, 8(%r10)
.L94:
	addq	$256, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	ret
.L108:
	addl	$48, %r8d
	jmp	.L109
.L106:
	addl	$48, %r9d
	jmp	.L107
.L104:
	addl	$48, %r8d
	jmp	.L105
.L102:
	addl	$48, %r9d
	jmp	.L103
.L100:
	addl	$48, %r8d
	jmp	.L101
.L98:
	addl	$48, %r9d
	jmp	.L99
.L95:
	movq	%rdx, %rax
	movq	%rsp, %rbx
	movq	%rbx, %r8
	leaq	64(%rsp), %rsi
	jmp	.L132
.L168:
	addl	$87, %r9d
.L116:
	movb	%r9b, 7(%r8)
	movl	%edx, %r9d
	shrl	$8, %r9d
	andl	$15, %r11d
	cmpb	$9, %r11b
	jle	.L117
	addl	$87, %r11d
.L118:
	movb	%r11b, 6(%r8)
	movl	%edx, %r11d
	shrl	$12, %r11d
	andl	$15, %r9d
	cmpb	$9, %r9b
	jle	.L119
	addl	$87, %r9d
.L120:
	movb	%r9b, 5(%r8)
	movl	%edx, %r9d
	shrl	$16, %r9d
	andl	$15, %r11d
	cmpb	$9, %r11b
	jle	.L121
	addl	$87, %r11d
.L122:
	movb	%r11b, 4(%r8)
	movl	%edx, %r11d
	shrl	$20, %r11d
	andl	$15, %r9d
	cmpb	$9, %r9b
	jle	.L123
	addl	$87, %r9d
.L124:
	movb	%r9b, 3(%r8)
	movl	%edx, %r9d
	shrl	$24, %r9d
	andl	$15, %r11d
	cmpb	$9, %r11b
	jle	.L125
	addl	$87, %r11d
.L126:
	movb	%r11b, 2(%r8)
	shrl	$28, %edx
	andl	$15, %r9d
	cmpb	$9, %r9b
	jle	.L127
	addl	$87, %r9d
.L128:
	movb	%r9b, 1(%r8)
	cmpl	$9, %edx
	jbe	.L129
	addl	$87, %edx
.L164:
	movb	%dl, (%r8)
	addq	$8, %r8
	addq	$4, %rax
	cmpq	%rsi, %r8
	je	.L167
.L132:
	movl	(%rax), %edx
	movl	%edx, %r11d
	shrl	$4, %r11d
	movl	%edx, %r9d
	andl	$15, %r9d
	cmpb	$9, %r9b
	jg	.L168
	addl	$48, %r9d
	jmp	.L116
.L129:
	addl	$48, %edx
	jmp	.L164
.L127:
	addl	$48, %r9d
	jmp	.L128
.L125:
	addl	$48, %r11d
	jmp	.L126
.L123:
	addl	$48, %r9d
	jmp	.L124
.L121:
	addl	$48, %r11d
	jmp	.L122
.L119:
	addl	$48, %r9d
	jmp	.L120
.L117:
	addl	$48, %r11d
	jmp	.L118
.L167:
	subq	%rcx, %rdi
	leaq	200(%rdi), %r8
	cmpq	$64, %r8
	movl	$64, %eax
	cmova	%rax, %r8
	movl	%r8d, %eax
	movq	%rcx, %rdi
	movq	%rbx, %rsi
	cmpl	$8, %r8d
	jnb	.L169
.L133:
	testb	$4, %al
	jne	.L170
.L139:
	testb	$2, %al
	jne	.L171
.L140:
	testb	$1, %al
	.p2align 4,,5
	jne	.L172
.L141:
	addq	%r8, 8(%r10)
	.p2align 4,,3
	jmp	.L94
.L172:
	movsb
	.p2align 4,,4
	jmp	.L141
.L171:
	movsw
	.p2align 4,,4
	jmp	.L140
.L170:
	movsl
	.p2align 4,,7
	jmp	.L139
.L169:
	testb	$1, %cl
	.p2align 4,,5
	jne	.L173
.L134:
	testb	$2, %dil
	.p2align 4,,3
	jne	.L174
.L135:
	testb	$4, %dil
	jne	.L175
.L136:
	cmpl	$8, %eax
	jb	.L133
	movl	%eax, %r9d
	andl	$-8, %r9d
	xorl	%edx, %edx
.L137:
	movl	%edx, %ecx
	movq	(%rsi,%rcx), %r11
	movq	%r11, (%rdi,%rcx)
	addl	$8, %edx
	cmpl	%r9d, %edx
	jb	.L137
	addq	%rdx, %rdi
	addq	%rdx, %rsi
	jmp	.L133
.L175:
	movl	(%rsi), %edx
	movl	%edx, (%rdi)
	addq	$4, %rdi
	addq	$4, %rsi
	subl	$4, %eax
	jmp	.L136
.L174:
	movzwl	(%rsi), %edx
	movw	%dx, (%rdi)
	addq	$2, %rdi
	addq	$2, %rsi
	subl	$2, %eax
	jmp	.L135
.L173:
	movzbl	(%rbx), %edx
	movb	%dl, (%rcx)
	addq	$1, %rdi
	leaq	1(%rsp), %rsi
	subl	$1, %eax
	jmp	.L134
	.seh_endproc
	.def	__main;	.scl	2;	.type	32;	.endef
	.section	.text.startup,"x"
	.globl	main
	.def	main;	.scl	2;	.type	32;	.endef
	.seh_proc	main
main:
.LFB14455:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$456, %rsp
	.seh_stackalloc	456
	.seh_endprologue
	call	__main
	movabsq	$-4942790177982912921, %rax
	movq	%rax, 128(%rsp)
	movabsq	$-6534734903820487822, %rax
	movq	%rax, 136(%rsp)
	movabsq	$-7276294671082564993, %rax
	movq	%rax, 144(%rsp)
	movabsq	$6620516960021240235, %rax
	movq	%rax, 152(%rsp)
	movq	$0, 160(%rsp)
	movq	$0, 168(%rsp)
	movl	$1048576, %ecx
.LEHB8:
	call	_Znay
.LEHE8:
	movq	%rax, 104(%rsp)
	movl	$200, %edx
	movq	%rax, %rcx
.LEHB9:
	call	SystemFunction036
.LEHE9:
	testl	%eax, %eax
	je	.L236
	movq	104(%rsp), %r15
	leaq	192(%r15), %rax
	movq	%rax, 88(%rsp)
	movl	128(%rsp), %eax
	movl	%eax, 40(%rsp)
	movl	132(%rsp), %eax
	movl	%eax, 44(%rsp)
	movl	136(%rsp), %eax
	movl	%eax, 48(%rsp)
	movl	140(%rsp), %eax
	movl	%eax, 52(%rsp)
	movl	144(%rsp), %eax
	movl	%eax, 56(%rsp)
	movl	148(%rsp), %eax
	movl	%eax, 60(%rsp)
	movl	152(%rsp), %eax
	movl	%eax, 64(%rsp)
	movl	156(%rsp), %eax
	movl	%eax, 68(%rsp)
	leaq	240(%rsp), %rax
	movq	%rax, 80(%rsp)
	jmp	.L180
.L238:
	addq	$64, %r15
	leaq	64(%r14), %rdi
	movq	%rdi, 96(%rsp)
	movq	%rdi, %r14
	movl	$16, %r13d
	movq	%r15, 72(%rsp)
	movl	%ebx, %r15d
.L179:
	leal	1(%r13), %ebp
	movl	%ebp, %r9d
	andl	$15, %r9d
	movl	240(%rsp,%r9,4), %ebx
	leal	14(%r13), %r9d
	andl	$15, %r9d
	movl	240(%rsp,%r9,4), %edi
	movl	%ebx, %r12d
	rorl	$7, %r12d
	movl	%ebx, %r9d
	roll	$14, %r9d
	xorl	%r12d, %r9d
	shrl	$3, %ebx
	xorl	%ebx, %r9d
	leal	9(%r13), %ebx
	andl	$15, %ebx
	andl	$15, %r13d
	movl	240(%rsp,%r13,4), %r12d
	addl	240(%rsp,%rbx,4), %r12d
	leal	(%r9,%r12), %ebx
	movl	%edi, %r12d
	roll	$15, %r12d
	movl	%edi, %r9d
	roll	$13, %r9d
	xorl	%r12d, %r9d
	shrl	$10, %edi
	xorl	%edi, %r9d
	addl	%r9d, %ebx
	movl	%ebx, 240(%rsp,%r13,4)
	movl	%r11d, %r9d
	rorl	$6, %r9d
	movl	%r11d, %edi
	rorl	$11, %edi
	xorl	%edi, %r9d
	movl	%r11d, %edi
	roll	$7, %edi
	xorl	%edi, %r9d
	movl	%r11d, %edi
	notl	%edi
	andl	%esi, %edi
	movl	%r11d, %r12d
	andl	%ecx, %r12d
	xorl	%r12d, %edi
	addl	%edi, %r9d
	addl	(%r14), %r9d
	addl	%ebx, %r9d
	addl	%r9d, %edx
	movl	%eax, %r9d
	rorl	$2, %r9d
	movl	%eax, %ebx
	rorl	$13, %ebx
	xorl	%ebx, %r9d
	movl	%eax, %ebx
	roll	$10, %ebx
	xorl	%r9d, %ebx
	movl	%r8d, %r9d
	xorl	%r10d, %r9d
	andl	%eax, %r9d
	movl	%r8d, %edi
	andl	%r10d, %edi
	xorl	%edi, %r9d
	addl	%ebx, %r9d
	leal	(%rdx,%r15), %edi
	addl	%r9d, %edx
	addq	$4, %r14
	cmpl	$64, %ebp
	jne	.L209
	movq	72(%rsp), %r15
	addl	%edx, 40(%rsp)
	addl	%eax, 44(%rsp)
	addl	%r8d, 48(%rsp)
	addl	%r10d, 52(%rsp)
	addl	%edi, 56(%rsp)
	addl	%r11d, 60(%rsp)
	addl	%ecx, 64(%rsp)
	addl	%esi, 68(%rsp)
	cmpq	%r15, 88(%rsp)
	je	.L237
.L180:
	movl	52(%rsp), %ebp
	movl	48(%rsp), %ebx
	movl	44(%rsp), %r10d
	movl	40(%rsp), %r8d
	movl	64(%rsp), %edx
	movl	60(%rsp), %esi
	movl	68(%rsp), %r9d
	movl	56(%rsp), %ecx
	xorl	%edi, %edi
	movq	80(%rsp), %r12
.L178:
	movl	(%r15,%rdi), %eax
	bswap	%eax
	movl	%eax, (%r12,%rdi)
	movl	%ecx, %r11d
	rorl	$6, %r11d
	movl	%ecx, %r13d
	rorl	$11, %r13d
	xorl	%r13d, %r11d
	movl	%ecx, %r13d
	roll	$7, %r13d
	xorl	%r13d, %r11d
	leaq	_ZN7fast_io7details6sha2564K256E(%rip), %r14
	addl	(%r14,%rdi), %eax
	addl	%r11d, %eax
	movl	%ecx, %r11d
	notl	%r11d
	andl	%edx, %r11d
	movl	%ecx, %r13d
	andl	%esi, %r13d
	xorl	%r13d, %r11d
	addl	%r11d, %eax
	addl	%r9d, %eax
	movl	%r8d, %r9d
	rorl	$2, %r9d
	movl	%r8d, %r11d
	rorl	$13, %r11d
	xorl	%r11d, %r9d
	movl	%r8d, %r11d
	roll	$10, %r11d
	xorl	%r9d, %r11d
	movl	%r10d, %r9d
	xorl	%ebx, %r9d
	andl	%r8d, %r9d
	movl	%r10d, %r13d
	andl	%ebx, %r13d
	xorl	%r13d, %r9d
	addl	%r11d, %r9d
	leal	(%rax,%rbp), %r11d
	addl	%r9d, %eax
	addq	$4, %rdi
	movl	%edx, %r9d
	movl	%ebx, %ebp
	cmpq	$64, %rdi
	je	.L238
	movl	%r10d, %ebx
	movl	%r8d, %r10d
	movl	%eax, %r8d
	movl	%esi, %edx
	movl	%ecx, %esi
	movl	%r11d, %ecx
	jmp	.L178
.L209:
	movl	%r10d, %r15d
	movl	%r8d, %r10d
	movl	%eax, %r8d
	movl	%edx, %eax
	movl	%esi, %edx
	movl	%ecx, %esi
	movl	%r11d, %ecx
	movl	%edi, %r11d
	movl	%ebp, %r13d
	jmp	.L179
.L237:
	movl	40(%rsp), %eax
	movl	%eax, 128(%rsp)
	movl	44(%rsp), %eax
	movl	%eax, 132(%rsp)
	movl	48(%rsp), %eax
	movl	%eax, 136(%rsp)
	movl	52(%rsp), %eax
	movl	%eax, 140(%rsp)
	movl	56(%rsp), %eax
	movl	%eax, 144(%rsp)
	movl	60(%rsp), %eax
	movl	%eax, 148(%rsp)
	movl	64(%rsp), %eax
	movl	%eax, 152(%rsp)
	movl	68(%rsp), %eax
	movl	%eax, 156(%rsp)
	addq	$3, 168(%rsp)
	movq	104(%rsp), %rsi
	movq	192(%rsi), %rbx
	movl	$1048576, %edx
	movq	%rsi, %rcx
	call	RtlSecureZeroMemory
	movq	%rsi, %rcx
	call	_ZdaPv
	movq	168(%rsp), %rax
	salq	$6, %rax
	leaq	64(,%rax,8), %r8
	leaq	184(%rsp), %rdi
	xorl	%eax, %eax
	xorl	%edx, %edx
.L181:
	movl	%edx, %ecx
	movq	%rax, 184(%rsp,%rcx)
	movq	%rax, 192(%rsp,%rcx)
	movq	%rax, 200(%rsp,%rcx)
	movq	%rax, 208(%rsp,%rcx)
	addl	$32, %edx
	jc	.L181
	addq	%rdx, %rdi
	stosq
	stosq
	movq	%rbx, 176(%rsp)
	movb	$-128, 184(%rsp)
	movq	%r8, %rax
	bswap	%rax
	movq	%rax, 232(%rsp)
	movl	128(%rsp), %r9d
	movl	%r9d, 40(%rsp)
	movl	132(%rsp), %edi
	movl	%edi, 44(%rsp)
	movl	136(%rsp), %r13d
	movl	%r13d, 48(%rsp)
	movl	140(%rsp), %r11d
	movl	%r11d, 52(%rsp)
	movl	144(%rsp), %esi
	movl	%esi, 56(%rsp)
	movl	148(%rsp), %r12d
	movl	%r12d, 60(%rsp)
	movl	152(%rsp), %eax
	movl	%eax, 64(%rsp)
	movl	156(%rsp), %ecx
	movl	%ecx, 68(%rsp)
	xorl	%r10d, %r10d
	leaq	176(%rsp), %r15
	jmp	.L183
.L210:
	movl	%edi, %r13d
	movl	%r9d, %edi
	movl	%edx, %r9d
	movl	%r12d, %eax
	movl	%esi, %r12d
	movl	%r8d, %esi
.L183:
	movl	(%r15,%r10), %r8d
	bswap	%r8d
	movq	80(%rsp), %rdx
	movl	%r8d, (%rdx,%r10)
	movl	%esi, %edx
	rorl	$6, %edx
	movl	%esi, %ebx
	rorl	$11, %ebx
	xorl	%ebx, %edx
	movl	%esi, %ebx
	roll	$7, %ebx
	xorl	%ebx, %edx
	leaq	_ZN7fast_io7details6sha2564K256E(%rip), %rbx
	addl	(%rbx,%r10), %r8d
	addl	%r8d, %edx
	movl	%esi, %r8d
	notl	%r8d
	andl	%eax, %r8d
	movl	%esi, %ebx
	andl	%r12d, %ebx
	xorl	%ebx, %r8d
	addl	%r8d, %edx
	addl	%ecx, %edx
	movl	%r9d, %ecx
	rorl	$2, %ecx
	movl	%r9d, %r8d
	rorl	$13, %r8d
	xorl	%r8d, %ecx
	movl	%r9d, %r8d
	roll	$10, %r8d
	xorl	%r8d, %ecx
	movl	%edi, %r8d
	xorl	%r13d, %r8d
	andl	%r9d, %r8d
	movl	%edi, %ebx
	andl	%r13d, %ebx
	xorl	%ebx, %r8d
	addl	%r8d, %ecx
	leal	(%rdx,%r11), %r8d
	addl	%ecx, %edx
	addq	$4, %r10
	movl	%eax, %ecx
	movl	%r13d, %r11d
	cmpq	$64, %r10
	jne	.L210
	movl	$16, %ebp
	movq	96(%rsp), %r14
	movq	%r15, 72(%rsp)
	jmp	.L185
.L211:
	movl	%r9d, %edi
	movl	%edx, %r9d
	movl	%ecx, %edx
	movl	%esi, %r12d
	movl	%r8d, %esi
	movl	%r10d, %r8d
.L185:
	movl	%ebp, %ecx
	addl	$1, %ebp
	movl	%ebp, %r10d
	andl	$15, %r10d
	movl	240(%rsp,%r10,4), %r10d
	leal	14(%rcx), %r11d
	andl	$15, %r11d
	movl	240(%rsp,%r11,4), %r11d
	movl	%r10d, %r15d
	rorl	$7, %r15d
	movl	%r10d, %ebx
	roll	$14, %ebx
	xorl	%r15d, %ebx
	shrl	$3, %r10d
	xorl	%r10d, %ebx
	leal	9(%rcx), %r10d
	andl	$15, %r10d
	movl	%ecx, %r15d
	andl	$15, %r15d
	movl	240(%rsp,%r15,4), %ecx
	addl	240(%rsp,%r10,4), %ecx
	leal	(%rbx,%rcx), %r10d
	movl	%r11d, %ebx
	roll	$15, %ebx
	movl	%r11d, %ecx
	roll	$13, %ecx
	xorl	%ebx, %ecx
	shrl	$10, %r11d
	xorl	%r11d, %ecx
	addl	%ecx, %r10d
	movl	%r10d, 240(%rsp,%r15,4)
	movl	%r8d, %ecx
	rorl	$6, %ecx
	movl	%r8d, %r11d
	rorl	$11, %r11d
	xorl	%r11d, %ecx
	movl	%r8d, %r11d
	roll	$7, %r11d
	xorl	%r11d, %ecx
	movl	%r8d, %r11d
	notl	%r11d
	andl	%r12d, %r11d
	movl	%r8d, %ebx
	andl	%esi, %ebx
	xorl	%ebx, %r11d
	addl	%r11d, %ecx
	addl	(%r14), %ecx
	addl	%r10d, %ecx
	addl	%ecx, %eax
	movl	%edx, %ecx
	rorl	$2, %ecx
	movl	%edx, %r10d
	rorl	$13, %r10d
	xorl	%r10d, %ecx
	movl	%edx, %r10d
	roll	$10, %r10d
	xorl	%ecx, %r10d
	movl	%r9d, %ecx
	xorl	%edi, %ecx
	andl	%edx, %ecx
	movl	%r9d, %r11d
	andl	%edi, %r11d
	xorl	%r11d, %ecx
	addl	%r10d, %ecx
	leal	(%rax,%r13), %r10d
	addl	%eax, %ecx
	addq	$4, %r14
	movl	%r12d, %eax
	movl	%edi, %r13d
	cmpl	$64, %ebp
	jne	.L211
	movq	72(%rsp), %r15
	movl	40(%rsp), %eax
	addl	%ecx, %eax
	movl	%eax, 128(%rsp)
	movl	44(%rsp), %eax
	addl	%edx, %eax
	movl	%eax, 132(%rsp)
	movl	48(%rsp), %eax
	addl	%r9d, %eax
	movl	%eax, 136(%rsp)
	movl	52(%rsp), %eax
	addl	%edi, %eax
	movl	%eax, 140(%rsp)
	movl	56(%rsp), %eax
	addl	%r10d, %eax
	movl	%eax, 144(%rsp)
	movl	60(%rsp), %eax
	addl	%r8d, %eax
	movl	%eax, 148(%rsp)
	movl	64(%rsp), %eax
	addl	%esi, %eax
	movl	%eax, 152(%rsp)
	movl	68(%rsp), %eax
	addl	%r12d, %eax
	movl	%eax, 156(%rsp)
	movq	80(%rsp), %rax
	movq	%rax, 176(%rsp)
	movabsq	$8314038741038363695, %rax
	movq	%rax, 240(%rsp)
	movl	$1869176692, 248(%rsp)
	leaq	252(%rsp), %rax
	movq	%rax, 184(%rsp)
	leaq	128(%rsp), %rdx
	movq	%r15, %rcx
	call	_ZN7fast_io7details13print_controlINS_5ospanIcLy200EEERNS_3shaINS_15sha256_functionELb1EEEEEvRT_OT0_
	movq	184(%rsp), %rax
	movq	176(%rsp), %rsi
	leaq	200(%rsi), %rdx
	cmpq	%rdx, %rax
	je	.L186
	movb	$0, (%rax)
	addq	$1, %rax
	movq	%rax, 184(%rsp)
.L186:
	movl	$1, %ecx
.LEHB10:
	call	*__imp___acrt_iob_func(%rip)
.LEHE10:
	movq	%rax, %rbx
	movq	__imp___iob_func(%rip), %rbp
	call	*%rbp
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L187
	sarq	$4, %rax
	imull	$-1431655765, %eax, %eax
	leal	16(%rax), %ecx
	call	_lock
	orl	$32768, 24(%rbx)
.L188:
	movq	%rbx, 120(%rsp)
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L239
	movq	(%rbx), %r12
	movslq	36(%rbx), %rax
	addq	%rax, %rsi
	movq	80(%rsp), %r14
	movq	%r14, %rcx
	call	strlen
	movq	%rax, %r13
	leaq	1(%r12,%rax), %rdx
	leaq	(%r12,%rax), %r8
	cmpq	%rdx, %rsi
	jbe	.L191
	movq	%r12, %rdi
	movq	%r14, %rsi
	cmpl	$8, %eax
	jnb	.L240
.L192:
	testb	$4, %al
	jne	.L241
.L196:
	testb	$2, %al
	jne	.L242
.L197:
	testb	$1, %al
	.p2align 4,,5
	jne	.L243
.L198:
	movb	$10, (%r8)
.L235:
	movq	120(%rsp), %rax
	orl	$65536, 24(%rax)
	movq	%rdx, %rcx
	subq	(%rax), %rcx
	subl	%ecx, 8(%rax)
	movq	%rdx, (%rax)
.L190:
	call	*%rbp
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpq	$912, %rax
	ja	.L203
	sarq	$4, %rax
	imull	$-1431655765, %eax, %eax
	leal	16(%rax), %ecx
	call	_lock
	orl	$32768, 24(%rbx)
.L234:
	xorl	%eax, %eax
	addq	$456, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L187:
	leaq	48(%rbx), %rcx
	call	EnterCriticalSection
	jmp	.L188
.L203:
	leaq	48(%rbx), %rcx
	call	EnterCriticalSection
	.p2align 4,,4
	jmp	.L234
.L243:
	movsb
	.p2align 4,,3
	jmp	.L198
.L242:
	movsw
	.p2align 4,,8
	jmp	.L197
.L241:
	movsl
	.p2align 4,,7
	jmp	.L196
.L240:
	testb	$1, %r12b
	.p2align 4,,4
	jne	.L244
.L193:
	testb	$2, %dil
	.p2align 4,,2
	jne	.L245
.L194:
	testb	$4, %dil
	jne	.L246
.L195:
	movl	%eax, %ecx
	shrl	$3, %ecx
	rep movsq
	jmp	.L192
.L239:
	leaq	120(%rsp), %rcx
	movq	80(%rsp), %rdx
.LEHB11:
	call	_ZN7fast_io7details14print_fallbackILb1ENS_28basic_c_io_observer_unlockedIcEEJNS_5manip4chvwIPcEEEEEvRT0_DpOT1_.isra.0
	jmp	.L190
.L191:
	cmpq	%r8, %rsi
	jbe	.L199
	movl	%eax, %ecx
	movq	%r12, %rdi
	movq	80(%rsp), %rsi
	rep movsb
	orl	$65536, 24(%rbx)
	movq	%r8, %rax
	subq	(%rbx), %rax
	subl	%eax, 8(%rbx)
	movq	%r8, (%rbx)
.L200:
	movq	120(%rsp), %rdx
	movq	(%rdx), %rax
	movslq	36(%rdx), %rcx
	addq	16(%rdx), %rcx
	cmpq	%rcx, %rax
	je	.L247
	movb	$10, (%rax)
	leaq	1(%rax), %rdx
	jmp	.L235
.L244:
	movzbl	(%r14), %eax
	movb	%al, (%r12)
	addq	$1, %rdi
	leaq	241(%rsp), %rsi
	leal	-1(%r13), %eax
	jmp	.L193
.L245:
	movzwl	(%rsi), %ecx
	movw	%cx, (%rdi)
	addq	$2, %rdi
	addq	$2, %rsi
	subl	$2, %eax
	jmp	.L194
.L246:
	movl	(%rsi), %ecx
	movl	%ecx, (%rdi)
	addq	$4, %rdi
	addq	$4, %rsi
	subl	$4, %eax
	jmp	.L195
.L247:
	orl	$65536, 24(%rdx)
	movl	$10, %ecx
	call	*__imp__flsbuf(%rip)
	addl	$1, %eax
	jne	.L190
	call	_ZN7fast_io17throw_posix_errorEv
.L199:
	movq	%rbx, %r9
	movq	%rax, %r8
	movl	$1, %edx
	movq	80(%rsp), %rcx
	call	fwrite
	cmpq	%rax, %r13
	jbe	.L200
	call	_ZN7fast_io17throw_posix_errorEv
.LEHE11:
.L236:
	movl	$24, %ecx
	call	__cxa_allocate_exception
	movq	%rax, %r12
	call	GetLastError
	movq	$0, 8(%r12)
	leaq	16+_ZTVN7fast_io11win32_errorE(%rip), %rsi
	movq	%rsi, (%r12)
	movl	%eax, 16(%r12)
	leaq	_ZN7fast_io11win32_errorD1Ev(%rip), %r8
	leaq	_ZTIN7fast_io11win32_errorE(%rip), %rdx
	movq	%r12, %rcx
.LEHB12:
	call	__cxa_throw
.LEHE12:
.L213:
	movq	%rax, %r12
	call	*%rbp
	movq	%rax, %r8
	movq	%rbx, %rax
	subq	%r8, %rax
	movq	%rax, %rcx
	sarq	$4, %rcx
	movabsq	$-6148914691236517205, %rdx
	imulq	%rdx, %rcx
	cmpq	$912, %rax
	jbe	.L248
	leaq	48(%rbx), %rcx
	call	EnterCriticalSection
.L207:
	movq	%r12, %rcx
.LEHB13:
	call	_Unwind_Resume
.LEHE13:
.L248:
	addl	$16, %ecx
	call	_lock
	orl	$32768, 24(%rbx)
	jmp	.L207
.L212:
	movq	%rax, %r12
	movl	$1048576, %edx
	movq	104(%rsp), %rsi
	movq	%rsi, %rcx
	call	RtlSecureZeroMemory
	movq	%rsi, %rcx
	call	_ZdaPv
	movq	%r12, %rcx
.LEHB14:
	call	_Unwind_Resume
	nop
.LEHE14:
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA14455:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE14455-.LLSDACSB14455
.LLSDACSB14455:
	.uleb128 .LEHB8-.LFB14455
	.uleb128 .LEHE8-.LEHB8
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB9-.LFB14455
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L212-.LFB14455
	.uleb128 0
	.uleb128 .LEHB10-.LFB14455
	.uleb128 .LEHE10-.LEHB10
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB11-.LFB14455
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L213-.LFB14455
	.uleb128 0
	.uleb128 .LEHB12-.LFB14455
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L212-.LFB14455
	.uleb128 0
	.uleb128 .LEHB13-.LFB14455
	.uleb128 .LEHE13-.LEHB13
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB14-.LFB14455
	.uleb128 .LEHE14-.LEHB14
	.uleb128 0
	.uleb128 0
.LLSDACSE14455:
	.section	.text.startup,"x"
	.seh_endproc
	.section	.text$_ZN7fast_io7details33reserve_write_extremely_cold_pathINS_14error_reporterEZNS0_18report_win32_errorERS2_jEUlT_E_EEvRS4_yRT0_,"x"
	.linkonce discard
	.globl	_ZN7fast_io7details33reserve_write_extremely_cold_pathINS_14error_reporterEZNS0_18report_win32_errorERS2_jEUlT_E_EEvRS4_yRT0_
	.def	_ZN7fast_io7details33reserve_write_extremely_cold_pathINS_14error_reporterEZNS0_18report_win32_errorERS2_jEUlT_E_EEvRS4_yRT0_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN7fast_io7details33reserve_write_extremely_cold_pathINS_14error_reporterEZNS0_18report_win32_errorERS2_jEUlT_E_EEvRS4_yRT0_
_ZN7fast_io7details33reserve_write_extremely_cold_pathINS_14error_reporterEZNS0_18report_win32_errorERS2_jEUlT_E_EEvRS4_yRT0_:
.LFB15681:
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$1104, %rsp
	.seh_stackalloc	1104
	.seh_endprologue
	movq	%rcx, %rsi
	cmpq	$1024, %rdx
	ja	.L250
	movq	(%r8), %rax
	movq	$0, 48(%rsp)
	movl	$32768, 40(%rsp)
	leaq	80(%rsp), %r12
	movq	%r12, 32(%rsp)
	movl	$1024, %r9d
	movl	(%rax), %r8d
	xorl	%edx, %edx
	movl	$4608, %ecx
	call	FormatMessageA
	movl	%eax, %ebx
	addq	%r12, %rbx
	leaq	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_(%rip), %rdi
.L256:
	movq	(%rsi), %rax
	movq	(%rax), %rax
	cmpq	%rdi, %rax
	jne	.L251
	movq	8(%rsi), %rsi
	subq	%r12, %rbx
	movq	4112(%rsi), %rax
	movq	4104(%rsi), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %rbx
	ja	.L268
	movq	%rbx, %r8
	movq	%r12, %rdx
	call	memcpy
	addq	%rbx, 4104(%rsi)
.L249:
	addq	$1104, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.L251:
	movq	%rbx, %r8
	movq	%r12, %rdx
	movq	%rsi, %rcx
.LEHB15:
	call	*%rax
	movq	%rax, %r12
	cmpq	%rbx, %rax
	jne	.L256
	jmp	.L249
.L268:
	subq	4096(%rsi), %rax
	leaq	(%rax,%rax), %rdi
	cmpq	%rbx, %rdi
	cmovb	%rbx, %rdi
	testq	%rdi, %rdi
	js	.L269
	movq	%rdi, %rcx
	call	_Znwy
	movq	%rax, %rbp
	movq	4096(%rsi), %rdx
	movq	4104(%rsi), %r8
	subq	%rdx, %r8
	movq	%rax, %rcx
	call	memcpy
	movq	4096(%rsi), %rcx
	movq	4104(%rsi), %r13
	subq	%rcx, %r13
	cmpq	%rsi, %rcx
	je	.L254
	movq	4112(%rsi), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
.L254:
	movq	%rbp, 4096(%rsi)
	leaq	0(%rbp,%r13), %rcx
	movq	%rcx, 4104(%rsi)
	addq	%rdi, %rbp
	movq	%rbp, 4112(%rsi)
	movq	%rbx, %r8
	movq	%r12, %rdx
	call	memcpy
	addq	%rbx, 4104(%rsi)
	jmp	.L249
.L250:
	movq	%r8, 72(%rsp)
	movq	%rdx, %rcx
	call	_Znay
.LEHE15:
	movq	%rax, %r13
	movq	72(%rsp), %r8
	movq	(%r8), %rax
	movq	$0, 48(%rsp)
	movl	$32768, 40(%rsp)
	movq	%r13, 32(%rsp)
	movl	$1024, %r9d
	movl	(%rax), %r8d
	xorl	%edx, %edx
	movl	$4608, %ecx
	call	FormatMessageA
	movl	%eax, %ebx
	addq	%r13, %rbx
	movq	%r13, %r12
	leaq	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_(%rip), %rdi
.L263:
	movq	(%rsi), %rax
	movq	(%rax), %rax
	cmpq	%rdi, %rax
	jne	.L258
	movq	8(%rsi), %rsi
	subq	%r12, %rbx
	movq	4112(%rsi), %rax
	movq	4104(%rsi), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %rbx
	ja	.L270
	movq	%rbx, %r8
	movq	%r12, %rdx
	call	memcpy
	addq	%rbx, 4104(%rsi)
.L262:
	movq	%r13, %rcx
	addq	$1104, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	jmp	_ZdaPv
.L258:
	movq	%rbx, %r8
	movq	%r12, %rdx
	movq	%rsi, %rcx
.LEHB16:
	call	*%rax
	movq	%rax, %r12
	cmpq	%rbx, %rax
	jne	.L263
	jmp	.L262
.L270:
	subq	4096(%rsi), %rax
	leaq	(%rax,%rax), %rdi
	cmpq	%rbx, %rdi
	cmovb	%rbx, %rdi
	testq	%rdi, %rdi
	js	.L271
	movq	%rdi, %rcx
	call	_Znwy
	movq	%rax, %rbp
	movq	4096(%rsi), %rdx
	movq	4104(%rsi), %r8
	subq	%rdx, %r8
	movq	%rax, %rcx
	call	memcpy
	movq	4096(%rsi), %rcx
	movq	4104(%rsi), %r14
	subq	%rcx, %r14
	cmpq	%rsi, %rcx
	je	.L261
	movq	4112(%rsi), %rdx
	subq	%rcx, %rdx
	call	_ZdlPvy
.L261:
	movq	%rbp, 4096(%rsi)
	leaq	0(%rbp,%r14), %rcx
	movq	%rcx, 4104(%rsi)
	addq	%rdi, %rbp
	movq	%rbp, 4112(%rsi)
	movq	%rbx, %r8
	movq	%r12, %rdx
	call	memcpy
	addq	%rbx, 4104(%rsi)
	jmp	.L262
.L271:
	call	_ZSt17__throw_bad_allocv
.LEHE16:
.L269:
.LEHB17:
	call	_ZSt17__throw_bad_allocv
.L265:
	movq	%rax, %r12
	movq	%r13, %rcx
	call	_ZdaPv
	movq	%r12, %rcx
	call	_Unwind_Resume
	nop
.LEHE17:
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA15681:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE15681-.LLSDACSB15681
.LLSDACSB15681:
	.uleb128 .LEHB15-.LFB15681
	.uleb128 .LEHE15-.LEHB15
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB16-.LFB15681
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L265-.LFB15681
	.uleb128 0
	.uleb128 .LEHB17-.LFB15681
	.uleb128 .LEHE17-.LEHB17
	.uleb128 0
	.uleb128 0
.LLSDACSE15681:
	.section	.text$_ZN7fast_io7details33reserve_write_extremely_cold_pathINS_14error_reporterEZNS0_18report_win32_errorERS2_jEUlT_E_EEvRS4_yRT0_,"x"
	.linkonce discard
	.seh_endproc
	.section	.text$_ZNK7fast_io11win32_error6reportERNS_14error_reporterE,"x"
	.linkonce discard
	.align 2
	.globl	_ZNK7fast_io11win32_error6reportERNS_14error_reporterE
	.def	_ZNK7fast_io11win32_error6reportERNS_14error_reporterE;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNK7fast_io11win32_error6reportERNS_14error_reporterE
_ZNK7fast_io11win32_error6reportERNS_14error_reporterE:
.LFB12935:
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$104, %rsp
	.seh_stackalloc	104
	.seh_endprologue
	movq	%rdx, %r12
	movl	16(%rcx), %eax
	movl	%eax, 68(%rsp)
	movq	$32768, 72(%rsp)
	leaq	68(%rsp), %rax
	movq	%rax, 80(%rsp)
	leaq	72(%rsp), %rax
	movq	%rax, 88(%rsp)
	movq	(%rdx), %rax
	movq	8(%rax), %rax
	leaq	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L273
	movq	8(%r12), %rax
	movq	4104(%rax), %rbx
	leaq	32768(%rbx), %rdx
	cmpq	%rdx, 4112(%rax)
	jbe	.L276
.L274:
	testq	%rbx, %rbx
	je	.L276
	movq	$0, 48(%rsp)
	movl	$32768, 40(%rsp)
	movq	%rbx, 32(%rsp)
	movl	$1024, %r9d
	movq	80(%rsp), %rax
	movl	(%rax), %r8d
	xorl	%edx, %edx
	movl	$4608, %ecx
	call	FormatMessageA
	movl	%eax, %eax
	leaq	(%rbx,%rax), %rdx
	movq	(%r12), %rax
	movq	16(%rax), %rax
	leaq	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc(%rip), %rcx
	cmpq	%rcx, %rax
	jne	.L277
	movq	8(%r12), %rax
	movq	%rdx, 4104(%rax)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	ret
.L273:
	movl	$32768, %edx
	movq	%r12, %rcx
	call	*%rax
	movq	%rax, %rbx
	jmp	.L274
.L277:
	movq	%r12, %rcx
	call	*%rax
	nop
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	ret
.L276:
	leaq	80(%rsp), %r8
	movl	$32768, %edx
	movq	%r12, %rcx
	call	_ZN7fast_io7details33reserve_write_extremely_cold_pathINS_14error_reporterEZNS0_18report_win32_errorERS2_jEUlT_E_EEvRS4_yRT0_
	nop
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	ret
	.seh_endproc
	.globl	_ZTSSt9exception
	.section	.rdata$_ZTSSt9exception,"dr"
	.linkonce same_size
	.align 8
_ZTSSt9exception:
	.ascii "St9exception\0"
	.globl	_ZTISt9exception
	.section	.rdata$_ZTISt9exception,"dr"
	.linkonce same_size
	.align 8
_ZTISt9exception:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSSt9exception
	.globl	_ZTSN7fast_io14error_reporterE
	.section	.rdata$_ZTSN7fast_io14error_reporterE,"dr"
	.linkonce same_size
	.align 16
_ZTSN7fast_io14error_reporterE:
	.ascii "N7fast_io14error_reporterE\0"
	.globl	_ZTIN7fast_io14error_reporterE
	.section	.rdata$_ZTIN7fast_io14error_reporterE,"dr"
	.linkonce same_size
	.align 8
_ZTIN7fast_io14error_reporterE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN7fast_io14error_reporterE
	.globl	_ZTSN7fast_io13fast_io_errorE
	.section	.rdata$_ZTSN7fast_io13fast_io_errorE,"dr"
	.linkonce same_size
	.align 16
_ZTSN7fast_io13fast_io_errorE:
	.ascii "N7fast_io13fast_io_errorE\0"
	.globl	_ZTIN7fast_io13fast_io_errorE
	.section	.rdata$_ZTIN7fast_io13fast_io_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTIN7fast_io13fast_io_errorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN7fast_io13fast_io_errorE
	.quad	_ZTISt9exception
	.globl	_ZTSN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE
	.section	.rdata$_ZTSN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE,"dr"
	.linkonce same_size
	.align 32
_ZTSN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE:
	.ascii "N7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE\0"
	.globl	_ZTIN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE
	.section	.rdata$_ZTIN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE,"dr"
	.linkonce same_size
	.align 8
_ZTIN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE
	.quad	_ZTIN7fast_io14error_reporterE
	.globl	_ZTSN7fast_io11posix_errorE
	.section	.rdata$_ZTSN7fast_io11posix_errorE,"dr"
	.linkonce same_size
	.align 16
_ZTSN7fast_io11posix_errorE:
	.ascii "N7fast_io11posix_errorE\0"
	.globl	_ZTIN7fast_io11posix_errorE
	.section	.rdata$_ZTIN7fast_io11posix_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTIN7fast_io11posix_errorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN7fast_io11posix_errorE
	.quad	_ZTIN7fast_io13fast_io_errorE
	.globl	_ZTSN7fast_io11win32_errorE
	.section	.rdata$_ZTSN7fast_io11win32_errorE,"dr"
	.linkonce same_size
	.align 16
_ZTSN7fast_io11win32_errorE:
	.ascii "N7fast_io11win32_errorE\0"
	.globl	_ZTIN7fast_io11win32_errorE
	.section	.rdata$_ZTIN7fast_io11win32_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTIN7fast_io11win32_errorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN7fast_io11win32_errorE
	.quad	_ZTIN7fast_io13fast_io_errorE
	.globl	_ZTVN7fast_io13fast_io_errorE
	.section	.rdata$_ZTVN7fast_io13fast_io_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTVN7fast_io13fast_io_errorE:
	.quad	0
	.quad	_ZTIN7fast_io13fast_io_errorE
	.quad	0
	.quad	0
	.quad	_ZNK7fast_io13fast_io_error4whatEv
	.quad	__cxa_pure_virtual
	.globl	_ZTVN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE
	.section	.rdata$_ZTVN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE,"dr"
	.linkonce same_size
	.align 8
_ZTVN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE:
	.quad	0
	.quad	_ZTIN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEEE
	.quad	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE10write_implEPKcS7_
	.quad	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13oreserve_implEy
	.quad	_ZN7fast_io7details19error_reporter_dervIRNS_25internal_temporary_bufferIcEEE13orelease_implEPc
	.globl	_ZTVN7fast_io11posix_errorE
	.section	.rdata$_ZTVN7fast_io11posix_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTVN7fast_io11posix_errorE:
	.quad	0
	.quad	_ZTIN7fast_io11posix_errorE
	.quad	_ZN7fast_io11posix_errorD1Ev
	.quad	_ZN7fast_io11posix_errorD0Ev
	.quad	_ZNK7fast_io13fast_io_error4whatEv
	.quad	_ZNK7fast_io11posix_error6reportERNS_14error_reporterE
	.globl	_ZTVN7fast_io11win32_errorE
	.section	.rdata$_ZTVN7fast_io11win32_errorE,"dr"
	.linkonce same_size
	.align 8
_ZTVN7fast_io11win32_errorE:
	.quad	0
	.quad	_ZTIN7fast_io11win32_errorE
	.quad	_ZN7fast_io11win32_errorD1Ev
	.quad	_ZN7fast_io11win32_errorD0Ev
	.quad	_ZNK7fast_io13fast_io_error4whatEv
	.quad	_ZNK7fast_io11win32_error6reportERNS_14error_reporterE
	.globl	_ZN7fast_io7details6sha2564K256E
	.section	.rdata$_ZN7fast_io7details6sha2564K256E,"dr"
	.linkonce same_size
	.align 32
_ZN7fast_io7details6sha2564K256E:
	.long	1116352408
	.long	1899447441
	.long	-1245643825
	.long	-373957723
	.long	961987163
	.long	1508970993
	.long	-1841331548
	.long	-1424204075
	.long	-670586216
	.long	310598401
	.long	607225278
	.long	1426881987
	.long	1925078388
	.long	-2132889090
	.long	-1680079193
	.long	-1046744716
	.long	-459576895
	.long	-272742522
	.long	264347078
	.long	604807628
	.long	770255983
	.long	1249150122
	.long	1555081692
	.long	1996064986
	.long	-1740746414
	.long	-1473132947
	.long	-1341970488
	.long	-1084653625
	.long	-958395405
	.long	-710438585
	.long	113926993
	.long	338241895
	.long	666307205
	.long	773529912
	.long	1294757372
	.long	1396182291
	.long	1695183700
	.long	1986661051
	.long	-2117940946
	.long	-1838011259
	.long	-1564481375
	.long	-1474664885
	.long	-1035236496
	.long	-949202525
	.long	-778901479
	.long	-694614492
	.long	-200395387
	.long	275423344
	.long	430227734
	.long	506948616
	.long	659060556
	.long	883997877
	.long	958139571
	.long	1322822218
	.long	1537002063
	.long	1747873779
	.long	1955562222
	.long	2024104815
	.long	-2067236844
	.long	-1933114872
	.long	-1866530822
	.long	-1538233109
	.long	-1090935817
	.long	-965641998
	.weak	__cxa_pure_virtual
	.ident	"GCC: (GCC with MCF thread model, built by LH_Mouse.) 11.0.0 20200712 (experimental)"
	.def	_ZdaPv;	.scl	2;	.type	32;	.endef
	.def	_ZNSt9exceptionD2Ev;	.scl	2;	.type	32;	.endef
	.def	_ZdlPvy;	.scl	2;	.type	32;	.endef
	.def	memcpy;	.scl	2;	.type	32;	.endef
	.def	_Znwy;	.scl	2;	.type	32;	.endef
	.def	_ZSt17__throw_bad_allocv;	.scl	2;	.type	32;	.endef
	.def	strerror;	.scl	2;	.type	32;	.endef
	.def	strlen;	.scl	2;	.type	32;	.endef
	.def	_Znay;	.scl	2;	.type	32;	.endef
	.def	memmove;	.scl	2;	.type	32;	.endef
	.def	__cxa_begin_catch;	.scl	2;	.type	32;	.endef
	.def	__cxa_end_catch;	.scl	2;	.type	32;	.endef
	.def	__cxa_allocate_exception;	.scl	2;	.type	32;	.endef
	.def	__cxa_throw;	.scl	2;	.type	32;	.endef
	.def	__cxa_free_exception;	.scl	2;	.type	32;	.endef
	.def	_Unwind_Resume;	.scl	2;	.type	32;	.endef
	.def	fwrite;	.scl	2;	.type	32;	.endef
	.def	write;	.scl	2;	.type	32;	.endef
	.def	SystemFunction036;	.scl	2;	.type	32;	.endef
	.def	RtlSecureZeroMemory;	.scl	2;	.type	32;	.endef
	.def	_lock;	.scl	2;	.type	32;	.endef
	.def	strlen;	.scl	2;	.type	32;	.endef
	.def	EnterCriticalSection;	.scl	2;	.type	32;	.endef
	.def	GetLastError;	.scl	2;	.type	32;	.endef
	.def	FormatMessageA;	.scl	2;	.type	32;	.endef
	.def	__cxa_pure_virtual;	.scl	2;	.type	32;	.endef
