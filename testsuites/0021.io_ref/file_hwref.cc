#include"../../include/fast_io_device.h"

int main()
{
	fast_io::obuf_file obf("hw.txt");
	auto wrapper{fast_io::io_ref(obf)};
	static_assert(fast_io::buffer_output_stream<decltype(wrapper)>);
	static_assert(fast_io::value_based_stream<decltype(wrapper)>);
	static_assert(fast_io::random_access_stream<decltype(wrapper)>);
	static_assert(fast_io::contiguous_output_stream<decltype(wrapper)>);
}